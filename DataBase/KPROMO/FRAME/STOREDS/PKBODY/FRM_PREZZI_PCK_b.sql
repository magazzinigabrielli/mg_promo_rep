CREATE OR REPLACE PACKAGE BODY FRAME."FRM_PREZZI_PCK" AS
  --
  /******************************************************************************
   NAME:       BASE_UTIL_PCK
   PURPOSE:    Package di utilit���

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        12/02/2019           r.doti       1. Created this package.
   ******************************************************************************/
  --
  kNomePackage        CONSTANT VARCHAR2(30) := 'FRM_PREZZI_PCK';
  --
  PROCEDURE GET_PRICE(
    PO_ERR_SEVERITA       IN OUT NOCOPY  VARCHAR2,
    PO_ERR_CODICE         IN OUT NOCOPY  VARCHAR2,
    PO_MESSAGGIO          IN OUT NOCOPY  VARCHAR2,
    P_INTCODE             IN     MG_VS_VARPRICE.INTCODE%TYPE,
    P_HOST_SHOP_ID        IN     MG_VS_VARPRICE.HOST_SHOP_ID%TYPE,
    PO_PRICEAMOUNT           OUT NUMBER
    )
  IS
    vProcName VARCHAR2(30) := 'GET_PRICE';
    vPasso    VARCHAR2(100);
    vCntPDVBO NUMBER;
  BEGIN
  --
    vPasso := 'CheckPescheria';
    SELECT DPES_ART_PREZZO
      INTO PO_PRICEAMOUNT
      FROM FRM_DDT_PESCHERIA
     WHERE DPES_PDV_COD = TO_NUMBER(P_HOST_SHOP_ID)
       AND DPES_ART_COD = TO_NUMBER(P_INTCODE)
       AND DPES_DATA_MOD = (SELECT MAX(DPES_DATA_MOD)
                              FROM FRM_DDT_PESCHERIA B
                             WHERE DPES_PDV_COD = TO_NUMBER(P_HOST_SHOP_ID)
                               AND DPES_ART_COD = TO_NUMBER(P_INTCODE))
       AND ROWNUM <= 1;
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'Prezzo:'||PO_PRICEAMOUNT, vPasso);
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      --
      BEGIN
      --
        vPasso := 'CHECK_PDV_BO';
        SELECT COUNT(0)
          INTO vCntPDVBO
          FROM MGBO.MGO_ACTIVE_PDV@CRSMG
         WHERE ACP_STORECODE = P_HOST_SHOP_ID
           AND ACP_STOREACTIVE =1;
       BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'NEW_BO:'||vCntPDVBO, vPasso);
       --
       IF vCntPDVBO > 0 THEN  --Prezzi BackOffice
       --
         vPasso := 'PrezzoBO';
         SELECT REPLACE(TO_CHAR(ITEM_PRICE,'FM9999.00'),'.') ITEM_PRICE
           INTO PO_PRICEAMOUNT
           FROM VSBO.BO_PRICE_HST@CRSMG 
          WHERE STORE_ID = P_HOST_SHOP_ID
            AND ITEM_ID = P_INTCODE    
            AND LOCALTIMESTAMP BETWEEN  PRICE_EFF_TS AND NVL(PRICE_EXP_TS,LOCALTIMESTAMP)
            AND ROWNUM <= 1;
         BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'Prezzo:'||PO_PRICEAMOUNT, vPasso);
       ELSE
        --
        vPasso := 'PrezzoKPROMO';
        SELECT PRICEAMOUNT --TO_NUMBER(RPAD(PRICEAMOUNT, LENGTH(PRICEAMOUNT)-2) ||','|| SUBSTR(PRICEAMOUNT,LENGTH(PRICEAMOUNT)-1))
          INTO PO_PRICEAMOUNT
          FROM MG_VS_VARPRICE A
         WHERE HOST_SHOP_ID = P_HOST_SHOP_ID
           AND INTCODE = P_INTCODE
           AND A.ACTIVEDATE = (SELECT MAX(B.ACTIVEDATE)
                                 FROM MG_VS_VARPRICE B
                                WHERE A.HOST_SHOP_ID= B.HOST_SHOP_ID
                                 AND A.INTCODE = B.INTCODE
                              )
           AND ROWNUM <= 1;
          BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'Prezzo:'||PO_PRICEAMOUNT, vPasso);
       END IF;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          vPasso := 'NO_DATA_FOUND';
          PO_PRICEAMOUNT := 0;
          BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'Prezzo:'||PO_PRICEAMOUNT, vPasso);
      END;

    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'B';
      PO_ERR_CODICE     := SQLCODE;
      PO_MESSAGGIO      := kNomePackage || '.' || vProcName||'['||vPasso||']' || ' - Error: ' || SQLERRM;
  END GET_PRICE;
  --
  FUNCTION GET_PRICE(
    P_INTCODE             IN     MG_VS_VARPRICE.INTCODE%TYPE,
    P_HOST_SHOP_ID        IN     MG_VS_VARPRICE.HOST_SHOP_ID%TYPE
  )
  RETURN NUMBER
   IS
    vPrice NUMBER;
    vO_ERR_SEVERITA       VARCHAR2(4000);
    vO_ERR_CODICE         VARCHAR2(4000);
    vO_MESSAGGIO          VARCHAR2(4000);
  BEGIN
    --
    GET_PRICE(
    PO_ERR_SEVERITA       => vO_ERR_SEVERITA,
    PO_ERR_CODICE         => vO_ERR_CODICE,
    PO_MESSAGGIO          => vO_MESSAGGIO,
    P_INTCODE             => P_INTCODE,
    P_HOST_SHOP_ID        => P_HOST_SHOP_ID,
    PO_PRICEAMOUNT        => vPrice
    );
    --
    RETURN vPrice;
  EXCEPTION
    WHEN OTHERS THEN
       RETURN NULL;
  END GET_PRICE;
  --
  PROCEDURE GET_PROMO_PRICE(
    PO_ERR_SEVERITA       IN OUT NOCOPY  VARCHAR2,
    PO_ERR_CODICE         IN OUT NOCOPY  VARCHAR2,
    PO_MESSAGGIO          IN OUT NOCOPY  VARCHAR2,
    P_OBJ_CODE            IN     VARCHAR2,
    P_ID_SHOP             IN     VARCHAR2,
    P_DATA_DEC            IN     TIMESTAMP,
    PO_FIDE                  OUT VARCHAR2, --[F: Fidelity- M: Mass Market]
    PO_TIPO_PROMO            OUT VARCHAR2, --Tipologia Promo A400[TP, VI, PI, MN]    KLOY[PC- VC]
    PO_PREZZO                OUT NUMBER,
    PO_PREZZO_PROMO          OUT NUMBER,
    PO_VALORE_SCNT           OUT NUMBER,
    PO_DIPROMO               OUT NUMBER,
    PO_DFPROMO               OUT NUMBER
  )
    IS
    vProcName    VARCHAR2(30) := 'GET_PRICE';
    vPasso       VARCHAR2(100);
    vDataDec     NUMBER;
    vTipoSconto  VARCHAR2(5);
    vPrezzoBase  NUMBER;
    vPrezzoPromo NUMBER;
    vFidelity    VARCHAR2(5);
  BEGIN
  --
    --TP= Taglio Prezzo
    --PI= Sconto percentuale incondizionato
    --VI= Sconto valore incondizionato
    --MN= MxN (2x1, 3x2, 4x3���)
    --PC= Sconto percentuale condizionato
    --VC= Sconto valore condizionato
    vPasso := 'SET_DATA_DEC';
    vDataDec := TO_NUMBER(TO_CHAR(TRUNC(P_DATA_DEC),'YYYYMMDD'));
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'P_DATA_DEC:'||vDataDec, vPasso);
    --
    BEGIN
      vPasso := 'GET_SCNT_IST';
      SELECT NVL(TRIM(E4FIDE),'M'),--[F: Fidelity- M: Mass Market]
             NVL(TRIM(E4TSCO),'-'), --[NULL o %]
             E4PSCO, --[VALORE dello SCONTO]
             E4PRZN, --Prezzo Base
             E4PRZP,  --Prezzo Scnt
             E4IPRO,
             E4FPRO
        INTO PO_FIDE,
             vTipoSconto,
             PO_VALORE_SCNT,
             PO_PREZZO,
             PO_PREZZO_PROMO,
             PO_DIPROMO,
             PO_DFPROMO
        FROM MG.GEAD040F
       WHERE E4CART = P_OBJ_CODE
         AND E4CPDV = P_ID_SHOP
         AND vDataDec BETWEEN E4IPRO AND E4FPRO;
        --
      IF vTipoSconto = '-' AND PO_VALORE_SCNT = 0 THEN
        PO_TIPO_PROMO := 'TP'; -- Taglio Prezzo
      END IF;
      --
      IF vTipoSconto = '-' AND PO_VALORE_SCNT > 0 THEN
        PO_TIPO_PROMO := 'VI'; -- Sconto Valore Incondizionato
      END IF;

      IF vTipoSconto = '%' AND PO_VALORE_SCNT > 0 THEN
        PO_TIPO_PROMO := 'PI'; -- Sconto Percentuale Incondizionato
      END IF;
      --
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'Fidelity:'||PO_FIDE, vPasso);
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'TipoSconto:'||vTipoSconto, vPasso);
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'ValoreSconto:'||PO_VALORE_SCNT, vPasso);
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PrezzoBase:'||vPrezzoBase, vPasso);
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'PrezzoPromo:'||vprezzoPromo, vPasso);
    --
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        vPasso := 'NO_DATA[GEAD040F]';
        BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'CodArticolo:'||P_OBJ_CODE, vPasso);
        BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'CodPDV:'||P_ID_SHOP, vPasso);
    END;
    --
    IF PO_TIPO_PROMO IS NULL THEN
    --
      BEGIN
        vPasso := 'GET_SCNT_M_x_N';
        SELECT E5TMOF, --[F: Fidelity- M: Mass Market]
               E5PRZP,  --Prezzo Scnt
               E5IMXN,
               E5FMXN
          INTO PO_FIDE,
               PO_PREZZO,
               PO_DIPROMO,
               PO_DFPROMO
          FROM MG.GEAD050F
         WHERE E5CART = P_OBJ_CODE
           AND E5CPDV = P_ID_SHOP
           AND vDataDec BETWEEN E5IMXN AND E5FMXN;
        --
        PO_TIPO_PROMO := 'MN';
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          vPasso := 'NO_DATA[GEAD050F]';
          BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'CodArticolo:'||P_OBJ_CODE, vPasso);
          BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'CodPDV:'||P_ID_SHOP, vPasso);
      END;
      --
    END IF;
    --
    --Verifico se ci sono promo create su K-Loy
    IF PO_TIPO_PROMO IS NULL THEN
    --
      NULL;
    END IF;
    --


    --SELECT /*+ INDEX(PROM) USE_NL(PROM)
    --          INDEX(WIZ) USE_NL(WIZ)
    --       */
    --        DECODE(WIZ.HOST_PROMO_TYPE,
    --               'SCFP', '2', --Carta
    --               'TPF',  '2',
    --               'TPF',  '2',
    --               '2X1F', '2',
    --               '3X2F', '2',
    --               '1'),        --Tutti
    --        PROM.VALUE
    --   INTO PO_TIPO_PROMO,
    --        PO_PRICEAMOUNT
    --   FROM PROMO_IMPT PROM,
    --        MG_SPROMO_WIZARD WIZ
    --  WHERE ID_SHOP = P_ID_SHOP
    --    AND OBJ_CODE =  P_OBJ_CODE
    --    AND WIZ.KCRM_ID_PROMO_WIZARD = PROMO_TYPE
    --    AND SYSDATE BETWEEN DT_START AND DT_END
    --   AND ROWNUM <=1;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      PO_FIDE          := '';
      PO_TIPO_PROMO    := '';
      PO_PREZZO        := 0;
      PO_PREZZO_PROMO  := 0;
      PO_VALORE_SCNT   := 0;
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'B';
      PO_ERR_CODICE     := SQLCODE;
      PO_MESSAGGIO      := kNomePackage || '.' || vProcName||'['||vPasso||']' || ' - Error: ' || SQLERRM;
  END GET_PROMO_PRICE;
--
END FRM_PREZZI_PCK;
/