CREATE OR REPLACE PACKAGE FRAME.FRM_PREZZI_PCK
AS
  --
  /******************************************************************************
   NAME:       FRM_PREZZI_PCK
   PURPOSE:    Package di gestione prezzi articoli

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        11/04/2019           r.doti       1. Created this package.
   ******************************************************************************/
  --
  PROCEDURE GET_PRICE(
    PO_ERR_SEVERITA       IN OUT NOCOPY  VARCHAR2,
    PO_ERR_CODICE         IN OUT NOCOPY  VARCHAR2,
    PO_MESSAGGIO          IN OUT NOCOPY  VARCHAR2,
    P_INTCODE             IN     MG_VS_VARPRICE.INTCODE%TYPE,
    P_HOST_SHOP_ID        IN     MG_VS_VARPRICE.HOST_SHOP_ID%TYPE,
    PO_PRICEAMOUNT           OUT NUMBER
    );
  --
  FUNCTION GET_PRICE(
    P_INTCODE             IN     MG_VS_VARPRICE.INTCODE%TYPE,
    P_HOST_SHOP_ID        IN     MG_VS_VARPRICE.HOST_SHOP_ID%TYPE
  )RETURN NUMBER;
   --
   PROCEDURE GET_PROMO_PRICE(
    PO_ERR_SEVERITA       IN OUT NOCOPY  VARCHAR2,
    PO_ERR_CODICE         IN OUT NOCOPY  VARCHAR2,
    PO_MESSAGGIO          IN OUT NOCOPY  VARCHAR2,
    P_OBJ_CODE            IN     VARCHAR2,
    P_ID_SHOP             IN     VARCHAR2,
    P_DATA_DEC            IN     TIMESTAMP,
    PO_FIDE                  OUT VARCHAR2, --[F: Fidelity- M: Mass Market]
    PO_TIPO_PROMO            OUT VARCHAR2, --Tipologia Promo A400[TP, VI, PI, MN]    KLOY[PC- VC]
    PO_PREZZO                OUT NUMBER,
    PO_PREZZO_PROMO          OUT NUMBER,
    PO_VALORE_SCNT           OUT NUMBER,
    PO_DIPROMO               OUT NUMBER,
    PO_DFPROMO               OUT NUMBER
    ); 
--
END FRM_PREZZI_PCK;
--
/
CREATE OR REPLACE PUBLIC SYNONYM FRM_PREZZI_PCK FOR FRAME."FRM_PREZZI_PCK"
/
GRANT EXECUTE ON FRM_PREZZI_PCK TO MG
/