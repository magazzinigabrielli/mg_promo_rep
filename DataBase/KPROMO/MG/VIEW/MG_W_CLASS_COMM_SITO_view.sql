DROP VIEW MG_W_CLASS_COMM_SITO;

/* Formatted on 05/11/2020 15:37:01 (QP5 v5.215.12089.38647) */
CREATE OR REPLACE FORCE VIEW MG_W_CLASS_COMM_SITO
(
   E1CS0D,
   E1ORD,
   E1CS1D,
   E1CS2D,
   E1CS3D
)
AS
   SELECT E1CS0D,
          E1ORD,
          E1CS1D,
          E1CS2D,
          E1CS3D
     FROM (  SELECT /*+ USE_NL(S10) INDEX(S10) */
                    CASE E1CS0C
                       WHEN '10' THEN 1                           --Ortofrutta
                       WHEN '11' THEN 2                                --Carne
                       WHEN '16' THEN 3                                --Pesce
                       WHEN '13' THEN 4                      --Banco al taglio
                       WHEN '17' THEN 5                           --Banco pane
                       WHEN '12' THEN 6                      --ALIM.DEPERIBILI
                       WHEN '02' THEN 7                       --SURGEL./GELATI
                       WHEN '15' THEN 8                               --CUCINA
                       WHEN '21' THEN 9                             --BEVERAGE
                       WHEN '22' THEN 10                           --DOLCIARIO
                       WHEN '23' THEN 11                           --DROGHERIA
                       WHEN '01' THEN 12                      --IGIENE PERSONA
                       WHEN '24' THEN 13                       --PET FOOD/CARE
                       WHEN '04' THEN 14                       --PRODOTTI CASA
                       ELSE 99
                    END
                       E1ORD,
                    E1CS0D,
                    E1CS1D,
                    E1CS2D,
                    E1CS3D
               FROM GEAD010F S10
              WHERE     E1SITO = 'S'
                    AND TRIM (S10.E1STAT) IS NULL
                    AND EXISTS
                           (SELECT 0
                              FROM ARTICLES_IMAGES_SITO FOTO
                             WHERE     FOTO.ARTCODE = S10.E1CART
                                   AND TRIM (FOTO.ANNULL) IS NULL)
                    AND EXISTS
                           (SELECT 0
                              FROM MG_PDV_REL_SERVICE PVSER, GEAD030F S30
                             WHERE     TRIM (PVSER_STAT) IS NULL
                                   AND S30.E3CPDV = PVSER.PVSER_PDV_REL_PDV_ID
                                   AND S30.E3FSIT = 'S'
                                   AND S30.E3CART = S10.E1CART)
           GROUP BY E1CS0C,
                    E1CS0D,
                    E1CS1D,
                    E1CS2D,
                    E1CS3D
            ORDER BY E1ORD ,E1CS0D, E1CS1D, E1CS2D, E1CS3D);
