

CREATE OR REPLACE FORCE VIEW MG.MG_W_CLASS_COMM_STD
(
   E1CLPG,
   E1CG1D,
   E1CREP,
   E1CG2D,
   E1CUGE,
   E1CG3D,
   E1CRAG,
   E1CG4D,
   E1CFAM,
   E1CG5D
)
AS
     SELECT /*+ USE_NL(S10) INDEX(S10) */
           DISTINCT E1CLPG,
                    E1CG1D,
                    E1CREP,
                    E1CG2D,
                    E1CUGE,
                    E1CG3D,
                    E1CRAG,
                    E1CG4D,
                    E1CFAM,
                    E1CG5D
       FROM GEAD010F S10
      WHERE E1SITO = 'S'
   ORDER BY E1CLPG,
            E1CREP,
            E1CUGE,
            E1CRAG,
            E1CFAM;

COMMENT ON TABLE MG.MG_W_CLASS_COMM_STD IS
   'Classificazione commerciale gabrielli dei soli articoli visibili sul sito';

COMMENT ON COLUMN MG.MG_W_CLASS_COMM_STD.E1CLPG IS 'Linea prodotto';

COMMENT ON COLUMN MG.MG_W_CLASS_COMM_STD.E1CG1D IS
   'Descrizione linea prodotto';

COMMENT ON COLUMN MG.MG_W_CLASS_COMM_STD.E1CREP IS 'Reparto';

COMMENT ON COLUMN MG.MG_W_CLASS_COMM_STD.E1CG2D IS 'Descrizione reparto';

COMMENT ON COLUMN MG.MG_W_CLASS_COMM_STD.E1CUGE IS 'Unit� di gestione';

COMMENT ON COLUMN MG.MG_W_CLASS_COMM_STD.E1CG3D IS
   'Descrizione unit� di gestione';

COMMENT ON COLUMN MG.MG_W_CLASS_COMM_STD.E1CRAG IS 'Raggruppamento';

COMMENT ON COLUMN MG.MG_W_CLASS_COMM_STD.E1CG4D IS
   'Descrizione raggruppamento';

COMMENT ON COLUMN MG.MG_W_CLASS_COMM_STD.E1CFAM IS 'Famiglia';

COMMENT ON COLUMN MG.MG_W_CLASS_COMM_STD.E1CG5D IS 'Descrizione famiglia';