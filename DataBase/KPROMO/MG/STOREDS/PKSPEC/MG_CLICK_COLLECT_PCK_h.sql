CREATE OR REPLACE PACKAGE MG."MG_CLICK_COLLECT_PCK" AS
  --
  /******************************************************************************
   NAME:       MG_CLICK_COLLECT_PCK
   PURPOSE:    Package per la gestione dei servizi di ClikCollect

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        03/04/2019           r.doti  1. Created this package.
   ******************************************************************************/
  --
  kServiceDRIVE        CONSTANT VARCHAR2(30) := 'CCDRIVE';
  kServiceCASA         CONSTANT VARCHAR2(30) := 'CCCASA';
  kServiceSTORE        CONSTANT VARCHAR2(30) := 'CCSTORE';
  --
  ErroreGestito EXCEPTION;
  ErroreGestArt       EXCEPTION;
  --
  /* Funzione di lettura della costante kServiceDRIVE
  */
  FUNCTION GetKServiceDRIVE RETURN VARCHAR2;
  /* Funzione di lettura della costante kServiceCASA
  */
  FUNCTION GetKServiceCASA  RETURN VARCHAR2;
  /* Funzione di lettura della costante kServiceSTORE
  */
  FUNCTION GetKServiceSTORE RETURN VARCHAR2;
  /* Funzione per determinare l'identificativo di servizio su un PDV reale
  */
  FUNCTION GET_ID_SERVICE(P_PDV_COD NUMBER, P_SERVICE VARCHAR2) RETURN NUMBER;
  /* Funzione per determinare se un articolo � abilitato ad essere esposto sul sito
  */
  FUNCTION GET_FLAG_ARTICOLO(P_ART_COD NUMBER, P_PDV_COD NUMBER, P_SERVICE VARCHAR2) RETURN VARCHAR2;
  --
  /* Funzione per determinare la presenza della foto*/
  FUNCTION GET_INFO_FOTO(P_ART_COD    IN NUMBER)RETURN NUMBER;
  /* Funzione per determinare la presenza di dati aggiuntivi
   */
  FUNCTION GET_DATI_AGG(
    P_KEYAM    IN VARCHAR2,
    P_KEY01    IN VARCHAR2,
    P_KEY02    IN VARCHAR2
  )RETURN NUMBER;
  --
  FUNCTION GET_VIRTUAL_PDV(P_PDV_COD NUMBER, P_SERVICE VARCHAR2) RETURN NUMBER;
  --
  /**
   * Description: La funzione effettua l'arrotondamento Bancker'Rounding
   *  
   * Author: R.Doti
   * Created: 28/11/2019
   *
   * Param: P_PREZZO prezzo
   * Param: P_RND_DIGIT diagnostico
   */
  --FUNCTION BANCKER_ROUND(P_PREZZO IN NUMBER, P_RND_DIGIT NUMBER := 0) RETURN NUMBER;
 /**
  * CREATE_SERVICE(): Utility per la creazione di un servizio su un punto vendita reale
  *
  * @param PO_METADATo il metadato estratto
  */
  PROCEDURE CREATE_SERVICE(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_PDV_COD          IN     NUMBER,
      P_SERVICE          IN     VARCHAR2,
      P_DATA_INI         IN     DATE,
      PO_SERVICE_ID         OUT NUMBER
   );
  --
  /**
  * AGGIORNA_ASSORTIMENTO_PDV(): Utility per l'aggiornamento dell'assortimento dei SERVIZI legato ad un PDV.
  *
  * @param PO_METADATo il metadato estratto
  */
  PROCEDURE AGGIORNA_ASSORTIMENTO_PDV(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_PDV_COD          IN     NUMBER,
      P_SERVICE          IN     VARCHAR2 DEFAULT NULL
    );
 /**
  * AGGIORNA_PREZZI_PDV(): Utility per l'aggiornamento dei Prezzi dei SERVIZI legato ad un PDV.
  *
  * @param PO_METADATo il metadato estratto
  */
  PROCEDURE AGGIORNA_PREZZI_PDV(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_PDV_COD          IN     NUMBER,
      P_SERVICE          IN     VARCHAR2 DEFAULT NULL
    );
  --
  PROCEDURE REFRESH_PRZ(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_PDV_COD          IN     NUMBER  DEFAULT NULL
  );
  --
  PROCEDURE AGGIORNA_ASRT_AUT(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_PDV_COD          IN     NUMBER  DEFAULT NULL
  );
  --
  PROCEDURE REFRESH_ASRT(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_PDV_COD          IN     NUMBER  DEFAULT NULL
  );
  --
  PROCEDURE REFRESH_ALL(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_PDV_COD          IN     NUMBER  DEFAULT NULL
  );
  --
  PROCEDURE REFRESH_DATI_AGG(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2
  );
  --
  FUNCTION SPLIT_WORD(
      P_LIST                 IN VARCHAR2,
      P_DELIMITER            IN VARCHAR2 DEFAULT ','
  )RETURN SPLIT_TBL;
  --
  PROCEDURE REFRESH_DICT_SITO(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2
  );
  --
  PROCEDURE NORMALIZZA_PESCHERIA(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2 
  );
  --
  PROCEDURE ELABORA_TIPI_ATTRIBUTI(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_TIPO_ATTR        IN            VARCHAR2
  );
  --
  PROCEDURE COPY_SERVICE_BY_PDV(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_PDV_COD_COPY     IN            VARCHAR2,
      P_SERVICE          IN            VARCHAR2,
      P_PDV_COD          IN            VARCHAR2,
      P_IP               IN            VARCHAR2,
      P_EMAIL            IN            VARCHAR2
  );
  --
END MG_CLICK_COLLECT_PCK;
/

--CREATE OR REPLACE PUBLIC SYNONYM MG_CLICK_COLLECT_PCK FOR MG.MG_CLICK_COLLECT_PCK
--/


