CREATE OR REPLACE PACKAGE MG."MG_PROMO_STORE_PCK" AS
  --
  /******************************************************************************
   NAME:       MG_PROMO_STORE_PCK
   PURPOSE:    Package per la gestione delle promozioni gestite direttamente dai PDV

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        03/10/2019           r.doti  1. Created this package.
   ******************************************************************************/
  --
  ErroreGestito EXCEPTION;
  --

  --
  /**
  * ADD_ART_PROMO(): Aggiunge un articolo alla tabella MG_PROMO_STORE per essere elaborata.
  *
  */
  PROCEDURE ADD_ART_PROMO(
      PO_ERR_SEVERITA     IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE       IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO        IN OUT NOCOPY VARCHAR2,
      P_PDV_COD           IN     VARCHAR2,  --Codice PDV
      P_ART_COD           IN     VARCHAR2,  --Codice Articolo
      P_CAUSALE_PROMO_COD IN     VARCHAR2,  --Codice Causale
      P_PREZZO_OLD        IN     NUMBER,    --proveniente da AS400
      P_COSTO             IN     NUMBER,    --Costo Ultimo
      P_PERC_SCNT         IN     INTEGER,  --PERC_SCNT
      P_PREZZO_NEW        IN     NUMBER,
      P_DATA_INIZIO       IN     TIMESTAMP,
      P_DATA_FINE         IN     TIMESTAMP,
      P_OPERATORE         IN     VARCHAR2,
      P_TIPO_OPERAZIONE   IN     VARCHAR2  DEFAULT 'NEW'
   );
  --
  /**
  * ELAB_PROMO(): Elabora le richieste di articoli in promozione
  *
  */
  PROCEDURE ELAB_PROMO(
      PO_ERR_SEVERITA     IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE       IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO        IN OUT NOCOPY VARCHAR2,
      P_PDV_COD           IN     VARCHAR2
   );
  --
  /*
  * La funzione verifica se l'articolo è abilitato per l'inseirmento in promo.
  */
  FUNCTION CHECK_CAU_FILTER(
    P_CAUSALE_PROMO_COD  IN VARCHAR2,
    P_ART_COD            IN VARCHAR2
  ) RETURN NUMBER;
END MG_PROMO_STORE_PCK;
/

CREATE OR REPLACE PUBLIC SYNONYM MG_PROMO_STORE_PCK FOR MG.MG_PROMO_STORE_PCK
/


