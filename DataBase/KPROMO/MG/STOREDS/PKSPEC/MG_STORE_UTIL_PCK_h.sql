CREATE OR REPLACE PACKAGE MG."MG_STORE_UTIL_PCK" AS
  --
  /******************************************************************************
   NAME:       MG_STORE_UTIL_PCK
   PURPOSE:    Package per la gestione degli store

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        04/12/2019           r.doti  1. Created this package.
   ******************************************************************************/
  --
  ErroreGestito EXCEPTION;
  /**
  * Description: Utility per la creazione di PDV Virtuale
  *
  * Author: R.Doti
  * Created: 04/12/2019
  * @param P_PDV_COD Codice del PDV Fisico
  */
  PROCEDURE CREATE_PDV_VIRTUAL(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_PDV_COD          IN     NUMBER,
      P_DECORRENZA       IN     DATE,
      PO_PDV_VIRTUAL_COD    OUT NUMBER,
      PO_JOB_ID             OUT NUMBER
   );
  --
  /**
  * Description: Utility per aggiornare VS_VAT : Tabella dell'iva
  * MG_VS_VAT --> VS_VAT 
  * Author: R.Doti
  * Created: 04/12/2019
  * @param P_PDV_COD Codice del PDV Virtuale
  */
  PROCEDURE FLOW_VS_VAT(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_PDV_COD          IN            VARCHAR2,
      PO_JOB_ID             OUT        NUMBER
   );
  --
  /**
  * Description: Utility per aggiornare VS_DEPT : Tabella contenente la classificazione merceologica.
  * MG_VS_DEPT --> VS_DEPT 
  * Author: R.Doti
  * Created: 27/01/2020
  * @param P_PDV_COD Codice del PDV Virtuale
  */
  PROCEDURE FLOW_VS_DEPT(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_PDV_COD          IN            VARCHAR2,
      PO_JOB_ID             OUT        NUMBER
   );
 /**
  * Description: Utility per aggiornare VS_ITEM : Tabella contenente l'anagrafica degli articoli.
  * MG_VS_ITEM --> VS_ITEM 
  * Author: R.Doti
  * Created: 27/01/2020
  * @param P_PDV_COD Codice del PDV Virtuale
  */
  PROCEDURE FLOW_VS_ITEM(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_PDV_COD          IN            VARCHAR2,
      PO_JOB_ID             OUT        NUMBER
   );
  --
 /**
  * Description: Utility per aggiornare VS_XREF : Tabella contenente l'anagrafica dei barcode.
  * MG_VS_XREF --> VS_XREF 
  * Author: R.Doti
  * Created: 27/01/2020
  * @param P_PDV_COD Codice del PDV Virtuale
  */
  PROCEDURE FLOW_VS_XREF(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_PDV_COD          IN            VARCHAR2,
      PO_JOB_ID             OUT        NUMBER
   );
  --
 /**
  * Description: Utility per aggiornare PROMO_IMPT : Tabella contenente le promozioni.
  * MG_PROMO_IMPT --> PROMO_IMPT 
  * Author: R.Doti
  * Created: 27/01/2020
  * @param P_PDV_COD Codice del PDV Virtuale
  */
  PROCEDURE FLOW_PROMO_IMPT(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_PDV_COD          IN            VARCHAR2,
      P_TIPO_PROMO       IN            VARCHAR2, --ALL/BIL
      PO_JOB_ID             OUT        NUMBER
   );
  --
  /**
  * Description: Utility per aggiornare VS_VARPRICE : Tabella contenente le variazioni prezzo.
  * MG_VS_VARPRICE --> VS_VARPRICE 
  * Author: R.Doti
  * Created: 27/01/2020
  * @param P_PDV_COD Codice del PDV Virtuale
  */
  PROCEDURE FLOW_VS_VARPRICE(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_PDV_COD          IN            VARCHAR2,
      PO_JOB_ID             OUT        NUMBER
   );
  --
  /**
   * Description: La funzione restituisce il prezzo attuale dell'articolo sul PDV.
   *
   * Author: R.Doti
   * Created: 17/12/2019
   *
   * Param: P_ART_COD Codice articolo
   * Param :P_PDV_COD Codice punto vendita
   * Return: prezzo
   */
  FUNCTION GET_PRICE(
    P_ART_COD  VARCHAR2,
    P_PDV_COD  VARCHAR2
  ) RETURN NUMBER;
  --
  /**
   * Description: La funzione effettua l'arrotondamento Bancker'Rounding
   *  
   * Author: R.Doti
   * Created: 28/11/2019
   *
   * Param: P_PREZZO prezzo
   * Param: P_RND_DIGIT diagnostico
   */
  FUNCTION BANCKER_ROUND(P_PREZZO IN NUMBER, P_RND_DIGIT NUMBER := 0) 
    RETURN NUMBER;
  /**
   * Description: La funzione restituisce il nuovo prezzo per PDV Virtuale.
   *
   * Author: R.Doti
   * Created: 30/01/2020
   *
   * Param: P_ART_COD Codice articolo
   * Param :P_PDV_COD Codice punto vendita
   * Param :P_SERVIZIO Codice punto vendita
   * Return: prezzo
   */
  FUNCTION GET_NEW_PRICE(
    P_ART_COD    VARCHAR2,
    P_PDV_COD    VARCHAR2,
    P_PRICE_OLD  NUMBER DEFAULT NULL
  ) RETURN NUMBER PARALLEL_ENABLE;
  --
  /**
   * Description: La procedura genera articoli coupon.
   *
   * Author: R.Doti
   * Created: 30/01/2020
   *
   * Param :P_PDV_COD Codice punto vendita
   */
   PROCEDURE CREATE_ARTICOLO_CP(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_TIPO_COUPON      IN            VARCHAR2, --B-Bruciabile/M-Mass Market
      P_DESCRIZIONE      IN            VARCHAR2, --Descrizione del Coupon
      PO_ART_COD            OUT        VARCHAR2
   );
  --
END MG_STORE_UTIL_PCK;
/

CREATE OR REPLACE PUBLIC SYNONYM MG_STORE_UTIL_PCK FOR MG.MG_STORE_UTIL_PCK
/


