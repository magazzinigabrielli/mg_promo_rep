CREATE OR REPLACE PACKAGE BODY MG."MG_PROMO_STORE_PCK" AS
  --
  /******************************************************************************
   NAME:       MG_PROMO_STORE_PCK
   PURPOSE:    Package per la gestione delle promozioni gestite direttamente dai PDV

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        03/10/2019           r.doti  1. Created this package.
   ******************************************************************************/
  --
  /**
  * Package di gestione servizi ClickCollect
  * @headcom
  */
  -- COSTANTI A LIVELLO PACKAGE
  kNomePackage       CONSTANT VARCHAR2(30) := 'MG_PROMO_STORE_PCK';
  --
  kFlagAnn           CONSTANT VARCHAR2(1)   := 'A';
  kFlagS             CONSTANT VARCHAR2(1)   := 'S';
  kFlagN             CONSTANT VARCHAR2(1)   := 'N';
  kCausaleETO        CONSTANT VARCHAR2(5)   := 'ETO';
  kCausaleNO_FOOD    CONSTANT VARCHAR2(10)  := 'NO_FOOD';
  kCausaleDURI       CONSTANT VARCHAR2(10)  := 'DURI';
  kCausaleLOCAL      CONSTANT VARCHAR2(10)  := 'LOCAL';
  kStatoKO           CONSTANT VARCHAR2(5)   := 'KO';
  kStatoOK           CONSTANT VARCHAR2(5)   := 'OK';
  vAccapo            VARCHAR2(10)           := CHR(10);
  --
  -- TIPI E VARIABILI GLOBALI
  --
  FUNCTION GET_DATA_FINE_PROMO(
    P_PDV_COD           IN VARCHAR2,
    P_ART_COD           IN VARCHAR2,
    P_DATA_INIZIO       IN TIMESTAMP
  ) RETURN TIMESTAMP
  IS
    vDataFine TIMESTAMP;
    vProcName VARCHAR2(30) := 'IS_ART_PROMO';
  BEGIN
  --
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName,'P_PDV_COD: '|| P_PDV_COD);
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName,'P_ART_COD: '|| P_ART_COD);
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName,'P_DATA_INIZIO: '|| P_DATA_INIZIO);
    SELECT --OPN.ENT_FK AS COD_NEG,
           --OPN.DT_START AS DT_START_NEG,
           --OPN.DT_END AS DT_END_NEG,
           --OPRI.OBJ_CODE AS COD_ART,
           --OPRI.DT_START AS DT_START_ART,
           --OPRI.DT_END AS DT_ENT_ART
           NVL(MAX(OPN.DT_END), TO_DATE('01010001','DDMMYYYY'))
           INTO vDataFine
      FROM KCRM.OPROMO_OFF OPF
     INNER JOIN KCRM.OPROMO_HEAD OPH
           ON (OPF.ID_PROMO_OFF = OPH.ID_PROMO_OFF)
             AND (OPH.FLG_STATE = OPF.FLG_STATE)
             AND (OPF.PROMO_HOST_ID = OPH.PROMO_HOST_ID)
     INNER JOIN KCRM.OPROMO_NODE OPN
           ON (OPH.FLG_STATE = OPN.FLG_STATE)
             AND (OPH.ID_PROMO_OFF = OPN.ID_PROMO_OFF_FK)
             AND (OPH.ID_PROMO_HEAD = OPN.ID_PROMO_HEAD_FK)
     INNER JOIN KCRM.OPROMO_RULE_ITM OPRI
           ON (OPRI.ID_PROMO_HEAD_FK = ID_PROMO_HEAD)
             AND (OPRI.ID_PROMO_OFF_FK = OPH.ID_PROMO_OFF)
             AND (OPRI.FLG_STATE = OPH.FLG_STATE)
       WHERE OPF.FLG_STATE = '0'
         AND OPN.ENT_FK = P_PDV_COD
         AND OPRI.OBJ_CODE = P_ART_COD
         --AND OPRI.DT_START <= P_DATA_INIZIO
         --AND OPRI.DT_END >= P_DATA_INIZIO 
         ;
  --
  /**
SELECT  OPN.ENT_FK AS COD_NEG,
          OPN.DT_START AS DT_START_NEG,
          OPN.DT_END AS DT_END_NEG,
          OPRI.OBJ_CODE AS COD_ART,
          OPRI.DT_START AS DT_START_ART,
          OPRI.DT_END AS DT_ENT_ART
  FROM KCRM.OPROMO_OFF OPF
       INNER JOIN KCRM.OPROMO_HEAD OPH
          ON     (OPF.ID_PROMO_OFF = OPH.ID_PROMO_OFF)
             AND (OPH.FLG_STATE = OPF.FLG_STATE)
             AND (OPF.PROMO_HOST_ID = OPH.PROMO_HOST_ID)
       INNER JOIN KCRM.OPROMO_NODE OPN
          ON     (OPH.FLG_STATE = OPN.FLG_STATE)
             AND (OPH.ID_PROMO_OFF = OPN.ID_PROMO_OFF_FK)
             AND (OPH.ID_PROMO_HEAD = OPN.ID_PROMO_HEAD_FK)
       INNER JOIN KCRM.OPROMO_RULE_ITM OPRI
          ON     (OPRI.ID_PROMO_HEAD_FK = ID_PROMO_HEAD)
             AND (OPRI.ID_PROMO_OFF_FK = OPH.ID_PROMO_OFF)
             AND (OPRI.FLG_STATE = OPH.FLG_STATE)
 WHERE     OPF.FLG_STATE = '0'
       AND OPN.ENT_FK = '00538'
       AND OPRI.OBJ_CODE = ('1214', '26')
       AND OPRI.DT_END < TO_DATE('10112019','DDMMYYYY');
  **/
  --
    RETURN vDataFine;
  --
  END GET_DATA_FINE_PROMO;
  --
  /*
  * La funzione verifica se l'articolo è abilitato per l'inseirmento in promo.
  */
  FUNCTION CHECK_CAU_FILTER(
    P_CAUSALE_PROMO_COD  IN VARCHAR2,
    P_ART_COD            IN VARCHAR2
  ) RETURN NUMBER
  IS
    vOutFilter NUMBER:= 0;
    vProcName  VARCHAR2(30) := 'CHECK_CAU_FILTER';
  BEGIN
  --
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName,'P_CAUSALE_PROMO_COD: '|| P_CAUSALE_PROMO_COD);
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName,'P_ART_COD: '|| P_ART_COD);
    --SELECT ART.E1CLPG, ART.E1CREP, ART.E1CUGE,ART.E1CRAG,ART.E1CFAM,ART.E1CFOR
    SELECT DECODE(COUNT(0),0, 0,1)
      INTO vOutFilter
      FROM MG.GEAD010F ART
     WHERE ART.E1CART = P_ART_COD
       AND (EXISTS (SELECT 0
                      FROM MG_CAUSALI_PROMO_FILTER F1_LIP
                     WHERE F1_LIP.CPR_CAUSALE_PROMO_COD = P_CAUSALE_PROMO_COD
                       AND F1_LIP.CPF_LIP = ART.E1CLPG
                       AND F1_LIP.CPF_REPARTO IS NULL
                       AND F1_LIP.CPF_UG IS NULL
                       AND F1_LIP.CPF_RAGG IS NULL
                       AND F1_LIP.CPF_FAM IS NULL
                       AND (F1_LIP.CPF_FORNITORE IS NULL OR F1_LIP.CPF_FORNITORE = ART.E1CFOR))
         OR EXISTS (SELECT 0
                      FROM MG_CAUSALI_PROMO_FILTER F2_REP
                     WHERE F2_REP.CPR_CAUSALE_PROMO_COD = P_CAUSALE_PROMO_COD
                       AND F2_REP.CPF_LIP = ART.E1CLPG
                       AND F2_REP.CPF_REPARTO = ART.E1CREP
                       AND F2_REP.CPF_UG IS NULL
                       AND F2_REP.CPF_RAGG IS NULL
                       AND F2_REP.CPF_FAM IS NULL
                       AND (F2_REP.CPF_FORNITORE IS NULL OR F2_REP.CPF_FORNITORE = ART.E1CFOR))
         OR EXISTS (SELECT 0
                      FROM MG_CAUSALI_PROMO_FILTER F3_UG
                     WHERE F3_UG.CPR_CAUSALE_PROMO_COD = P_CAUSALE_PROMO_COD
                       AND F3_UG.CPF_LIP = ART.E1CLPG
                       AND F3_UG.CPF_REPARTO = ART.E1CREP
                       AND F3_UG.CPF_UG = ART.E1CUGE
                       AND F3_UG.CPF_RAGG IS NULL
                       AND F3_UG.CPF_FAM IS NULL
                       AND (F3_UG.CPF_FORNITORE IS NULL OR F3_UG.CPF_FORNITORE = ART.E1CFOR))
         OR EXISTS (SELECT 0
                     FROM MG_CAUSALI_PROMO_FILTER F4_RAGGR
                    WHERE F4_RAGGR.CPR_CAUSALE_PROMO_COD = P_CAUSALE_PROMO_COD
                      AND F4_RAGGR.CPF_LIP = ART.E1CLPG
                      AND F4_RAGGR.CPF_REPARTO = ART.E1CREP
                      AND F4_RAGGR.CPF_UG = ART.E1CUGE
                      AND F4_RAGGR.CPF_RAGG = ART.E1CRAG
                      AND F4_RAGGR.CPF_FAM IS NULL
                      AND (F4_RAGGR.CPF_FORNITORE IS NULL OR F4_RAGGR.CPF_FORNITORE = ART.E1CFOR))
         OR EXISTS (SELECT 0
                      FROM MG_CAUSALI_PROMO_FILTER F5_FAM
                     WHERE F5_FAM.CPR_CAUSALE_PROMO_COD = P_CAUSALE_PROMO_COD
                       AND F5_FAM.CPF_LIP = ART.E1CLPG
                       AND F5_FAM.CPF_REPARTO = ART.E1CREP
                       AND F5_FAM.CPF_UG = ART.E1CUGE
                       AND F5_FAM.CPF_RAGG = ART.E1CRAG
                       AND F5_FAM.CPF_FAM = ART.E1CFAM
                       AND (F5_FAM.CPF_FORNITORE IS NULL OR F5_FAM.CPF_FORNITORE = ART.E1CFOR))
         OR EXISTS (SELECT 0
                      FROM MG_CAUSALI_PROMO_FILTER F_FORN
                     WHERE F_FORN.CPR_CAUSALE_PROMO_COD = P_CAUSALE_PROMO_COD
                       AND F_FORN.CPF_LIP IS NULL
                       AND F_FORN.CPF_REPARTO IS NULL
                       AND F_FORN.CPF_UG IS NULL
                       AND F_FORN.CPF_RAGG IS NULL
                       AND F_FORN.CPF_FAM IS NULL
                       AND F_FORN.CPF_FORNITORE = ART.E1CFOR));
  --
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName,'vOutFilter: '|| vOutFilter);
    RETURN TO_NUMBER(vOutFilter);
  --
  EXCEPTION
    WHEN OTHERS THEN
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName,'ERRORE: '|| SQLERRM);
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, SQLERRM,BASE_LOG_PCK.Errore);
      RETURN 0;
  END CHECK_CAU_FILTER;
  --
  PROCEDURE INSERT_PROMO(
      PO_ERR_SEVERITA     IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE       IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO        IN OUT NOCOPY VARCHAR2,
      P_PDV_COD           IN VARCHAR2,
      P_CANALE_COD        IN NUMBER,
      P_ART_COD           IN VARCHAR2,
      P_CAUSALE_PROMO_COD IN VARCHAR2,
      P_PREZZO_OLD        IN NUMBER,
      P_COSTO             IN NUMBER,
      P_PERC_SCNT         IN NUMBER,
      P_PREZZO_NEW        IN NUMBER,
      P_DATA_INIZIO       IN TIMESTAMP,
      P_DATA_FINE         IN TIMESTAMP,
      P_TIPO_OPERAZIONE   IN VARCHAR2,
      P_OPERATORE         IN VARCHAR2
   )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_PROMO_STORE_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    vCNT                NUMBER;
  BEGIN
  --
    vPasso := 'CHECK_ART_PRS';
    SELECT COUNT(0)
      INTO vCNT
      FROM MG_PROMO_STORE
     WHERE PRS_PDV_COD = P_PDV_COD
       AND PRS_ART_COD = P_ART_COD
       AND PRS_DATA_FINE >= P_DATA_INIZIO;
     IF vCNT > 0 THEN
       PO_MESSAGGIO := 'L''ARTICOLO <'||P_ART_COD ||'> E'' GIA'' IN PROMOZIONE STORE';
       RAISE erroreGestito;
     END IF;
     --
     INSERT INTO MG_PROMO_STORE (
        PRS_PDV_COD          ,
        PRS_CANALE           ,
        PRS_ART_COD          ,
        PRS_COSTO            ,
        CPR_CAUSALE_PROMO_COD,
        PRS_PREZZO_OLD       ,
        PRS_PERC_SCNT        ,
        PRS_PREZZO_NEW       ,
        PRS_DATA_INIZIO      ,
        PRS_DATA_FINE        ,
        PRS_TIPO_OPERAZIONE  ,
        PRS_OPERATORE
        )
     VALUES(
       P_PDV_COD          ,
       P_CANALE_COD       ,
       P_ART_COD          ,
       P_COSTO            ,
       P_CAUSALE_PROMO_COD,
       P_PREZZO_OLD       ,
       P_PERC_SCNT        ,
       P_PREZZO_NEW       ,
       TRUNC(P_DATA_INIZIO),
       TRUNC(P_DATA_FINE),
       P_TIPO_OPERAZIONE  ,
       P_OPERATORE
     );
  EXCEPTION
    WHEN erroreGestito THEN
       PO_ERR_SEVERITA := 'E';
       PO_ERR_CODICE   := SQLCODE;
       PO_MESSAGGIO    :=  PO_MESSAGGIO;
       BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, PO_MESSAGGIO , vPasso);
       BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END INSERT_PROMO;
  --
  PROCEDURE ADD_ART_PROMO(
      PO_ERR_SEVERITA     IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE       IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO        IN OUT NOCOPY VARCHAR2,
      P_PDV_COD           IN     VARCHAR2,  --Codice PDV
      P_ART_COD           IN     VARCHAR2,  --Codice Articolo
      P_CAUSALE_PROMO_COD IN     VARCHAR2,  --Codice Causale
      P_PREZZO_OLD        IN     NUMBER,    --proveniente da AS400
      P_COSTO             IN     NUMBER,    --Costo Ultimo
      P_PERC_SCNT         IN     INTEGER,  --PERC_SCNT
      P_PREZZO_NEW        IN     NUMBER,
      P_DATA_INIZIO       IN     TIMESTAMP,
      P_DATA_FINE         IN     TIMESTAMP,
      P_OPERATORE         IN     VARCHAR2,
      P_TIPO_OPERAZIONE   IN     VARCHAR2  DEFAULT 'NEW'
   )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_PROMO_STORE_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100); 
    timeStart           PLS_INTEGER;
    vNomeProc           VARCHAR2(100);
    vInput              VARCHAR2(100);
    exDatiObbl          EXCEPTION;
    PRAGMA              EXCEPTION_INIT( exDatiObbl, -20001 );
    exCheck             EXCEPTION;
    PRAGMA              EXCEPTION_INIT( exCheck, -20002 );
    vShopId             FRAME.MG_SHOP.ID_SHOP%TYPE;
    vCodArt             GEAD010F.E1XART%TYPE;
    vDescrArt           GEAD010F.E1XART%TYPE;
    vUMArt              GEAD010F.E1UMPR%TYPE;
    vStatoArt           GEAD010F.E1CIVI%TYPE;
    vLIP                GEAD010F.E1CLPG%TYPE;
    vLoc                GEAD010F.E1LOC%TYPE;
    vCount              NUMBER := 0;
    vTabCodLIP          TAB_COD_NUM := TAB_COD_NUM();
    vTabCodSTATI        TAB_COD_CHAR := TAB_COD_CHAR();
    vCPR_ROW            MG_CAUSALI_PROMO%ROWTYPE;
    vPriceOld           NUMBER;
    vPriceNEW           NUMBER;
    vCodCanale          NUMBER;
    vDataFinePromo      TIMESTAMP;
    --
    vPromoFIDE          VARCHAR2(5);
    vPromoTIPO_PROMO    VARCHAR2(5);
    vPromoPREZZO        NUMBER;
    vPromoPREZZO_PROMO  NUMBER;
    vPromoVALORE_SCNT   NUMBER;
    vPromoDIPROMO       NUMBER;
    vPromoDFPROMO       NUMBER;
    vNumDay             NUMBER;
  BEGIN 
    --
    vProcName := 'ADD_ART_PROMO';
    --
    vNomeProc := kNomePackage||'.'||vProcName;
    --
    timeStart := dbms_utility.get_time;
    --
    vPasso := 'START';
    BASE_LOG_PCK.WriteLog(vNomeProc, vPasso, BASE_LOG_PCK.Info);
    --
    vPasso := 'CHECK_INPUT';
    --
    IF P_PDV_COD IS NULL THEN vInput     := 'P_PDV_COD'; RAISE exDatiObbl; END IF;
    IF P_ART_COD IS NULL THEN vInput     := 'P_ART_COD'; RAISE exDatiObbl; END IF;
    IF P_CAUSALE_PROMO_COD IS NULL THEN vInput     := 'P_CAUSALE_PROMO_COD'; RAISE exDatiObbl; END IF;
    IF P_DATA_INIZIO IS NULL THEN vInput := 'P_DATA_INIZIO'; RAISE exDatiObbl; END IF;
    IF P_DATA_FINE IS NULL THEN vInput   := 'P_DATA_FINE'; RAISE exDatiObbl; END IF;
    IF P_OPERATORE IS NULL THEN vInput   := 'P_OPERATORE'; RAISE exDatiObbl; END IF;
    IF (P_PERC_SCNT IS NULL OR P_PERC_SCNT = 0) AND (P_PREZZO_NEW IS NULL OR P_PREZZO_NEW = 0) THEN
      PO_MESSAGGIO := 'INDICARE IL NUOVO PREZZO PROMOZIONALE O PERCENTUALE DI SCONTO';
      RAISE exCheck;
    END IF;
    
    --IF (P_PERC_SCNT IS NULL OR P_PERC_SCNT = 0) AND (P_PREZZO_NEW IS NULL OR P_PREZZO_NEW = 0) THEN vInput := 'P_PERC_SCNT O P_PREZZO_NEW'; RAISE exDatiObbl; END IF;
    --
    BEGIN
      vPasso := 'CHECK_PDV';
      SELECT ID_SHOP, 
             ID_DIV
        INTO vShopId, 
             vCodCanale
        FROM FRAME.MG_SHOP
       WHERE TO_NUMBER(ID_SHOP) = P_PDV_COD;
    EXCEPTION 
      WHEN NO_DATA_FOUND THEN
        PO_MESSAGGIO := 'IL PDV <'||P_PDV_COD||'> NON E'' PRESENTE';
        RAISE exCheck;
    END;
    --
    BEGIN
      vPasso := 'CHECK_ART';
      SELECT ASRT.E3CART, 
             ART.E1XART, 
             ART.E1UMPR, 
             TRIM(ART.E1CIVI), 
             ART.E1CLPG, 
             ART.E1LOC
        INTO vCodArt, 
             vDescrArt,
             vUMArt, 
             vStatoArt, 
             vLIP, 
             vLoc
        FROM FRAME.MG_SHOP PDV,
             MG.GEAD030F ASRT,
             MG.GEAD010F ART
       WHERE TO_NUMBER(PDV.ID_SHOP) = P_PDV_COD
         AND TO_NUMBER(PDV.ID_SHOP) =  ASRT.E3CPDV
         AND TRIM(ASRT.E3STAT) IS NULL
         AND ASRT.E3CART = ART.E1CART
         AND TRIM(ART.E1STAT) IS NULL
         AND ART.E1CART = P_ART_COD;
    EXCEPTION 
      WHEN NO_DATA_FOUND THEN
        PO_MESSAGGIO := 'L''ARTICOLO <'||P_ART_COD||'> NON E'' PRESENTE SUL PDV <'||P_PDV_COD||'>';
        RAISE exCheck;
    END;
    --
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName,'vLIP: '|| vLIP, vPasso);
    --
    vPasso := 'CHECK_DECORRENZA';
    IF P_DATA_FINE < P_DATA_INIZIO THEN
      PO_MESSAGGIO := 'LA DATA FINE NON PUO'' ESSERE INFERIORE ALLA DATA INIZIO.';
      RAISE exCheck;
    END IF;
    --
    IF TRUNC(P_DATA_INIZIO) < TRUNC(SYSDATE) THEN
      PO_MESSAGGIO := 'LA DATA INIZIO NON PUO'' ESSERE INFERIORE ALLA DATA DI OGGI.';
      RAISE exCheck;
    END IF;
    --
    BEGIN
      vPasso := 'GET_CAU';
      SELECT *
        INTO vCPR_ROW
        FROM MG_CAUSALI_PROMO
       WHERE CPR_CAUSALE_PROMO_COD = P_CAUSALE_PROMO_COD
         AND TRIM(CPR_STAT) IS NULL;
    EXCEPTION 
      WHEN NO_DATA_FOUND THEN
        PO_MESSAGGIO := 'LA CAUSALE <'||P_CAUSALE_PROMO_COD||'> NON E'' PRESENTE.';
        RAISE exCheck;
    END;
    --
    IF vCPR_ROW.CPR_NO_PROMO = kFlagS  OR NVL(vCPR_ROW.CPR_NO_PROMO_DA, -1) > 0 THEN
      vPasso := 'CHECK_IN_PROMO';
      vDataFinePromo := GET_DATA_FINE_PROMO(P_PDV_COD           => P_PDV_COD,
                                            P_ART_COD           => P_ART_COD,
                                            P_DATA_INIZIO       => P_DATA_INIZIO);
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName,'DataFinePromozione: '|| vDataFinePromo, vPasso);	
      --
      IF vCPR_ROW.CPR_NO_PROMO = kFlagS THEN
        IF NVL(vDataFinePromo, P_DATA_INIZIO -1) >= P_DATA_INIZIO THEN
          PO_MESSAGGIO := 'L''ARTICOLO <'||P_ART_COD||'> E'' GIA'' IN PROMOZIONE.';
          RAISE exCheck;
        END IF;
      END IF;
      --
      IF NVL(vCPR_ROW.CPR_NO_PROMO_DA, -1) > 0 THEN
      --
        vPasso := 'CHECK_PROMO';
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName,'P_DATA_INIZIO: '|| TRUNC(P_DATA_INIZIO), vPasso);
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName,'vDataFinePromo: '|| TRUNC(vDataFinePromo), vPasso);
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName,'CPR_NO_PROMO_DA: '|| vCPR_ROW.CPR_NO_PROMO_DA, vPasso);
        vNumDay := TO_NUMBER(TRUNC(P_DATA_INIZIO)  - TRUNC(vDataFinePromo));
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName,'vNumDay: '|| vNumDay, vPasso);
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName,'CPR_NO_PROMO_DA: '|| vCPR_ROW.CPR_NO_PROMO_DA, vPasso);
        --
        IF vNumDay IS NOT NULL AND  vNumDay <= TO_NUMBER(vCPR_ROW.CPR_NO_PROMO_DA) THEN
          PO_MESSAGGIO := 'L''ARTICOLO <'||P_ART_COD||'> E'' GIA'' STATO IN PROMOZIONE NEGLI ULTIMI <'||vCPR_ROW.CPR_NO_PROMO_DA||'> GIORNI. Data fine promozione <'|| TRUNC(vDataFinePromo)||'>';
          RAISE exCheck;
        ELSE
          BASE_TRACE_PCK.TRACE(kNomePackage,vProcName,'Sono Qui: ', vPasso);
         -- BASE_TRACE_PCK.TRACE(kNomePackage,vProcName,'Sono Qui: '|| P_DATA_INIZIO  - vDataFinePromo, vPasso);
        END IF;
      END IF;
    END IF;
    --
    IF NVL(vCPR_ROW.CPR_DURATA_MAX,-1) > 0 THEN
      vPasso := 'CHECK_IN_PROMO';
      IF P_DATA_FINE > P_DATA_INIZIO + vCPR_ROW.CPR_DURATA_MAX THEN
        PO_MESSAGGIO := 'LA DURATA DELLA PROMOZIONE NON PUO'' ESSERE OLTRE <'||vCPR_ROW.CPR_DURATA_MAX||'> GIORNI';
        RAISE exCheck;
      END IF;
    END IF;
    --
    vPasso := 'CHECK_FILTRI';
    vCount := CHECK_CAU_FILTER( P_CAUSALE_PROMO_COD  => P_CAUSALE_PROMO_COD,
                                P_ART_COD            => P_ART_COD);
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName,'vCount: '|| TO_NUMBER(vCount), vPasso);
    --
    IF vCount = 0 THEN
      PO_MESSAGGIO := 'LA VARIAZIONE NON E'' AMMESSA PER QUESTO ARTICOLO';
      RAISE exCheck;
    ELSIF vCount > 0 AND  P_CAUSALE_PROMO_COD = 'LOC' AND vLoc != 'S' THEN
      PO_MESSAGGIO := 'LA VARIAZIONE NON E'' AMMESSA PER QUESTO ARTICOLO';
      RAISE exCheck;
    ELSE
    --
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName,'CHECK_CAU_FILTER OK.', vPasso);
    END IF;
    --
    --vTabCodLIP := FRAME.BASE_UTIL_PCK.GetTAB_COD_NUM(vCPR_ROW.CPR_LINEE_PRODOTTO);
    --IF vTabCodLIP.COUNT > 0 THEN
    ----
    --  SELECT COUNT(0) INTO vCount FROM TABLE(vTabCodLIP) FIL WHERE FIL.COLUMN_VALUE = vLIP;
    --  BASE_TRACE_PCK.TRACE(kNomePackage,vProcName,'vLIP: '|| vLIP, vPasso);	
    --  BASE_TRACE_PCK.TRACE(kNomePackage,vProcName,'vCount: '|| vCount, vPasso);	
    --  IF vCount = 0 THEN
    --    PO_MESSAGGIO := 'LA LINEA PRODOTTO <'||vLIP||'> NON E'' AMMESSA.';
    --    RAISE exCheck;
    --  END IF;
    --  vCount := 0;
    --  --
    --END IF;
    --
    vPasso := 'CHECK_STATO';
    vTabCodSTATI := FRAME.BASE_UTIL_PCK.GetTAB_COD_CHAR(vCPR_ROW.CPR_STATI_ARTICOLI);
    IF vStatoArt IS NOT NULL AND vTabCodSTATI.COUNT >0 THEN
      --
      SELECT COUNT(0) INTO vCount FROM TABLE(vTabCodSTATI) FIL WHERE FIL.COLUMN_VALUE = vStatoArt;
      --
      IF vCount = 0  THEN
        PO_MESSAGGIO := 'LO STATO DEL PRODOTTO <'||vStatoArt||'> NON E'' AMMESSO.';
        RAISE exCheck;
      END IF;
    END IF;
    --
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName,'vTabCodSTATI: '|| vTabCodSTATI.COUNT, vPasso);	
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName,'vStatoArt: '|| vStatoArt, vPasso);	
    IF vStatoArt IS NOT NULL AND vTabCodSTATI.COUNT = 0 THEN
      --
       PO_MESSAGGIO := 'L''ARTICOLO E'' NELLO STATO <'||vStatoArt||'>.';
       RAISE exCheck;
    END IF;
    --
    vPasso := 'CHECK_PRZ_PRMU';
    IF NVL(vCPR_ROW.CPR_CK_PRZ_PRMU, kFlagN) = kFlagS THEN
      vPasso := 'GET_PRICE';
      FRAME.FRM_PREZZI_PCK.GET_PROMO_PRICE(
          PO_ERR_SEVERITA       => PO_ERR_SEVERITA,
          PO_ERR_CODICE         => PO_ERR_CODICE  ,
          PO_MESSAGGIO          => PO_MESSAGGIO   ,
          P_OBJ_CODE            => P_ART_COD,
          P_ID_SHOP             => P_PDV_COD,
          P_DATA_DEC            => vDataFinePromo, --Passo la data dell'ultima promozione
          PO_FIDE               => vPromoFIDE , --[F: Fidelity- M: Mass Market]
          PO_TIPO_PROMO         => vPromoTIPO_PROMO , --Tipologia Promo A400[TP, VI, PI, MN]    KLOY[PC- VC]
          PO_PREZZO             => vPromoPREZZO ,
          PO_PREZZO_PROMO       => vPromoPREZZO_PROMO ,
          PO_VALORE_SCNT        => vPromoVALORE_SCNT,
          PO_DIPROMO            => vPromoDIPROMO,
          PO_DFPROMO            => vPromoDFPROMO
        );
      IF PO_MESSAGGIO IS NOT NULL THEN RAISE exCheck; END IF;
      --
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName,'PO_PREZZO: '|| vPromoPREZZO, vPasso);
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName,'PO_PREZZO_PROMO: '|| vPromoPREZZO_PROMO, vPasso);
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName,'PO_VALORE_SCNT: '|| vPromoVALORE_SCNT, vPasso);
      --IF vPromoPREZZO_PROMO > P_DATA_INIZIO + vCPR_ROW.CPR_DURATA_MAX THEN
      --  PO_MESSAGGIO := 'LA DURATA DELLA PROMOZIONE NON PUO'' ESSERE OLTRE <'||vCPR_ROW.CPR_DURATA_MAX||'> GIORNI';
      --  RAISE exCheck;
      --END IF;
    END IF;
    --
    IF P_PERC_SCNT IS NOT NULL THEN
    --
      vPasso := 'vPriceNEW';
      IF P_PREZZO_OLD IS NOT NULL THEN
         vPriceOld := P_PREZZO_OLD;
      END IF;
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName,'vPriceOld: '|| vPriceOld, vPasso);
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName,'P_PERC_SCNT: '|| P_PERC_SCNT, vPasso);
      vPriceNEW := ROUND(vPriceOld - vPriceOld/100*P_PERC_SCNT,2);
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName,'vPriceNEW: '|| vPriceNEW, vPasso);
    ELSE
      vPriceNEW := P_PREZZO_NEW;
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName,'vPriceNEW: '|| vPriceNEW, vPasso);
    END IF;
    --Check sugli ETO
    IF vCPR_ROW.CPR_CAUSALE_PROMO_COD = kCausaleETO THEN
    --
      vPasso := 'CHECK_ETO';
      IF NVL(vStatoArt,'X') != kCausaleETO THEN
        PO_MESSAGGIO := '[ETO] L''ARTICOLO <'||P_ART_COD||'> NON E'' NELLO STATO <'||kCausaleETO||'>';
        RAISE exCheck;
      END IF;
      --
      vPasso := 'CHECK_DATA_FIN';
      IF P_DATA_FINE > P_DATA_INIZIO + 365 THEN
        PO_MESSAGGIO := 'LA DURATA DELLA PROMOZIONE NON PUO'' ESSERE OLTRE 365 GIORNI';
        RAISE exCheck;
      END IF;
      --
      vPasso := 'CHECK_PERC';
      IF P_PERC_SCNT > 70 THEN
        PO_MESSAGGIO := '[ETO]LA PERCENTUALE DI SCONTO NON PUO'' ESSERE MAGGIORE DEL 70%.';
        RAISE exCheck;
      END IF;
    --
    END IF;
    --
    IF P_PREZZO_OLD IS NULL THEN
    --
      vPasso := 'GET_PREZZO';
      FRAME.FRM_PREZZI_PCK.GET_PRICE( PO_ERR_SEVERITA, PO_ERR_CODICE , PO_MESSAGGIO   ,
        P_INTCODE      => P_ART_COD,
        P_HOST_SHOP_ID => P_PDV_COD,
        PO_PRICEAMOUNT => vPriceOld
        );
      IF PO_MESSAGGIO IS NOT NULL THEN RAISE exCheck; END IF;
      --
      vPriceOld := vPriceOld/100;
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName,'vPriceOld: '|| vPriceOld, vPasso);
    ELSE
      vPriceOld := P_PREZZO_OLD;
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName,'vPriceOld: '|| vPriceOld, vPasso);
    END IF;
    --
    --Check sugli NO_FOOD
    IF vCPR_ROW.CPR_CAUSALE_PROMO_COD = kCausaleNO_FOOD THEN
    --
      vPasso := 'CHECK_NO_FOOD';
      --
      IF NVL(vLIP, 0) NOT IN (61,62,63) THEN
        PO_MESSAGGIO := '[NO_FOOD] L''ARTICOLO <'||P_ART_COD||'> NON APPARTIENE ALLA LINEA PRODOTTO <'||kCausaleNO_FOOD||'>';
        RAISE exCheck;
      END IF;
      --
      vPasso := 'CHECK_DATA_FIN_LAST_PROMO';
      IF NVL(vDataFinePromo, SYSDATE) < TRUNC(SYSDATE) - 15 THEN
        PO_MESSAGGIO := 'L''ARTICOLO E''STATO  GIA''IN PROMOZIONE NEGLI ULTIMI 15 GIORNI';
        RAISE exCheck;
      END IF;
      --
      vPasso := 'CHECK_COSTO_MEDIO';
      IF vPriceNEW < P_COSTO THEN
        --P_PERC_SCNT
        PO_MESSAGGIO := 'IL PREZZO NON PUO'' ESSERE INFERIORE AL COSTO ULTIMO:<'||P_COSTO||'>';
        RAISE exCheck;
      END IF;
      --
      vPasso := 'CHECK_DATA_FIN';
      IF P_DATA_FINE > P_DATA_INIZIO + 15 THEN
        PO_MESSAGGIO := 'LA DURATA DELLA PROMOZIONE NON PUO'' ESSERE OLTRE 15 GIORNI';
        RAISE exCheck;
      END IF;
    --
    END IF;
    --
    vPasso := 'INSERT_PROMO';
    INSERT_PROMO(PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
        P_PDV_COD           => P_PDV_COD          ,
        P_CANALE_COD        => vCodCanale         ,
        P_ART_COD           => P_ART_COD          ,
        P_CAUSALE_PROMO_COD => P_CAUSALE_PROMO_COD,
        P_PREZZO_OLD        => vPriceOld          ,
        P_COSTO             => P_COSTO            ,
        P_PERC_SCNT         => P_PERC_SCNT        ,
        P_PREZZO_NEW        => vPriceNEW          ,
        P_DATA_INIZIO       => P_DATA_INIZIO      ,
        P_DATA_FINE         => P_DATA_FINE        ,
        P_TIPO_OPERAZIONE   => P_TIPO_OPERAZIONE  ,
        P_OPERATORE         => P_OPERATORE);
    IF PO_MESSAGGIO IS NOT NULL THEN RAISE exCheck; END IF;
    --
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName,'TimeElab (min.): '|| (dbms_utility.get_time - timeStart)/100/60, BASE_LOG_PCK.Info);
    -- 
    IF PO_MESSAGGIO IS NOT NULL THEN RAISE exCheck; END IF;
    vPasso := 'END';
    BASE_LOG_PCK.WriteLog(vNomeProc, vPasso, BASE_LOG_PCK.Info);
  --
  EXCEPTION
    WHEN exDatiObbl THEN
       PO_ERR_SEVERITA := 'E';
       PO_ERR_CODICE   := SQLCODE;
       PO_MESSAGGIO    :=  'ERRORE: IL PARAMETRO '|| vInput ||' E'' OBBLIGATORIO.';
       BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, PO_MESSAGGIO , vPasso);
    WHEN exCheck THEN
       PO_ERR_SEVERITA := 'B';
       PO_ERR_CODICE   := SQLCODE;
       PO_MESSAGGIO    :=  'ERRORE:'||PO_MESSAGGIO;
       BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, PO_MESSAGGIO , vPasso);
       BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END ADD_ART_PROMO;
  --
  PROCEDURE ELAB_PROMO(
      PO_ERR_SEVERITA     IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE       IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO        IN OUT NOCOPY VARCHAR2,
      P_PDV_COD           IN     VARCHAR2
   )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_PROMO_STORE_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100); 
    timeStart           PLS_INTEGER;
    vNomeProc           VARCHAR2(100);
    vInput              VARCHAR2(50);
    exDatiObbl          EXCEPTION;
    vJobID              NUMBER;
    vPromoID            NUMBER;
    vPromoType          VARCHAR2(15);
    vRowNum             NUMBER := 0;
    exArt               EXCEPTION;
    vError              MG_PROMO_STORE.PRS_LOG%TYPE;
  BEGIN 
    --
    vProcName := 'ELAB_PROMO';
    --
    vNomeProc := kNomePackage||'.'||vProcName;
    --
    timeStart := dbms_utility.get_time;
    --
    vPasso := 'START';
    BASE_LOG_PCK.WriteLog(vNomeProc, vPasso, BASE_LOG_PCK.Info);
    --
    vPasso := 'CHECK_INPUT';
    IF P_PDV_COD IS NULL THEN vInput     := 'P_PDV_COD'; RAISE exDatiObbl; END IF;
    --
    FOR wiz IN (SELECT DISTINCT WIZ.KCRM_ID_PROMO_WIZARD, WIZ.HOST_PROMO_TYPE
                  FROM FRAME.MG_SPROMO_WIZARD WIZ,
                       MG_PROMO_STORE PRS
                 WHERE WIZ.HOST_PROMO_TYPE = NVL2(PRS_PERC_SCNT, 'SCP', NVL2(PRS_PREZZO_NEW, 'TP', 'OK' ))
                   AND PRS_PDV_COD = P_PDV_COD
                   AND PRS_TIPO_OPERAZIONE = 'NEW'
                   AND PRS_ELAB IS NULL
                )
    LOOP
      vPasso := 'GET_JOB_ID';
      SELECT FRAME.SEQ_ID_JOB_DPL6.NextVal INTO vJobID FROM DUAL;
      --
      vPasso := 'GET_PROMO_ID';
      SELECT MG_PRS_ID.NEXTVAL INTO vPromoID FROM DUAL;
      --
      FOR rec IN (SELECT PRS_PROMO_STORE_ID,
                         PRS_CANALE ID_DIV,
                         EXTRACT(YEAR FROM CURRENT_DATE)||CPR_CAUSALE_PROMO_COD||TO_NUMBER(PRS_PDV_COD)||'-'||vPromoID ID_PROMO,
                         wiz.KCRM_ID_PROMO_WIZARD PROMO_TYPE,
                         PRS_ART_COD OBJ_CODE,
                         NVL(PRS_PERC_SCNT,PRS_PREZZO_NEW)  VALUE,
                         PRS_PDV_COD ID_SHOP,
                         PRS_DATA_INIZIO DT_START, 
                         PRS_DATA_FINE  DT_END ,
                         PRS_DATA_INIZIO DT_STARTL, 
                         PRS_DATA_FINE DT_ENDL,
                         PRS_PERC_SCNT PRC,
                         '0' FLG_STATE,
                         '0002017001' ID_ACM_FK,
                         '00000' ID_LOY_ACTION_FK,
                         '' FLG_ACTION,
                         EXTRACT(YEAR FROM CURRENT_DATE)||'-'||TO_NUMBER(PRS_PDV_COD)||'-'||vPromoID DESCR,
                         EXTRACT(YEAR FROM CURRENT_DATE)||'-'||TO_NUMBER(PRS_PDV_COD)||'-'||vPromoID DESCR_BILL,
                         '' COUP_CODE
                    FROM MG_PROMO_STORE PRS
                   WHERE PRS_PDV_COD = P_PDV_COD
                     AND ( (wiz.HOST_PROMO_TYPE = 'SCP' AND PRS_PERC_SCNT IS NOT NULL) 
                           OR
                           (wiz.HOST_PROMO_TYPE = 'TP' AND PRS_PREZZO_NEW IS NOT NULL)
                          )
                     AND PRS_TIPO_OPERAZIONE = 'NEW'
                     AND PRS_ELAB IS NULL
                  )
      LOOP
      --
        BEGIN
        vRowNum := vRowNum + 1;
        --
        INSERT INTO FRAME.PROMO_IMPT (
                          ID_JOB, ID_ROW, 
                          JOB_DATE, JOB_TIME, 
                          FLG_ACT, ID_DIV,
                          PROMO_HOST, ID_PROMO, ROW_NO,
                          ID_PROMOH, PROMO_TYPE,
                          OBJ_TYPE, OBJ_CODE,
                          VALUE, ID_GRP,
                          ID_SHOP,
                          DT_START, DT_END,
                          DT_STARTL, DT_ENDL,
                          PRC,
                          FLG_STATE,ID_ACM_FK,
                          ID_LOY_ACTION_FK, FLG_ACTION,
                          DESCR, DESCR_BILL, COUP_CODE
                          )
        VALUES(vJobID, vRowNum,
               CURRENT_DATE, CURRENT_TIMESTAMP,
               'I', rec.ID_DIV,
               rec.ID_PROMO, rec.ID_PROMO, vRowNum,
               '$$', rec.PROMO_TYPE,
               '001', rec.OBJ_CODE,
               rec.VALUE, 0,
               rec.ID_SHOP,
               rec.DT_START, 
               rec.DT_END ,
               rec.DT_STARTL, 
               rec.DT_ENDL,
               rec.PRC,
               rec.FLG_STATE,
               rec.ID_ACM_FK,
               rec.ID_LOY_ACTION_FK,
               rec.FLG_ACTION,
               rec.DESCR,
               rec.DESCR_BILL,
               rec.COUP_CODE
               );
        --
        UPDATE MG_PROMO_STORE
           SET PRS_ELAB = kStatoOK,
               PRS_LOG  = 'Promozione:<'||rec.ID_PROMO||'>'||vAccapo||
                          'IdJOB:<'||vJobID||'>'
         WHERE PRS_PROMO_STORE_ID = rec.PRS_PROMO_STORE_ID;
        --
        EXCEPTION
          WHEN OTHERS THEN
          --
            vError := SQLCODE ||'-'||SQLERRM;
            --
            UPDATE MG_PROMO_STORE
               SET PRS_ELAB = kStatoKO,
                   PRS_LOG  = vError
             WHERE PRS_PROMO_STORE_ID = rec.PRS_PROMO_STORE_ID;
        END;
      END LOOP;
      --
      vRowNum := SQL%ROWCOUNT;
      --
      INSERT INTO FRAME.KSYNCR(ID_DPL_FK,ID_FLOW_FK,ID_JOB,JOB_DATE,JOB_TIME,FLG_JOB,ROW_NO)
      SELECT 6 ID_DPL_FK,
             DECODE(LEVEL,1,36010,99999) ID_FLOW_FK,
             vJobID ID_JOB,
             CURRENT_DATE JOB_DATE,
             CURRENT_TIMESTAMP JOB_TIME,
             'I' FLG_JOB,
             vRowNum ROW_NO
        FROM DUAL CONNECT BY LEVEL <=2;
    --
    END LOOP;
    --
    IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
    vPasso := 'END';
    BASE_LOG_PCK.WriteLog(vNomeProc, vPasso, BASE_LOG_PCK.Info);
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName,'TimeElab (min.): '|| (dbms_utility.get_time - timeStart)/100/60, vPasso);
  --
  EXCEPTION
    WHEN exDatiObbl THEN
       PO_ERR_SEVERITA := 'E';
       PO_ERR_CODICE   := SQLCODE;
       PO_MESSAGGIO    :=  'ERRORE: IL PARAMETRO '|| vInput ||' E'' OBBLIGATORIO.';
       BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, PO_MESSAGGIO , vPasso);
    WHEN erroreGestito THEN
       PO_ERR_SEVERITA := 'E';
       PO_ERR_CODICE   := SQLCODE;
       PO_MESSAGGIO    :=  'ERRORE:'||PO_MESSAGGIO || ' [vPasso:'||vPasso||']';
       BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, PO_MESSAGGIO , vPasso);
       BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END ELAB_PROMO;
  --
--
END MG_PROMO_STORE_PCK;
/
