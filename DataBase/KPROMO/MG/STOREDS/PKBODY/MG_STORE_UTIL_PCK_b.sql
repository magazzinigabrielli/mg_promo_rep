CREATE OR REPLACE PACKAGE BODY MG."MG_STORE_UTIL_PCK" AS
  --
 /******************************************************************************
   NAME:       MG_STORE_UTIL_PCK
   PURPOSE:    Package per la gestione degli store

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        04/12/2019           r.doti  1. Created this package.
   ******************************************************************************/
  -- COSTANTI A LIVELLO PACKAGE
  kNomePackage            CONSTANT VARCHAR2(30) := 'MG_STORE_UTIL_PCK';
  --
  kFlagAnn                CONSTANT VARCHAR2(1)  := 'A';
  kFlagS                  CONSTANT VARCHAR2(1)  := 'S';
  kFlagN                  CONSTANT VARCHAR2(1)  := 'N';
  kFlagActN               CONSTANT VARCHAR2(1)  := 'N';
  KDivisioneVirtual       CONSTANT NUMBER       := 99; --Canale Store Virtuali
  KBarcodeBilan           CONSTANT VARCHAR2(5)  := '2%';
  KTabellaSHOP            CONSTANT VARCHAR2(30) := 'SHOP';
  KTabellaVS_VAT          CONSTANT VARCHAR2(30) := 'VS_VAT';
  KTabellaVS_DEPT         CONSTANT VARCHAR2(30) := 'VS_DEPT';
  KTabellaVS_DPL_GRP      CONSTANT VARCHAR2(30) := 'VS_DPL_GRP';
  KTabellaVS_ITEM         CONSTANT VARCHAR2(30) := 'VS_ITEM';
  KTabellaVS_XREF         CONSTANT VARCHAR2(30) := 'VS_XREF';
  KTabellaPROMO_IMPT      CONSTANT VARCHAR2(30) := 'PROMO_IMPT';
  KTabellaTappo           CONSTANT VARCHAR2(30) := 'Tappo';
  vAccapo                          VARCHAR2(10) := CHR(10);
  --
  exNotPDV EXCEPTION;
  PRAGMA EXCEPTION_INIT( exNotPDV, -20002 );
  --
  TYPE tyJobInfo IS RECORD
  (
    ID_JOB        NUMBER,
    JOB_DATE      DATE,
    JOB_TIME      TIMESTAMP(6),
    ID_ROW        NUMBER,
    ROWS          NUMBER,
    FLG_ACT       VARCHAR2(5) := kFlagActN,
    DPL_GRP_ID_FK NUMBER
  );
  --
  FUNCTION GET_PDV_FISICO(
    P_PDV_VIRTUAL_COD IN NUMBER
  )
    RETURN NUMBER
  IS
    vCodPDV NUMBER;
  BEGIN
  --
    SELECT PVSER_PDV_COD
      INTO vCodPDV
      FROM MG.MG_PDV_REL_SERVICE
     WHERE PVSER_PDV_REL_PDV_ID = P_PDV_VIRTUAL_COD;
     --
    RETURN vCodPDV;
  --
  EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( -20002, 'PDV :['||P_PDV_VIRTUAL_COD ||'] NON PRESENTE.' );
  END GET_PDV_FISICO;
  --
  /**
  * Description: Utility per la creazione di PDV Virtuale
  *
  */
  PROCEDURE CREATE_INFO_JOB(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      PO_JOB_INFO           OUT tyJobInfo
   )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_STORE_UTIL_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
  BEGIN 
    --
    vProcName := 'CREATE_INFO_JOB';
    --
    SELECT FRAME.SEQ_ID_JOB_DPL6.NextVal INTO PO_JOB_INFO.ID_JOB FROM DUAL;
    PO_JOB_INFO.JOB_DATE := TRUNC(SYSDATE);
    PO_JOB_INFO.JOB_TIME := SYSTIMESTAMP;
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END CREATE_INFO_JOB;
  --
  /**
  * Description: Utility per la creazione del gruppo di variazione flusso
  *
  */
  PROCEDURE CREATE_GRUPPOFLOW(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_JOB_INFO         IN            tyJobInfo,
      P_TAB_ENTITY       IN            FRAME.TAB_COD_CHAR,
      PO_GPR_ID             OUT        VARCHAR2
   )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_STORE_UTIL_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
  BEGIN 
    --
    vProcName := 'CREATE_GRUPPOFLOW';
    --
    vPasso := 'GRUPPO';
    SELECT FRAME.SEQ_DPL_GRP_ID.NextVal INTO PO_GPR_ID FROM DUAL;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PO_GPR_ID:'||PO_GPR_ID, vPasso);
    --
    vPasso := 'INS-VS_DPL_GRP';
    INSERT INTO FRAME.VS_DPL_GRP(ID_JOB,
                                 ID_ROW,
                                 DPL_GRP_ID,
                                 ID_CMP_FK,
                                 ID_DIV_FK,
                                 ID_ENT_FK,
                                 TS_DPL)
         SELECT P_JOB_INFO.ID_JOB, 
                ROWNUM, 
                PO_GPR_ID, 
                ID_CMP_FK,
                ID_DIV_FK,
                ID_SHOP ID_ENT_FK,
                SYSTIMESTAMP
           FROM FRAME.MG_SHOP NEG,
                TABLE(P_TAB_ENTITY) FIL
          WHERE ID_SHOP = FIL.COLUMN_VALUE
            AND ID_CMP_FK IS NOT NULL
            AND ID_DIV_FK IS NOT NULL;
    --
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'CNT(Rows):'||SQL%ROWCOUNT, vPasso);
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END CREATE_GRUPPOFLOW;
  --
  PROCEDURE CREATE_SHOP(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_SHOP_ROW         IN     FRAME.MG_SHOP%ROWTYPE,
      P_JOB_INFO         IN     tyJobInfo
   )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_STORE_UTIL_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    vRowShop            FRAME.SHOP%ROWTYPE;
  BEGIN 
    --
    vRowShop.ID_JOB := P_JOB_INFO.ID_JOB;
    vRowShop.ID_ROW := 1;
    vRowShop.JOB_DATE := P_JOB_INFO.JOB_DATE;
    vRowShop.JOB_TIME := P_JOB_INFO.JOB_TIME;
    --
    vRowShop.ID_SHOP     := P_SHOP_ROW.ID_SHOP    ;
    vRowShop.ID_EXT      := P_SHOP_ROW.ID_EXT     ;
    vRowShop.ID_DIV      := P_SHOP_ROW.ID_DIV     ;
    vRowShop.ID_CLASS    := P_SHOP_ROW.ID_CLASS   ;
    vRowShop.DESCR       := P_SHOP_ROW.DESCR      ;
    vRowShop.VAT_NO      := P_SHOP_ROW.VAT_NO     ;
    vRowShop.FISC_CODE   := P_SHOP_ROW.FISC_CODE  ;
    vRowShop.NAME1       := P_SHOP_ROW.NAME1      ;
    vRowShop.NAME2       := P_SHOP_ROW.NAME2      ;
    vRowShop.ADDRESS     := P_SHOP_ROW.ADDRESS    ;
    vRowShop.ZIP_CODE    := P_SHOP_ROW.ZIP_CODE   ;
    vRowShop.CITY        := P_SHOP_ROW.CITY       ;
    vRowShop.PROVINCE    := P_SHOP_ROW.PROVINCE   ;
    vRowShop.ID_COUNTRY  := P_SHOP_ROW.ID_COUNTRY ;
    vRowShop.SNAME1      := P_SHOP_ROW.SNAME1     ;
    vRowShop.SNAME2      := P_SHOP_ROW.SNAME2     ;
    vRowShop.SADDRESS    := P_SHOP_ROW.SADDRESS   ;
    vRowShop.SZIP_CODE   := P_SHOP_ROW.SZIP_CODE  ;
    vRowShop.SCITY       := P_SHOP_ROW.SCITY      ;
    vRowShop.SPROVINCE   := P_SHOP_ROW.SPROVINCE  ;
    vRowShop.ID_SCOUNTRY := P_SHOP_ROW.ID_SCOUNTRY;
    vRowShop.ID_CO_COGE  := P_SHOP_ROW.ID_CO_COGE ;
    vRowShop.ID_CAT      := P_SHOP_ROW.ID_CAT     ;
    vRowShop.FLG_STYPE   := P_SHOP_ROW.FLG_STYPE  ;
    vRowShop.FLG_FID     := P_SHOP_ROW.FLG_FID    ;
    vRowShop.FLG_FO      := P_SHOP_ROW.FLG_FO     ;
    vRowShop.FLG_SCO     := P_SHOP_ROW.FLG_SCO    ;
    vRowShop.FLG_ELBL    := P_SHOP_ROW.FLG_ELBL   ;
    vRowShop.FLG_STATE   := P_SHOP_ROW.FLG_STATE  ;
    vRowShop.ID_AZIENDA  := 1 ;
    --
    vProcName := 'CREATE_SHOP';
    INSERT INTO FRAME.SHOP
    VALUES vRowShop;
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END CREATE_SHOP;
  --
  PROCEDURE CREATE_FLOW(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_NOME_TABELLA     IN            VARCHAR2,
      P_TAPPO            IN            VARCHAR2,
      P_JOB_INFO         IN            tyJobInfo
   )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_STORE_UTIL_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    vDPLFK              FRAME.KFLOW.ID_DPL_FK%TYPE;
    vCountTot           NUMBER :=0;
  BEGIN 
    --
    vProcName := 'CREATE_FLOW';
    --
    FOR rec IN (SELECT ID_DPL_FK,ID_FLOW, TABLE_NAME
                  FROM FRAME.KFLOW
                 WHERE TABLE_NAME = P_NOME_TABELLA
                 UNION ALL
                SELECT ID_DPL_FK,ID_FLOW, TABLE_NAME
                  FROM FRAME.KFLOW
                 WHERE TABLE_NAME = NVL2(P_TAPPO, KTabellaTappo, 'VUOTO')
                )
    LOOP
      --
      IF rec.TABLE_NAME != KTabellaTappo THEN
      --
        vDPLFK := rec.ID_DPL_FK;
        --
      ELSE
        SELECT SUM(ROW_NO)
          INTO vCountTot
          FROM FRAME.KSYNCR
         WHERE ID_JOB = P_JOB_INFO.ID_JOB;
      END IF;
      vPasso:='INS_KSYNCR';
      INSERT INTO FRAME.KSYNCR(ID_DPL_FK, 
                               ID_FLOW_FK, 
                               ID_JOB, 
                               JOB_DATE,
                               JOB_TIME,
                               FLG_JOB,
                               ROW_NO)
           SELECT vDPLFK,
                  rec.ID_FLOW, 
                  P_JOB_INFO.ID_JOB,
                  P_JOB_INFO.JOB_DATE, 
                  P_JOB_INFO.JOB_TIME, 
                  'N',
                  DECODE(rec.TABLE_NAME,KTabellaTappo,vCountTot, P_JOB_INFO.ROWS) ROW_NO
             FROM DUAL;
    END LOOP;
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END CREATE_FLOW;
  --
  /**
  * CREATE_PDV_VIRTUAL(): Utility per la creazione di PDV Virtuale
  *
  * @param P_PDV_COD Codice del PDV Fisico
  */
  PROCEDURE CREATE_PDV_VIRTUAL(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_PDV_COD          IN     NUMBER,
      P_DECORRENZA       IN     DATE,
      PO_PDV_VIRTUAL_COD    OUT NUMBER,
      PO_JOB_ID             OUT NUMBER
   )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_STORE_UTIL_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    vRowPDV             FRAME.MG_SHOP%ROWTYPE;
    vCodPDVVirtual      MG_PDV_REL_SERVICE.PVSER_PDV_REL_PDV_ID%TYPE;
    vDataJob            TIMESTAMP := SYSTIMESTAMP;
    vContVirt           NUMBER;
    VJobInfo            tyJobInfo;
  BEGIN 
    --
    vProcName := 'CREATE_PDV_VIRTUAL';
    --
    vPasso := 'GET_COD_PDV_FIS';
    SELECT *
      INTO vRowPDV
      FROM FRAME.MG_SHOP
     WHERE HOST_ID = P_PDV_COD;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'CodPDV(Fisico):'||vRowPDV.ID_SHOP, vPasso);
     --
    vPasso := 'GET_COD_PDV_VIRTUAL';
    SELECT PVSER_PDV_REL_PDV_ID
      INTO vCodPDVVirtual
      FROM MG.MG_PDV_REL_SERVICE
     WHERE PVSER_PDV_COD = P_PDV_COD
       AND PVSER_MASTER = 'S'; 
     --
     BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'CodPDV(Virtual):'||vCodPDVVirtual , vPasso);
     --
     vPasso := 'MK_ROW_SHOP_VIRT';
     vRowPDV.ID_SHOP       := vCodPDVVirtual;
     vRowPDV.ID_DIV        := KDivisioneVirtual;
     vRowPDV.DESCR         := vRowPDV.DESCR ||'**Virtual**';
     vRowPDV.HOST_JOB_DATE := vDataJob;
     vRowPDV.HOST_ID       := vCodPDVVirtual;
     vRowPDV.ID_DIV_FK     := KDivisioneVirtual;
     vRowPDV.HOST_STAT_ID  := vCodPDVVirtual;
     vRowPDV.DT_START      := P_DECORRENZA;
     vRowPDV.DT_START_VS   := P_DECORRENZA;
     vRowPDV.DT_INITLOAD_VS:= P_DECORRENZA;
     --
     SELECT SUBSTR(VAL, 1,LENGTH(VAL)-2 )HOST_CAZI, SUBSTR(VAL, -2) HOST_CFIL
       INTO  vRowPDV.HOST_CAZI, vRowPDV.HOST_CFIL
       FROM (SELECT vCodPDVVirtual VAL
               FROM DUAL);
     --
     vPasso := 'CHECK_MG_SHOP';
     --
     SELECT COUNT(0)
       INTO vContVirt
       FROM FRAME.MG_SHOP
       WHERE ID_SHOP = vRowPDV.ID_SHOP;
     --
     BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'vContVirt ROWS:'||vContVirt , vPasso);
     IF vContVirt = 0 THEN
       vPasso := 'INSRT_MG_SHOP';
       INSERT INTO FRAME.MG_SHOP
       VALUES vRowPDV;
       BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'CodPDV(Virtual) ROWS:'||SQL%ROWCOUNT , vPasso);
     ELSE
       vPasso := 'UPDT_MG_SHOP';
       UPDATE FRAME.MG_SHOP
          SET ID_DIV         = vRowPDV.ID_DIV        ,
              DESCR          = vRowPDV.DESCR         ,
              HOST_JOB_DATE  = vRowPDV.HOST_JOB_DATE ,
              HOST_ID        = vRowPDV.HOST_ID       ,
              ID_DIV_FK      = vRowPDV.ID_DIV_FK     ,
              HOST_STAT_ID   = vRowPDV.HOST_STAT_ID  ,
              DT_START       = vRowPDV.DT_START      ,
              DT_START_VS    = vRowPDV.DT_START_VS   ,
              DT_INITLOAD_VS = vRowPDV.DT_INITLOAD_VS,
              HOST_CAZI      = vRowPDV.HOST_CAZI     ,
              HOST_CFIL      = vRowPDV.HOST_CFIL
        WHERE ID_SHOP = vRowPDV.ID_SHOP;
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'CodPDV(Virtual) ROWS:'||SQL%ROWCOUNT , vPasso);
     END IF;
     --
     vPasso := 'CREATE_INFO_JOB';
     CREATE_INFO_JOB(
      PO_ERR_SEVERITA   => PO_ERR_SEVERITA,
      PO_ERR_CODICE     => PO_ERR_CODICE  ,
      PO_MESSAGGIO      => PO_MESSAGGIO   ,
      PO_JOB_INFO       => VJobInfo    
     );
     IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
     BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'ID_JOB:'||VJobInfo.ID_JOB, vPasso);
     VJobInfo.ROWS := 1;
     PO_JOB_ID := VJobInfo.ID_JOB;
     --
     vPasso := 'CREATE_FLOW';
     CREATE_SHOP(
      PO_ERR_SEVERITA   => PO_ERR_SEVERITA,
      PO_ERR_CODICE     => PO_ERR_CODICE  ,
      PO_MESSAGGIO      => PO_MESSAGGIO   ,
      P_SHOP_ROW        => vRowPDV        ,
      P_JOB_INFO        => VJobInfo
     );
     IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
     BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
     --
     vPasso := 'CREATE_FLOW';
     CREATE_FLOW(
      PO_ERR_SEVERITA   => PO_ERR_SEVERITA,
      PO_ERR_CODICE     => PO_ERR_CODICE  ,
      PO_MESSAGGIO      => PO_MESSAGGIO   ,
      P_NOME_TABELLA    => KTabellaSHOP   ,
      P_TAPPO           => 'S'            ,
      P_JOB_INFO        => VJobInfo
     );
     IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
     BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
     PO_PDV_VIRTUAL_COD := vCodPDVVirtual;
     --
  EXCEPTION
    WHEN erroreGestito THEN
       PO_ERR_SEVERITA := 'E';
       PO_ERR_CODICE   := SQLCODE;
       PO_MESSAGGIO    :=  'ERRORE - vPasso:'|| vPasso ||' - '||PO_MESSAGGIO;
       BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, PO_MESSAGGIO , vPasso);
       BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END CREATE_PDV_VIRTUAL;
  --
 /**
  * Description: Utility per aggiornare VS_VAT : Tabella dell'iva
  * MG_VS_VAT --> VS_VAT 
  * Author: R.Doti
  * Created: 04/12/2019
  * @param P_PDV_COD Codice del PDV Fisico
  */
  PROCEDURE FLOW_VS_VAT(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_PDV_COD          IN            VARCHAR2,
      PO_JOB_ID             OUT        NUMBER
   )
   IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_STORE_UTIL_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    vRowPDV             FRAME.MG_SHOP%ROWTYPE;
    vDataJob            TIMESTAMP := SYSTIMESTAMP;
    vContVirt           NUMBER;
    vJobInfo            tyJobInfo;
    vTabStore           FRAME.TAB_COD_CHAR;
    vIdGRP              FRAME.VS_DPL_GRP.DPL_GRP_ID%TYPE;
  BEGIN 
    --
    vProcName := 'FLOW_VS_VAT';
    --
    vPasso := 'CREATE_INFO_JOB';
    CREATE_INFO_JOB(
      PO_ERR_SEVERITA   => PO_ERR_SEVERITA,
      PO_ERR_CODICE     => PO_ERR_CODICE  ,
      PO_MESSAGGIO      => PO_MESSAGGIO   ,
      PO_JOB_INFO       => VJobInfo
     );
    IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'VJobInfo.ID_JOB:'||VJobInfo.ID_JOB, vPasso);
    --
    vPasso := 'TabStore';
    SELECT ID_SHOP
      BULK COLLECT INTO vTabStore
      FROM FRAME.MG_SHOP
     WHERE ID_SHOP = NVL(P_PDV_COD, ID_SHOP)
       AND ID_CMP_FK IS NOT NULL
       AND ID_DIV_FK IS NOT NULL;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'vTabStore(CNT):'||vTabStore.COUNT, vPasso);
    --
    VJobInfo.ROWS := vTabStore.COUNT;
    vPasso := 'CREATE_GRUPPOFLOW';
    CREATE_GRUPPOFLOW(
      PO_ERR_SEVERITA    => PO_ERR_SEVERITA,
      PO_ERR_CODICE      => PO_ERR_CODICE  ,
      PO_MESSAGGIO       => PO_MESSAGGIO   ,
      P_JOB_INFO         => VJobInfo,
      P_TAB_ENTITY       => vTabStore,
      PO_GPR_ID          => vIdGRP
    );
    IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PO_GPR_ID:'||vIdGRP, vPasso);
    vPasso := 'CREATE_FLOW';
    CREATE_FLOW(
      PO_ERR_SEVERITA   => PO_ERR_SEVERITA,
      PO_ERR_CODICE     => PO_ERR_CODICE  ,
      PO_MESSAGGIO      => PO_MESSAGGIO   ,
      P_NOME_TABELLA    => KTabellaVS_DPL_GRP,
      P_TAPPO           => NULL,
      P_JOB_INFO        => VJobInfo
     );
    IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
    --
    vPasso := 'VS_VAT';
    INSERT INTO FRAME.VS_VAT
           (ID_JOB, 
            ID_ROW,
            JOB_DATE, 
            JOB_TIME, 
            FLAG_ACT, 
            DPL_GRP_ID_FK, 
            STOREID, 
            ACTDATE, 
            CODE, 
            AMOUNT, 
            DESCR1, 
            DESCR2, 
            SPECIAL_VAT, 
            ALPHACODE, 
            RECEIPT_DESCR1, 
            RECEIPT_DESCR2
           )
    SELECT VJobInfo.ID_JOB,
           ROWNUM, 
           VJobInfo.JOB_DATE,
           VJobInfo.JOB_TIME,
           'I', --I:Inserimento M:Modifica D:Delete
           vIdGRP, 
           STOREID, 
           ACTDATE, 
           CODE, 
           AMOUNT, 
           DESCR1, 
           DESCR2, 
           SPECIAL_VAT, 
           ALPHACODE, 
           RECEIPT_DESCR1, 
           RECEIPT_DESCR2
      FROM FRAME.MG_VS_VAT;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'VS_VAT(CNT):'||SQL%ROWCOUNT, vPasso);
    --
    VJobInfo.ROWS := SQL%ROWCOUNT;
    --
    vPasso := 'CREATE_FLOW';
    CREATE_FLOW(
      PO_ERR_SEVERITA   => PO_ERR_SEVERITA,
      PO_ERR_CODICE     => PO_ERR_CODICE  ,
      PO_MESSAGGIO      => PO_MESSAGGIO   ,
      P_NOME_TABELLA    => KTabellaVS_VAT ,
      P_TAPPO           => 'SI',
      P_JOB_INFO        => VJobInfo
     );
    IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
    --
    PO_JOB_ID := VJobInfo.ID_JOB;
  --
  EXCEPTION
    WHEN erroreGestito THEN
       PO_ERR_SEVERITA := 'E';
       PO_ERR_CODICE   := SQLCODE;
       PO_MESSAGGIO    :=  'ERRORE - vPasso:'|| vPasso ||' - '||PO_MESSAGGIO;
       BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, PO_MESSAGGIO , vPasso);
       BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END FLOW_VS_VAT;
  --
  /**
  * Description: Utility per aggiornare VS_DEPT : Tabella contenente la classificazione merceologica.
  * MG_VS_DEPT --> VS_DEPT
  * Author: R.Doti
  * Created: 27/01/2020
  * @param P_PDV_COD Codice del PDV Fisico
  */
  PROCEDURE FLOW_VS_DEPT(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_PDV_COD          IN            VARCHAR2,
      PO_JOB_ID             OUT        NUMBER
   )
   IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_STORE_UTIL_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    vRowPDV             FRAME.MG_SHOP%ROWTYPE;
    vDataJob            TIMESTAMP := SYSTIMESTAMP;
    vContVirt           NUMBER;
    vJobInfo            tyJobInfo;
    vTabStore           FRAME.TAB_COD_CHAR;
    vIdGRP              FRAME.VS_DPL_GRP.DPL_GRP_ID%TYPE;
  BEGIN 
    --
    vProcName := 'FLOW_VS_DEPT';
    --
    vPasso := 'CREATE_INFO_JOB';
    CREATE_INFO_JOB(
      PO_ERR_SEVERITA   => PO_ERR_SEVERITA,
      PO_ERR_CODICE     => PO_ERR_CODICE  ,
      PO_MESSAGGIO      => PO_MESSAGGIO   ,
      PO_JOB_INFO       => VJobInfo
     );
    IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'VJobInfo.ID_JOB:'||VJobInfo.ID_JOB, vPasso);
    --
    vPasso := 'TabStore';
    SELECT ID_SHOP
      BULK COLLECT INTO vTabStore
      FROM FRAME.MG_SHOP
     WHERE ID_SHOP = NVL(P_PDV_COD, ID_SHOP)
       AND ID_CMP_FK IS NOT NULL
       AND ID_DIV_FK IS NOT NULL;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'vTabStore(CNT):'||vTabStore.COUNT, vPasso);
    --
    VJobInfo.ROWS := vTabStore.COUNT;
    vPasso := 'CREATE_GRUPPOFLOW';
    CREATE_GRUPPOFLOW(
      PO_ERR_SEVERITA    => PO_ERR_SEVERITA,
      PO_ERR_CODICE      => PO_ERR_CODICE  ,
      PO_MESSAGGIO       => PO_MESSAGGIO   ,
      P_JOB_INFO         => VJobInfo,
      P_TAB_ENTITY       => vTabStore,
      PO_GPR_ID          => vIdGRP
    );
    IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PO_GPR_ID:'||vIdGRP, vPasso);
    vPasso := 'CREATE_FLOW';
    CREATE_FLOW(
      PO_ERR_SEVERITA   => PO_ERR_SEVERITA,
      PO_ERR_CODICE     => PO_ERR_CODICE  ,
      PO_MESSAGGIO      => PO_MESSAGGIO   ,
      P_NOME_TABELLA    => KTabellaVS_DPL_GRP,
      P_TAPPO           => NULL,
      P_JOB_INFO        => VJobInfo
     );
    IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
    --
    vPasso := 'VS_DEPT';
    INSERT INTO FRAME.VS_DEPT
           (ID_JOB, 
            ID_ROW, 
            JOB_DATE, 
            JOB_TIME, 
            FLAG_ACT, 
            DPL_GRP_ID_FK, 
            STOREID, 
            NUMB, 
            ADMNUMB, 
            DEPOT, 
            DESCR1DEPT, 
            DESCR2DEPT, 
            VATCODE, 
            SQM, 
            RSQM, 
            "LEVEL", 
            CLASS, 
            DISCCUST, 
            DISCEMPL, 
            FILTER1, 
            FILTER2, 
            FILTER3, 
            FILTER4, 
            DEPT_IS_FOOD_FLAG,
            ACC_ALL_DISC
           )
    SELECT VJobInfo.ID_JOB,
           ROWNUM, 
           VJobInfo.JOB_DATE,
           VJobInfo.JOB_TIME,
           'N', --N:Nuovo, I:Inserimento M:Modifica D:Delete
           vIdGRP, 
           STOREID,
           NUMB,
           ADMNUMB,
           DEPOT,
           DESCR1DEPT,
           DESCR2DEPT,
           VATCODE,
           SQM,
           RSQM,
           "LEVEL",
           CLASS,
           DISCCUST,
           DISCEMPL,
           FILTER1,
           FILTER2,
           FILTER3,
           FILTER4,
           DEPT_IS_FOOD_FLAG,
           ACC_ALL_DISC
      FROM FRAME.MG_VS_DEPT
     WHERE HOST_GUID = (SELECT MAX (HOST_GUID)
                          FROM FRAME.MG_VS_DEPT 
                       GROUP BY HOST_JOB_DATE);
    --
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'VS_DEPT(CNT):'||SQL%ROWCOUNT, vPasso);
    --
    VJobInfo.ROWS := SQL%ROWCOUNT;
    --
    vPasso := 'CREATE_FLOW';
    CREATE_FLOW(
      PO_ERR_SEVERITA   => PO_ERR_SEVERITA,
      PO_ERR_CODICE     => PO_ERR_CODICE  ,
      PO_MESSAGGIO      => PO_MESSAGGIO   ,
      P_NOME_TABELLA    => KTabellaVS_DEPT ,
      P_TAPPO           => 'SI',
      P_JOB_INFO        => VJobInfo
     );
    IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
    --
    PO_JOB_ID := VJobInfo.ID_JOB;
  --
  EXCEPTION
    WHEN erroreGestito THEN
       PO_ERR_SEVERITA := 'E';
       PO_ERR_CODICE   := SQLCODE;
       PO_MESSAGGIO    :=  'ERRORE - vPasso:'|| vPasso ||' - '||PO_MESSAGGIO;
       BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, PO_MESSAGGIO , vPasso);
       BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END FLOW_VS_DEPT;
  --
  /**
  * Description: Utility per aggiornare VS_ITEM : Tabella contenente l'anagrafica articoli.
  * MG_VS_ITEM --> VS_ITEM
  * Author: R.Doti
  * Created: 27/01/2020
  * @param P_PDV_COD Codice del PDV Fisico
  */
  PROCEDURE FLOW_VS_ITEM(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_PDV_COD          IN            VARCHAR2,
      PO_JOB_ID             OUT        NUMBER
   )
   IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_STORE_UTIL_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    vRowPDV             FRAME.MG_SHOP%ROWTYPE;
    vDataJob            TIMESTAMP := SYSTIMESTAMP;
    vContVirt           NUMBER;
    vJobInfo            tyJobInfo;
    vTabStore           FRAME.TAB_COD_CHAR;
    vIdGRP              FRAME.VS_DPL_GRP.DPL_GRP_ID%TYPE;
    vPDVFisico          NUMBER := NULL;
    vJobHost            NUMBER := NULL;
  BEGIN 
    --
    vProcName := 'FLOW_VS_ITEM';
    --
    vPasso := 'CREATE_INFO_JOB';
    CREATE_INFO_JOB(
      PO_ERR_SEVERITA   => PO_ERR_SEVERITA,
      PO_ERR_CODICE     => PO_ERR_CODICE  ,
      PO_MESSAGGIO      => PO_MESSAGGIO   ,
      PO_JOB_INFO       => VJobInfo
     );
    IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'VJobInfo.ID_JOB:'||VJobInfo.ID_JOB, vPasso);
    --
    vPasso := 'TabStore';
    SELECT ID_SHOP
      BULK COLLECT INTO vTabStore
      FROM FRAME.MG_SHOP
     WHERE ID_SHOP = NVL(P_PDV_COD, ID_SHOP)
       AND ID_CMP_FK IS NOT NULL
       AND ID_DIV_FK IS NOT NULL;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'vTabStore(CNT):'||vTabStore.COUNT, vPasso);
    --
    VJobInfo.ROWS := vTabStore.COUNT;
    vPasso := 'CREATE_GRUPPOFLOW';
    CREATE_GRUPPOFLOW(
      PO_ERR_SEVERITA    => PO_ERR_SEVERITA,
      PO_ERR_CODICE      => PO_ERR_CODICE  ,
      PO_MESSAGGIO       => PO_MESSAGGIO   ,
      P_JOB_INFO         => VJobInfo,
      P_TAB_ENTITY       => vTabStore,
      PO_GPR_ID          => vIdGRP
    );
    IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PO_GPR_ID:'||vIdGRP, vPasso);
    vPasso := 'CREATE_FLOW';
    CREATE_FLOW(
      PO_ERR_SEVERITA   => PO_ERR_SEVERITA,
      PO_ERR_CODICE     => PO_ERR_CODICE  ,
      PO_MESSAGGIO      => PO_MESSAGGIO   ,
      P_NOME_TABELLA    => KTabellaVS_DPL_GRP,
      P_TAPPO           => NULL,
      P_JOB_INFO        => VJobInfo
     );
    IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
    --
    vPasso := 'GET_PDV_FISICO';
    vPDVFisico := GET_PDV_FISICO(
      P_PDV_VIRTUAL_COD => P_PDV_COD
    );
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PDV_FISICO_COD:'||vPDVFisico, vPasso);
    --
    vPasso := 'GET_HOST_JOB';
    SELECT PRC.HOST_D1NJOB
      INTO vJobHost
      FROM FRAME.MG_VS_VARPRICE PRC
     WHERE HOST_JOB_DATE = (SELECT MAX(HOST_JOB_DATE)
                              FROM FRAME.MG_VS_VARPRICE
                             WHERE HOST_SHOP_ID = vPDVFisico)
       AND ROWNUM = 1;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'HOST_D1NJOB:'||vJobHost, vPasso);
    --
    IF vJobHost IS NOT NULL AND vPDVFisico IS NOT NULL THEN
    --
      vPasso := 'INSERT_MG_VS_VARPRICE';
      MERGE INTO FRAME.MG_VS_VARPRICE A
           USING (SELECT /*+ use_nl(PRC ) index(PRC) */
                         STOREID,
                         INTCODE,
                         ACTIVEDATE,
                         VALIDDATETO,
                         PEC,
                         "TYPE",
                         PRICEAMOUNT, 
                         HOST_ACT,
                         HOST_D1NJOB,
                         HOST_D1ANNO,
                         HOST_D1NELA,
                         HOST_GUID,
                         HOST_JOB_DATE,
                         VJobInfo.ID_JOB FRAME_ID_JOB,
                         FRAME_ID_ROW,
                         FRAME_STATUS,
                         FRAME_FLAG_ACT,
                         HOST_COMPUTER,
                         HOST_USER,
                         kNomePackage || '.' || vProcName HOST_PROGRAM,
                         P_PDV_COD HOST_SHOP_ID,
                         HOST_CAZI,
                         HOST_CFIL,
                         FRAME_SYNC,
                         ITEMISFOOD
                    FROM FRAME.MG_VS_VARPRICE PRC
                   WHERE PRC.HOST_D1NJOB = vJobHost
                     AND PRC.HOST_SHOP_ID = vPDVFisico
                     AND PRC.INTCODE = '100123'
                     ) B
                      ON (    A.STOREID = B.STOREID
                          AND A.INTCODE = B.INTCODE
                          AND A.ACTIVEDATE = B.ACTIVEDATE
                          AND A.HOST_SHOP_ID = B.HOST_SHOP_ID
                          AND A.VALIDDATETO = B.VALIDDATETO)
            WHEN NOT MATCHED THEN
              INSERT (STOREID,
                      INTCODE,
                      ACTIVEDATE,
                      VALIDDATETO,
                      PEC,
                      TYPE,
                      PRICEAMOUNT,
                      HOST_ACT,
                      HOST_D1NJOB,
                      HOST_D1ANNO,
                      HOST_D1NELA,
                      HOST_GUID,
                      HOST_JOB_DATE,
                      FRAME_ID_JOB,
                      FRAME_ID_ROW,
                      FRAME_STATUS,
                      FRAME_FLAG_ACT,
                      HOST_COMPUTER,
                      HOST_USER,
                      HOST_PROGRAM,
                      HOST_SHOP_ID,
                      HOST_CAZI,
                      HOST_CFIL,
                      FRAME_SYNC,
                      ITEMISFOOD)
              VALUES (B.STOREID,
                      B.INTCODE,
                      B.ACTIVEDATE,
                      B.VALIDDATETO,
                      B.PEC,
                      B.TYPE,
                      B.PRICEAMOUNT,
                      B.HOST_ACT,
                      B.HOST_D1NJOB,
                      B.HOST_D1ANNO,
                      B.HOST_D1NELA,
                      B.HOST_GUID,
                      B.HOST_JOB_DATE,
                      B.FRAME_ID_JOB,
                      B.FRAME_ID_ROW,
                      B.FRAME_STATUS,
                      B.FRAME_FLAG_ACT,
                      B.HOST_COMPUTER,
                      B.HOST_USER,
                      B.HOST_PROGRAM,
                      B.HOST_SHOP_ID,
                      B.HOST_CAZI,
                      B.HOST_CFIL,
                      B.FRAME_SYNC,
                      B.ITEMISFOOD) 
            WHEN MATCHED THEN
              UPDATE SET A.PEC = B.PEC,
                         A.TYPE = B.TYPE,
                         A.PRICEAMOUNT = B.PRICEAMOUNT,
                         A.HOST_ACT = B.HOST_ACT,
                         A.HOST_D1NJOB = B.HOST_D1NJOB,
                         A.HOST_D1ANNO = B.HOST_D1ANNO,
                         A.HOST_D1NELA = B.HOST_D1NELA,
                         A.HOST_GUID = B.HOST_GUID,
                         A.HOST_JOB_DATE = B.HOST_JOB_DATE,
                         A.FRAME_ID_JOB = B.FRAME_ID_JOB,
                         A.FRAME_ID_ROW = B.FRAME_ID_ROW,
                         A.FRAME_STATUS = B.FRAME_STATUS,
                         A.FRAME_FLAG_ACT = B.FRAME_FLAG_ACT,
                         A.HOST_COMPUTER = B.HOST_COMPUTER,
                         A.HOST_USER = B.HOST_USER,
                         A.HOST_PROGRAM = B.HOST_PROGRAM,
                         A.HOST_CAZI = B.HOST_CAZI,
                         A.HOST_CFIL = B.HOST_CFIL,
                         A.FRAME_SYNC = B.FRAME_SYNC,
                         A.ITEMISFOOD = B.ITEMISFOOD;
      --
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'ROWS:'||SQL%ROWCOUNT, vPasso);
      --Estraggo solo articoli non soggetti a peso
      FOR rec IN (SELECT /*+
                            USE_NL(ASR) INDEX(ASR)
                            USE_NL(ART) INDEX(ART)
                          */
                         ASR.E3CART
                    FROM MG.GEAD030F ASR, 
                         MG.GEAD060F BCOD
                   WHERE ASR.E3CPDV = P_PDV_COD
                     AND ASR.E3FSIT = kFlagS
                     AND TRIM (ASR.E3STAT) IS NULL
                      --
                     AND ASR.E3CART = BCOD.E6CART
                     AND TRIM (BCOD.E6STAT) IS NULL
                     AND BCOD.E6BCOD NOT LIKE KBarcodeBilan --
                     --AND ASR.E3CART = '517347'
                     AND ASR.E3CART = '100123'
                 )
      LOOP
      --
        vPasso := 'NEW_PRICE';
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'Art:'||rec.E3CART, vPasso);
        UPDATE /*+ INDEX(PRC FRAME.MG_VS_VARPRICE_PK) */ FRAME.MG_VS_VARPRICE PRC
           SET PRICEAMOUNT = GET_NEW_PRICE( P_ART_COD=> rec.E3CART, P_PDV_COD => P_PDV_COD , P_PRICE_OLD  => PRICEAMOUNT) 
         WHERE STOREID = 1
           AND INTCODE = TO_CHAR(rec.E3CART)
           AND ACTIVEDATE = '2000-01-01'
           AND HOST_SHOP_ID = P_PDV_COD
           AND VALIDDATETO  = '2099-12-31'
          -- AND FRAME_ID_JOB = VJobInfo.ID_JOB 
           ;
      --
      END LOOP;
    --
    END IF;
    --
    IF vJobHost IS NOT NULL AND vPDVFisico IS NOT NULL THEN
    --
      vPasso := 'INS_VS_ITEM';
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'ID_JOB:'||VJobInfo.ID_JOB, vPasso);
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'JOB_DATE:'||VJobInfo.JOB_DATE, vPasso);
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'JOB_TIME:'||VJobInfo.JOB_TIME, vPasso);
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'vIdGRP:'||vIdGRP, vPasso);
      vPasso := 'VS_ITEM';
      INSERT INTO FRAME.VS_ITEM
             (ID_JOB, ID_ROW, JOB_DATE, JOB_TIME, FLAG_ACT, DPL_GRP_ID_FK, 
              STOREID, INTCODE, DEPTNUMB, ITEMTYPE, ADDITEMTYPE, 
              ALPHA_CODE, SUPPLIER_CODE, CELIAC, 
              ACC_ALL_DISC, SERVICE_FLAG2, LINKITEM, 
              DESCR1ITM,EXTDESCR1ITM, DESCR2ITM, 
              EXTDESCR2, PKG, UNITMEASURE1, UNITMEASURE2, VATITM, TARE, 
              EXPDATE, WEIGHT, REQQTY, NEGATIVE, DEPOSIT, DISCCUST, 
              DISCEMPL, NOTDISPLAY, ITEMPERISH, LABELTYPE, PACKAGING, 
              CODE, NOTACT, NOTQTY, DESCDISP, ITEMISFOOD, PRICEOVR, 
              GSM_VOUCHER_FLAG, KIT_FLAG, LOCK_KIT_FLAG, KIT_ITM_NBR, 
              GIFT_CARD_FLAG, SERVICE_FLAG, GIFT_NO_AMT, GIFT_WITH_AMT, "MEMBER", 
              REGISTER_NOT_SELL, SCO_NOT_SELL, PAYSTATION_NOT_SELL, 
              MOBILE_NOT_SELL, CALL_OPERATOR, NO_WGHT_VALIDATION, SCO_NO_WEIGHT, SCO_QUANTITY_MANDATORY, 
              HOSTPRICE, 
              LOCALPRICE, EXPLOCPRICE, CODPRICE, 
              SUGGEST, EP, FP, SECONDHAND_PRICE, MINIMUM_PRICE, 
              SHELFLABEL1, SHELFLABEL2, SHELFLABEL3, 
              PACKAGE1, PACKAGE2, MG_THEMA, MG_PRODUCT, 
              SERVICE, SELF, PREPACKED, 
              PROMOFILTER1, PROMOFILTER2, PROMOFILTER3, PROMOFILTER4, 
              AGELIMITID, ADD_DESCR1, ADD_DESCR2, CONSUMEUNTIL, "ROWS", 
              LABELFORMAT, PLUGROUP, GROUPDESCR, PLUREGROUP, REGROUPDESCR, 
              DATATYPE, SELLUNTIL, PACKINGDATE, 
              LINE1, LINE2, LINE3, LINE4, LINE5, LINE6, LINE7, LINE8, 
              ECOTAX, SCALELABELFORMAT, OPERIMAGESTART, OPERIMAGECOUNT, CUSTIMAGESTART, 
              CUSTIMAGECOUNT, BRAND_NUMBER, COLOR_ID, SIZE_ID, 
              REGISTER_MSG_ID, SCO_MSG_ID, PAYSTATION_MSG_ID, MOBILE_MSG_ID
             )
      SELECT VJobInfo.ID_JOB, ROWNUM, VJobInfo.JOB_DATE, VJobInfo.JOB_TIME, 'N', vIdGRP, 
             ITM.STOREID,PRC.INTCODE, ITM.DEPTNUMB, ITM.ITEMTYPE, ITM.ADDITEMTYPE, 
             ITM.ALPHA_CODE, ITM.SUPPLIER_CODE, ITM.CELIAC,
             ITM.ACC_ALL_DISC, ITM.SERVICE_FLAG2, ITM.LINKITEM, 
             ITM.DESCR1ITM, ITM.EXTDESCR1ITM, ITM.DESCR2ITM, 
             ITM.EXTDESCR2, ITM.PKG, ITM.UNITMEASURE1, ITM.UNITMEASURE2, ITM.VATITM, ITM.TARE, 
             ITM.EXPDATE, ITM.WEIGHT, ITM.REQQTY, ITM.NEGATIVE, ITM.DEPOSIT, ITM.DISCCUST, 
             ITM.DISCEMPL, ITM.NOTDISPLAY, ITM.ITEMPERISH, ITM.LABELTYPE, ITM.PACKAGING, 
             ITM.CODE, ITM.NOTACT, ITM.NOTQTY, ITM.DESCDISP, ITM.ITEMISFOOD, ITM.PRICEOVR, 
             ITM.GSM_VOUCHER_FLAG, ITM.KIT_FLAG, ITM.LOCK_KIT_FLAG, ITM.KIT_ITM_NBR, 
             ITM.GIFT_CARD_FLAG, ITM.SERVICE_FLAG, ITM.GIFT_NO_AMT, ITM.GIFT_WITH_AMT, ITM.MEMBER, 
             ITM.REGISTER_NOT_SELL, ITM.SCO_NOT_SELL, ITM.PAYSTATION_NOT_SELL, 
             ITM.MOBILE_NOT_SELL, ITM.CALL_OPERATOR, ITM.NO_WGHT_VALIDATION, ITM.SCO_NO_WEIGHT, ITM.SCO_QUANTITY_MANDATORY, 
             MG_STORE_UTIL_PCK.GET_NEW_PRICE(PRC.INTCODE, P_PDV_COD ,ITM.HOSTPRICE), 
             ITM.LOCALPRICE, ITM.EXPLOCPRICE, ITM.CODPRICE, 
             ITM.SUGGEST, ITM.EP, ITM.FP, ITM.SECONDHAND_PRICE, ITM.MINIMUM_PRICE, 
             ITM.SHELFLABEL1, ITM.SHELFLABEL2, ITM.SHELFLABEL3, 
             ITM.PACKAGE1, ITM.PACKAGE2, ITM.MG_THEMA, ITM.MG_PRODUCT, 
             ITM.SERVICE, ITM.SELF, ITM.PREPACKED, 
             ITM.PROMOFILTER1, ITM.PROMOFILTER2, ITM.PROMOFILTER3, ITM.PROMOFILTER4, 
             ITM.AGELIMITID, ITM.ADD_DESCR1, ITM.ADD_DESCR2, ITM.CONSUMEUNTIL, "ITM"."ROWS", 
             ITM.LABELFORMAT, ITM.PLUGROUP, ITM.GROUPDESCR, ITM.PLUREGROUP, ITM.REGROUPDESCR, 
             ITM.DATATYPE, ITM.SELLUNTIL, ITM.PACKINGDATE, 
             ITM.LINE1, ITM.LINE2, ITM.LINE3, ITM.LINE4, ITM.LINE5, ITM.LINE6, ITM.LINE7, ITM.LINE8, 
             ITM.ECOTAX, ITM.SCALELABELFORMAT, ITM.OPERIMAGESTART, ITM.OPERIMAGECOUNT, ITM.CUSTIMAGESTART,
             ITM.CUSTIMAGECOUNT, ITM.BRAND_NUMBER, ITM.COLOR_ID, ITM.SIZE_ID, ITM.REGISTER_MSG_ID, 
             ITM.SCO_MSG_ID, ITM.PAYSTATION_MSG_ID, ITM.MOBILE_MSG_ID
       FROM FRAME.MG_VS_ITEM ITM,
            FRAME.MG_VS_VARPRICE PRC
      WHERE PRC.HOST_D1NJOB = vJobHost
        AND PRC.HOST_SHOP_ID = vPDVFisico
         --
        AND ITM.INTCODE = PRC.INTCODE
        AND ITM.INTCODE ='100123'
        ;
      --
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'VS_ITEM(CNT):'||SQL%ROWCOUNT, vPasso);
      --
      VJobInfo.ROWS := SQL%ROWCOUNT;
      --
      vPasso := 'CREATE_FLOW';
      CREATE_FLOW(
        PO_ERR_SEVERITA   => PO_ERR_SEVERITA,
        PO_ERR_CODICE     => PO_ERR_CODICE  ,
        PO_MESSAGGIO      => PO_MESSAGGIO   ,
        P_NOME_TABELLA    => KTabellaVS_ITEM ,
        P_TAPPO           => 'SI',
        P_JOB_INFO        => VJobInfo
       );
      IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
      --
    END IF;
    --
    PO_JOB_ID := VJobInfo.ID_JOB;
  --
  EXCEPTION
    WHEN erroreGestito THEN
       PO_ERR_SEVERITA := 'E';
       PO_ERR_CODICE   := SQLCODE;
       PO_MESSAGGIO    :=  'ERRORE - vPasso:'|| vPasso ||' - '||PO_MESSAGGIO;
       BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, PO_MESSAGGIO , vPasso);
       BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END FLOW_VS_ITEM;
  --
 /**
  * Description: Utility per aggiornare VS_XREF : Tabella contenente l'anagrafica dei barcode.
  * MG_VS_XREF --> VS_XREF 
  * Author: R.Doti
  * Created: 27/01/2020
  * @param P_PDV_COD Codice del PDV Virtuale
  */
  PROCEDURE FLOW_VS_XREF(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_PDV_COD          IN            VARCHAR2,
      PO_JOB_ID             OUT        NUMBER
   )
   IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_STORE_UTIL_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    vRowPDV             FRAME.MG_SHOP%ROWTYPE;
    vDataJob            TIMESTAMP := SYSTIMESTAMP;
    vContVirt           NUMBER;
    vJobInfo            tyJobInfo;
    vTabStore           FRAME.TAB_COD_CHAR;
    vIdGRP              FRAME.VS_DPL_GRP.DPL_GRP_ID%TYPE;
    vPDVFisico          NUMBER := NULL;
    vJobHost            NUMBER := NULL;
  BEGIN 
    --
    vProcName := 'FLOW_VS_XREF';
    --
    vPasso := 'CREATE_INFO_JOB';
    CREATE_INFO_JOB(
      PO_ERR_SEVERITA   => PO_ERR_SEVERITA,
      PO_ERR_CODICE     => PO_ERR_CODICE  ,
      PO_MESSAGGIO      => PO_MESSAGGIO   ,
      PO_JOB_INFO       => VJobInfo
     );
    IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'VJobInfo.ID_JOB:'||VJobInfo.ID_JOB, vPasso);
    --
    vPasso := 'TabStore';
    SELECT ID_SHOP
      BULK COLLECT INTO vTabStore
      FROM FRAME.MG_SHOP
     WHERE ID_SHOP = NVL(P_PDV_COD, ID_SHOP)
       AND ID_CMP_FK IS NOT NULL
       AND ID_DIV_FK IS NOT NULL;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'vTabStore(CNT):'||vTabStore.COUNT, vPasso);
    --
    VJobInfo.ROWS := vTabStore.COUNT;
    vPasso := 'CREATE_GRUPPOFLOW';
    CREATE_GRUPPOFLOW(
      PO_ERR_SEVERITA    => PO_ERR_SEVERITA,
      PO_ERR_CODICE      => PO_ERR_CODICE  ,
      PO_MESSAGGIO       => PO_MESSAGGIO   ,
      P_JOB_INFO         => VJobInfo,
      P_TAB_ENTITY       => vTabStore,
      PO_GPR_ID          => vIdGRP
    );
    IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PO_GPR_ID:'||vIdGRP, vPasso);
    vPasso := 'CREATE_FLOW';
    CREATE_FLOW(
      PO_ERR_SEVERITA   => PO_ERR_SEVERITA,
      PO_ERR_CODICE     => PO_ERR_CODICE  ,
      PO_MESSAGGIO      => PO_MESSAGGIO   ,
      P_NOME_TABELLA    => KTabellaVS_DPL_GRP,
      P_TAPPO           => NULL,
      P_JOB_INFO        => VJobInfo
     );
    IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
    --
    vPasso := 'GET_PDV_FISICO';
    vPDVFisico := GET_PDV_FISICO(
      P_PDV_VIRTUAL_COD => P_PDV_COD
    );
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PDV_FISICO_COD:'||vPDVFisico, vPasso);
    --
    vPasso := 'GET_HOST_JOB';
    SELECT PRC.HOST_D1NJOB
      INTO vJobHost
      FROM FRAME.MG_VS_VARPRICE PRC
     WHERE HOST_JOB_DATE = (SELECT MAX(HOST_JOB_DATE)
                              FROM FRAME.MG_VS_VARPRICE
                             WHERE HOST_SHOP_ID = vPDVFisico)
       AND ROWNUM = 1;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'HOST_D1NJOB:'||vJobHost, vPasso);
    --
    IF vJobHost IS NOT NULL AND vPDVFisico IS NOT NULL THEN
    --
      vPasso := 'VS_XREF';
      INSERT INTO FRAME.VS_XREF (ID_JOB,
                                 ID_ROW,
                                 JOB_DATE,
                                 JOB_TIME,
                                 FLAG_ACT,
                                 DPL_GRP_ID_FK,
                                 STOREID,
                                 BARCODE,
                                 PACK_TYPE,
                                 TRAYQTY,
                                 INTCODE,
                                 SCALE,
                                 EAN_TYPE,
                                 PACK_QTY,
                                 PRICE,
                                 LINKITEMID)
      SELECT VJobInfo.ID_JOB,
             ROWNUM, 
             VJobInfo.JOB_DATE,
             VJobInfo.JOB_TIME,
             'N', --N:Inserimento M:Modifica D:Delete
            vIdGRP, 
            XRF.STOREID,
            BARCODE,
            PACK_TYPE,
            TRAYQTY,
            XRF.INTCODE,
            SCALE,
            EAN_TYPE,
            PACK_QTY,
            PRICE,
            LINKITEMID
       FROM FRAME.MG_VS_XREF XRF,
            FRAME.MG_VS_VARPRICE PRC
      WHERE PRC.HOST_D1NJOB = vJobHost
        AND PRC.HOST_SHOP_ID = vPDVFisico
        --
        AND XRF.INTCODE = PRC.INTCODE;
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'VS_XREF(CNT):'||SQL%ROWCOUNT, vPasso);
      --
      VJobInfo.ROWS := SQL%ROWCOUNT;
      --
      vPasso := 'CREATE_FLOW';
      CREATE_FLOW(
        PO_ERR_SEVERITA   => PO_ERR_SEVERITA,
        PO_ERR_CODICE     => PO_ERR_CODICE  ,
        PO_MESSAGGIO      => PO_MESSAGGIO   ,
        P_NOME_TABELLA    => KTabellaVS_XREF,
        P_TAPPO           => 'SI',
        P_JOB_INFO        => VJobInfo
       );
      IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
    --
    END IF;
    PO_JOB_ID := VJobInfo.ID_JOB;
  --
  EXCEPTION
    WHEN erroreGestito THEN
       PO_ERR_SEVERITA := 'E';
       PO_ERR_CODICE   := SQLCODE;
       PO_MESSAGGIO    :=  'ERRORE - vPasso:'|| vPasso ||' - '||PO_MESSAGGIO;
       BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, PO_MESSAGGIO , vPasso);
       BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END FLOW_VS_XREF;
  --
 /**
  * Description: Utility per aggiornare VS_VARPRICE : Tabella contenente le variazioni prezzo.
  * MG_VS_VARPRICE --> VS_VARPRICE 
  * Author: R.Doti
  * Created: 27/01/2020
  * @param P_PDV_COD Codice del PDV Virtuale
  */
  PROCEDURE FLOW_VS_VARPRICE(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_PDV_COD          IN            VARCHAR2,
      PO_JOB_ID             OUT        NUMBER
  )
   IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_STORE_UTIL_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    vRowPDV             FRAME.MG_SHOP%ROWTYPE;
    vDataJob            TIMESTAMP := SYSTIMESTAMP;
    vContVirt           NUMBER;
    vJobInfo            tyJobInfo;
    vTabStore           FRAME.TAB_COD_CHAR;
    vIdGRP              FRAME.VS_DPL_GRP.DPL_GRP_ID%TYPE;
    vPDVFisico          NUMBER := NULL;
    vJobHost            NUMBER := NULL;
  BEGIN 
    --
    vProcName := 'FLOW_VS_XREF';
    --
    vPasso := 'CREATE_INFO_JOB';
  EXCEPTION
    WHEN erroreGestito THEN
       PO_ERR_SEVERITA := 'E';
       PO_ERR_CODICE   := SQLCODE;
       PO_MESSAGGIO    :=  'ERRORE - vPasso:'|| vPasso ||' - '||PO_MESSAGGIO;
       BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, PO_MESSAGGIO , vPasso);
       BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END FLOW_VS_VARPRICE;
  --
  /**
  * Description: Utility per aggiornare PROMO_IMPT : Tabella contenente le promozioni.
  * MG_PROMO_IMPT --> PROMO_IMPT 
  * Author: R.Doti
  * Created: 27/01/2020
  * @param P_PDV_COD Codice del PDV Virtuale
  */
  PROCEDURE FLOW_PROMO_IMPT(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_PDV_COD          IN            VARCHAR2,
      P_TIPO_PROMO       IN            VARCHAR2, --ALL/BIL
      PO_JOB_ID             OUT        NUMBER
   )
   IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_STORE_UTIL_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    vRowPDV             FRAME.MG_SHOP%ROWTYPE;
    vDataJob            TIMESTAMP := SYSTIMESTAMP;
    vContVirt           NUMBER;
    vJobInfo            tyJobInfo;
    vTabStore           FRAME.TAB_COD_CHAR;
    vIdGRP              FRAME.VS_DPL_GRP.DPL_GRP_ID%TYPE;
    vPDVFisico          NUMBER := NULL;
    vJobHost            NUMBER := NULL;
  BEGIN 
    --
    vProcName := 'FLOW_PROMO_IMPT';
    --
    vPasso := 'INPUT';
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'P_TIPO_PROMO:'||P_TIPO_PROMO, vPasso);
    --
    vPasso := 'CREATE_INFO_JOB';
    CREATE_INFO_JOB(
      PO_ERR_SEVERITA   => PO_ERR_SEVERITA,
      PO_ERR_CODICE     => PO_ERR_CODICE  ,
      PO_MESSAGGIO      => PO_MESSAGGIO   ,
      PO_JOB_INFO       => VJobInfo
     );
    IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'VJobInfo.ID_JOB:'||VJobInfo.ID_JOB, vPasso);
    --
    vPasso := 'GET_PDV_FISICO';
    vPDVFisico := GET_PDV_FISICO(
      P_PDV_VIRTUAL_COD => P_PDV_COD
    );
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PDV_FISICO_COD:'||vPDVFisico, vPasso);
    --
    IF P_TIPO_PROMO = 'BIL' THEN
    --
      vPasso := 'INSRT_MG_PROMO_IMPT';
      INSERT INTO FRAME.MG_PROMO_IMPT(ID_DIV, ID_PROMO, PROMO_HOST,
                                      ROW_NO, ID_PROMOH, PROMO_TYPE,
                                      OBJ_TYPE, OBJ_CODE,
                                      VALUE, ID_GRP, ID_SHOP,
                                      DT_START, DT_END,
                                      DT_STARTL, DT_ENDL,
                                      PRC, FLG_STATE,
                                      ID_ACM_FK, ID_LOY_ACTION_FK, FLG_ACTION, 
                                      DESCR, DESCR_BILL,
                                      HOST_ACT, 
                                      HOST_D1NJOB, HOST_D1ANNO, HOST_D1NELA,
                                      HOST_GUID, HOST_JOB_DATE,
                                      HOST_GUID_ROW, FRAME_ID_JOB,
                                      FRAME_STATUS, FRAME_FLAG_ACT,
                                      FRAME_ID_ROW, PROMO_TYPE_HOST,
                                      FRAME_SYNC)
                               SELECT KDivisioneVirtual, ID_PROMO, PROMO_HOST,
                                      ROW_NO, ID_PROMOH, PROMO_TYPE,
                                      OBJ_TYPE, OBJ_CODE, 
                                      VALUE, ID_GRP, P_PDV_COD,
                                      DT_START, DT_END,
                                      DT_STARTL, DT_ENDL,
                                      PRC, FLG_STATE,
                                      ID_ACM_FK, ID_LOY_ACTION_FK, FLG_ACTION,
                                      DESCR, DESCR_BILL,
                                      HOST_ACT,
                                      HOST_D1NJOB, HOST_D1ANNO, HOST_D1NELA,
                                      HOST_GUID, HOST_JOB_DATE,
                                      HOST_GUID_ROW, VJobInfo.ID_JOB,
                                      '1' FRAME_STATUS, FRAME_FLAG_ACT,
                                      ROWNUM FRAME_ID_ROW, PROMO_TYPE_HOST,
                                      FRAME_SYNC
                                 FROM FRAME.MG_PROMO_IMPT PRM
                                WHERE ID_SHOP = LPAD(vPDVFisico, 5, '0')
                                  AND FLG_STATE = '0'
                                  AND TRUNC(DT_END) >= TRUNC (SYSDATE)
                                  --
                                  AND EXISTS (SELECT 0
                                                FROM MG.GEAD060F BCOD
                                               WHERE BCOD.E6CART = PRM.OBJ_CODE
                                                 AND TRIM (BCOD.E6STAT) IS NULL
                                                 AND BCOD.E6BCOD LIKE KBarcodeBilan )
                                  AND NOT EXISTS(SELECT 0
                                                   FROM FRAME.MG_PROMO_IMPT PRM_VRT
                                                  WHERE PRM_VRT.ID_SHOP = P_PDV_COD
                                                    AND PRM_VRT.OBJ_CODE = PRM.OBJ_CODE
                                                    AND PRM_VRT.ID_PROMO = PRM.ID_PROMO
                                                    AND PRM_VRT.DT_END >= TRUNC(SYSDATE)
                                                    --AND PRM_VRT.FRAME_STATUS = 0
                                                    );
      --
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'ROWS:'||SQL%ROWCOUNT, vPasso);
      VJobInfo.ROWS := SQL%ROWCOUNT;
    ELSE
    --
      vPasso := 'INSRT_MG_PROMO_IMPT';
      INSERT INTO FRAME.MG_PROMO_IMPT(ID_DIV, ID_PROMO, PROMO_HOST,
                                      ROW_NO, ID_PROMOH, PROMO_TYPE,
                                      OBJ_TYPE, OBJ_CODE,
                                      VALUE, ID_GRP, ID_SHOP,
                                      DT_START, DT_END,
                                      DT_STARTL, DT_ENDL,
                                      PRC, FLG_STATE,
                                      ID_ACM_FK, ID_LOY_ACTION_FK, FLG_ACTION, 
                                      DESCR, DESCR_BILL,
                                      HOST_ACT, 
                                      HOST_D1NJOB, HOST_D1ANNO, HOST_D1NELA,
                                      HOST_GUID, HOST_JOB_DATE,
                                      HOST_GUID_ROW, FRAME_ID_JOB,
                                      FRAME_STATUS, FRAME_FLAG_ACT,
                                      FRAME_ID_ROW, PROMO_TYPE_HOST,
                                      FRAME_SYNC)
                               SELECT KDivisioneVirtual, ID_PROMO, PROMO_HOST,
                                      ROW_NO, ID_PROMOH, PROMO_TYPE,
                                      OBJ_TYPE, OBJ_CODE, 
                                      VALUE, ID_GRP, P_PDV_COD,
                                      DT_START, DT_END,
                                      DT_STARTL, DT_ENDL,
                                      PRC, FLG_STATE,
                                      ID_ACM_FK, ID_LOY_ACTION_FK, FLG_ACTION,
                                      DESCR, DESCR_BILL,
                                      HOST_ACT,
                                      HOST_D1NJOB, HOST_D1ANNO, HOST_D1NELA,
                                      HOST_GUID, HOST_JOB_DATE,
                                      HOST_GUID_ROW, VJobInfo.ID_JOB,
                                      '1' FRAME_STATUS, FRAME_FLAG_ACT,
                                      ROWNUM FRAME_ID_ROW, PROMO_TYPE_HOST,
                                      FRAME_SYNC
                                 FROM FRAME.MG_PROMO_IMPT PRM
                                WHERE ID_SHOP = LPAD(vPDVFisico, 5, '0')
                                  AND FLG_STATE = '0'
                                  AND TRUNC(DT_END) >= TRUNC (SYSDATE)
                                  --
                                  AND NOT EXISTS(SELECT 0
                                                   FROM FRAME.MG_PROMO_IMPT PRM_VRT
                                                  WHERE PRM_VRT.ID_SHOP = P_PDV_COD
                                                    AND PRM_VRT.OBJ_CODE = PRM.OBJ_CODE
                                                    AND PRM_VRT.ID_PROMO = PRM.ID_PROMO
                                                    AND PRM_VRT.DT_END >= TRUNC(SYSDATE)
                                                    --AND PRM_VRT.FRAME_STATUS = 0
                                                    );
      --
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'ROWS:'||SQL%ROWCOUNT, vPasso);
      VJobInfo.ROWS := SQL%ROWCOUNT;
    END IF;
    --
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, ' VJobInfo.ROWS:'|| VJobInfo.ROWS, vPasso);
    IF VJobInfo.ROWS > 0 THEN
      --
      vPasso := 'INS_PROMO_IMP';
      INSERT INTO FRAME.PROMO_IMPT(ID_JOB, ID_ROW, JOB_DATE, JOB_TIME, 
                                   FLG_ACT, ID_DIV, ID_PROMO, PROMO_HOST, 
                                   ROW_NO, ID_PROMOH, PROMO_TYPE, OBJ_TYPE, 
                                   OBJ_CODE, VALUE, ID_GRP, ID_SHOP, 
                                   DT_START, DT_END, DT_STARTL, DT_ENDL, 
                                   PRC, FLG_STATE, ID_ACM_FK, 
                                   ID_LOY_ACTION_FK, FLG_ACTION, 
                                   DESCR, DESCR_BILL, COUP_CODE)
      SELECT VJobInfo.ID_JOB, ROWNUM,  VJobInfo.JOB_DATE, VJobInfo.JOB_TIME,
             'N' FLG_ACT, ID_DIV, ID_PROMO, PROMO_HOST,
             ROW_NO, ID_PROMOH, PROMO_TYPE, OBJ_TYPE, 
             OBJ_CODE, VALUE, ID_GRP, ID_SHOP, 
             DT_START, DT_END, DT_STARTL, DT_ENDL, 
             PRC, FLG_STATE, ID_ACM_FK, 
             ID_LOY_ACTION_FK, FLG_ACTION, 
             DESCR, DESCR_BILL, NULL COUP_CODE
        FROM FRAME.MG_PROMO_IMPT
       WHERE FRAME_ID_JOB = VJobInfo.ID_JOB;
      --
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'ROWS:'||SQL%ROWCOUNT, vPasso);
      --
      vPasso := 'CREATE_FLOW';
      CREATE_FLOW(
        PO_ERR_SEVERITA   => PO_ERR_SEVERITA,
        PO_ERR_CODICE     => PO_ERR_CODICE  ,
        PO_MESSAGGIO      => PO_MESSAGGIO   ,
        P_NOME_TABELLA    => KTabellaPROMO_IMPT,
        P_TAPPO           => 'SI',
        P_JOB_INFO        => VJobInfo
       );
      IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
    END IF;
    --
    PO_JOB_ID := VJobInfo.ID_JOB;
  --
  EXCEPTION
    WHEN erroreGestito THEN
       PO_ERR_SEVERITA := 'E';
       PO_ERR_CODICE   := SQLCODE;
       PO_MESSAGGIO    :=  'ERRORE - vPasso:'|| vPasso ||' - '||PO_MESSAGGIO;
       BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, PO_MESSAGGIO , vPasso);
       BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END FLOW_PROMO_IMPT;
  --
  FUNCTION GET_PRICE_PROMO(
    P_ART_COD  VARCHAR2,
    P_PDV_COD  VARCHAR2,
    P_PREZZO   NUMBER
  ) RETURN NUMBER
  IS
    vProcName VARCHAR2(30);
    vPasso    VARCHAR2(100);
    vPrice    NUMBER;
    vPercentuale    NUMBER;
  BEGIN
  --
    vProcName := 'GET_PRICE';
    --
    vPasso := 'GetPromo';
    SELECT CASE WIZ.KCRM_ID_PROMO_WIZARD
                WHEN '2000' THEN PROMO.VALUE 
                WHEN '4001' THEN PROMO.VALUE 
                WHEN '2003' THEN PROMO.VALUE 
                WHEN '1014' THEN PROMO.VALUE 
           ELSE NULL
           END PREZZO,
           CASE WIZ.KCRM_ID_PROMO_WIZARD
                WHEN '3001' THEN PROMO.VALUE 
                WHEN '3018' THEN PROMO.VALUE 
                WHEN '4018' THEN PROMO.VALUE 
                --WHEN '5000' THEN PROMO.VALUE 
                WHEN '4001' THEN PROMO.VALUE
                WHEN '5025' THEN PROMO.VALUE
                WHEN '5030' THEN PROMO.VALUE
           ELSE NULL
           END PERCENTUALE
      INTO vPrice, vPercentuale
      FROM FRAME.PROMO_IMPT PROMO,
           FRAME.MG_SPROMO_WIZARD WIZ
     WHERE PROMO.ID_SHOP = P_PDV_COD
       AND PROMO.OBJ_CODE = P_PDV_COD
       AND SYSDATE BETWEEN DT_START AND DT_END
       AND WIZ.KCRM_ID_PROMO_WIZARD= PROMO.PROMO_TYPE;
      --
      IF vPercentuale IS NOT NULL THEN
        vPrice := P_PREZZO + P_PREZZO/100*vPercentuale;
      END IF;
    --
    RETURN vPrice;
    --
  EXCEPTION
    WHEN OTHERS THEN
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, SQLERRM,BASE_LOG_PCK.Errore);
      RETURN 0;
  END GET_PRICE_PROMO;
  --
  /**
   * Description: La funzione restituisce il prezzo attuale dell'articolo sul PDV.
   *
   * Author: R.Doti
   * Created: 17/12/2019
   *
   * Param: P_ART_COD Codice articolo
   * Param :P_PDV_COD Codice punto vendita
   * Return: prezzo
   */
  FUNCTION GET_PRICE(
    P_ART_COD  VARCHAR2,
    P_PDV_COD  VARCHAR2
  ) RETURN NUMBER
  IS
    vProcName VARCHAR2(30);
    vPasso    VARCHAR2(100);
    vPrice    NUMBER;
  BEGIN
  --
    vProcName := 'GET_PRICE';
    --
    BEGIN
      vPasso := 'CheckPescheria';
      SELECT DPES_ART_PREZZO
        INTO vPrice
        FROM FRAME.FRM_DDT_PESCHERIA
       WHERE TO_NUMBER(TRIM(DPES_PDV_COD_EXT)) = P_PDV_COD
         AND TRIM(DPES_ART_COD_EXT) = P_ART_COD
         AND DPES_DATA_MOD = (SELECT MAX(DPES_DATA_MOD)
                                FROM FRAME.FRM_DDT_PESCHERIA B
                               WHERE TO_NUMBER(TRIM(B.DPES_PDV_COD_EXT)) = P_PDV_COD
                                 AND TRIM(B.DPES_ART_COD_EXT) = P_ART_COD)
         AND ROWNUM <= 1;
         BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PREZZO(PESCHERIA):'||vPrice , vPasso);
    EXCEPTION 
      WHEN NO_DATA_FOUND THEN
      --
        vPasso := 'CheckVsVarPrice';
        WITH MAX_DATE
          AS (SELECT GRP.ID_ENT_FK, PRZ.INTCODE, MAX (ACTIVEDATE) ACTIVEDATE
                FROM FRAME.VS_VARPRICE PRZ, FRAME.VS_DPL_GRP GRP
               WHERE PRZ.DPL_GRP_ID_FK = GRP.DPL_GRP_ID
                 AND GRP.ID_ENT_FK = P_PDV_COD
                 AND PRZ.INTCODE = P_ART_COD 
            GROUP BY GRP.ID_ENT_FK, PRZ.INTCODE)
        SELECT PRZ.PRICEAMOUNT/100
          INTO vPrice
          FROM FRAME.VS_VARPRICE PRZ, FRAME.VS_DPL_GRP GRP, MAX_DATE
         WHERE PRZ.DPL_GRP_ID_FK = GRP.DPL_GRP_ID
           AND PRZ.ACTIVEDATE = MAX_DATE.ACTIVEDATE
           AND PRZ.INTCODE = MAX_DATE.INTCODE
           AND GRP.ID_ENT_FK = MAX_DATE.ID_ENT_FK
           AND GRP.ID_ENT_FK = P_PDV_COD
           AND PRZ.INTCODE = P_ART_COD
           AND ROWNUM <= 1;
           BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PREZZO:'||vPrice , vPasso);
    END;
    --
    RETURN vPrice;
    --
    
  EXCEPTION
    WHEN OTHERS THEN
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, SQLERRM,BASE_LOG_PCK.Errore);
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PREZZO(ERR):'||vPrice , vPasso);
      RETURN NULL;
  END GET_PRICE;
  --
  /**
   * Description: La funzione restituisce TRUE se si tratta di articolo a peso variabile, FALSE altrimenti.
   *              Se il barcode inizia per 2
   * Author: R.Doti
   * Created: 17/12/2019
   *
   * Param: P_ART_COD Codice articolo
   * Return: TRUE: se si tratta di articolo a peso variabile
   */
  FUNCTION GET_ART_PESO_VAR(
    P_ART_COD  VARCHAR2
  ) RETURN VARCHAR2
  IS
    vProcName VARCHAR2(30);
    vPasso    VARCHAR2(100);
    vCnt      NUMBER;
  BEGIN
  --
    vProcName := 'GET_ART_PESO_VAR';
    --
      SELECT COUNT(0)
        INTO vCnt
        FROM MG.GEAD060F BCOD
       WHERE BCOD.E6CART = P_ART_COD
         AND BCOD.E6BCOD LIKE KBarcodeBilan
         AND ROWNUM <=1;
    --
    IF vCnt > 0 THEN
      RETURN kFlagS;
    ELSE
      RETURN kFlagN;
    END IF;
    --
  EXCEPTION
    WHEN OTHERS THEN
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, SQLERRM,BASE_LOG_PCK.Errore);
      RETURN kFlagN;
  END GET_ART_PESO_VAR;
  --
  /**
   * Description: La funzione restituisce TRUE se l'articolo è in assortimento su uno dei servizi attivi sul negozio fisico.
   * Author: R.Doti
   * Created: 17/12/2019
   *
   * Param: P_ART_COD Codice articolo
   * Return: TRUE: se l'articolo è in assortimento
   */
  FUNCTION GET_ART_VIRTUAL_SERVICE(
    P_ART_COD   VARCHAR2,
    P_PDV_COD   VARCHAR2
  ) RETURN VARCHAR2
  IS
    vProcName VARCHAR2(30);
    vPasso    VARCHAR2(100);
    vCnt      NUMBER;
  BEGIN
  --
    vProcName := 'GET_ART_VIRTUAL_SERVICE';
    --
   SELECT /*+ INDEX(ASRSV) USE_NL(ASRSV) */
          COUNT(0)
     INTO vCnt
     FROM MG_ASSORTIMENTO_SERVIZIO
    WHERE ASRSV_PDV_COD = P_PDV_COD
      AND ASRSV_ART_COD = P_ART_COD
      AND ASRSV_IN_ASS_AUT = kFlagS
      AND ASRSV_VIS_SITO_ANA = kFlagS
      AND ASRSV_VIS_SITO_PDV = kFlagS;
    --
    IF vCnt > 0 THEN
      RETURN kFlagS;
    ELSE
      RETURN kFlagN;
    END IF;
    --
  EXCEPTION
    WHEN OTHERS THEN
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, SQLERRM,BASE_LOG_PCK.Errore);
      RETURN kFlagN;
  END GET_ART_VIRTUAL_SERVICE;
  --
  /**
   * Description: La funzione effettua l'arrotondamento Bancker'Rounding
   *  
   * Author: R.Doti
   * Created: 28/11/2019
   *
   * Param: P_PREZZO prezzo
   * Param: P_RND_DIGIT diagnostico
   */
  FUNCTION BANCKER_ROUND(P_PREZZO IN NUMBER, P_RND_DIGIT NUMBER := 0) 
    RETURN NUMBER
  IS
    v_rnd_digit  number;
    v_remainder  number;
  BEGIN
    v_rnd_digit := trunc(P_RND_DIGIT);

    v_remainder := (P_PREZZO - trunc(P_PREZZO,v_rnd_digit)) * power(10,v_rnd_digit + 1);

    if Abs(v_remainder) < 5
    or (Abs(v_remainder) = 5 and mod(trunc(P_PREZZO * power(10,v_rnd_digit)),2) = 0) then
        return trunc(P_PREZZO,v_rnd_digit);
    else
        return round(P_PREZZO,v_rnd_digit);
    end if;
  --
  END BANCKER_ROUND;
  --
  /**
   * Description: La funzione restituisce il nuovo prezzo per PDV Virtuale.
   *
   * Author: R.Doti
   * Created: 30/01/2020
   *
   * Param: P_ART_COD Codice articolo
   * Param :P_PDV_COD Codice punto vendita
   * Return: prezzo
   */
  FUNCTION GET_NEW_PRICE(
    P_ART_COD    VARCHAR2,
    P_PDV_COD    VARCHAR2,
    P_PRICE_OLD  NUMBER DEFAULT NULL
  ) RETURN NUMBER PARALLEL_ENABLE
   IS
    vProcName   VARCHAR2(30);
    vPasso      VARCHAR2(100);
    vPrice      NUMBER;
    vPerc       NUMBER := NULL;
    vPDVFisico  NUMBER;
    vRep        GEAD010F.E1CREP%TYPE;
    vUg         GEAD010F.E1CUGE%TYPE;
    vRagg       GEAD010F.E1CRAG%TYPE;
    vFam        GEAD010F.E1CFAM%TYPE;
    vFound      BOOLEAN;
    vArtPesoVar VARCHAR2(5) := NULL;
    vArtCC      VARCHAR2(5) := NULL;
  BEGIN
  --
    --Gli articoli bilancia non vanno aumentati...(verificare esistenza PLU)
    vProcName := 'GET_PRICE';
    --
    vPasso := 'GET_PDV_FISICO';
    vPDVFisico := GET_PDV_FISICO( P_PDV_VIRTUAL_COD => P_PDV_COD);
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PDV_FISICO:'||vPDVFisico , vPasso);
    --
    IF P_PRICE_OLD IS NULL THEN
      vPasso := 'GET_PRICE';
      vPrice  := GET_PRICE(P_ART_COD  => P_ART_COD, P_PDV_COD  => LPAD(vPDVFisico,5,'0'));
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PREZZO(PDV_FISICO):'||vPrice , vPasso);
    ELSE
    vPrice := P_PRICE_OLD;
    END IF;
    --
    vPAsso := 'GET_ART_VIRTUAL_SERVICE';
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'P_PDV_COD:'||vPDVFisico , vPasso);
    vArtCC := GET_ART_VIRTUAL_SERVICE(P_ART_COD => P_ART_COD, P_PDV_COD => vPDVFisico);
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'Presente:'||vArtCC , vPasso);
    --
    IF vArtCC = kFlagS THEN
    --
      vPasso := 'GET_PESO_VAR';
      vArtPesoVar := GET_ART_PESO_VAR(P_ART_COD => P_ART_COD);
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PesoVariabile:'||vArtPesoVar , vPasso);
      --
      IF vArtPesoVar = kFlagN THEN
      --
        vPasso := 'GET_ART_INFO';
        SELECT E1CREP, E1CUGE, E1CRAG, E1CFAM 
          INTO vRep,  vUg, vRagg, vFam 
          FROM MG.GEAD010F
         WHERE E1CART = P_ART_COD
           AND TRIM(E1STAT) IS NULL;
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'Reparto:'||vRep , vPasso);
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'UG:'||vUg , vPasso);
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'Raggruppamento:'||vRagg , vPasso);
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'Famiglia:'||vFam , vPasso);
        --
        FOR i IN 1..4 LOOP
        --
          vPasso := 'GET_PERC';
          EXIT WHEN vPerc IS NOT NULL;
          --
          BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'Step:'||i , vPasso);
          BEGIN
          --
            SELECT LSC_PERC_AUM
              INTO vPerc
              FROM MG.MG_LISTINI_CAC LST
             WHERE LST.LSC_PDV_COD = vPDVFisico
               AND LST.LSC_STAT IS NULL
               AND (DECODE(i, 1, LSC_REPARTO||LSC_UG||LSC_RAGG||LSC_FAM) = DECODE(i, 1, vRep||vUg||vRagg||vFam)
                    OR
                    (DECODE(i, 2, LSC_REPARTO||LSC_UG||LSC_RAGG) = DECODE(i, 2, vRep||vUg||vRagg)
                     AND LSC_FAM IS NULL
                    )
                    OR
                    (DECODE(i, 3, LSC_REPARTO||LSC_UG) = DECODE(i, 3, vRep||vUg)
                     AND LSC_RAGG IS NULL
                     AND LSC_FAM IS NULL
                    )
                    OR
                    (DECODE(i, 4, LSC_REPARTO) = DECODE(i, 4, vRep)
                     AND LSC_UG IS NULL
                     AND LSC_RAGG IS NULL
                     AND LSC_FAM IS NULL
                    )
                   );
               --
            BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'Percentuale:'||vPerc , vPasso);
          EXCEPTION 
            WHEN NO_DATA_FOUND THEN vPerc := NULL;
          END;
          --
        END LOOP;
        --
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'Percentuale:'||vPerc , vPasso);
        --
        vPasso := 'SET_PERC';
        vPrice := BANCKER_ROUND(vPrice + vPrice/100*NVL(vPerc,0),2);
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'Prezzo:'||vPrice , vPasso);
      END IF;
    END IF;
    --
    RETURN vPrice;
   --
  EXCEPTION
    WHEN OTHERS THEN
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'ERR:'||SQLERRM , vPasso);
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, SQLERRM,BASE_LOG_PCK.Errore);
      RETURN 0;
  END GET_NEW_PRICE;
  --
    /**
   * Description: La procedura genera articoli coupon.
   *
   * Author: R.Doti
   * Created: 30/01/2020
   *
   * Param :P_PDV_COD Codice punto vendita
   * Param :P_SERVIZIO Codice punto vendita
   * Return: prezzo
   */
  PROCEDURE CREATE_ARTICOLO_CP(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_TIPO_COUPON      IN            VARCHAR2, --2-Bruciabile/1-Mass Market
      P_DESCRIZIONE      IN            VARCHAR2, --Descrizione del Coupon
      PO_ART_COD            OUT        VARCHAR2
   )
   IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_STORE_UTIL_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    vRowMG_VS_ITEM      FRAME.MG_VS_ITEM%ROWTYPE;
    vRowPDV             FRAME.MG_SHOP%ROWTYPE;
    vDataJob            TIMESTAMP := SYSTIMESTAMP;
    vContVirt           NUMBER;
    vJobInfo            tyJobInfo;
    vTabStore           FRAME.TAB_COD_CHAR;
    vIdGRP              FRAME.VS_DPL_GRP.DPL_GRP_ID%TYPE;
    vPDVFisico          NUMBER := NULL;
    vJobHost            NUMBER := NULL;
  BEGIN 
    --
    vProcName := 'FLOW_VS_ITEM';
    --
    vPasso := 'MAKE_ROW_ITEM';
    vRowMG_VS_ITEM.STOREID  := 1;
    vRowMG_VS_ITEM.INTCODE  := MG_ART_COD_CP_SEQ.nextval;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'INTCODE:'||vRowMG_VS_ITEM.INTCODE, vPasso);
    --
    vRowMG_VS_ITEM.DEPTNUMB      := 910912011;
    vRowMG_VS_ITEM.ITEMTYPE      := 0;
    vRowMG_VS_ITEM.ADDITEMTYPE   := 0;
    vRowMG_VS_ITEM.ALPHA_CODE	   := LPAD(vRowMG_VS_ITEM.INTCODE, 14, '0');
    vRowMG_VS_ITEM.SUPPLIER_CODE := 55095; --MG
    vRowMG_VS_ITEM.CELIAC        := 0;
    vRowMG_VS_ITEM.ACC_ALL_DISC  := 1;
    vRowMG_VS_ITEM.SERVICE_FLAG2 := P_TIPO_COUPON;
    vRowMG_VS_ITEM.LINKITEM      := '';
    vRowMG_VS_ITEM.DESCR1ITM     := P_DESCRIZIONE;
    vRowMG_VS_ITEM.EXTDESCR1ITM  := P_DESCRIZIONE;
    vRowMG_VS_ITEM.DESCR2ITM     := P_DESCRIZIONE;
    vRowMG_VS_ITEM.EXTDESCR2     := P_DESCRIZIONE;

    
    --1) censimento su MG_VS_ITEM
    --2) censimento su MG_VS_VARPRICE
    --3) censimento su MG_VS_XREF
    --4) deploy su VS_ITEM con il numero di negozio associato
    --5) deploy su VS_XREF con il numero di negozio associato
    vPasso := 'GET_HOST_JOB';
    --SELECT PRC.HOST_D1NJOB
    --  INTO vJobHost
    --  FROM FRAME.MG_VS_VARPRICE PRC
    -- WHERE HOST_JOB_DATE = (SELECT MAX(HOST_JOB_DATE)
    --                          FROM FRAME.MG_VS_VARPRICE
    --                         WHERE HOST_SHOP_ID = vPDVFisico)
   --    AND ROWNUM = 1;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'HOST_D1NJOB:'||vJobHost, vPasso);
    --

    vPasso := 'CREATE_INFO_JOB';
    CREATE_INFO_JOB(
      PO_ERR_SEVERITA   => PO_ERR_SEVERITA,
      PO_ERR_CODICE     => PO_ERR_CODICE  ,
      PO_MESSAGGIO      => PO_MESSAGGIO   ,
      PO_JOB_INFO       => VJobInfo
     );
    IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'VJobInfo.ID_JOB:'||VJobInfo.ID_JOB, vPasso);
    --
    vPasso := 'TabStore';
    SELECT ID_SHOP
      BULK COLLECT INTO vTabStore
      FROM FRAME.MG_SHOP
     WHERE ID_CMP_FK IS NOT NULL
       AND ID_DIV_FK IS NOT NULL;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'vTabStore(CNT):'||vTabStore.COUNT, vPasso);
    --
    VJobInfo.ROWS := vTabStore.COUNT;
    vPasso := 'CREATE_GRUPPOFLOW';
    CREATE_GRUPPOFLOW(
      PO_ERR_SEVERITA    => PO_ERR_SEVERITA,
      PO_ERR_CODICE      => PO_ERR_CODICE  ,
      PO_MESSAGGIO       => PO_MESSAGGIO   ,
      P_JOB_INFO         => VJobInfo,
      P_TAB_ENTITY       => vTabStore,
      PO_GPR_ID          => vIdGRP
    );
    IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PO_GPR_ID:'||vIdGRP, vPasso);
    vPasso := 'CREATE_FLOW';
    CREATE_FLOW(
      PO_ERR_SEVERITA   => PO_ERR_SEVERITA,
      PO_ERR_CODICE     => PO_ERR_CODICE  ,
      PO_MESSAGGIO      => PO_MESSAGGIO   ,
      P_NOME_TABELLA    => KTabellaVS_DPL_GRP,
      P_TAPPO           => NULL,
      P_JOB_INFO        => VJobInfo
     );
    IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
  --
  EXCEPTION
    WHEN erroreGestito THEN
       PO_ERR_SEVERITA := 'E';
       PO_ERR_CODICE   := SQLCODE;
       PO_MESSAGGIO    :=  'ERRORE - vPasso:'|| vPasso ||' - '||PO_MESSAGGIO;
       BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, PO_MESSAGGIO , vPasso);
       BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END CREATE_ARTICOLO_CP;
  --
END MG_STORE_UTIL_PCK;
/
