CREATE OR REPLACE PACKAGE BODY MG."MG_CLICK_COLLECT_PCK" AS
  --
  /******************************************************************************
   NAME:       MG_CLICK_COLLECT_PCK
   PURPOSE:    Package per la gestione dei servizi di ClikCollect

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        03/04/2019           r.doti   1. Created this package.
   ******************************************************************************/
  --
  /**
  * Package di gestione servizi ClickCollect
  * @headcom
  */
  -- COSTANTI A LIVELLO PACKAGE
  kNomePackage       CONSTANT VARCHAR2(30) := 'MG_CLICK_COLLECT_PCK';
  --
  kFlagAnn           CONSTANT VARCHAR2(1)  := 'A';
  kFlagS             CONSTANT VARCHAR2(1)  := 'S';
  kFlagN             CONSTANT VARCHAR2(1)  := 'N';
  --
  exNotService EXCEPTION;
  PRAGMA EXCEPTION_INIT( exNotService, -20001 );
  --
  -- TIPI E VARIABILI GLOBALI
  --
  --
  --
  /* Funzione di lettura della costante kServiceDRIVE
  */
  FUNCTION GetKServiceDRIVE RETURN VARCHAR2
  IS BEGIN RETURN kServiceDRIVE; END GetKServiceDRIVE;
  --
  /* Funzione di lettura della costante kServiceCASA
  */
  FUNCTION GetKServiceCASA RETURN VARCHAR2
  IS BEGIN RETURN kServiceCASA; END GetKServiceCASA;
  --
  /* Funzione di lettura della costante kServiceSTORE
  */
  FUNCTION GetKServiceSTORE RETURN VARCHAR2
  IS BEGIN RETURN kServiceSTORE; END GetKServiceSTORE;
  --
  /* Funzione per determinare l'identificativo di servizio su un PDV reale
  */
  FUNCTION GET_ID_SERVICE(
    P_PDV_COD NUMBER,
    P_SERVICE VARCHAR2
  )
  RETURN NUMBER IS
    vOutServiceNum NUMBER;
    vAppServizeNum VARCHAR2(15);
    vPrefix        VARCHAR2(5);
  BEGIN

    CASE P_SERVICE
      WHEN kServiceDRIVE THEN vPrefix := '9';
      WHEN kServiceCASA  THEN vPrefix := '8';
      WHEN kServiceSTORE THEN vPrefix := '7';
      ELSE RAISE ErroreGestito;
    END CASE;
    vAppServizeNum := vPrefix || LPAD (TO_CHAR(P_PDV_COD) , 4, 0) ;

    vOutServiceNum := TO_NUMBER(vAppServizeNum);

    RETURN vOutServiceNum;
  EXCEPTION
    WHEN ErroreGestito THEN
      raise_application_error( -20001, 'Servizio :['||P_SERVICE ||'] non gestito' );
    WHEN OTHERS THEN
      RETURN 0;
  END GET_ID_SERVICE;
  --
  /* Funzione per determinare la presenza di dati aggiuntivi
  */
  FUNCTION GET_DATI_AGG(
    P_KEYAM    IN VARCHAR2,
    P_KEY01    IN VARCHAR2,
    P_KEY02    IN VARCHAR2
  )
  RETURN NUMBER IS
    vOutNum NUMBER := 0;
  BEGIN
  --
    SELECT COUNT (0)
      INTO vOutNum
      FROM DATIAGGIUNTIVI
     WHERE E7KYAM = P_KEYAM
       AND E7KY01 = P_KEY01
       AND E7KY02 = P_KEY02;
    IF vOutNum>0 THEN
      vOutNum := 1;
    END IF;
    RETURN vOutNum;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 0;
  END GET_DATI_AGG;
  --
    /* Funzione per determinare la presenza di dati aggiuntivi
  */
  FUNCTION GET_INFO_FOTO(
    P_ART_COD    IN NUMBER
  )
  RETURN NUMBER IS
    vOutNum NUMBER := 0;
  BEGIN
  --
    SELECT COUNT (0)
      INTO vOutNum
      FROM ARTICLES_IMAGES_SITO
     WHERE ARTCODE = P_ART_COD;
    IF vOutNum>0 THEN
      vOutNum := 1;
    END IF;
    RETURN vOutNum;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 0;
  END GET_INFO_FOTO;
  --
  FUNCTION GET_VIRTUAL_PDV(
    P_PDV_COD NUMBER,
    P_SERVICE VARCHAR2
  )
  RETURN NUMBER IS
    vOutPDV NUMBER;
  BEGIN

    SELECT PVSER_PDV_REL_PDV_ID
      INTO vOutPDV
      FROM MG_PDV_REL_SERVICE
     WHERE PVSER_PDV_COD = P_PDV_COD
       AND PVSER_SERVIZIO = P_SERVICE
       AND TRIM(PVSER_STAT) IS NULL
       AND PVSER_SERVIZIO IN (kServiceDRIVE, kServiceCASA, kServiceSTORE);

    RETURN vOutPDV;
  EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( -20001, 'Servizio :['||P_SERVICE ||'] non gestito' );
  END GET_VIRTUAL_PDV;
 --
 /* Funzione per determinare se un articolo ��� abilitato ad essere esposto sul sito
  */
  FUNCTION GET_FLAG_ARTICOLO(
    P_ART_COD     NUMBER,
    P_PDV_COD     NUMBER,
    P_SERVICE     VARCHAR2
  )
  RETURN VARCHAR2 IS
  --
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_CLICK_COLLECT_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    vOut                VARCHAR2(2) := kFlagN;
    vRep                GEAD010F.E1CREP%TYPE;
    vUg                 GEAD010F.E1CUGE%TYPE;
    vRagg               GEAD010F.E1CRAG%TYPE;
    vFam                GEAD010F.E1CFAM%TYPE;
    vUmPR               GEAD010F.E1UMPR%TYPE;
    vFound              BOOLEAN;
  BEGIN
  --
    vProcName:= 'GET_FLAG_ARTICOLO';
    --
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'P_SERVICE:'||P_SERVICE);
    --
    vPasso:='LVL_0_GEAD010F';
    SELECT E1SITO,
           E1CREP,
           E1CUGE,
           E1CRAG,
           E1CFAM,
           E1UMPR
      INTO vOut,
           vRep,
           vUg,
           vRagg,
           vFam,
           vUmPR
      FROM GEAD010F
     WHERE E1CART = P_ART_COD
       AND TRIM(E1STAT) IS NULL;
    --
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'Flag:'||vOut, vPasso);
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, '   -Reparto:'||vRep, vPasso);
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, '   -Ug:'||vUg, vPasso);
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, '   -Raggr:'||vRagg, vPasso);
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, '   -Fam:'||vFam, vPasso);
    --
    IF vOut = kFlagS THEN
    --
      vPasso:='LVL_1_GEAD030F';
      SELECT E3FSIT
        INTO vOut
        FROM GEAD030F
       WHERE E3CPDV = P_PDV_COD
         AND E3CART = P_ART_COD
         AND TRIM(E3STAT) IS NULL;
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'Flag:'||vOut, vPasso);
      --
      IF vOut = kFlagS THEN
      --
        vPasso:='LVL_1_ASRT_CAC';
        BEGIN
          SELECT CASE
                 WHEN ASRC_UNITA_VEND = 'PZ'
                      THEN ASRC_IN_ASS_AUT
                 WHEN ASRC_UNITA_VEND != 'PZ' AND
                      NVL(ASRC_DATI_AGG,kFlagN) != kFlagS
                      THEN kFlagN
                 ELSE ASRC_IN_ASS_AUT
                 END
            INTO vOut
            FROM MG_ASSORTIMENTO_CAC
           WHERE ASRC_ART_COD = P_ART_COD;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
           --
           IF vUmPR != 'PZ' THEN
             --Verifico se ci sono i dati aggiuntivi
             IF GET_DATI_AGG('ARTICOLI',P_ART_COD,'CLICK_COLLECT')>0 THEN
               vFound := TRUE;
             END IF;
           ELSE
             vFound := TRUE;
           END IF;
        END;
      END IF;
      --
      IF vOut = kFlagS AND P_SERVICE IS NOT NULL THEN
        --
        vOut := kFlagN;
        --
        vPasso := 'LVL_2_ASRDET(Art)';
        BEGIN
        --
          SELECT CASE
                   WHEN ASRSV_IN_ASSORTIMENTO = 'S' THEN 'S'
                   WHEN ASRSV_IN_ASSORTIMENTO IS NULL AND ASRSV_IN_ASS_AUT= 'S' THEN 'S'
                   ELSE 'N'
                 END FLAG_ASS
            INTO vOut
            FROM MG_ASSORTIMENTO_SERVIZIO
           WHERE ASRSV_PDV_COD = P_PDV_COD
             AND ASRSV_SERVIZIO = P_SERVICE
             AND ASRSV_ART_COD = P_ART_COD
             AND TRIM(ASRSV_STAT) IS NULL;
         --
           vFound := TRUE;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
           vFound := FALSE;
        END;
        --
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'Flag:'||vOut, vPasso);
        --
        IF NOT(vFound) THEN
        --
          BEGIN
          --
            vPasso := 'LVL_3_ASRSV(Fam)';
            SELECT CASE
                   WHEN ASRSV_IN_ASSORTIMENTO = 'S' THEN 'S'
                   WHEN ASRSV_IN_ASSORTIMENTO IS NULL AND ASRSV_IN_ASS_AUT= 'S' THEN 'S'
                   ELSE 'N'
                 END FLAG_ASS
              INTO vOut
              FROM MG_ASSORTIMENTO_SERVIZIO ASRSV
             WHERE ASRSV_PDV_COD = P_PDV_COD
               AND ASRSV_SERVIZIO = P_SERVICE
               AND ASRSV_REPARTO = vRep
               AND ASRSV_UG = vUg
               AND ASRSV_RAGG = vRagg
               AND NVL(ASRSV_FAM,0) = vFam
               AND TRIM(ASRSV_STAT) IS NULL;
             --
             vFound := TRUE;
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              vFound := FALSE;
          END;
          --
          BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'Flag:'||vOut, vPasso);
        --
        END IF;
        --

        --
        IF NOT(vFound) THEN
        --
          BEGIN
          --
            vPasso := 'LVL_4_ASRSV(Ragg)';
            SELECT CASE
                   WHEN ASRSV_IN_ASSORTIMENTO = 'S' THEN 'S'
                   WHEN ASRSV_IN_ASSORTIMENTO IS NULL AND ASRSV_IN_ASS_AUT= 'S' THEN 'S'
                   ELSE 'N'
                 END FLAG_ASS
              INTO vOut
              FROM MG_ASSORTIMENTO_SERVIZIO ASRSV
             WHERE ASRSV_PDV_COD = P_PDV_COD
               AND ASRSV_SERVIZIO = P_SERVICE
               AND ASRSV_REPARTO = vRep
               AND ASRSV_UG = vUg
               AND ASRSV_RAGG = vRagg
               AND ASRSV_FAM IS NULL
               AND TRIM(ASRSV_STAT) IS NULL;
             --
             vFound := TRUE;
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              vFound := FALSE;
          END;
          --
          BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'Flag:'||vOut, vPasso);
        END IF;
        --
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'Flag:'||vOut, vPasso);
        --
        IF NOT(vFound) THEN
        --
          BEGIN
          --
            vPasso := 'LVL_6_ASRSV(UG)';
            SELECT CASE
                   WHEN ASRSV_IN_ASSORTIMENTO = 'S' THEN 'S'
                   WHEN ASRSV_IN_ASSORTIMENTO IS NULL AND ASRSV_IN_ASS_AUT= 'S' THEN 'S'
                   ELSE 'N'
                 END FLAG_ASS
              INTO vOut
              FROM MG_ASSORTIMENTO_SERVIZIO ASRSV
             WHERE ASRSV_PDV_COD = P_PDV_COD
               AND ASRSV_SERVIZIO = P_SERVICE
               AND ASRSV_REPARTO = vRep
               AND ASRSV_UG = vUg
               AND ASRSV_RAGG IS NULL
               AND ASRSV_FAM IS NULL
               AND TRIM(ASRSV_STAT) IS NULL;
             --
             vFound := TRUE;
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              vFound := FALSE;
          END;
          --
          BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'Flag:'||vOut, vPasso);
        END IF;
        --
        IF NOT(vFound) THEN
        --
          BEGIN
          --
            vPasso := 'LVL_7_ASRSV(Rep)';
            SELECT CASE
                   WHEN ASRSV_IN_ASSORTIMENTO = 'S' THEN 'S'
                   WHEN ASRSV_IN_ASSORTIMENTO IS NULL AND ASRSV_IN_ASS_AUT= 'S' THEN 'S'
                   ELSE 'N'
                   END FLAG_ASS
              INTO vOut
              FROM MG_ASSORTIMENTO_SERVIZIO ASRSV
             WHERE ASRSV_PDV_COD = P_PDV_COD
               AND ASRSV_SERVIZIO = P_SERVICE
               AND ASRSV_REPARTO = vRep
               AND ASRSV_UG IS NULL
               AND ASRSV_RAGG IS NULL
               AND ASRSV_FAM IS NULL
               AND TRIM(ASRSV_STAT) IS NULL;
             --
             vFound := TRUE;
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              vFound := FALSE;
          END;
        --
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'Flag:'||vOut, vPasso);
        END IF;
        --
      END IF;
    --
    END IF;
    --
    vPasso:='Result';
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'Flag:'||vOut, vPasso);
    RETURN vOut;
  EXCEPTION
    WHEN ErroreGestito THEN
      NULL;
      RETURN vOut;
      --raise_application_error( -20001, 'Servizio :['||P_SERICE_NAME ||'] non gestito' );
    WHEN OTHERS THEN
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'ERRORE:'||SQLERRM , vPasso);
      RETURN kFlagN;
  END GET_FLAG_ARTICOLO;
  --
  PROCEDURE UPDT_GEAD030F(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_ART_COD          IN NUMBER,
      P_PDV_COD          IN NUMBER,
      P_SITO             IN VARCHAR2
   )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_CLICK_COLLECT_PCK';
    vProcName           VARCHAR2(100);
  BEGIN
  --
    vProcName := 'UPDT_GEAD030F';
    --
    MERGE INTO GEAD030F E3
      USING (SELECT P_ART_COD AS E3CART,
                    P_PDV_COD AS E3CPDV,
                    P_SITO AS E3FSIT
               FROM DUAL) S
      ON (E3.E3CPDV = S.E3CPDV AND
          E3.E3CART = S.E3CART)
      WHEN MATCHED THEN
        UPDATE  SET E3.E3FSIT = S.E3FSIT
      WHEN NOT MATCHED THEN
        INSERT(E3STAT,
               E3DTEX,
               E3CPDV,
               E3CART,
               E3FSIT)
        VALUES(NULL,
               TO_NUMBER(EXTRACT(YEAR FROM SYSDATE) ||
                LPAD(EXTRACT(MONTH FROM SYSDATE),2,0)||
                LPAD(EXTRACT(DAY FROM SYSDATE),2,0)),
               S.E3CPDV,
               S.E3CART,
               S.E3FSIT);
    --
  EXCEPTION
   WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := SQLERRM;
  END UPDT_GEAD030F;
  --
  PROCEDURE UPDT_PRZPV(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_ART_COD          IN NUMBER,
      P_PDV_COD          IN NUMBER,
      P_PREZZO           IN NUMBER
   )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_CLICK_COLLECT_PCK';
    vProcName           VARCHAR2(100);
    vPrezzoOld          MG_PREZZI_REL_PDV.PRZPV_PREZZO%TYPE;
  BEGIN
  --
    vProcName := 'UPDT_PRZPV';
    --
    BEGIN
    --
      SELECT PRZPV_PREZZO
        INTO vPrezzoOld 
        FROM(SELECT PRZPV_PREZZO
               FROM MG_PREZZI_REL_PDV
              WHERE PRZPV_ART_COD = P_ART_COD
                AND PRZPV_PDV_COD = P_PDV_COD
                AND LOCALTIMESTAMP BETWEEN PRZPV_DATA_INI AND PRZPV_DATA_FIN
              ORDER BY PRZPV_DATA_INI DESC)
        WHERE ROWNUM <= 1;
    --
    EXCEPTION WHEN NO_DATA_FOUND THEN
      vPrezzoOld := 0;
    END;
    --
    IF vPrezzoOld <> P_PREZZO THEN
    --
      UPDATE MG_PREZZI_REL_PDV
         SET PRZPV_STAT = 'A'
       WHERE PRZPV_ART_COD = P_ART_COD
         AND PRZPV_PDV_COD = P_PDV_COD
         AND PRZPV_STAT IS NULL;
      --
      INSERT INTO MG_PREZZI_REL_PDV(
               PRZPV_PREZZO_REL_PDV_ID,
               PRZPV_ART_COD,
               PRZPV_PDV_COD,
               PRZPV_PREZZO,
               PRZPV_DATA_INI)
        VALUES(MG_PRZPV_ID_SEQ.NEXTVAL,
               P_ART_COD,
               P_PDV_COD,
               P_PREZZO,
               LOCALTIMESTAMP);
    END IF;
    --
  EXCEPTION
   WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := SQLERRM;
  END UPDT_PRZPV;
  /**
  * CREATE_SERVICE(): Utility per la creazione di un servizio su un punto vendita reale
  *
  * @param PO_METADATo il metadato estratto
  */
  PROCEDURE CREATE_SERVICE(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_PDV_COD           IN     NUMBER,
      P_SERVICE          IN     VARCHAR2,
      P_DATA_INI         IN     DATE,
      PO_SERVICE_ID         OUT NUMBER
    )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_CLICK_COLLECT_PCK';
    vProcName           VARCHAR2(100);
    vPdv                GEAD020F.E2CPDV%TYPE;
    vPasso              VARCHAR2(100);
    vFlgCnt             NUMBER;
  BEGIN
    --
    vProcName := 'CREATE_SERVICE';
    --
    IF P_PDV_COD IS NULL THEN
      vPasso := 'P_PDV_COD';
      RAISE ErroreGestito;
    END IF;
    --
    IF P_SERVICE IS NULL THEN
      vPasso := 'P_SERVICE';
      RAISE ErroreGestito;
    END IF;
    --
    IF P_DATA_INI IS NULL THEN
      vPasso := 'P_DATA_INI';
      RAISE ErroreGestito;
    END IF;
    --
    IF P_SERVICE <> kServiceDRIVE AND
       P_SERVICE <> kServiceCASA  AND
       P_SERVICE <> kServiceSTORE
    THEN
      vPasso := 'SERVICE_NOT_FOUND';
      RAISE ErroreGestito;
    END IF;
    --
    vPasso := 'GET_PDV';
    BEGIN
    --
      SELECT E2CPDV
        INTO vPdv
        FROM GEAD020F
       WHERE E2CPDV = P_PDV_COD
         AND TRIM(E2STAT) IS NULL;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        RAISE ErroreGestito;
    END;
    --
    vPasso := 'GET_ID_SERVICE';
    PO_SERVICE_ID := GET_ID_SERVICE(P_PDV_COD   => vPdv,
                                    P_SERVICE   => P_SERVICE);
    --
    SELECT COUNT(0)
      INTO vFlgCnt
      FROM MG_PDV_REL_SERVICE
     WHERE PVSER_PDV_COD = vPdv
       AND PVSER_SERVIZIO = P_SERVICE
       AND PVSER_STAT IS NULL;
    --
    IF vFlgCnt>0 THEN
    --
      vPasso := 'SERVICE_ON';
      RAISE ErroreGestito;
    ELSE
    --
      vPasso := 'INS_SERVICE';
      INSERT INTO MG_PDV_REL_SERVICE(
        PVSER_PDV_REL_PDV_ID,
        PVSER_PDV_COD,
        PVSER_SERVIZIO,
        PVSER_DATA_INI)
      VALUES(PO_SERVICE_ID,
        vPdv,
        P_SERVICE,
        TRUNC(P_DATA_INI));
    END IF;
    --
  EXCEPTION
    WHEN ErroreGestito THEN
      --
      IF vPasso = 'SERVICE_ON' THEN
      --
        PO_ERR_SEVERITA := 'E';
        PO_ERR_CODICE   := 'MGCLIK004';
        PO_MESSAGGIO    := 'IL SERVIZIO ['||P_SERVICE||'] E'' GIA'' PRESENTE SUL PDV ['||P_PDV_COD||']';
      END IF;
      --
      IF vPasso = 'SERVICE_NOT_FOUND' THEN
      --
        PO_ERR_SEVERITA := 'E';
        PO_ERR_CODICE   := 'MGCLIK003';
        PO_MESSAGGIO    := 'IL SERVIZIO ['||P_SERVICE||'] NON E'' GESTITO';
      END IF;

      IF vPasso = 'P_PDV_COD'  OR
         vPasso = 'P_SERVICE' OR
         vPasso = 'P_DATA_INI'
      THEN
        PO_ERR_SEVERITA := 'E';
        PO_ERR_CODICE   := 'MGCLIK002';
        PO_MESSAGGIO    := 'INPUT ['||vPasso||'] OBBLIGATORIO';
      END IF;
      --
      IF vPasso = 'GET_PDV' THEN
      --
        PO_ERR_SEVERITA := 'E';
        PO_ERR_CODICE   := 'MGCLIK001';
        PO_MESSAGGIO    := 'PUNTO VENDITA NON PRESENTE IN ANAGRAFICA';
      END IF;
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := SQLERRM;
  END CREATE_SERVICE;
  --
PROCEDURE NORMALIZZA_PESCHERIA(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2
    )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_CLICK_COLLECT_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    timeStart           PLS_INTEGER;
    vCount              PLS_INTEGER;
  BEGIN
    --
    vProcName := 'NORMALIZZA_PESCHERIA';
    timeStart := dbms_utility.get_time;
    --
    vPasso := 'DPES_PDV_FORN';
    UPDATE FRAME.FRM_DDT_PESCHERIA DPES
       SET (DPES.DPES_PDV_COD, DPES.DPES_FORNITORE_COD)  =
              (SELECT /*+ USE_NL(T8) INDEX(T8) USE_NL(T9) INDEX(T9) */ 
                      LPAD (T9.T9CAZI, 2, '0') || LPAD (T9.T9CFIL, 2, '0'),T8.T8CFOR
                 FROM FRAME.MG_AS_CODFO00F T8, FRAME.MG_AS_CLIFO00F T9
                WHERE TRIM (T8.T8CFDK) = TRIM (DPES.DPES_FORNITORE_PIVA)
                  AND TRIM (T9.T9CFOR) = TRIM (T8.T8CFOR)
                  AND TRIM (T9CPVF) = TRIM (DPES.DPES_PDV_COD_EXT))
     WHERE TRIM (DPES.DPES_PDV_COD) IS NULL;
    --
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'ROWS:'||SQL%ROWCOUNT,vPasso);
    FRAME.BASE_UTIL_PCK.DO_COMMIT(P_PROC_NAME=> vProcName, P_PASSO => vPasso);
    --
    vPasso := 'DPES_PDV_ART';
    UPDATE FRAME.FRM_DDT_PESCHERIA DPES
       SET DPES.DPES_ART_COD  =
          (SELECT /*+ USE_NL(A9) INDEX(A9) 
                  */ 
                  DISTINCT A9.A9CART
             FROM FRAME.MG_AS_ARTFO00F A9
            WHERE A9.A9CFOR = DPES.DPES_FORNITORE_COD
              AND TRIM(A9CFES) = TRIM(DPES.DPES_ART_COD_EXT)
              AND ROWNUM<=1
              )
     WHERE DPES.DPES_FORNITORE_COD IS NOT NULL 
       AND TRIM (DPES.DPES_ART_COD) IS NULL;
    --
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'ROWS:'||SQL%ROWCOUNT,vPasso);
    FRAME.BASE_UTIL_PCK.DO_COMMIT(P_PROC_NAME=> vProcName, P_PASSO => vPasso);
    --
    vPasso := 'GEAD060F';
    --
    SELECT COUNT(0) INTO vCount FROM MG.GEAD060F_TEMP WHERE ROWNUM<=1;
    --
    IF vCount > 0 THEN
    --
      DELETE GEAD060F A
      WHERE NOT EXISTS (SELECT 0 FROM GEAD060F_TEMP B WHERE A.E6CART = B.E6CART AND A.E6BCOD = B.E6BCOD);
      --
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'ROWS:'||SQL%ROWCOUNT,vPasso);
      FRAME.BASE_UTIL_PCK.DO_COMMIT(P_PROC_NAME=> vProcName, P_PASSO => vPasso);
    END IF;
    --
    
    --
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'Excecution time:'||(dbms_utility.get_time - timeStart)/100 || ' seconds' , vPasso);
  --
  EXCEPTION
    WHEN ErroreGestito THEN
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PO_MESSAGGIO :'||PO_MESSAGGIO , vPasso);
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END NORMALIZZA_PESCHERIA;
  --
  /**
  * AGGIORNA_ASRT_AUT(): Utility per l'aggiornamento dell'
  * assortimento MG_ASSORTIMENTO_CAC generale. Si estraggono gli articoli
  * appartenenti all'assortimento 4B e 4M. Per ciascuno si verifica:
  * - ��� disponibile su un PDV con C_AND_C
  * - presenta una foto
  * - se ��� venduto a <> PZ si verificano gli attributi aggiuntivi
  *
  */
  PROCEDURE AGGIORNA_ASRT_AUT(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_PDV_COD          IN     NUMBER  DEFAULT NULL
    )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_CLICK_COLLECT_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    vIndex              NUMBER;
    vTotAsrt            NUMBER;
    timeStart           PLS_INTEGER;
    errorArt            EXCEPTION;
    vFlagS_N            VARCHAR2(5);
  BEGIN
    --
    vProcName := 'AGGIORNA_ASRT_AUT';
    timeStart := dbms_utility.get_time;
    --
    vPasso := 'TRUNCATE MG_ASRC';
    --
    vPasso := 'DEL_ASRC';
    DELETE MG_ASSORTIMENTO_CAC;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, '',vPasso);
    --
    vPasso := 'INSERT_ASRC';
    INSERT INTO MG_ASSORTIMENTO_CAC(ASRC_ASSORTIMENTO_CAC_ID, ASRC_ART_COD,
      ASRC_UNITA_VEND, ASRC_IN_ASS_AUT, ASRC_DATA_MOD, ASRC_STAT,ASRC_SITO_VIS)
    SELECT FRAME.BASE_UTIL_PCK.MG_GUID ASRC_ASSORTIMENTO_CAC_ID,
           E1.E1CART AS ASRC_ART_COD,
           E1.E1UMPR ASRC_UNITA_VEND,
           kFlagS AS ASRSV_IN_ASS_AUT,
           LOCALTIMESTAMP ASRSV_DATA_MOD,
           NULL ASRC_STAT,
           E1.E1SITO ASRC_SITO_VIS
      FROM GEAD010F E1
     WHERE (E1ASSO LIKE '%4B%' OR  E1ASSO LIKE '%4M%')
       AND TRIM(E1SPOT) IS NULL --Escludo gli spot
       AND TRIM(E1FESP) IS NULL --Escludo gli espositori
       AND TRIM (E1STAT) IS NULL
       AND EXISTS(SELECT 0
                    FROM MG_PDV_REL_SERVICE PVSER,
                         GEAD030F E3
                   WHERE E3CPDV = PVSER.PVSER_PDV_COD
                     AND E3CPDV = NVL(P_PDV_COD, E3CPDV)
                     AND E3CART = E1.E1CART
                     AND TRIM (E3STAT) IS NULL
                     AND PVSER.PVSER_SERVIZIO IN (kServiceDRIVE, kServiceCASA, kServiceSTORE)
                     );
    --
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'ROWS:'||SQL%ROWCOUNT,vPasso);
    --
    vPasso := 'UPDATE_ASRC_DATI_AGG';
    UPDATE MG_ASSORTIMENTO_CAC ASRC
       SET ASRC_DATI_AGG =  DECODE(GET_DATI_AGG('ARTICOLI',ASRC.ASRC_ART_COD,'CLICK_COLLECT'),0,'N','S')
     WHERE ASRC_UNITA_VEND != 'PZ';
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'ROWS:'||SQL%ROWCOUNT,vPasso);
    --
    vPasso := 'UPDATE_ASRC_FOTO';
    UPDATE MG_ASSORTIMENTO_CAC ASRC
       SET ASRC_FOTO = DECODE(GET_INFO_FOTO(ASRC.ASRC_ART_COD),0,'N','S');
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'ROWS:'||SQL%ROWCOUNT,vPasso);
    --FRAME.BASE_UTIL_PCK.DO_COMMIT(P_PROC_NAME=> vProcName, P_PASSO => vPasso);
    --
    --vPasso := 'DEL_ASRSV';
    --DELETE MG.MG_ASSORTIMENTO_SERVIZIO A
    -- WHERE NVL(ASRSV_PDV_COD, 1) = NVL(P_PDV_COD,NVL(ASRSV_PDV_COD, 1));
    ----
    --vPasso := 'INSRT_ASRSV';
    --INSERT INTO MG.MG_ASSORTIMENTO_SERVIZIO A
    --       (ASRSV_ASSORTIMENTO_SERVIZIO_ID,
    --        ASRSV_PDV_COD,
    --        ASRSV_SERVIZIO,
    --        ASRSV_ART_COD,
    --        ASRSV_IN_ASS_AUT,
    --        ASRSV_DATA_MOD,
    --        ASRSV_VIS_SITO_ANA,
    --        ASRSV_VIS_SITO_PDV)
    --SELECT  FRAME.BASE_UTIL_PCK.MG_GUID ASRSV_ASSORTIMENTO_SERVIZIO_ID,
    --        PVSER_PDV_COD ASRSV_PDV_COD,
    --        PVSERV.PVSER_SERVIZIO ASRSV_SERVIZIO,
    --        ASRC.ASRC_ART_COD ASRSV_ART_COD,
    --        ASRC.ASRC_IN_ASS_AUT ASRSV_IN_ASS_AUT,
    --        LOCALTIMESTAMP ASRSV_DATA_MOD,
    --        ASRC.ASRC_SITO_VIS   ASRSV_VIS_SITO_ANA,
    --        E3.E3FSIT ASRSV_VIS_SITO_PDV
    --    FROM MG_ASSORTIMENTO_CAC ASRC,
    --         MG_PDV_REL_SERVICE PVSERV,
    --         GEAD030F E3
    --   WHERE E3CPDV = NVL(P_PDV_COD,E3CPDV)
    --     AND E3CART = ASRC.ASRC_ART_COD
    --     AND PVSERV.PVSER_PDV_COD = E3.E3CPDV
    --     AND E3.E3FSIT = 'S'
    --     AND TRIM (E3STAT) IS NULL;
    vPasso := 'MRG_ASRSV';
    MERGE INTO MG.MG_ASSORTIMENTO_SERVIZIO A
     USING (SELECT FRAME.BASE_UTIL_PCK.MG_GUID ASRSV_ASSORTIMENTO_SERVIZIO_ID,
                   PVSER_PDV_COD ASRSV_PDV_COD,
                   PVSERV.PVSER_SERVIZIO ASRSV_SERVIZIO,
                   ASRC.ASRC_ART_COD ASRSV_ART_COD,
                   ASRC.ASRC_IN_ASS_AUT ASRSV_IN_ASS_AUT,
                   LOCALTIMESTAMP ASRSV_DATA_MOD,
                   ASRC.ASRC_SITO_VIS ASRSV_VIS_SITO_ANA,
                   E3.E3FSIT ASRSV_VIS_SITO_PDV,
                   NULL ASRSV_STAT
              FROM MG_ASSORTIMENTO_CAC ASRC,
                   MG_PDV_REL_SERVICE PVSERV,
                   GEAD030F E3
             WHERE     E3CPDV = NVL (P_PDV_COD, E3CPDV)
                   AND E3CART = ASRC.ASRC_ART_COD
                   AND PVSERV.PVSER_PDV_COD = E3.E3CPDV
                   --AND E3.E3FSIT = 'S'
                   AND TRIM (E3STAT) IS NULL
                   AND PVSERV.PVSER_SERVIZIO IN (kServiceDRIVE, kServiceCASA, kServiceSTORE)
                                      ) B
        ON (    A.ASRSV_PDV_COD = B.ASRSV_PDV_COD
            AND A.ASRSV_SERVIZIO = B.ASRSV_SERVIZIO
            AND A.ASRSV_ART_COD = B.ASRSV_ART_COD)
       WHEN NOT MATCHED
       THEN
          INSERT     (ASRSV_ASSORTIMENTO_SERVIZIO_ID,
                      ASRSV_PDV_COD,
                      ASRSV_SERVIZIO,
                      ASRSV_ART_COD,
                      ASRSV_DATA_MOD,
                      ASRSV_STAT,
                      ASRSV_IN_ASS_AUT,
                      ASRSV_VIS_SITO_ANA,
                      ASRSV_VIS_SITO_PDV)
              VALUES (B.ASRSV_ASSORTIMENTO_SERVIZIO_ID,
                      B.ASRSV_PDV_COD,
                      B.ASRSV_SERVIZIO,
                      B.ASRSV_ART_COD,
                      B.ASRSV_DATA_MOD,
                      B.ASRSV_STAT,
                      B.ASRSV_IN_ASS_AUT,
                      B.ASRSV_VIS_SITO_ANA,
                      B.ASRSV_VIS_SITO_PDV)
       WHEN MATCHED
       THEN
          UPDATE SET A.ASRSV_DATA_MOD = B.ASRSV_DATA_MOD,
                     A.ASRSV_STAT = B.ASRSV_STAT,
                     A.ASRSV_IN_ASS_AUT = B.ASRSV_IN_ASS_AUT,
                     A.ASRSV_VIS_SITO_ANA = B.ASRSV_VIS_SITO_ANA,
                     A.ASRSV_VIS_SITO_PDV = B.ASRSV_VIS_SITO_PDV;
  --
  BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'Excecution time:'||(dbms_utility.get_time - timeStart)/100 || ' seconds' , vPasso);
  --
  EXCEPTION
    WHEN ErroreGestito THEN
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PO_MESSAGGIO :'||PO_MESSAGGIO , vPasso);
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END AGGIORNA_ASRT_AUT;
  --
   /**
  * AGGIORNA_ASSORTIMENTO_PDV(): Utility per l'aggiornamento dell'assortimento di SERVIZIO legato ad un PDV.
  *
  * @param PO_METADATo il metadato estratto
  */
  PROCEDURE AGGIORNA_ASSORTIMENTO_PDV(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_PDV_COD          IN     NUMBER,
      P_SERVICE          IN     VARCHAR2
    )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_CLICK_COLLECT_PCK';
    vProcName           VARCHAR2(100);
    vPdv                GEAD020F.E2CPDV%TYPE;
    vPasso              VARCHAR2(100);
    vFlgCnt             NUMBER;
    timeStart           PLS_INTEGER;
    errorArt            EXCEPTION;
    vFlagS_N            VARCHAR2(5);
  BEGIN
    --
    vProcName := 'AGGIORNA_ASSORTIMENTO_PDV';
    timeStart := dbms_utility.get_time;
    --
    vPasso := 'GET_SERVICE_ID';
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'Pdv:'||P_PDV_COD);
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'Service:'||P_SERVICE);
    --
    --Seleziono gli articoli che sono in assortimento sul PDV e filtro per quelli
    -- che sono abilitati sul sito
    -- a livello di PDV e di anagrafica articolo e appartengono all'assortimento 4B/4M
    vPasso := 'GET_ASSRT_BY_PDV';
    FOR rec_art IN (SELECT E3.E3CART
                      FROM GEAD030F E3
                     WHERE E3.E3CPDV =  P_PDV_COD
                       AND E3.E3FSIT = kFlagS
                       AND TRIM(E3.E3STAT) IS NULL
                       AND (EXISTS
                                  (SELECT 0
                                     FROM GEAD010F E1
                                    WHERE E1.E1CART = E3.E3CART
                                      AND TRIM (E1.E1STAT) IS NULL
                                      AND (E1ASSO LIKE '%4B%' OR E1ASSO LIKE '%4M%')
                                      AND TRIM (E1SPOT) IS NULL             --Escludo gli spot
                                      AND TRIM (E1FESP) IS NULL       --Escludo gli espositori
                                               --AND E1.E1SITO = kFlagS
                                  ) OR
                             EXISTS
                                  (SELECT 0
                                     FROM MG_ASSORTIMENTO_SERVIZIO ASRSV
                                    WHERE ASRSV.ASRSV_ART_COD= E3.E3CART
                                      AND ASRSV.ASRSV_PDV_COD = P_PDV_COD
                                      AND ASRSV.ASRSV_SERVIZIO = P_SERVICE
                                      AND ASRSV_IN_ASSORTIMENTO = kFlagS
                                      AND ASRSV_STAT IS NULL
                                  ) 
                             )
                       --AND EXISTS(SELECT 0
                       --             FROM GEAD010F E1
                       --            WHERE E1.E1CART = E3.E3CART
                       --              AND TRIM(E1.E1STAT) IS NULL
                       --              AND (E1ASSO LIKE '%4B%' OR  E1ASSO LIKE '%4M%')
                       --              AND TRIM(E1SPOT) IS NULL --Escludo gli spot
                       --              AND TRIM(E1FESP) IS NULL --Escludo gli espositori
                       --              --AND E1.E1SITO = kFlagS
                       --              )
                    )
    LOOP
    --
      BEGIN
      --
        PO_ERR_SEVERITA := NULL;
        PO_ERR_CODICE   := NULL;
        PO_MESSAGGIO    := NULL;
        vFlagS_N        := NULL;
        --
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'CodArt:'||rec_art.E3CART);
        --
        vPasso := 'CHECK_ABLT_ART';
        vFlagS_N := GET_FLAG_ARTICOLO(
                     P_ART_COD     => rec_art.E3CART,
                     P_PDV_COD     => P_PDV_COD,
                     P_SERVICE     => P_SERVICE);
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'Flag:'||vFlagS_N, vPasso);
        --
        vPasso := 'UPDT_GEAD030F';
        UPDT_GEAD030F(PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
                      P_ART_COD => rec_art.E3CART,
                      P_PDV_COD => GET_VIRTUAL_PDV(P_PDV_COD,P_SERVICE),
                      P_SITO    => vFlagS_N
                      );
        IF PO_MESSAGGIO IS NOT NULL THEN RAISE errorArt; END IF;
      --
      EXCEPTION
        WHEN errorArt THEN
          PO_ERR_SEVERITA := 'E';
          PO_ERR_CODICE   := SQLCODE;
          PO_MESSAGGIO    :=  'ERRORE - ARTICOLO:'||rec_art.E3CART||' PDV:'|| GET_VIRTUAL_PDV(P_PDV_COD,P_SERVICE) ||' - '||PO_MESSAGGIO;
          BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, PO_MESSAGGIO , vPasso);
          BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
      END;
     --
    END LOOP;
    --
    vPasso := 'ANN_ART_ESC';
    UPDATE GEAD030F E3
       SET E3STAT   = 'A'
     WHERE E3.E3CPDV = GET_VIRTUAL_PDV(P_PDV_COD,P_SERVICE)
       AND TRIM (E3.E3STAT) IS NULL
       AND NOT EXISTS (SELECT 0
                         FROM GEAD030F E31
                        WHERE E31.E3CART = E3.E3CART
                          AND TRIM (E31.E3STAT) IS NULL
                          AND E31.E3CPDV = P_PDV_COD
                       );
    --
    vPasso := 'Art Esc ASRT';
    UPDATE GEAD030F E3
       SET E3.E3FSIT = kFlagN
     WHERE E3.E3CPDV = GET_VIRTUAL_PDV(P_PDV_COD,P_SERVICE)
       AND E3.E3FSIT = kFlagS
       AND TRIM (E3.E3STAT) IS NULL
       AND EXISTS (SELECT 0
                         FROM GEAD030F E31
                        WHERE E31.E3CART = E3.E3CART
                          AND TRIM (E31.E3STAT) IS NULL
                          AND E31.E3CPDV = P_PDV_COD
                          AND E31.E3FSIT = kFlagN
                       );
  --
  BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'Excecution time:'||(dbms_utility.get_time - timeStart)/100 || ' seconds' , vPasso);
  --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END AGGIORNA_ASSORTIMENTO_PDV;
  --
  /**
  * AGGIORNA_PREZZI_PDV(): Utility per l'aggiornamento dei Prezzi dei SERVIZI legato ad un PDV.
  *
  * @param PO_METADATo il metadato estratto
  */
  PROCEDURE AGGIORNA_PREZZI_PDV(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_PDV_COD          IN     NUMBER,
      P_SERVICE          IN     VARCHAR2
  )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_CLICK_COLLECT_PCK';
    vProcName           VARCHAR2(100);
    vPdv                GEAD020F.E2CPDV%TYPE;
    vPdvVirtual         GEAD020F.E2CPDV%TYPE;
    vPasso              VARCHAR2(100);
    vPrezzo             NUMBER;
    timeStart           PLS_INTEGER;
    errorArt            EXCEPTION;
  BEGIN
    --
    vProcName := 'AGGIORNA_PREZZI_PDV';
    timeStart := dbms_utility.get_time;
    --
    vPasso := 'GET_VIRTUAL_PDV';
    --
    --
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'P_PDV_COD:'||P_PDV_COD , vPasso);
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'P_SERVICE:'||P_SERVICE , vPasso);
    --
    vPdvVirtual := GET_VIRTUAL_PDV(P_PDV_COD => P_PDV_COD, P_SERVICE=> P_SERVICE);
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'VirtualPDV:'||vPdvVirtual , vPasso);
    --Seleziono gli articoli che sono in assortimento sul PDV virtuale e filtro per quelli
    -- che sono abilitati sul sito
    -- a livello di PDV e di anagrafica articolo.
    vPasso := 'GET_ASSRT_BY_PDV';
    FOR rec_art IN (SELECT E3.E3CART
                      FROM GEAD030F E3
                     WHERE E3.E3CPDV =  vPdvVirtual
                       AND E3.E3FSIT = kFlagS
                       AND TRIM(E3.E3STAT) IS NULL
                       AND EXISTS(SELECT 0
                                    FROM GEAD010F E1
                                   WHERE E1.E1CART = E3.E3CART
                                     AND TRIM(E1.E1STAT) IS NULL
                                     AND E1.E1SITO = kFlagS)
                     )
    LOOP
    --
      BEGIN
      --
        PO_ERR_SEVERITA := NULL;
        PO_ERR_CODICE   := NULL;
        PO_MESSAGGIO    := NULL;
        --
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'CodArt:'||rec_art.E3CART);
        --
        vPasso := 'GET_PRICE';
        FRM_PREZZI_PCK.GET_PRICE(
          PO_ERR_SEVERITA => PO_ERR_SEVERITA,
          PO_ERR_CODICE   => PO_ERR_CODICE,
          PO_MESSAGGIO    => PO_MESSAGGIO,
          P_INTCODE       => rec_art.E3CART,
          P_HOST_SHOP_ID  => P_PDV_COD,
          PO_PRICEAMOUNT  => vPrezzo);
        --
        IF PO_MESSAGGIO IS NOT NULL THEN RAISE errorArt; END IF;
        --
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'Prezzo:'||vPrezzo/100, vPasso);
        --
        vPasso := 'UPDT_PRZPV';
        UPDT_PRZPV(PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
                   P_ART_COD => rec_art.E3CART,
                   P_PDV_COD => vPdvVirtual,
                   P_PREZZO    => vPrezzo/100
                   );
        IF PO_MESSAGGIO IS NOT NULL THEN RAISE errorArt; END IF;
       --
      EXCEPTION
        WHEN errorArt THEN
          PO_ERR_SEVERITA := 'E';
          PO_ERR_CODICE   := SQLCODE;
          PO_MESSAGGIO    :=  'ERRORE - ARTICOLO:'||rec_art.E3CART||' PDV:'|| vPdvVirtual ||' - '||PO_MESSAGGIO;
          BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, PO_MESSAGGIO , vPasso);
          BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
          --
        PO_ERR_SEVERITA := NULL;
        PO_ERR_CODICE   := NULL;
        PO_MESSAGGIO    := NULL;
      END;
    --
    END LOOP;
    --
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'Excecution time:'||(dbms_utility.get_time - timeStart)/100 || ' seconds' , vPasso);
  --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END AGGIORNA_PREZZI_PDV;
  --
  /**
  * AGGIORNA_PROMO_PDV(): Utility per l'aggiornamento dei Prezzi promo dei SERVIZI legato ad un PDV.
  *
  * @param PO_METADATo il metadato estratto
  */
  PROCEDURE AGGIORNA_PROMO_PDV(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_PDV_COD          IN     NUMBER,
      P_SERVICE          IN     VARCHAR2
  )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_CLICK_COLLECT_PCK';
    vProcName           VARCHAR2(100);
    vPdv                GEAD020F.E2CPDV%TYPE;
    vPdvVirtual         GEAD020F.E2CPDV%TYPE;
    vPasso              VARCHAR2(100);
    vPrezzo             NUMBER;
    timeStart           PLS_INTEGER;
    errorArt            EXCEPTION;
  BEGIN
    --
    vProcName := 'AGGIORNA_PROMO_PDV';
    --
    vPasso := 'GET_VIRTUAL_PDV';
    --
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'P_PDV_COD:'||P_PDV_COD , vPasso);
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'P_SERVICE:'||P_SERVICE , vPasso);
    --
    vPdvVirtual := GET_VIRTUAL_PDV(P_PDV_COD => P_PDV_COD, P_SERVICE=> P_SERVICE);
    --
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'VirtualPDV:'||vPdvVirtual , vPasso);
    --Seleziono gli articoli che sono in assortimento sul PDV virtuale e filtro per quelli
    -- che sono abilitati sul sito
    -- a livello di PDV e di anagrafica articolo.
    vPasso := 'MERGE_GEAD040F';
    MERGE INTO MG.GEAD040F TARGET_E4 USING (
        SELECT MAX(E4DTEX) E4DTEX, MAX(E4OREX) E4OREX, E4APRO,
               E4NPRO, E4DESC, E4IPRO,
               E4FPRO, E4ICOM, E4FCOM,
               vPdvVirtual E4CPDV,
               E4CART, E4FIDE, E4TSCO,
               E4PRZP, E4PSCO, E4VOLA,
               E4TVOL, E4PRZN
          FROM MG.GEAD040F E4
         WHERE E4CPDV = P_PDV_COD
           --AND SYSDATE >= TO_DATE(E4FPRO,'YYYYMMDD')
           AND TRUNC(SYSDATE) BETWEEN TO_DATE(E4IPRO,'YYYYMMDD') AND TO_DATE(E4FPRO,'YYYYMMDD')
           AND EXISTS ( SELECT 0
                          FROM MG.GEAD030F E3
                         WHERE E3.E3CPDV = vPdvVirtual
                           AND E3.E3FSIT = 'S'
                           AND TRIM (E3.E3STAT) IS NULL
                           AND E3.E3CART = E4.E4CART)
      GROUP BY E4APRO,E4NPRO, E4DESC, E4IPRO,
               E4FPRO, E4ICOM, E4FCOM,
               E4CPDV,
               E4CART, E4FIDE, E4TSCO,
               E4PRZP, E4PSCO, E4VOLA,
               E4TVOL, E4PRZN          
                ) SOURCE_E4
            ON (TARGET_E4.E4CPDV = SOURCE_E4.E4CPDV AND
                TARGET_E4.E4CART = SOURCE_E4.E4CART)
          WHEN NOT MATCHED THEN
          INSERT (E4DTEX, E4OREX, E4APRO,
                  E4NPRO, E4DESC, E4IPRO,
                  E4FPRO, E4ICOM, E4FCOM,
                  E4CPDV, E4CART, E4FIDE,
                  E4TSCO, E4PRZP, E4PSCO,
                  E4VOLA, E4TVOL, E4PRZN)
          VALUES (SOURCE_E4.E4DTEX, SOURCE_E4.E4OREX, SOURCE_E4.E4APRO,
                  SOURCE_E4.E4NPRO, SOURCE_E4.E4DESC, SOURCE_E4.E4IPRO,
                  SOURCE_E4.E4FPRO, SOURCE_E4.E4ICOM, SOURCE_E4.E4FCOM,
                  SOURCE_E4.E4CPDV, SOURCE_E4.E4CART, SOURCE_E4.E4FIDE,
                  SOURCE_E4.E4TSCO, SOURCE_E4.E4PRZP, SOURCE_E4.E4PSCO,
                  SOURCE_E4.E4VOLA, SOURCE_E4.E4TVOL, SOURCE_E4.E4PRZN)
          WHEN MATCHED THEN
          UPDATE SET
                 TARGET_E4.E4OREX = SOURCE_E4.E4OREX,
                 TARGET_E4.E4APRO = SOURCE_E4.E4APRO,
                 TARGET_E4.E4NPRO = SOURCE_E4.E4NPRO,
                 TARGET_E4.E4DESC = SOURCE_E4.E4DESC,
                 TARGET_E4.E4IPRO = SOURCE_E4.E4IPRO,
                 TARGET_E4.E4FPRO = SOURCE_E4.E4FPRO,
                 TARGET_E4.E4ICOM = SOURCE_E4.E4ICOM,
                 TARGET_E4.E4FCOM = SOURCE_E4.E4FCOM,
                 TARGET_E4.E4FIDE = SOURCE_E4.E4FIDE,
                 TARGET_E4.E4TSCO = SOURCE_E4.E4TSCO,
                 TARGET_E4.E4PRZP = SOURCE_E4.E4PRZP,
                 TARGET_E4.E4PSCO = SOURCE_E4.E4PSCO,
                 TARGET_E4.E4VOLA = SOURCE_E4.E4VOLA,
                 TARGET_E4.E4TVOL = SOURCE_E4.E4TVOL,
                 TARGET_E4.E4PRZN = SOURCE_E4.E4PRZN;
    --
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'RowsUPDT:'||SQL%ROWCOUNT , vPasso);
    --
    vPasso := 'MERGE_GEAD050F';
    --
    MERGE INTO MG.GEAD050F E5_SOURCE USING(
        SELECT MAX(E5DTEX) E5DTEX, MAX(E5OREX) E5OREX, E5AMXN,
               E5NMXN, E5DESC, E5IMXN,
               E5FMXN, E5TMOF, vPdvVirtual E5CPDV,
               E5CAGG, E5CART, E5MXNT,
               E5MXND, E5MXNM, E5MXNN,
               E5IFIS, E5PRZP
          FROM MG.GEAD050F E5
         WHERE E5CPDV = P_PDV_COD
           --AND SYSDATE >= TO_DATE(E5FMXN,'YYYYMMDD') 
           AND TRUNC(SYSDATE) BETWEEN TO_DATE(E5IMXN,'YYYYMMDD') AND TO_DATE(E5FMXN,'YYYYMMDD')
           AND EXISTS(
               SELECT 0
                 FROM MG.GEAD030F E3
                WHERE E3.E3CPDV = vPdvVirtual
                  AND E3.E3FSIT = 'S'
                  AND TRIM (E3.E3STAT) IS NULL
                  AND E3.E3CART = E5.E5CART)
      GROUP BY E5AMXN,E5NMXN, E5DESC, E5IMXN,
               E5FMXN, E5TMOF, E5CPDV,
               E5CAGG, E5CART, E5MXNT,
               E5MXND, E5MXNM, E5MXNN,
               E5IFIS, E5PRZP
                  ) E5_START
            ON (E5_SOURCE.E5CPDV = E5_START.E5CPDV AND
                E5_SOURCE.E5CART = E5_START.E5CART)
          WHEN NOT MATCHED THEN
        INSERT (E5DTEX, E5OREX, E5AMXN, E5NMXN, E5DESC,
                E5IMXN, E5FMXN, E5TMOF, E5CPDV, E5CAGG,
                E5CART, E5MXNT, E5MXND, E5MXNM, E5MXNN,
                E5IFIS, E5PRZP)
        VALUES (E5_START.E5DTEX, E5_START.E5OREX, E5_START.E5AMXN, E5_START.E5NMXN, E5_START.E5DESC,
                E5_START.E5IMXN, E5_START.E5FMXN, E5_START.E5TMOF, E5_START.E5CPDV, E5_START.E5CAGG,
                E5_START.E5CART, E5_START.E5MXNT, E5_START.E5MXND, E5_START.E5MXNM, E5_START.E5MXNN,
                E5_START.E5IFIS, E5_START.E5PRZP)
          WHEN MATCHED THEN
        UPDATE SET E5_SOURCE.E5OREX = E5_START.E5OREX,
                   E5_SOURCE.E5AMXN = E5_START.E5AMXN,
                   E5_SOURCE.E5NMXN = E5_START.E5NMXN,
                   E5_SOURCE.E5DESC = E5_START.E5DESC,
                   E5_SOURCE.E5IMXN = E5_START.E5IMXN,
                   E5_SOURCE.E5FMXN = E5_START.E5FMXN,
                   E5_SOURCE.E5TMOF = E5_START.E5TMOF,
                   E5_SOURCE.E5CAGG = E5_START.E5CAGG,
                   E5_SOURCE.E5MXNT = E5_START.E5MXNT,
                   E5_SOURCE.E5MXND = E5_START.E5MXND,
                   E5_SOURCE.E5MXNM = E5_START.E5MXNM,
                   E5_SOURCE.E5MXNN = E5_START.E5MXNN,
                   E5_SOURCE.E5IFIS = E5_START.E5IFIS,
                   E5_SOURCE.E5PRZP = E5_START.E5PRZP;
    --
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'RowsUPDT:'||SQL%ROWCOUNT , vPasso);
  --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END AGGIORNA_PROMO_PDV;
  --
  PROCEDURE REFRESH_ASRT(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_PDV_COD          IN     NUMBER  DEFAULT NULL
  )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_CLICK_COLLECT_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    timeStart           PLS_INTEGER;
    vNomeProc           VARCHAR2(100);
    vTotAsrt            NUMBER := 0;
    vIndex              NUMBER := 0;
  BEGIN
    --
    vProcName := 'REFRESH_ASRT';
    vNomeProc := kNomePackage||'.'||vProcName;
    timeStart := dbms_utility.get_time;
    --
    vPasso := 'NORMALIZZA_PESCHERIA';
    NORMALIZZA_PESCHERIA(
      PO_ERR_SEVERITA => PO_ERR_SEVERITA,
      PO_ERR_CODICE   => PO_ERR_CODICE,
      PO_MESSAGGIO    => PO_MESSAGGIO
      );
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PO_MESSAGGIO: '|| PO_MESSAGGIO, vPasso);
    IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
    --
    vPasso := 'AGGIORNA_ASSORTIMENTO_AUT';
    AGGIORNA_ASRT_AUT(
      PO_ERR_SEVERITA => PO_ERR_SEVERITA,
      PO_ERR_CODICE   => PO_ERR_CODICE,
      PO_MESSAGGIO    => PO_MESSAGGIO
      );
    --
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PO_MESSAGGIO: '|| PO_MESSAGGIO, vPasso);
    IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
    --
     vPasso := 'CNT_PDV_SERV';
    SELECT COUNT(0)
      INTO vTotAsrt
      FROM MG_PDV_REL_SERVICE
     WHERE TRIM(PVSER_STAT) IS NULL
       AND PVSER_PDV_COD = NVL(P_PDV_COD,PVSER_PDV_COD)
       AND LOCALTIMESTAMP BETWEEN PVSER_DATA_INI AND PVSER_DATA_FIN
       AND PVSER_SERVIZIO IN (kServiceDRIVE, kServiceCASA, kServiceSTORE);
    --
    vPasso := 'LOOP_PDV_SERV';
    FOR rec_pdv IN (SELECT PVSER_PDV_COD, PVSER_SERVIZIO
                      FROM MG_PDV_REL_SERVICE
                     WHERE TRIM(PVSER_STAT) IS NULL
                       AND PVSER_PDV_COD = NVL(P_PDV_COD,PVSER_PDV_COD)
                       AND LOCALTIMESTAMP BETWEEN PVSER_DATA_INI AND PVSER_DATA_FIN
                       AND PVSER_SERVIZIO IN (kServiceDRIVE, kServiceCASA, kServiceSTORE)
                       )
    LOOP
      --
      vIndex := vIndex + 1;
      --
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'Pdv: ['||rec_pdv.PVSER_PDV_COD||'] Servizio:['|| rec_pdv.PVSER_SERVIZIO|| '] - '|| vIndex ||'/'||vTotAsrt, vPasso);
      --
      BASE_LOG_PCK.WriteLog(vNomeProc, 'Pdv: ['||rec_pdv.PVSER_PDV_COD||'] Servizio:['|| rec_pdv.PVSER_SERVIZIO|| '] - '|| vIndex ||'/'||vTotAsrt ,BASE_LOG_PCK.Info);
      --
      BEGIN
        --
        PO_ERR_SEVERITA := NULL;
        PO_ERR_CODICE   := NULL;
        PO_MESSAGGIO    := NULL;
        --
        vPasso := 'AGGIORNA_ASSORTIMENTO_PDV';
        AGGIORNA_ASSORTIMENTO_PDV(
            PO_ERR_SEVERITA => PO_ERR_SEVERITA,
            PO_ERR_CODICE   => PO_ERR_CODICE,
            PO_MESSAGGIO    => PO_MESSAGGIO,
            P_PDV_COD       => rec_pdv.PVSER_PDV_COD,
            P_SERVICE       => rec_pdv.PVSER_SERVIZIO
            );
        --
        IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
        --
      EXCEPTION
        WHEN erroreGestito THEN
          PO_ERR_SEVERITA := 'E';
          PO_ERR_CODICE   := SQLCODE;
          PO_MESSAGGIO    :=  'ERRORE - PDV:'|| rec_pdv.PVSER_PDV_COD ||' - '||PO_MESSAGGIO;
          BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, PO_MESSAGGIO , vPasso);
          --
          BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
      END;
    --
    END LOOP;
    --
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'Excecution time:'||(dbms_utility.get_time - timeStart)/100 || ' seconds' , vPasso);
  --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END REFRESH_ASRT;
  --
  PROCEDURE INTEGRA_DATI_AGG(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2
  )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_CLICK_COLLECT_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    timeStart           PLS_INTEGER;
    vNomeProc           VARCHAR2(100);
    vTotAsrt            NUMBER := 0;
    vIndex              NUMBER := 0;
  BEGIN
    --
    vProcName := 'INTEGRA_DATI_AGG';
    --
    vPasso := 'LOOP';
    FOR rec IN (SELECT ANG.E1CART,
                       ANG.E1XARE,
                       DECODE (MG_CLICK_COLLECT_PCK.GET_INFO_FOTO (ANG.E1CART),
                               1, 'SI',
                               'NO')
                          FOTO,
                       AGG.E7KYCI UNITA_VENDITA,
                       NVL (VAL.E7KYCI, OFF.E7KYCI) VALORE
                  FROM GEAD010F ANG,
                       DATIAGGIUNTIVI AGG,
                       DATIAGGIUNTIVI VAL,
                       DATIAGGIUNTIVI OFF
                 WHERE     ANG.E1UMPR != 'PZ'
                       AND ANG.E1SITO = 'S'
                       AND ANG.E1CPLU != 0
                       --
                       AND AGG.E7KYAM(+) = 'ARTICOLI'
                       AND AGG.E7KY01(+) = ANG.E1CART
                       AND AGG.E7KY02(+) = 'CLICK_COLLECT'
                       AND AGG.E7KY03(+) = 'UM_ORDINE'
                       --
                       AND VAL.E7KYAM(+) = 'ARTICOLI'
                       AND VAL.E7KY01(+) = ANG.E1CART
                       AND VAL.E7KY02(+) = 'CLICK_COLLECT'
                       AND VAL.E7KY03(+) = 'VAL_MEDIO'
                       --
                       AND OFF.E7KYAM(+) = 'ARTICOLI'
                       AND OFF.E7KY01(+) = ANG.E1CART
                       AND OFF.E7KY02(+) = 'CLICK_COLLECT'
                       AND OFF.E7KY03(+) = 'OFFSET_SCOST'
                       --
                       AND MG_CLICK_COLLECT_PCK.GET_INFO_FOTO (ANG.E1CART) = 1
                       AND (   AGG.E7KYCI IS NULL
                            OR (VAL.E7KYCI IS NULL AND OFF.E7KYCI IS NULL))
                       AND EXISTS
                              (SELECT 0
                                 FROM GEAD030f PDV, MG.MG_PDV_REL_SERVICE SRV
                                WHERE     PDV.E3CART = ANG.E1CART
                                      AND PDV.E3FSIT = 'S'
                                      AND SRV.PVSER_PDV_COD = PDV.E3CPDV)
                       )
  LOOP
  --
    vPasso  := 'INS_UNITA_VENDITA';
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'Art:'|| rec.E1CART ||' - UNITA_VENDITA:'||  rec.UNITA_VENDITA, vPasso);
    --
    IF rec.UNITA_VENDITA IS NULL THEN
    --
      MERGE INTO MG.DATIAGGIUNTIVI A
           USING (SELECT 'ARTICOLI' AS E7KYAM,
                          rec.E1CART AS E7KY01,
                          'CLICK_COLLECT' AS E7KY02,
                          'UM_ORDINE' AS E7KY03,
                          ' ' AS E7KY04,
                          ' ' AS E7KY05,
                          'GR' AS E7KYCI,
                          'GRAMMI' AS E7INFO,
                          NULL AS E7TINF,
                          LOCALTIMESTAMP AS E7TIMS
                     FROM DUAL) B
              ON (     A.E7KYAM = B.E7KYAM
                   AND A.E7KY01 = B.E7KY01
                   AND A.E7KY02 = B.E7KY02
                   AND A.E7KY03 = B.E7KY03)
            WHEN NOT MATCHED
            THEN
          INSERT (E7KYAM,
                  E7KY01,
                  E7KY02,
                  E7KY03,
                  E7KY04,
                  E7KY05,
                  E7KYCI,
                  E7INFO,
                  E7TINF,
                  E7TIMS)
          VALUES (B.E7KYAM,
                  B.E7KY01,
                  B.E7KY02,
                  B.E7KY03,
                  B.E7KY04,
                  B.E7KY05,
                  B.E7KYCI,
                  B.E7INFO,
                  B.E7TINF,
                  B.E7TIMS)
       WHEN MATCHED
       THEN
          UPDATE SET A.E7KYCI = B.E7KYCI,
                     A.E7INFO = B.E7INFO,
                     A.E7TINF = B.E7TINF,
                     A.E7TIMS = B.E7TIMS;
    END IF;
    --
    vPasso := 'INS_VALORE';
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'Art:'|| rec.E1CART ||' - VALORE:'||  rec.VALORE, vPasso);
    --
    IF rec.VALORE IS NULL THEN
    --
      MERGE INTO MG.DATIAGGIUNTIVI A
           USING (SELECT 'ARTICOLI' AS E7KYAM,
                          rec.E1CART AS E7KY01,
                          'CLICK_COLLECT' AS E7KY02,
                          'OFFSET_SCOST' AS E7KY03,
                          ' ' AS E7KY04,
                          ' ' AS E7KY05,
                          '50' AS E7KYCI,
                          '50' AS E7INFO,
                          NULL AS E7TINF,
                          LOCALTIMESTAMP AS E7TIMS
                      FROM DUAL) B
              ON (    A.E7KYAM = B.E7KYAM
                  AND A.E7KY01 = B.E7KY01
                  AND A.E7KY02 = B.E7KY02
                  AND A.E7KY03 = B.E7KY03)
         WHEN NOT MATCHED
         THEN
            INSERT     (E7KYAM,
                        E7KY01,
                        E7KY02,
                        E7KY03,
                        E7KY04,
                        E7KY05,
                        E7KYCI,
                        E7INFO,
                        E7TINF,
                        E7TIMS)
                VALUES (B.E7KYAM,
                        B.E7KY01,
                        B.E7KY02,
                        B.E7KY03,
                        B.E7KY04,
                        B.E7KY05,
                        B.E7KYCI,
                        B.E7INFO,
                        B.E7TINF,
                        B.E7TIMS)
         WHEN MATCHED
         THEN
            UPDATE SET A.E7KYCI = B.E7KYCI,
                       A.E7INFO = B.E7INFO,
                       A.E7TINF = B.E7TINF,
                       A.E7TIMS = B.E7TIMS;
    END IF;
  --
  END LOOP;
  --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END INTEGRA_DATI_AGG;
  --
  PROCEDURE OVERRIDE_ASRT(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2
  )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_CLICK_COLLECT_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    timeStart           PLS_INTEGER;
  BEGIN
    --
    vProcName := 'OVERRIDE_ASRT';
    --
    -- a livello di PDV e di anagrafica articolo.
    vPasso := 'GET_DIFF_VIS';
    FOR rec IN (SELECT A.ART_CODICE,
                       A.PDV_CODICE,
                       A.VISIBILE,
                       B.E3FSIT
                  FROM MG_ARTICOLI_REL_PDV_OVERRIDE A,
                       MG.GEAD030F B
                 WHERE A.ART_CODICE = B.E3CART
                   AND A.PDV_CODICE = B.E3CPDV
                   AND A.VISIBILE != B.E3FSIT
                )
    LOOP
    --
      UPDATE MG.GEAD030F
         SET E3FSIT = rec.VISIBILE
       WHERE E3CPDV = rec.PDV_CODICE
         AND E3CART = rec.ART_CODICE;
    END LOOP;
  --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END OVERRIDE_ASRT;
  --
  PROCEDURE REFRESH_ASRT_FULL(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2
  )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_CLICK_COLLECT_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    timeStart           PLS_INTEGER;
    vNomeProc           VARCHAR2(100);
    vTotAsrt            NUMBER := 0;
    vIndex              NUMBER := 0;
  BEGIN
    --
    vProcName := 'REFRESH_ASRT';
    vNomeProc := kNomePackage||'.'||vProcName;
    --
    vPasso := 'NORMALIZZA_PESCHERIA';
    NORMALIZZA_PESCHERIA(
      PO_ERR_SEVERITA => PO_ERR_SEVERITA,
      PO_ERR_CODICE   => PO_ERR_CODICE,
      PO_MESSAGGIO    => PO_MESSAGGIO
      );
    BASE_LOG_PCK.WriteLog(vNomeProc, vPasso||' - PO_MESSAGGIO: '|| PO_MESSAGGIO, BASE_LOG_PCK.Info);
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PO_MESSAGGIO: '|| PO_MESSAGGIO, vPasso);
    IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
    --
    vPasso := 'INTEGRA_DATI_AGG';
    INTEGRA_DATI_AGG(
      PO_ERR_SEVERITA => PO_ERR_SEVERITA,
      PO_ERR_CODICE   => PO_ERR_CODICE,
      PO_MESSAGGIO    => PO_MESSAGGIO
      );
    BASE_LOG_PCK.WriteLog(vNomeProc, vPasso||' - PO_MESSAGGIO: '|| PO_MESSAGGIO, BASE_LOG_PCK.Info);
    --
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PO_MESSAGGIO: '|| PO_MESSAGGIO, vPasso);
    IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
    --
    vPasso := 'REFRESH_DATI_AGG';
    REFRESH_DATI_AGG(
         PO_ERR_SEVERITA => PO_ERR_SEVERITA,
         PO_ERR_CODICE   => PO_ERR_CODICE,
         PO_MESSAGGIO    => PO_MESSAGGIO
     );
    BASE_LOG_PCK.WriteLog(vNomeProc, vPasso||' - PO_MESSAGGIO: '|| PO_MESSAGGIO, BASE_LOG_PCK.Info);
    --
    vPasso := 'OVERRIDE_ASRT';
    OVERRIDE_ASRT(
         PO_ERR_SEVERITA => PO_ERR_SEVERITA,
         PO_ERR_CODICE   => PO_ERR_CODICE,
         PO_MESSAGGIO    => PO_MESSAGGIO
     );
    BASE_LOG_PCK.WriteLog(vNomeProc, vPasso||' - PO_MESSAGGIO: '|| PO_MESSAGGIO, BASE_LOG_PCK.Info);
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PO_MESSAGGIO: '|| PO_MESSAGGIO, vPasso);
    --
    IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
    vPasso := 'REFRESH_DICT_SITO';
    REFRESH_DICT_SITO(
         PO_ERR_SEVERITA => PO_ERR_SEVERITA,
         PO_ERR_CODICE   => PO_ERR_CODICE,
         PO_MESSAGGIO    => PO_MESSAGGIO
     );
    BASE_LOG_PCK.WriteLog(vNomeProc, vPasso||' - PO_MESSAGGIO: '|| PO_MESSAGGIO, BASE_LOG_PCK.Info);
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PO_MESSAGGIO: '|| PO_MESSAGGIO, vPasso);
    --
    COMMIT;
    --
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'Excecution time:'||(dbms_utility.get_time - timeStart)/100 || ' seconds' , vPasso);
  --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END REFRESH_ASRT_FULL;
  --
  PROCEDURE REFRESH_PRZ_FULL(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_PDV_COD          IN            NUMBER
  )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_CLICK_COLLECT_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    vNomeProc           VARCHAR2(100);
    vTotAsrt            NUMBER := 0;
    vIndex              NUMBER := 0;
    vService            VARCHAR2(100) := 'CCDRIVE';
  BEGIN
    --
    vProcName := 'REFRESH_PRZ_FULL';
    --
    vNomeProc := kNomePackage||'.'||vProcName;
    --
    FOR rec IN (SELECT PVSER_PDV_COD, PVSER_SERVIZIO
                  FROM MG.MG_PDV_REL_SERVICE
                 WHERE PVSER_STAT IS NULL
                   AND PVSER_SERVIZIO IN ('CCDRIVE', 'CCCASA', 'CCSTORE')
                   AND LOCALTIMESTAMP BETWEEN PVSER_DATA_INI AND PVSER_DATA_FIN)
    LOOP
      vIndex := vIndex + 1;
      BASE_LOG_PCK.WriteLog(vNomeProc, 'Pdv: ['||rec.PVSER_PDV_COD||'] Servizio:['|| rec.PVSER_SERVIZIO|| '] - '|| vIndex ,BASE_LOG_PCK.Info);
      vPasso := 'AGGIORNA_PREZZI_PDV';
      AGGIORNA_PREZZI_PDV(
        PO_ERR_SEVERITA => PO_ERR_SEVERITA,
        PO_ERR_CODICE   => PO_ERR_CODICE,
        PO_MESSAGGIO    => PO_MESSAGGIO,
        P_PDV_COD       => rec.PVSER_PDV_COD,
        P_SERVICE       => rec.PVSER_SERVIZIO
        );
       BASE_LOG_PCK.WriteLog(vNomeProc, 'vPasso:'||vPasso||' PO_MESSAGGIO:'||PO_MESSAGGIO,BASE_LOG_PCK.Info);
    END LOOP;
    --
    IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
    vPasso := 'CLEAN_OLD_PRICE';
    DELETE MG_PREZZI_REL_PDV
     WHERE TRIM(PRZPV_STAT) IS NOT NULL;
  --
  EXCEPTION
    WHEN erroreGestito THEN
      PO_ERR_SEVERITA := 'E';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    :=  'ERRORE - PDV:'|| P_PDV_COD ||' - '||PO_MESSAGGIO;
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, PO_MESSAGGIO , vPasso);
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END REFRESH_PRZ_FULL;
  --
  PROCEDURE REFRESH_PRZ(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_PDV_COD          IN     NUMBER  DEFAULT NULL
  )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_CLICK_COLLECT_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    vNomeProc           VARCHAR2(100);
    vTotAsrt            NUMBER := 0;
    vIndex              NUMBER := 0;
  BEGIN
    --
    vProcName := 'REFRESH_PRZ';
    --
     vNomeProc := kNomePackage||'.'||vProcName;
     --
     vPasso := 'CNT_PDV_SERV';
     SELECT COUNT(0)
       INTO vTotAsrt
       FROM MG_PDV_REL_SERVICE
      WHERE TRIM(PVSER_STAT) IS NULL
        AND PVSER_PDV_COD = NVL(P_PDV_COD,PVSER_PDV_COD)
        AND LOCALTIMESTAMP BETWEEN PVSER_DATA_INI AND PVSER_DATA_FIN
        AND PVSER_SERVIZIO IN (kServiceDRIVE, kServiceCASA, kServiceSTORE);
     --
    vPasso := 'GET_PDV_SERVICE';
    --
    FOR rec_pdv IN (SELECT PVSER_PDV_COD, PVSER_SERVIZIO
                      FROM MG_PDV_REL_SERVICE
                     WHERE TRIM(PVSER_STAT) IS NULL
                       AND PVSER_PDV_COD = NVL(P_PDV_COD,PVSER_PDV_COD)
                       AND LOCALTIMESTAMP BETWEEN PVSER_DATA_INI AND PVSER_DATA_FIN
                       AND PVSER_SERVIZIO IN (kServiceDRIVE, kServiceCASA, kServiceSTORE)
                       )
    LOOP
      --
      vIndex := vIndex + 1;
      --
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PDV:'||rec_pdv.PVSER_PDV_COD , vPasso);
      --
      BASE_LOG_PCK.WriteLog(vNomeProc, 'Pdv: ['||rec_pdv.PVSER_PDV_COD||'] Servizio:['|| rec_pdv.PVSER_SERVIZIO|| '] - '|| vIndex ||'/'||vTotAsrt,BASE_LOG_PCK.Info);
      --
      BEGIN
        --
        PO_ERR_SEVERITA := NULL;
        PO_ERR_CODICE   := NULL;
        PO_MESSAGGIO    := NULL;
        --
        vPasso := 'AGGIORNA_PREZZI_PDV';
        AGGIORNA_PREZZI_PDV(
            PO_ERR_SEVERITA => PO_ERR_SEVERITA,
            PO_ERR_CODICE   => PO_ERR_CODICE,
            PO_MESSAGGIO    => PO_MESSAGGIO,
            P_PDV_COD       => rec_pdv.PVSER_PDV_COD,
            P_SERVICE       => rec_pdv.PVSER_SERVIZIO
            );
          --
        IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
        --
        vPasso := 'AGGIORNA_PROMO_PDV';
        --AGGIORNA_PROMO_PDV(
        --   PO_ERR_SEVERITA => PO_ERR_SEVERITA,
        --    PO_ERR_CODICE   => PO_ERR_CODICE,
        --    PO_MESSAGGIO    => PO_MESSAGGIO,
        --    P_PDV_COD       => rec_pdv.PVSER_PDV_COD,
         --   P_SERVICE       => rec_pdv.PVSER_SERVIZIO
         --   );
          --
        IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
        --
      EXCEPTION
        WHEN erroreGestito THEN
          PO_ERR_SEVERITA := 'E';
          PO_ERR_CODICE   := SQLCODE;
          PO_MESSAGGIO    :=  'ERRORE - PDV:'|| rec_pdv.PVSER_PDV_COD ||' - '||PO_MESSAGGIO;
          BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, PO_MESSAGGIO , vPasso);
          BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
      END;
    --
    END LOOP;
  --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END REFRESH_PRZ;
  --
  PROCEDURE REFRESH_DATI_AGG(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2
  )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_CLICK_COLLECT_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    vTagPreparazione    CONSTANT VARCHAR2(20) := 'DETPR0000100';
    vTagAvvertenza      CONSTANT VARCHAR2(20) := 'DETPR0000200';
    vTagConservazione   CONSTANT VARCHAR2(20) := 'DETPR0000300';
    vTagIngredienti     CONSTANT VARCHAR2(20) := 'DETPR0000600';
    vTagFeatures        CONSTANT VARCHAR2(20) := 'FEATU0000100';
    vTagApp             VARCHAR2(20);
    vIngredienti        VARCHAR2(4000);
    --
    FUNCTION GET_TXT_FISSO(P_ART VARCHAR2)
      RETURN VARCHAR2
    IS
      vTxtFisso VARCHAR2(4000);
    BEGIN
      --
      SELECT TRIM(
             TFRI01||
             TFRI02||
             TFRI03||
             TFRI04||
             TFRI05||
             TFRI06||
             TFRI07||
             TFRI08||
             TFRI09||
             TFRI10||
             TFRI11||
             TFRI12||
             TFRI13||
             TFRI14||
             TFRI15||
             TFRI16||
             TFRI17||
             TFRI18||
             TFRI19||
             TFRI20)
        INTO vTxtFisso
        FROM FRAME.MG_AS_TESFI00F TFC
       WHERE TFC.TFCTES = P_ART;
    --
      RETURN vTxtFisso;
    EXCEPTION
      WHEN OTHERS THEN RETURN NULL;
    END GET_TXT_FISSO;
    --
    PROCEDURE INS_DATI_AGG (P_TAG VARCHAR2, P_ART VARCHAR2, P_TESTO VARCHAR2)
      IS
    BEGIN
    --
      MERGE INTO MG.DATIAGGIUNTIVI A
           USING (SELECT 'ARTICOLI' AS E7KYAM,
                         P_ART AS E7KY01,
                         'IMMAGINO' AS E7KY02,
                         'AS400_BIL' AS E7KY03,
                         ' ' AS E7KY04,
                         ' ' AS E7KY05,
                         P_TAG AS E7KYCI,
                         P_TESTO AS E7INFO,
                         'text/plain' AS E7TINF,
                         LOCALTIMESTAMP AS E7TIMS
                    FROM DUAL) B
              ON (    A.E7KYAM = B.E7KYAM
                  AND A.E7KY01 = B.E7KY01
                  AND A.E7KY02 = B.E7KY02
                  AND A.E7KY03 = B.E7KY03
                  AND A.E7KYCI = B.E7KYCI)
      WHEN NOT MATCHED
      THEN
         INSERT     (E7KYAM,
                     E7KY01,
                     E7KY02,
                     E7KY03,
                     E7KY04,
                     E7KY05,
                     E7KYCI,
                     E7INFO,
                     E7TINF,
                     E7TIMS)
             VALUES (B.E7KYAM,
                     B.E7KY01,
                     B.E7KY02,
                     B.E7KY03,
                     B.E7KY04,
                     B.E7KY05,
                     B.E7KYCI,
                     B.E7INFO,
                     B.E7TINF,
                     B.E7TIMS)
      WHEN MATCHED
      THEN
         UPDATE SET
            A.E7INFO = B.E7INFO, A.E7TINF = B.E7TINF, A.E7TIMS = B.E7TIMS;
   END INS_DATI_AGG;
   --
   PROCEDURE INS_TXT_FISSI(P_TXT_FIX VARCHAR2, P_ART VARCHAR2, P_TESTO VARCHAR2)
   IS
     vTagApp    VARCHAR2(20) := NULL;
     vTxtPrdDaLav VARCHAR2(100) := 'Prodotto da lavare';
   BEGIN
   --
     CASE
       WHEN TRIM(P_TXT_FIX) = '000004' OR
            TRIM(P_TXT_FIX) = '000005' OR
            TRIM(P_TXT_FIX) = '000006' OR
            TRIM(P_TXT_FIX) = '000007' OR
            TRIM(P_TXT_FIX) = '000008' OR
            TRIM(P_TXT_FIX) = '000009' OR
            TRIM(P_TXT_FIX) = '000010' OR
            TRIM(P_TXT_FIX) = '000011' OR
            TRIM(P_TXT_FIX) = '000012' OR
            TRIM(P_TXT_FIX) = '000013' OR
            TRIM(P_TXT_FIX) = '000014' OR
            TRIM(P_TXT_FIX) = '000015' OR
            TRIM(P_TXT_FIX) = '000016' OR
            TRIM(P_TXT_FIX) = '000017' OR
            TRIM(P_TXT_FIX) = '000018' OR
            TRIM(P_TXT_FIX) = '000019' OR
            TRIM(P_TXT_FIX) = '000020' OR
            TRIM(P_TXT_FIX) = '000021' OR
            TRIM(P_TXT_FIX) = '000022' OR
            TRIM(P_TXT_FIX) = '000023' OR
            TRIM(P_TXT_FIX) = '000024' OR
            TRIM(P_TXT_FIX) = '000025' OR
            TRIM(P_TXT_FIX) = '000026' OR
            TRIM(P_TXT_FIX) = '000027' OR
            TRIM(P_TXT_FIX) = '000028' OR
            TRIM(P_TXT_FIX) = '000029'
       THEN
       --
         vTagApp := vTagPreparazione;
       --
       WHEN TRIM(P_TXT_FIX) = '000001' --OR
            --TRIM(P_TXT_FIX) = '000030' OR
            --TRIM(P_TXT_FIX) = '000031'
       THEN
       --
         vTagApp := vTagAvvertenza;
       --
       WHEN TRIM(P_TXT_FIX) = '000002' OR
            TRIM(P_TXT_FIX) = '000040' OR
            TRIM(P_TXT_FIX) = '000061'
       THEN
       --
         vTagApp := vTagConservazione;
       --
       WHEN TRIM(P_TXT_FIX) = '000003' OR
            TRIM(P_TXT_FIX) = '000050' OR
            TRIM(P_TXT_FIX) = '000051' OR
            TRIM(P_TXT_FIX) = '000052'
       THEN
         vTagApp := vTagFeatures;
       ELSE
         vTagApp := NULL;
     END CASE;
     --
     IF vTagApp IS NOT NULL THEN
       INS_DATI_AGG (P_TAG => vTagApp, P_ART=> P_ART, P_TESTO=> P_TESTO);
     --
       vTagApp := NULL;
     END IF;
     --Eccezioni
     --
     IF TRIM(P_TXT_FIX) = '000030' THEN
     --000002  +  000001
       INS_DATI_AGG (P_TAG => vTagConservazione, P_ART=> P_ART, P_TESTO=> GET_TXT_FISSO('000002'));
       --
       INS_DATI_AGG (P_TAG => vTagAvvertenza, P_ART=> P_ART, P_TESTO=> GET_TXT_FISSO('000001'));
       --
     END IF;
     --
     IF TRIM(P_TXT_FIX) = '000031' THEN
     --000002 +  vTxtPrdDaLav da lavare + Conservare In Frigo da 0��C a +4��C - Prodotto da lavare
       INS_DATI_AGG (P_TAG => vTagConservazione, P_ART=> P_ART, P_TESTO=> GET_TXT_FISSO('000002'));
       --
       INS_DATI_AGG (P_TAG => vTagAvvertenza, P_ART=> P_ART, P_TESTO=> 'Prodotto da lavare');
       --
     END IF;
     --
   EXCEPTION
     WHEN OTHERS THEN
     NULL;
   END INS_TXT_FISSI;
   --
  BEGIN
    --
    vProcName := 'REFRESH_DATI_AGG';
    --
    vPasso := 'LOOP_ART_BILAN';
    FOR REC IN (SELECT BIL.BLCART,
                       TRIM (BLXI01
                          || BLXI02
                          || BLXI03
                          || BLXI04
                          || BLXI05
                          || BLXI06
                          || BLXI07
                          || BLXI08
                          || BLXI09
                          || BLXI10
                          || BLXI11
                          || BLXI12
                          || BLXI13
                          || BLXI14
                          || BLXI15
                          || BLXI16
                          || BLXI17
                          || BLXI18
                          || BLXI19
                          || BLXI20) Ingredienti,
                          BIL.BLCXF1,
                          BIL.BLCXF2,
                          BIL.BLCXF3,
                          BIL.BLCXF4
                   FROM FRAME.MG_AS_BILAN00F BIL, MG.GEAD010F ANG
                  WHERE BIL.BLCART = ANG.E1CART
                    AND ANG.E1SITO = 'S'
                    AND NOT EXISTS(SELECT 0   --Escludo articoli multifornitore.
                                    FROM (SELECT BLCART
                                            FROM FRAME.MG_AS_BILAN00F BIL
                                           WHERE TRIM (BLFANN) IS NULL
                                           GROUP BY BLCART
                                            HAVING COUNT (0) > 1
                                          ) FIL
                                    WHERE FIL.BLCART = BIL.BLCART)
                   --AND BIL.BLCART = '20644'
                   )
    LOOP
    --
      vPasso:= 'LOOP_BI_ART';
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'Art:'||rec.BLCART , vPasso);
      vIngredienti := NULL;
      --
      IF TRIM (rec.Ingredienti) IS NOT NULL THEN
        vIngredienti := REPLACE (rec.Ingredienti, 'Ingredienti:', '');
        vIngredienti := REPLACE (vIngredienti, 'INGREDIENTI:', '');
        vIngredienti := REPLACE (vIngredienti, '"ING:', '');
        --
        --Insert Ingrediente
        vPasso := 'INS_INGR';
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'Ingredienti:'||vIngredienti , vPasso);
        INS_DATI_AGG (P_TAG => vTagIngredienti, P_ART=>rec.BLCART, P_TESTO=> vIngredienti);
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'INFO:'||SQLERRM , vPasso);
       END IF;
       --
       IF TRIM(rec.BLCXF1) IS NOT NULL THEN
       --
         vPasso:='BLCXF1';
         INS_TXT_FISSI(P_TXT_FIX => rec.BLCXF1, P_ART => REC.BLCART, P_TESTO => GET_TXT_FISSO(rec.BLCXF1));
         BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'INFO:'||SQLERRM , vPasso);
       END IF;
       --
       IF TRIM(rec.BLCXF2) IS NOT NULL THEN
       --
         vPasso := 'BLCXF2';
         INS_TXT_FISSI(P_TXT_FIX => rec.BLCXF2, P_ART => REC.BLCART, P_TESTO => GET_TXT_FISSO(rec.BLCXF2));
         BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'INFO:'||SQLERRM , vPasso);
       END IF;
       --
       IF TRIM(rec.BLCXF3) IS NOT NULL THEN
       --
         vPasso := 'BLCXF3';
         INS_TXT_FISSI(P_TXT_FIX => rec.BLCXF3, P_ART => REC.BLCART, P_TESTO => GET_TXT_FISSO(rec.BLCXF3));
         BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'INFO:'||SQLERRM , vPasso);
       END IF;
       --
       IF TRIM(rec.BLCXF4) IS NOT NULL THEN
       --
         vPasso := 'BLCXF4';
         INS_TXT_FISSI(P_TXT_FIX => rec.BLCXF4, P_ART => REC.BLCART, P_TESTO => GET_TXT_FISSO(rec.BLCXF4));
         BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'INFO:'||SQLERRM , vPasso);
       END IF;
       --
    END LOOP;
    --
    IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
  --
  EXCEPTION
    WHEN erroreGestito THEN
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END REFRESH_DATI_AGG;
  --
  PROCEDURE CLEAN_TORDER(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2
  )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_CLICK_COLLECT_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    timeStart           PLS_INTEGER;
    vNomeProc           VARCHAR2(100);
  BEGIN
    --
    vProcName := 'CLEAN_TORDER';
    --
    vNomeProc := kNomePackage||'.'||vProcName;
    --
    vPasso := 'DEL(T_ORDERDETAILS)';
    DELETE CAC.T_ORDERDETAILS D 
     WHERE EXISTS (SELECT 0 
                     FROM CAC.T_ORDERMASTER M
                    WHERE M.ID = D.ORDER_ID
                      AND ORDER_STATUS = 3); --Stato Pronto
    BASE_LOG_PCK.WriteLog(vNomeProc, vPasso ||'Rows:'||SQL%ROWCOUNT, BASE_LOG_PCK.Info);
    --
    vPasso := 'DEL(T_ORDERDETAILS)';
    DELETE CAC.T_ORDERMASTER
     WHERE ORDER_STATUS = 3;  --Stato Pronto
    BASE_LOG_PCK.WriteLog(vNomeProc, vPasso ||'Rows:'||SQL%ROWCOUNT, BASE_LOG_PCK.Info);
  --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END CLEAN_TORDER;
  --
  PROCEDURE REFRESH_ALL(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_PDV_COD          IN     NUMBER  DEFAULT NULL
  )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_CLICK_COLLECT_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    timeStart           PLS_INTEGER;
    vNomeProc           VARCHAR2(100);
  BEGIN
    --
    vProcName := 'REFRESH_ALL';
    --
    vNomeProc := kNomePackage||'.'||vProcName;
    --
    timeStart := dbms_utility.get_time;
    --
    vPasso := 'START';
    BASE_LOG_PCK.WriteLog(vNomeProc, vPasso, BASE_LOG_PCK.Info);
    --
    vPasso := 'CLEAN_TORDER';
    CLEAN_TORDER(
      PO_ERR_SEVERITA => PO_ERR_SEVERITA,
      PO_ERR_CODICE   => PO_ERR_CODICE,
      PO_MESSAGGIO    => PO_MESSAGGIO
      );
    BASE_LOG_PCK.WriteLog(vNomeProc, vPasso||' - TimeElab (min.): '|| (dbms_utility.get_time - timeStart)/100/60, BASE_LOG_PCK.Info);
    --
    IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
    --
    vPasso := 'REFRESH_ASRT_FULL';
    REFRESH_ASRT_FULL(
      PO_ERR_SEVERITA    => PO_ERR_SEVERITA,
      PO_ERR_CODICE      => PO_ERR_CODICE,
      PO_MESSAGGIO       => PO_MESSAGGIO
    );
    --
    BASE_LOG_PCK.WriteLog(vNomeProc, vPasso||' - TimeElab (min.): '|| (dbms_utility.get_time - timeStart)/100/60, BASE_LOG_PCK.Info);
    --
    IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
    --
    vPasso := 'REFRESH_PRZ_FULL'; 
    REFRESH_PRZ_FULL(
        PO_ERR_SEVERITA => PO_ERR_SEVERITA,
        PO_ERR_CODICE   => PO_ERR_CODICE,
        PO_MESSAGGIO    => PO_MESSAGGIO,
        P_PDV_COD       => P_PDV_COD 
        );
    BASE_LOG_PCK.WriteLog(vNomeProc, vPasso||' - TimeElab (min.): '|| (dbms_utility.get_time - timeStart)/100/60, BASE_LOG_PCK.Info);
    --
    IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
    --
    vPasso := 'END';
    BASE_LOG_PCK.WriteLog(vNomeProc, vPasso, BASE_LOG_PCK.Info);
  --
  EXCEPTION
    WHEN erroreGestito THEN
       PO_ERR_SEVERITA := 'E';
       PO_ERR_CODICE   := SQLCODE;
       PO_MESSAGGIO    :=  'ERRORE - vPasso:'|| vPasso ||' - '||PO_MESSAGGIO;
       BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, PO_MESSAGGIO , vPasso);
       BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END REFRESH_ALL;
  --
  PROCEDURE REFRESH_ALL_OLD(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_PDV_COD          IN     NUMBER  DEFAULT NULL
  )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_CLICK_COLLECT_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    timeStart           PLS_INTEGER;
    vNomeProc           VARCHAR2(100);
  BEGIN
    --
    vProcName := 'REFRESH_ALL';
    --
    vNomeProc := kNomePackage||'.'||vProcName;
    --
    timeStart := dbms_utility.get_time;
    --
    vPasso := 'START';
    BASE_LOG_PCK.WriteLog(vNomeProc, vPasso, BASE_LOG_PCK.Info);
    --
    vPasso := 'REFRESH_ASRT';
    REFRESH_ASRT(
      PO_ERR_SEVERITA => PO_ERR_SEVERITA,
      PO_ERR_CODICE   => PO_ERR_CODICE,
      PO_MESSAGGIO    => PO_MESSAGGIO,
      P_PDV_COD       => P_PDV_COD);
    --
    BASE_LOG_PCK.WriteLog(vNomeProc, vPasso||' - TimeElab (min.): '|| (dbms_utility.get_time - timeStart)/100/60, BASE_LOG_PCK.Info);
    --
    IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
    --
    vPasso := 'REFRESH_PRZ';
    REFRESH_PRZ(
      PO_ERR_SEVERITA => PO_ERR_SEVERITA,
      PO_ERR_CODICE   => PO_ERR_CODICE,
      PO_MESSAGGIO    => PO_MESSAGGIO,
      P_PDV_COD       => P_PDV_COD);
    --
    BASE_LOG_PCK.WriteLog(vNomeProc, vPasso||' - TimeElab (min.): '|| (dbms_utility.get_time - timeStart)/100/60, BASE_LOG_PCK.Info);
    --
    IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
    vPasso := 'REFRESH_DATI_AGG';
    REFRESH_DATI_AGG(
         PO_ERR_SEVERITA => PO_ERR_SEVERITA,
         PO_ERR_CODICE   => PO_ERR_CODICE,
         PO_MESSAGGIO    => PO_MESSAGGIO
     );
    BASE_LOG_PCK.WriteLog(vNomeProc, vPasso||' - TimeElab (min.): '|| (dbms_utility.get_time - timeStart)/100/60, BASE_LOG_PCK.Info);
    --
    IF PO_MESSAGGIO IS NOT NULL THEN RAISE erroreGestito; END IF;
    vPasso := 'REFRESH_DICT_SITO';
    REFRESH_DICT_SITO(
         PO_ERR_SEVERITA => PO_ERR_SEVERITA,
         PO_ERR_CODICE   => PO_ERR_CODICE,
         PO_MESSAGGIO    => PO_MESSAGGIO
     );
    BASE_LOG_PCK.WriteLog(vNomeProc, vPasso||' - TimeElab (min.): '|| (dbms_utility.get_time - timeStart)/100/60, BASE_LOG_PCK.Info);
    --
    vPasso := 'END';
    BASE_LOG_PCK.WriteLog(vNomeProc, vPasso, BASE_LOG_PCK.Info);
  --
  EXCEPTION
    WHEN erroreGestito THEN
       PO_ERR_SEVERITA := 'E';
       PO_ERR_CODICE   := SQLCODE;
       PO_MESSAGGIO    :=  'ERRORE - vPasso:'|| vPasso ||' - '||PO_MESSAGGIO;
       BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, PO_MESSAGGIO , vPasso);
       BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END REFRESH_ALL_OLD;
  --
  FUNCTION SPLIT_WORD(
      P_LIST                 IN VARCHAR2,
      P_DELIMITER            IN VARCHAR2 DEFAULT ','
  ) RETURN SPLIT_TBL
  AS
    SPLITTED SPLIT_TBL := SPLIT_TBL();
    I PLS_INTEGER := 0;
    LIST_ VARCHAR2(32767) := P_LIST;
  BEGIN
    LOOP
      I := INSTR(LIST_, P_DELIMITER);
      IF I > 0 THEN
        SPLITTED.EXTEND(1);
        SPLITTED(SPLITTED.LAST) := SUBSTR(LIST_, 1, I - 1);
        LIST_ := SUBSTR(LIST_, I + LENGTH(P_DELIMITER));
      ELSE
        SPLITTED.EXTEND(1);
        SPLITTED(SPLITTED.LAST) := LIST_;
        RETURN SPLITTED;
      END IF;
    END LOOP;
  END SPLIT_WORD;
  --
  PROCEDURE REFRESH_DICT_SITO(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2
  )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_CLICK_COLLECT_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    vGot                SPLIT_TBL   := SPLIT_TBL();
    vGotApp             SPLIT_TBL   := SPLIT_TBL();
    v_str               VARCHAR2(4000);
    v_str_app           VARCHAR2(4000);
    v_str_app_brand     VARCHAR2(4000); --Classificazione merc 3 livello sito + brand
    vCntWord            NUMBER;
  BEGIN
    --
    vProcName := 'REFRESH_DICT_SITO';
    --
    DELETE MG_DICTONARY_SITO;
    --
    COMMIT;
    --
    FOR REC IN (
              SELECT ANG.E1CART,
                     ANG.E1XARC,
                     ANG.E1CS3D,
                     ANG.E1BRAND
                FROM MG.GEAD010F ANG
               WHERE TRIM (ANG.E1STAT) IS NULL
                 AND ANG.E1SITO = 'S'
                 AND MG.MG_CLICK_COLLECT_PCK.GET_INFO_FOTO (ANG.E1CART) = 1
                 AND EXISTS(SELECT 0
                              FROM MG.GEAD030F PDV,
                                   MG.MG_PDV_REL_SERVICE PVSER
                             WHERE PDV.E3CPDV = PVSER_PDV_REL_PDV_ID
                               AND PDV.E3CART = ANG.E1CART
                               AND PDV.E3FSIT = 'S'
                              AND TRIM(PDV.E3STAT) IS NULL
                              AND PVSER.PVSER_SERVIZIO IN (kServiceDRIVE, kServiceCASA, kServiceSTORE)
                            )
                )
    LOOP
      vPasso := rec.E1CART;
      vGot := split_word(rec.E1XARC,' ');
      vGotApp := split_tbl();
      v_str := NULL;
      FOR i IN vGot.FIRST .. vGot.LAST
      LOOP
         --dbms_output.put_line (vGot(i));
         --dbms_output.put_line (LENGTH(TRIM(vGot(i))));
        IF LENGTH(TRIM(vGot(i))) > 3 THEN --Parole pi��� lunghe di 3 char
        --
          v_str_app := NULL;
          --Cleaning word
          v_str_app := UPPER(vGot(i));
          --dbms_output.put_line (v_str_app);
          v_str_app := TRANSLATE(v_str_app,'+.0123456789',' ');
          --dbms_output.put_line('OUT:'||v_str_app);
          v_str_app := REPLACE(v_str_app,',','');
          v_str_app := REPLACE(v_str_app,'(','');
          v_str_app := REPLACE(v_str_app,')','');
          v_str_app := REPLACE(v_str_app,'&','');
          v_str_app := REPLACE(v_str_app,'!','');
          v_str_app := REPLACE(v_str_app,'%','');
          v_str_app := REPLACE(v_str_app,'"','');
          v_str_app := REPLACE(v_str_app,'*','');
          --dbms_output.put_line (v_str_app);
          --
          IF LENGTH(v_str_app) > 3 THEN
            --
            vCntWord := 0;
            --
            select COUNT(0)
              into vCntWord
              from MG_DICTONARY_SITO
             where WORD = UPPER(v_str_app);
            --
            IF vCntWord = 0 THEN
            --
              INSERT INTO MG_DICTONARY_SITO(WORD)
              VALUES(UPPER(v_str_app));
            END IF;
            --
            vGotApp.EXTEND(1);
            vGotApp(vGotApp.LAST)  := v_str_app;
          END IF;
        END IF;
      --
      END LOOP;
      --
      --dbms_output.put_line ('eccomi'||vGotApp.COUNT);
      --
      IF vGotApp.COUNT < 4 AND vGotApp.COUNT > 0 THEN
      --dbms_output.put_line ('3');
        FOR y IN vGotApp.FIRST .. vGotApp.LAST
        LOOP
        --
          v_str :=  vGotApp(y)||' '|| v_str ;
        END LOOP;
        --
        vCntWord := 0;
        --
        select COUNT(0)
          into vCntWord
          from MG_DICTONARY_SITO
         where WORD = UPPER(v_str_app);
        --
        IF vCntWord = 0 THEN
          INSERT INTO MG_DICTONARY_SITO(WORD)
          VALUES(LTRIM(UPPER(v_str)));
        END IF;
      END IF;
      --
      --classificazione merceologica + Brand
      IF TRIM(rec.E1BRAND) IS NOT NULL AND TRIM(rec.E1CS3D) IS NOT NULL THEN
      --
        v_str_app_brand :=  rec.E1CS3D ||' '|| rec.E1BRAND;
        --
        vCntWord := 0;
        --
        select COUNT(0)
          into vCntWord
          from MG_DICTONARY_SITO
         where WORD = UPPER(v_str_app_brand);
         --
         IF vCntWord = 0 THEN
         --
           INSERT INTO MG_DICTONARY_SITO(WORD)
           VALUES(UPPER(v_str_app_brand));
         END IF;
      END IF;
    --
    END LOOP;
    --
    DELETE DATIAGGIUNTIVI
     WHERE E7KYAM = 'DICT_SITO';
    --
    INSERT INTO DATIAGGIUNTIVI(
               E7KYAM,
               E7KY01,
               E7KY02,
               E7KY03,
               E7KY04,
               E7KY05,
               E7KYCI,
               E7INFO)
    SELECT DISTINCT 'DICT_SITO' E7KYAM,
                         ROWNUM E7KY01,
                         ' '    E7KY02,
                         ' '    E7KY03,
                         ' '    E7KY04,
                         ' '    E7KY05,
                         10     E7KYCI,
                         WORD AS E7INFO
      FROM MG_DICTONARY_SITO;
    --
  --
  EXCEPTION
    WHEN erroreGestito THEN
       PO_ERR_SEVERITA := 'E';
       PO_ERR_CODICE   := SQLCODE;
       PO_MESSAGGIO    :=  'ERRORE - vPasso:'|| vPasso ||' - '||PO_MESSAGGIO;
       BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, PO_MESSAGGIO , vPasso);
       BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END REFRESH_DICT_SITO;
  --
  PROCEDURE ELABORA_TIPI_ATTRIBUTI(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_TIPO_ATTR        IN            VARCHAR2
  )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_CLICK_COLLECT_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    kArticoli           CONSTANT VARCHAR2(25) := 'ARTICOLI';
    kClickCollect       CONSTANT VARCHAR2(25) := 'CLICK_COLLECT';
    kAttrGen            CONSTANT VARCHAR2(25) := 'ATTR_GEN';
    kUmUnitVend         CONSTANT VARCHAR2(25) := 'UM_ORDINE';
    kUnitVend           CONSTANT VARCHAR2(25) := 'UNITA_VEND';
    kValMedio           CONSTANT VARCHAR2(25) := 'VAL_MEDIO';
    kOffsetScost        CONSTANT VARCHAR2(25) := 'OFFSET_SCOST';
    vFlagCnt            NUMBER;
    vRowsElab           NUMBER := 0;
  BEGIN
    --
    vProcName := 'ELABORA_TIPI_ATTRIBUTI';
    --
    IF P_TIPO_ATTR = kAttrGen THEN
    --
      FOR rec IN (SELECT ARTA.ROWID, ARTA.ART_CODICE, TATTR_CODICE
                    FROM MG_ARTICOLI_REL_TATTR ARTA
                   WHERE ARTA.IMPORTED = 'NO'
                 )
      LOOP
      --
        vRowsElab := vRowsElab + 1; 
        vPasso:= 'CHECK_DATA';
        SELECT COUNT(0)
          INTO vFlagCnt
          FROM DATIAGGIUNTIVI
         WHERE E7KYAM = kArticoli
           AND E7KY01 = rec.ART_CODICE
           AND E7KY02 = kClickCollect
           AND E7KY03 = kAttrGen;
        BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, vPasso ||'-Art:'||rec.ART_CODICE,BASE_LOG_PCK.DEBUG);
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'vFlagCnt:'||vFlagCnt, vPasso);
        --
        IF vFlagCnt> 0 THEN
        --
          vPasso := 'DEL';
          DELETE MG.DATIAGGIUNTIVI
           WHERE E7KYAM = kArticoli
             AND E7KY01 = rec.ART_CODICE
             AND E7KY02 = kClickCollect
             AND E7KY03 = kAttrGen;
          BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, vPasso ||'-Art:'||rec.ART_CODICE||' - Rows:'||SQL%ROWCOUNT,BASE_LOG_PCK.DEBUG);
          BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'DEL:'||SQL%ROWCOUNT, vPasso);
        END IF;
        --
        vPasso := 'INS';
        INSERT INTO MG.DATIAGGIUNTIVI (E7KYAM,
                                       E7KY01,
                                       E7KY02,
                                       E7KY03,
                                       E7KY04,
                                       E7KY05,
                                       E7KYCI,
                                       E7INFO)
           SELECT kArticoli,
                  rec.ART_CODICE,
                  kClickCollect,
                  kAttrGen,
                  ' ',
                  ' ',
                  TATTR.TATTR_CODICE,
                  TATTR.TATTR_DESCRIZIONE
             FROM MG.MG_TIPI_ATTRIBUTI TATTR
            WHERE TATTR_CODICE = rec.TATTR_CODICE;
        BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, vPasso ||'-Art:'||rec.ART_CODICE||' - Rows:'||SQL%ROWCOUNT,BASE_LOG_PCK.DEBUG);
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'INS:'||SQL%ROWCOUNT, vPasso);
        --
        vPasso := 'UPDT';
        EXECUTE IMMEDIATE 'ALTER TRIGGER MG_ARTICOLI_REL_TATTR_BIU DISABLE';
        UPDATE MG_ARTICOLI_REL_TATTR ARTA
           SET IMPORTED = 'YES'
         WHERE ARTA.ROWID = rec.ROWID;
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'UPDT:'||SQL%ROWCOUNT, vPasso);
        BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, vPasso ||'-Art:'||rec.ART_CODICE||' - Rows:'||SQL%ROWCOUNT,BASE_LOG_PCK.DEBUG);
      --
      END LOOP;
    --
      PO_MESSAGGIO := 'Elaborazione terminata correttamente. [Righe Elaborate:'||vRowsElab||']';
      COMMIT;
      EXECUTE IMMEDIATE 'ALTER TRIGGER MG_ARTICOLI_REL_TATTR_BIU ENABLE';
    END IF;
    --
    IF P_TIPO_ATTR = kUnitVend THEN
    --
      FOR rec IN (SELECT TUM.ROWID, TUM.ART_CODICE, TUM_UNITA_MISURA_COD, ATUM_VALORE
                    FROM MG_ARTICOLI_REL_TUM TUM
                   WHERE TUM.IMPORTED = 'NO'
                 )
      LOOP
      --
        vRowsElab := vRowsElab + 1; 
        vPasso:= 'CHECK_DATA1';
        SELECT COUNT(0)
          INTO vFlagCnt
          FROM DATIAGGIUNTIVI
         WHERE E7KYAM = kArticoli
           AND E7KY01 = rec.ART_CODICE
           AND E7KY02 = kClickCollect
           AND E7KY03 = kUmUnitVend;
        BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, vPasso ||'-Art:'||rec.ART_CODICE,BASE_LOG_PCK.DEBUG);
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'vFlagCnt:'||vFlagCnt, vPasso);
        --
        IF vFlagCnt> 0 THEN
        --
          vPasso := 'DEL';
          DELETE MG.DATIAGGIUNTIVI
           WHERE E7KYAM = kArticoli
             AND E7KY01 = rec.ART_CODICE
             AND E7KY02 = kClickCollect
             AND E7KY03 = kUmUnitVend;
          BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, vPasso ||'-Art:'||rec.ART_CODICE||' - Rows:'||SQL%ROWCOUNT,BASE_LOG_PCK.DEBUG);
          BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'DEL:'||SQL%ROWCOUNT, vPasso);
        END IF;
        --
        vPasso := 'INS';
        INSERT INTO MG.DATIAGGIUNTIVI (E7KYAM,
                                       E7KY01,
                                       E7KY02,
                                       E7KY03,
                                       E7KY04,
                                       E7KY05,
                                       E7KYCI,
                                       E7INFO)
           SELECT kArticoli,
                  rec.ART_CODICE,
                  kClickCollect,
                  kUmUnitVend,
                  ' ',
                  ' ',
                  TUM_UNITA_MISURA_COD,
                  TUM_DESCRIZIONE
             FROM MG_TIPI_UNITA_MISURA
            WHERE TUM_UNITA_MISURA_COD = rec.TUM_UNITA_MISURA_COD;
        BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, vPasso ||'-Art:'||rec.ART_CODICE||' - Rows:'||SQL%ROWCOUNT,BASE_LOG_PCK.DEBUG);
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'INS:'||SQL%ROWCOUNT, vPasso);
        --
        vPasso:= 'CHECK_DATA2';
        SELECT COUNT(0)
          INTO vFlagCnt
          FROM DATIAGGIUNTIVI
         WHERE E7KYAM = kArticoli
           AND E7KY01 = rec.ART_CODICE
           AND E7KY02 = kClickCollect
           AND E7KY03 IN (kValMedio, kOffsetScost);
        BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, vPasso ||'-Art:'||rec.ART_CODICE,BASE_LOG_PCK.DEBUG);
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'vFlagCnt:'||vFlagCnt, vPasso);
        --
        IF vFlagCnt> 0 THEN
        --
          vPasso := 'DEL';
          DELETE MG.DATIAGGIUNTIVI
           WHERE E7KYAM = kArticoli
             AND E7KY01 = rec.ART_CODICE
             AND E7KY02 = kClickCollect
             AND E7KY03 IN (kValMedio, kOffsetScost);
          BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, vPasso ||'-Art:'||rec.ART_CODICE||' - Rows:'||SQL%ROWCOUNT,BASE_LOG_PCK.DEBUG);
          BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'DEL:'||SQL%ROWCOUNT, vPasso);
        END IF;
        --
        vPasso := 'INS';
        INSERT INTO MG.DATIAGGIUNTIVI (E7KYAM,
                                       E7KY01,
                                       E7KY02,
                                       E7KY03,
                                       E7KY04,
                                       E7KY05,
                                       E7KYCI,
                                       E7INFO)
           SELECT kArticoli,
                  rec.ART_CODICE,
                  kClickCollect,
                  DECODE(TUM_UNITA_MISURA_COD,'GR_PZ','VAL_MEDIO','PZKG','VAL_MEDIO','OFFSET_SCOST'),
                  ' ',
                  ' ',
                  ATUM_VALORE,
                  ATUM_VALORE
             FROM MG.MG_ARTICOLI_REL_TUM ATUM
            WHERE ATUM.ART_CODICE = rec.ART_CODICE;
        BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, vPasso ||'-Art:'||rec.ART_CODICE||' - Rows:'||SQL%ROWCOUNT,BASE_LOG_PCK.DEBUG);
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'INS:'||SQL%ROWCOUNT, vPasso);
        --
        vPasso := 'UPDT';
        EXECUTE IMMEDIATE 'ALTER TRIGGER MG_ARTICOLI_REL_TUM_BIU DISABLE';
        UPDATE MG_ARTICOLI_REL_TUM ATUM
           SET IMPORTED = 'YES'
         WHERE ATUM.ROWID = rec.ROWID;
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'UPDT:'||SQL%ROWCOUNT, vPasso);
        BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, vPasso ||'-Art:'||rec.ART_CODICE||' - Rows:'||SQL%ROWCOUNT,BASE_LOG_PCK.DEBUG);
      --
      END LOOP;
      --
      PO_MESSAGGIO := 'Elaborazione terminata correttamente... [Righe Elaborate:'||vRowsElab||']';
      COMMIT;
      EXECUTE IMMEDIATE 'ALTER TRIGGER MG_ARTICOLI_REL_TUM_BIU ENABLE';
    END IF;
    --
    apex_application.g_print_success_message := '<span style="color: white;">This is a Message.</span>';
    --
  EXCEPTION
    WHEN erroreGestito THEN
       PO_ERR_SEVERITA := 'E';
       PO_ERR_CODICE   := SQLCODE;
       PO_MESSAGGIO    :=  'ERRORE - vPasso:'|| vPasso ||' - '||PO_MESSAGGIO;
       BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, PO_MESSAGGIO , vPasso);
       BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END ELABORA_TIPI_ATTRIBUTI;
  --
  PROCEDURE COPY_SERVICE_BY_PDV(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_PDV_COD_COPY     IN            VARCHAR2,
      P_SERVICE          IN            VARCHAR2,
      P_PDV_COD          IN            VARCHAR2,
      P_IP               IN            VARCHAR2,
      P_EMAIL            IN            VARCHAR2
  )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_CLICK_COLLECT_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    vCnt                NUMBER;
  BEGIN
    --
    vProcName := 'COPY_SERVICE_BY_PDV';
    --
    IF P_PDV_COD_COPY IS NULL THEN
      --
      apex_error.add_error (
        p_message          => 'Specificare un PDV da copiare',
        p_display_location =>  apex_error.c_inline_with_field_and_notif,
        p_page_item_name   => 'P31_PDV_COD_COPY');
    END IF;
    --
    IF P_PDV_COD IS NULL THEN
      --
      apex_error.add_error (
        p_message          => 'Specificare un PDV',
        p_display_location =>  apex_error.c_inline_with_field_and_notif,
        p_page_item_name   => 'P31_PDV_COD');
    END IF;
    --
    IF P_SERVICE IS NULL THEN
      --
      apex_error.add_error (
        p_message          => 'Specificare il servizio da copiare',
        p_display_location =>  apex_error.c_inline_with_field_and_notif,
        p_page_item_name   => 'P31_SERVICE');
    END IF;
    --
    IF P_IP IS NULL THEN
      --
      apex_error.add_error (
        p_message          => 'Specificare l''IP della stampante',
        p_display_location =>  apex_error.c_inline_with_field_and_notif,
        p_page_item_name   => 'P31_IP');
    END IF;
    --
    IF P_EMAIL IS NULL THEN
      --
      apex_error.add_error (
        p_message          => 'Specificare l''email su cui arriveranno le email degli ordini',
        p_display_location =>  apex_error.c_inline_with_field_and_notif,
        p_page_item_name   => 'P31_EMAIL');
    END IF;
    --
    vPasso := 'T_NEGOZI';
    SELECT COUNT(0)
      INTO vCnt
      FROM CAC.T_NEGOZI
     WHERE ID =P_PDV_COD;
  --
  BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'num:'||vCnt ,vPasso);
  IF vCnt = 0 THEN
    INSERT INTO CAC.T_NEGOZI(ID, COD_NEGOZIO, DESC_NEGOZIO)
    SELECT F.E2CPDV, F.E2CPDV, F.E2RAGS
      FROM MG.GEAD020F F
     WHERE F.E2CPDV = P_PDV_COD;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'FINE rows:'||SQL%ROWCOUNT ,vPasso);
  END IF;
  --
  vPasso := 'D_MINIMIORDINE';
  SELECT COUNT(0)
    INTO vCnt
    FROM CAC.D_MINIMIORDINE
   WHERE STORE_ID =P_PDV_COD
     AND DELIVERY_PID =P_SERVICE;
  --
  BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'num:'||vCnt ,vPasso);
  --
  IF vCnt = 0 THEN
    vPasso := 'insert -D_MINIMIORDINE';
    INSERT INTO CAC.D_MINIMIORDINE(STORE_ID, DELIVERY_PID, VALORE, FLG_STATE)
    SELECT P_PDV_COD STORE_ID, P_SERVICE DELIVERY_PID, 30 VALORE, 0 FLG_STATE
      FROM DUAL;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'FINE rows:'||SQL%ROWCOUNT ,vPasso);
  END IF;
  
  vPasso:= 'D_SOGLIESERVIZIO';
  SELECT COUNT(0)
    INTO vCnt
    FROM CAC.D_SOGLIESERVIZIO
   WHERE STORE_ID =P_PDV_COD
     AND DELIVERY_PID =P_SERVICE;
  --
  BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'num:'||vCnt ,vPasso);
  --
  IF vCnt = 0 THEN
  --
    INSERT INTO CAC.D_SOGLIESERVIZIO (ID, STORE_ID, DELIVERY_PID, SOGLIA1, SOGLIA2, BCODE, BCODE_GOLD)
    SELECT (SELECT MAX(ID)FROM CAC.D_SOGLIESERVIZIO) +ROWNUM ID, P_PDV_COD STORE_ID, DELIVERY_PID, SOGLIA1, SOGLIA2, BCODE, BCODE_GOLD
      FROM CAC.D_SOGLIESERVIZIO S
     WHERE S.STORE_ID = P_PDV_COD_COPY
       AND s.DELIVERY_PID = P_SERVICE;
    --
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'FINE rows:'||SQL%ROWCOUNT ,vPasso);
  END IF;
  --
  IF P_SERVICE IN (11,12) THEN
    vPasso:= 'D_ZONESERVITE';
    SELECT COUNT(0)
      INTO vCnt
      FROM CAC.D_ZONESERVITE
     WHERE STORE_ID =P_PDV_COD
       AND DELIVERY_PID =P_SERVICE;
    --
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'num:'||vCnt ,vPasso);
    --
    IF vCnt = 0 THEN
    --
      INSERT INTO CAC.D_ZONESERVITE(STORE_ID, DELIVERY_PID,CAP, BCODE, BCODE_GOLD)
      SELECT F2.E2CPDV STORE_ID, DELIVERY_PID, F2.E2CAVP CAP, BCODE, BCODE_GOLD
        FROM CAC.D_ZONESERVITE S,
             MG.GEAD020F F,
             MG.GEAD020F F2
       WHERE S.STORE_ID = P_PDV_COD_COPY
         AND S.DELIVERY_PID = P_SERVICE
         --AND S.CAP      = F.E2CAP
         AND S.STORE_ID = F.E2CPDV
         AND F2.E2CPDV = P_PDV_COD;
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'FINE rows:'||SQL%ROWCOUNT ,vPasso); 
    END IF;
  END IF;
  --
  vPasso := 'DELIVERY_STORE';
  SELECT COUNT(0)
    INTO vCnt
    FROM CAC.DELIVERY_STORE
   WHERE STORE_ID =P_PDV_COD
     AND DELIVERY_PID =P_SERVICE;
  --
  BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'num:'||vCnt ,vPasso);
  --
  IF vCnt = 0 THEN
  --
    INSERT INTO CAC.DELIVERY_STORE( DELIVERY_PID, STORE_ID, STORE_HOST_CODE, SLOT_NO, SLOT_NAME, TIME_START, TIME_END, MAX_NO, DATE_START_VALIDITY, OFFSET_PICKING, OFFSET_RESERVE, RESERVATION_DAYS, FLG_ENABLE_LABELING, FLG_STATE)
    SELECT DELIVERY_PID, P_PDV_COD STORE_ID, STORE_HOST_CODE, SLOT_NO, SLOT_NAME, TIME_START, TIME_END, MAX_NO, DATE_START_VALIDITY, OFFSET_PICKING, OFFSET_RESERVE, RESERVATION_DAYS, FLG_ENABLE_LABELING,1
      FROM CAC.DELIVERY_STORE S
     WHERE S.STORE_ID = P_PDV_COD_COPY
       AND S.DELIVERY_PID = P_SERVICE ;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'FINE rows:'||SQL%ROWCOUNT ,vPasso);
  END IF; 
  --
  vPasso := 'S_STAMPANTI';
  SELECT COUNT(0)
    INTO vCnt
    FROM CAC.S_STAMPANTI
   WHERE NEGOZIO_ID =P_PDV_COD;
  --
  BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'num:'||vCnt ,vPasso);
  --
  IF vCnt = 0 THEN
  --
    INSERT INTO CAC.S_STAMPANTI(NEGOZIO_ID, IPADDR, DESCRIZIONE, STATO)
         SELECT P_PDV_COD, P_IP, (SELECT F.E2RAGS FROM MG.GEAD020F F WHERE F.E2CPDV = P_PDV_COD)  , STATO
           FROM CAC.S_STAMPANTI S
          WHERE S.NEGOZIO_ID = P_PDV_COD_COPY;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'FINE rows:'||SQL%ROWCOUNT ,vPasso);
  END IF;
  --
  vPasso := 'T_CATEGORIE_ORDER';
  SELECT COUNT(0)
    INTO vCnt
    FROM CAC.T_CATEGORIE_ORDER
   WHERE ID_NEGOZIO =P_PDV_COD;
  --
  BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'num:'||vCnt ,vPasso);
  --
  IF vCnt = 0 THEN
  --
    INSERT INTO CAC.T_CATEGORIE_ORDER(ID_NEGOZIO, ID_REPARTO, ID_CATEGORIA, ORDINALE_CATEGORIA)
    SELECT P_PDV_COD ID_NEGOZIO, ID_REPARTO, ID_CATEGORIA, ORDINALE_CATEGORIA
      FROM CAC.T_CATEGORIE_ORDER
     WHERE ID_NEGOZIO= P_PDV_COD_COPY;
    --
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'FINE rows:'||SQL%ROWCOUNT ,vPasso);
  END IF; 
  --
  vPasso := 'T_REPARTI_ORDER';
  SELECT COUNT(0)
    INTO vCnt
    FROM CAC.T_REPARTI_ORDER
   WHERE ID_NEGOZIO =P_PDV_COD;
  --
  BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'num:'||vCnt ,vPasso);
  --
  IF vCnt = 0 THEN
  --
    INSERT INTO CAC.T_REPARTI_ORDER(ID_NEGOZIO, ID_REPARTO, ORDINALE_REPARTO)
    SELECT P_PDV_COD ID_NEGOZIO, ID_REPARTO, ORDINALE_REPARTO
      FROM CAC.T_REPARTI_ORDER
     WHERE ID_NEGOZIO = P_PDV_COD_COPY;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'FINE rows:'||SQL%ROWCOUNT ,vPasso);
  END IF; 
  --
  vPasso := 'MG_PDV_REL_SERVICE';
  SELECT COUNT(0)
    INTO vCnt
    FROM MG.MG_PDV_REL_SERVICE,
         CAC.D_SERVIZI S
   WHERE PVSER_PDV_COD =P_PDV_COD
     AND PVSER_SERVIZIO= S.COD_SERVIZIO
     --
     AND S.ID_SERVIZIO = P_SERVICE
     ;
  --
  BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'num:'||vCnt ,vPasso);
  --
  IF vCnt = 0 THEN
  --
    INSERT INTO MG.MG_PDV_REL_SERVICE(PVSER_PDV_REL_PDV_ID, PVSER_PDV_COD, PVSER_SERVIZIO, PVSER_DATA_INI, PVSER_DATA_FIN, PVSER_STAT, PVSER_EMAIL, PVSER_MASTER)
    SELECT P_PDV_COD PVSER_PDV_REL_PDV_ID, P_PDV_COD PVSER_PDV_COD, PVSER_SERVIZIO, SYSTIMESTAMP PVSER_DATA_INI, PVSER_DATA_FIN, PVSER_STAT, P_EMAIL PVSER_EMAIL, PVSER_MASTER
      FROM MG.MG_PDV_REL_SERVICE A,
           CAC.D_SERVIZI S
     WHERE A. PVSER_PDV_COD = P_PDV_COD_COPY
       AND PVSER_SERVIZIO= S.COD_SERVIZIO
       AND S.ID_SERVIZIO = P_SERVICE
     ;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'FINE rows:'||SQL%ROWCOUNT ,vPasso); 
  END IF;
  --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
      apex_error.add_error (   
        p_message          => 'Errore: ' || PO_ERR_CODICE ||' ' ||PO_MESSAGGIO,
        p_display_location => apex_error.c_inline_in_notification );
  END COPY_SERVICE_BY_PDV;
  --
END MG_CLICK_COLLECT_PCK;
/