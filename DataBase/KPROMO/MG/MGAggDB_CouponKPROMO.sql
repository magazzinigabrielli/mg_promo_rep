whenever sqlerror exit sql.sqlcode rollback;
set ver off
set timing on
set pagesize 0
set linesize 132


PROMPT Creazione Sequence Articoli.
PROMPT CREAZIONE SEQUENZA MG_ART_COD_CP_SEQ PER LA TABELLA FRAME.MG_VS_ITEM
CREATE SEQUENCE MG_ART_COD_CP_SEQ
  INCREMENT BY 1
  START WITH 1000000
  MAXVALUE 9999999999999999
  MINVALUE 1
  CYCLE
  CACHE 2
/

