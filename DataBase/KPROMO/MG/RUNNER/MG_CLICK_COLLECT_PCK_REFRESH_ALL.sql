/* Formatted on 17/04/2019 18:13:38 (QP5 v5.215.12089.38647) */
DECLARE
   PO_ERR_SEVERITA   VARCHAR2 (32767) := NULL;
   PO_ERR_CODICE     VARCHAR2 (32767) := NULL;
   PO_MESSAGGIO      VARCHAR2 (32767) := NULL;
   timeStart           PLS_INTEGER;
BEGIN
   timeStart := dbms_utility.get_time;
   BASE_TRACE_PCK.DISABLE_TRACE_ALL;
   MG.MG_CLICK_COLLECT_PCK.REFRESH_ALL (PO_ERR_SEVERITA,
                                        PO_ERR_CODICE,
                                        PO_MESSAGGIO);
   DBMS_OUTPUT.PUT_LINE('Excecution time:'||(dbms_utility.get_time - timeStart)/100 || ' seconds');
   COMMIT;
END;
/