ALTER TABLE MG_PDV_REL_SERVICE DROP

  CONSTRAINT   MG_PVSER_SERVICE

/

ALTER TABLE MG_PDV_REL_SERVICE ADD
(
  CONSTRAINT   MG_PVSER_SERVICE CHECK(PVSER_SERVIZIO  IN ('CCCASA','CCDRIVE','CCSTORE','S24')) ENABLE
)
/