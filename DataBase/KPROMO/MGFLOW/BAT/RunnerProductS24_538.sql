SET FEEDBACK OFF
SET SERVEROUTPUT ON SIZE UNLIMITED
SET PAGESIZE 9000
SET LINESIZE 2000
--
SPOOL SUPERMERCATO24_538.CSV

DECLARE
   PO_ERR_SEVERITA   VARCHAR2 (32767);
   PO_ERR_CODICE     VARCHAR2 (32767);
   PO_MESSAGGIO      VARCHAR2 (32767);
   P_FLOW_NAME       VARCHAR2 (32767);
   P_PDV_COD         NUMBER := 538;
   P_RETENTION       NUMBER;
   PO_JOB_ID         NUMBER;
BEGIN
   PO_ERR_SEVERITA := NULL;
   PO_ERR_CODICE := NULL;
   PO_MESSAGGIO := NULL;
   P_FLOW_NAME := 'S24';
   P_RETENTION := 6;
   PO_JOB_ID := NULL;
   DBMS_OUTPUT.DISABLE ();
   MGFLOW.FLW_ESTRATTORE_PCK.ELABORAZIONE (PO_ERR_SEVERITA,
                                           PO_ERR_CODICE,
                                           PO_MESSAGGIO,
                                           P_FLOW_NAME,
                                           P_PDV_COD,
                                           P_RETENTION,
                                           PO_JOB_ID);

   --DBMS_OUTPUT.Put_Line ('PO_ERR_SEVERITA = ' || PO_ERR_SEVERITA);
   --DBMS_OUTPUT.Put_Line ('PO_ERR_CODICE = ' || PO_ERR_CODICE);
   --DBMS_OUTPUT.Put_Line ('PO_MESSAGGIO = ' || PO_MESSAGGIO);
   DBMS_OUTPUT.Put_Line ('PO_JOB_ID = ' || TO_CHAR (PO_JOB_ID));
   COMMIT;
   DBMS_OUTPUT.ENABLE (buffer_size => NULL);
   MGFLOW.FLW_ESTRATTORE_PCK.ESTRAZIONE (PO_ERR_SEVERITA,
                                         PO_ERR_CODICE,
                                         PO_MESSAGGIO,
                                         P_PDV_COD,
                                         PO_JOB_ID);
END;
/

SPOOL OFF

EXIT;