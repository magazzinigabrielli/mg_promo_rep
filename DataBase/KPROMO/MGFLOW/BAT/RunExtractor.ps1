﻿##########################################################################################
# Questo script esegue l'estrattore dei prodotti                                         #
#                                                                                        #
#                                                                                        #
# Storia delle versioni                                                                  #
# - 0.0.0.1 -- Prima versione                                                            #
##########################################################################################
$scriptVersion = "0.0.0.1"

$user_name="MGFLOW"
$password="MGFlow2019"
$net_service_name="ODA_KPROMO"

$script1 = "@RunnerProductS24_531.sql"

$outputfile = "C:\MAIN\MGKPromo\DataBase\KPROMO\MGFLOW\BAT\OutRunnerProductS24_531.txt"

#Main program
Try
{
  Write-Host "Info Product Extracor: Version $scriptVersion" -ForegroundColor Cyan

  #Invoke-Command -InputObject (sqlplus $user_name/$password@$net_service_name $script1)

  #start $script1 | sqlplus -silent $user_name/$password@$net_service_name | Out-File $outputfile

  sqlplus $user_name/$password@$net_service_name $script1

  Write-Host "Elaborazione script terminata" -ForegroundColor Green
}
catch
{
    Write-Host $Error[0] -ForegroundColor Red
    Write-Host $Error[0].ScriptStackTrace -ForegroundColor Red
}

exit