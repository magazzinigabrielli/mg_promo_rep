﻿##########################################################################################
# Questo script esegue l'estrattore dei prodotti                                         #
#                                                                                        #
#                                                                                        #
# Storia delle versioni                                                                  #
# - 0.0.0.1 -- Prima versione                                                            #
##########################################################################################
$scriptVersion = "0.0.0.1"

$asm = [System.Reflection.Assembly]::LoadWithPartialName("System.Data.OracleClient")

$user_name="MGFLOW"
$password="MGFlow2019"
$net_service_name="KPROMO"

$script = "C:\MAIN\MGKPromo\DataBase\KPROMO\MGFLOW\BAT\ScriptToExecute.sql"

$outputfile = "C:\MAIN\MGKPromo\DataBase\KPROMO\MGFLOW\BAT\output.lst"


#Main program
Try
{
  Write-Host "Info Product Extracor: Version $scriptVersion" -ForegroundColor Cyan

  
  sqlplus -silent $user_name/$password@$net_service_name "$script" | Out-File $outputfile

  Write-Host "Elaborazione script terminata" -ForegroundColor Green
}
catch
{
    Write-Host $Error[0] -ForegroundColor Red
    Write-Host $Error[0].ScriptStackTrace -ForegroundColor Red
}

exit