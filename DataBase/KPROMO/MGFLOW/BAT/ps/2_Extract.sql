DECLARE
   PO_ERR_SEVERITA   VARCHAR2 (32767);
   PO_ERR_CODICE     VARCHAR2 (32767);
   PO_MESSAGGIO      VARCHAR2 (32767);
   P_FLOW_NAME       VARCHAR2 (32767);
   P_PDV_COD         NUMBER;
   P_RETENTION       NUMBER;
   PO_JOB_ID         NUMBER;
BEGIN
   PO_ERR_SEVERITA := NULL;
   PO_ERR_CODICE := NULL;
   PO_MESSAGGIO := NULL;
   P_FLOW_NAME := NULL;
   P_PDV_COD := NULL;
   PO_JOB_ID := NULL; 
   MGFLOW.FLW_ESTRATTORE_PCK.ESTRAZIONE(PO_ERR_SEVERITA,
                                        PO_ERR_CODICE,
                                        PO_MESSAGGIO,
                                        P_FLOW_NAME,
                                        P_PDV_COD,
                                        PO_JOB_ID);
END;
/