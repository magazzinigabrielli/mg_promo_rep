CREATE OR REPLACE PACKAGE BODY MGFLOW."FLW_ESTRATTORE_PCK" AS
  --
  /******************************************************************************
   NAME:       FLW_ESTRATTORE_PCK
   PURPOSE:    Package che si occupa dell’estrazioni delle informazioni dei prodotti.

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        08/04/2020           r.doti  1. Created this package.
   ******************************************************************************/
  -- COSTANTI A LIVELLO PACKAGE
  kNomePackage            CONSTANT VARCHAR2(30) := 'FLW_ESTRATTORE_PCK';
  --
  kFlagAnn                CONSTANT VARCHAR2(1)  := 'A';
  kFlagS                  CONSTANT VARCHAR2(1)  := 'S';
  kFlagN                  CONSTANT VARCHAR2(1)  := 'N';
  kAccapo                 CONSTANT VARCHAR2(1)  := CHR(10);
  --
  kStatoElab_NEW          CONSTANT VARCHAR2(5)  := 'NEW';
  kStatoElab_WAIT         CONSTANT VARCHAR2(5)  := 'WAIT';
  kStatoElab_RUN          CONSTANT VARCHAR2(5)  := 'RUN';
  kStatoElab_ERR          CONSTANT VARCHAR2(5)  := 'ERR';
  kStatoElab_END          CONSTANT VARCHAR2(5)  := 'END';
  kStatoElab_SPOL         CONSTANT VARCHAR2(5)  := 'SPOL';
  kStatoElab_ESTR         CONSTANT VARCHAR2(5)  := 'ESTR';
  --
  KFlowIdS24              CONSTANT NUMBER       := 1001;
  KFlowIdUNKMKT           CONSTANT NUMBER       := 1002;
  --
  exNotPDV EXCEPTION;
  PRAGMA EXCEPTION_INIT( exNotPDV, -20002 );
  --
  FUNCTION GET_FLOW_ID(
    P_NAME IN VARCHAR2
  )
    RETURN NUMBER
  IS
    vFLWId NUMBER;
  BEGIN
  --
    SELECT FLW_FLOW_ID
      INTO vFLWId
      FROM FLW_FLOWS
     WHERE FLW_NAME = P_NAME;
     --
    RETURN vFLWId;
  --
  EXCEPTION
    WHEN OTHERS THEN
      raise_application_error( -20002, 'FLUSSO:['||P_NAME ||'] NON PRESENTE.' );
  END GET_FLOW_ID;
  --
  /**
  * Description: Funzione per recuperare l'ean più recente
  * 
  * Author: R.Doti
  * Created: 08/04/2020
  * @param P_ART_COD Codice dell'articolo
  */
  FUNCTION GET_EAN_COD(
    P_ART_COD    NUMBER
  ) 
    RETURN NUMBER PARALLEL_ENABLE
  IS
    vProcName   VARCHAR2(30);
    vPasso      VARCHAR2(100);
    vEANCode    NUMBER;
  BEGIN
  --
    vProcName := 'GET_EAN_COD';
    --
    SELECT BAR.E6BCOD
      INTO vEANCode
      FROM MG.GEAD060F BAR
     WHERE BAR.E6CART = P_ART_COD
       AND TRIM(BAR.E6STAT) IS NULL
       AND BAR.E6DINS = (SELECT MAX(E6DINS)
                           FROM MG.GEAD060F BAR1
                          WHERE BAR1.E6CART = P_ART_COD
                            AND TRIM(BAR1.E6STAT) IS NULL)
      AND ROWNUM <=1;
    RETURN vEANCode;
   --
  EXCEPTION
    WHEN OTHERS THEN
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'ERR:'||SQLERRM , vPasso);
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, SQLERRM,BASE_LOG_PCK.Errore);
      RETURN 0;
  END GET_EAN_COD;
  --
  FUNCTION GET_INFO_AGG(
    P_ART_COD  IN   NUMBER,
    P_KYCI     IN   VARCHAR2,
    P_PDV_COD  IN   NUMBER DEFAULT NULL
  ) 
    RETURN VARCHAR2 PARALLEL_ENABLE
  IS
    vProcName   VARCHAR2(30);
    vPasso      VARCHAR2(100);
    vInfo       VARCHAR2(4000);
    vUM         NUMBER :=0;
  BEGIN
  --
    vProcName := 'GET_INFO_AGG';
    --
    IF P_KYCI = 'TRACCIABILITA' THEN
    --
      SELECT SUBSTR(TRIM(DPES_ART_DES_SCIENT ||CHR(10)|| DPES_ART_MET_CATT||CHR(10)|| DPES_ART_PROVENIENZA),1,4000)
        INTO vInfo
        FROM FRAME.FRM_DDT_PESCHERIA A
       WHERE TRIM(A.DPES_ART_COD_EXT) = P_ART_COD
         AND TRIM(DPES_PDV_COD_EXT) = P_PDV_COD
         AND DPES_DATA_MOD = (SELECT MAX(DPES_DATA_MOD) 
                                FROM  FRAME.FRM_DDT_PESCHERIA B
                               WHERE TRIM(A.DPES_ART_COD_EXT) = TRIM(B.DPES_ART_COD_EXT)
                                 AND TRIM(A.DPES_PDV_COD_EXT) = TRIM(B.DPES_PDV_COD_EXT) 
                             );
    ELSIF P_KYCI = 'PESI_MEDI' THEN
    --
      --
      --SELECT COUNT(0)
      --  INTO vUM
      --  FROM MG.DATIAGGIUNTIVI
      -- WHERE E7KY02 = 'CLICK_COLLECT'
      --   AND E7KY03 = 'UM_ORDINE'
      --   AND E7KYAM = 'ARTICOLI'
      --   AND E7KYCI = 'GR_PZ'
      --   AND E7KY01 = P_ART_COD
      --   AND ROWNUM <=1;
      --IF vUM = 1 THEN
      --  vInfo := 1;
      --ELSE
      --
        SELECT E7KYCI
          INTO vInfo
          FROM MG.DATIAGGIUNTIVI
         WHERE E7KY02 = 'CLICK_COLLECT'
           AND E7KY03 IN ('VAL_MEDIO','OFFSET_SCOST')
           AND E7KYAM ='ARTICOLI'
           AND E7KY01 = P_ART_COD
           AND ROWNUM <=1;
      --END IF;
    ELSIF P_KYCI = 'UM_ORDINE' THEN
    --
      SELECT E7KYCI
        INTO vInfo
        FROM MG.DATIAGGIUNTIVI
       WHERE E7KY02 = 'CLICK_COLLECT'
         AND E7KY03 = 'UM_ORDINE'
         AND E7KYAM = 'ARTICOLI'
         AND E7KY01 = P_ART_COD
         AND ROWNUM <=1;
    ELSE
      SELECT REPLACE(E7INFO,'"','')
        INTO vInfo
        FROM MG.DATIAGGIUNTIVI
       WHERE E7KY02 = 'IMMAGINO'
         AND E7KY03 ='AS400_BIL'
         AND E7KYCI = P_KYCI
         AND E7KY01 = P_ART_COD
         AND ROWNUM <=1;
    END IF;
    --
    RETURN vInfo;
   --
  EXCEPTION
    WHEN OTHERS THEN
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'ERR:'||SQLERRM , vPasso);
      --BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, SQLERRM,BASE_LOG_PCK.Errore);
      RETURN NULL;
  END GET_INFO_AGG;
  --
 /**
  * Description: Funzione per recuperare la foto dell'articolo
  * 
  * Author: R.Doti
  * Created: 08/04/2020
  * @param P_ART_COD Codice dell'articolo
  * @param P_ORIGINE origine della foto
  */
  FUNCTION GET_INFO_FOTO(
    P_ART_COD    NUMBER,
    P_ORIGINE    VARCHAR2
  ) RETURN VARCHAR2 PARALLEL_ENABLE
  IS
    vProcName   VARCHAR2(30);
    vPasso      VARCHAR2(100);
    vPath       VARCHAR2(4000);
  BEGIN
  --
    vProcName := 'GET_INFO_FOTO';
    --
    SELECT PATH
      INTO vPath
      FROM MG.ARTICLES_IMAGES_SITO
     WHERE ARTCODE = P_ART_COD
       AND NVL(TRIM(ORIGINE),'X') != 'IMMAGINO'
       AND ROWNUM <=1;
    RETURN vPath;
   --
  EXCEPTION
    WHEN OTHERS THEN
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'ERR:'||SQLERRM , vPasso);
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, SQLERRM,BASE_LOG_PCK.Errore);
      RETURN NULL;
  END GET_INFO_FOTO;
  --
  /**
  * Description: Utility per la gestione della FLW_SYNCR
  * 
  * Author: R.Doti
  * Created: 08/04/2020
  * @param PO_SYNCR Row of FLW_SYNCR
  */
  PROCEDURE SET_SYNCR (
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      PO_SYNCR           IN OUT FLW_SYNCR%ROWTYPE
   )
  IS
    PRAGMA AUTONOMOUS_TRANSACTION;
    kNomePackage        CONSTANT VARCHAR2(30) := 'FLW_ESTRATTORE_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
  BEGIN 
    --
    vProcName := 'SET_SYNCR';
    --
    IF PO_SYNCR.FSY_SYNCR_ID IS NULL THEN
      vPasso := 'INSRT';
      --
      INSERT INTO FLW_SYNCR(FLW_FLOW_ID, FSY_JOB_ID, FSY_STATE, FSY_ROWS_TOT, FSY_ID_LOG)
      VALUES(PO_SYNCR.FLW_FLOW_ID, PO_SYNCR.FSY_JOB_ID, PO_SYNCR.FSY_STATE, PO_SYNCR.FSY_ROWS_TOT ,PO_SYNCR.FSY_ID_LOG)
      RETURNING FSY_SYNCR_ID INTO PO_SYNCR.FSY_SYNCR_ID;
      --
    ELSE
      vPasso := 'UPDT';
      UPDATE FLW_SYNCR
         SET FSY_JOB_END   = PO_SYNCR.FSY_JOB_END, 
             FSY_STATE     = PO_SYNCR.FSY_STATE ,
             FSY_ROWS_TOT  = PO_SYNCR.FSY_ROWS_TOT,
             FSY_ROWS_ELAB = PO_SYNCR.FSY_ROWS_ELAB,
             FSY_ID_LOG    = PO_SYNCR.FSY_ID_LOG,
             FSY_LOG       = PO_SYNCR.FSY_LOG,
             FSY_STAT      = PO_SYNCR.FSY_STAT 
       WHERE FSY_SYNCR_ID = PO_SYNCR.FSY_SYNCR_ID;
      --
    END IF;
    --
    COMMIT;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'ID_FSY:'||PO_SYNCR.FSY_SYNCR_ID, vPasso);
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END SET_SYNCR;
  --
  PROCEDURE EXEC_RETENTION(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_FLOW_ID          IN            NUMBER,
      P_RETENTION        IN            NUMBER
  )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'FLW_ESTRATTORE_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    vNumRows            NUMBER := 0;
  BEGIN 
    --
    vProcName := 'RUN_RETENTION';
    --
    IF P_FLOW_ID = KFlowIdS24 THEN
    --
      vPasso := 'DEL_FST';
      DELETE FLW_EXTRACTOR FST
       WHERE FLW_FLOW_ID = 1001
         AND EXISTS(SELECT 0
                      FROM FLW_SYNCR FSY
                     WHERE FST.FSY_JOB_ID = FSY.FSY_JOB_ID
                       AND FSY.FLW_FLOW_ID = FST.FLW_FLOW_ID
                       AND TRUNC(FSY.FSY_JOB_START) <= TRUNC(SYSTIMESTAMP) - P_RETENTION);
      --
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'NumRows:'||SQL%ROWCOUNT, vPasso);
      --
    END IF; 
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END EXEC_RETENTION;
  --
  PROCEDURE RUNS_FLOW_24(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      PO_SYNCR           IN OUT        FLW_SYNCR%ROWTYPE,
      P_PDV_COD          IN            NUMBER,
      P_RETENTION        IN            NUMBER
  )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'FLW_ESTRATTORE_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    vNumRows            NUMBER := 0;
    vNumRowsElab        NUMBER := 0;
    vPrice              NUMBER;
    vFIDE               VARCHAR2(5); --[F: Fidelity- M: Mass Market]
    vTIPO_PROMO         VARCHAR2(5);--Tipologia Promo A400[TP, VI, PI, MN]    KLOY[PC- VC]
    vPREZZO             NUMBER;
    vPREZZO_PROMO       NUMBER;
    vVALORE_SCNT        NUMBER;
    vDIPROMO            NUMBER;
    vDFPROMO            NUMBER;
  BEGIN 
    --
    vProcName := 'RUNS_FLOW_24';
    --
    FOR rec IN (SELECT PVSER_PDV_COD
                  FROM MG.MG_PDV_REL_SERVICE
                 WHERE PVSER_SERVIZIO  = 'S24'
                   AND LOCALTIMESTAMP BETWEEN PVSER_DATA_INI AND PVSER_DATA_FIN
                   AND PVSER_STAT IS NULL
                   AND  NVL(P_PDV_COD, PVSER_PDV_COD) = PVSER_PDV_COD
                 )
    LOOP
    --
      vPasso := 'INSERT';
      INSERT INTO FLW_EXTRACTOR(
                     FST_EXTRACTOR_ID, 
                     FLW_FLOW_ID, 
                     FSY_JOB_ID, 
                     FST_PDV, 
                     FST_KEY_1,   --ean
                     FST_KEY_2,   --gdo_id
                     FST_KEY_3,   --variable_weight
                     FST_KEY_4,   --stock
                     FST_KEY_5,   --brand
                     FST_KEY_6,   --name
                     FST_KEY_7,   --description
                     FST_KEY_8,   --type
                     FST_KEY_9,   --selling_type
                     FST_KEY_10,  --selling_type_display
                     FST_KEY_11,  --selling_value
                     FST_KEY_12,  --price
                     FST_KEY_13,  --price_promo
                     FST_KEY_14,  --price_promo_fidelity
                     FST_KEY_15,  --suggested_price
                     FST_KEY_16,  --suggested_price_promo
                     FST_KEY_17,  --start_promo_date
                     FST_KEY_18,  --end_promo_date
                     FST_KEY_19,  --suggested_price_promo_fidelity
                     FST_KEY_20,  --fidelity_points
                     FST_KEY_21,  --start_fidelity_date
                     FST_KEY_22,  --end_fidelity_date
                     FST_KEY_23,  --url_image
                     FST_KEY_24,  --url_ingredients
                     FST_KEY_25,  --url_nutritional_values
                     FST_KEY_26,  --url_allergens_values
                     FST_KEY_27,  --url_coadjuvants_values
                     FST_KEY_28,  --url_traceability_values
                     FST_KEY_29,  --area
                     FST_KEY_30,  --area_desc
                     FST_KEY_31,  --rep
                     FST_KEY_32,  --rep_desc
                     FST_KEY_33,  --gru
                     FST_KEY_34,  --gru_desc
                     FST_KEY_35,  --sett
                     FST_KEY_36,  --sett_desc
                     FST_KEY_37,  --fam
                     FST_KEY_38,  --fam_desc
                     FST_KEY_39,  --quantity_sold
                     FST_KEY_40   --updated_date
                     )
           SELECT /*+
                     USE_NL(ART) INDEX(ART)
                     INDEX(ASS)*/       
                     FRAME.BASE_UTIL_PCK.MG_GUID, 
                     PO_SYNCR.FLW_FLOW_ID, 
                     PO_SYNCR.FSY_JOB_ID, 
                     ASS.E3CPDV, 
                     NULL "ean",
                     ART.E1CART       "gdo_id",
                     CASE ART.E1UMPR 
                       WHEN 'LT' THEN 'true'
                       WHEN 'KG' THEN 'true'
                       ELSE 'false'
                     END              "variable_weight",
                     1                "stock",
                     ART.E1BRAND      "brand",
                     REPLACE(ART.E1XARC,'"','''' )    "name",
                     NULL             "description",
                    CASE ART.E1UMPR 
                       WHEN 'LT' THEN 'L'
                       ELSE ART.E1UMPR 
                     END              "type",
                     CASE ART.E1UMPR 
                       WHEN 'LT' THEN 'L'
                       ELSE ART.E1UMPR 
                     END          "selling_type", --da verificare
                     --CASE NVL(ART.E1CPLU,0)
                     --  WHEN 0 THEN 'PZ'
                     --  ELSE DECODE(ART.E1UMPR,'LT','L',ART.E1UMPR) 
                     --END               "selling_type",
                     CASE ART.E1TCNF
                       WHEN 'BAC' THEN 'BARATTOLO CARTONE'
                       WHEN 'BAP' THEN 'BARATTOLO PLASTICA'    
                       WHEN 'BLS' THEN 'IN BLISTER'            
                       WHEN 'BOP' THEN 'BOTTIGLIA PET'         
                       WHEN 'BOT' THEN 'BOTTIGLIA'             
                       WHEN 'BOV' THEN 'BOTTIGLIA VETRO'       
                       WHEN 'BRK' THEN 'BRIK'                  
                       WHEN 'BST' THEN 'BUSTA'                 
                       WHEN 'CLF' THEN 'CELLOPHANE'            
                       WHEN 'CNF' THEN 'CONFEZIONE'            
                       WHEN 'DAB' THEN 'DAMIGIANA BRIK'        
                       WHEN 'DAM' THEN 'DAMIGIANA'             
                       WHEN 'DAP' THEN 'DAMIGIANA PET'         
                       WHEN 'DAV' THEN 'DAMIGIANA VETRO'       
                       WHEN 'FLC' THEN 'FLACONE'               
                       WHEN 'LAT' THEN 'LATTINA'               
                       WHEN 'PCC' THEN 'PACCHETTO'             
                       WHEN 'ROT' THEN 'ROTOLI'                
                       WHEN 'SAC' THEN 'SACCO'                 
                       WHEN 'SCC' THEN 'SCATOLA DI CARTONE'    
                       WHEN 'SCL' THEN 'SCATOLA LATTA'         
                       WHEN 'SCP' THEN 'SCATOLA DI PLASTICA'   
                       WHEN 'TRS' THEN 'CONFEZ.TRIS'           
                       WHEN 'TUB' THEN 'TUBETTO'               
                       WHEN 'VAV' THEN 'VASO DI VETRO'         
                       WHEN 'VSC' THEN 'VASCHETTA'
                     ELSE ART.E1TCNF 
                     END              "selling_type_display",
                     1                "selling_value",
                     NULL             "price",
                     NULL             "price_promo",
                     NULL             "price_promo_fidelity",
                     NULL             "suggested_price",
                     NULL             "suggested_price_promo",
                     NULL             "start_promo_date",
                     NULL             "end_promo_date",
                     NULL             "suggested_price_promo_fidelity",
                     NULL             "fidelity_points",
                     NULL             "start_fidelity_date",
                     NULL             "end_fidelity_date",
                     NULL             "url_image",
                     NULL             "url_ingredients",
                     NULL             "url_nutritional_values",
                     NULL             "url_allergens_values",
                     NULL             "url_coadjuvants_values",
                     NULL             "url_traceability_values",
                     ART.E1CG1C       "area",
                     ART.E1CG1D       "area_desc",
                     ART.E1CG2C       "rep",
                     ART.E1CG2D       "rep_desc",
                     ART.E1CG3C       "gru",
                     ART.E1CG3D       "gru_desc",
                     ART.E1CG4C       "sett",
                     ART.E1CG4D       "sett_desc",
                     ART.E1CG5C       "fam",
                     ART.E1CG5D       "fam_desc",
                     NULL             "quantity_sold",
                     NULL             "updated_date"
                FROM MG.GEAD030F ASS,
                     MG.GEAD010F ART
               WHERE ASS.E3CPDV = rec.PVSER_PDV_COD
                 AND ASS.E3FSIT = kFlagS
                 AND TRIM(ASS.E3STAT) IS NULL
                  --
                 AND ASS.E3CART = ART.E1CART
                 AND ART.E1SITO = kFlagS
                 AND TRIM(ART.E1STAT) IS NULL
                 AND ART.E1CIVA != 'R'  --Escludo articoli a gestione autonoma
                 --
                 --AND ART.E1CART = 756541
                 ;
      --
      vNumRows := vNumRows + SQL%ROWCOUNT;
      --
    END LOOP;
    --
    vPasso := 'ADD_EXT_INFO_ALL';
    FOR rec IN (SELECT FST_EXTRACTOR_ID, 
                       FST_PDV, 
                       TO_NUMBER(FST_KEY_2) CODART
                  FROM FLW_EXTRACTOR
                 WHERE FLW_FLOW_ID =PO_SYNCR.FLW_FLOW_ID 
                   AND FSY_JOB_ID = PO_SYNCR.FSY_JOB_ID
                )
    LOOP
      --
      vPrice       := NULL;
      vFIDE        := NULL;
      vTIPO_PROMO  := NULL;
      vPREZZO      := NULL;
      vPREZZO_PROMO:= NULL;
      vVALORE_SCNT := NULL;
      vDIPROMO     := NULL;
      vDFPROMO     := NULL;
      BEGIN
        vPasso := 'GET_PRICE';
        FRAME.FRM_PREZZI_PCK.GET_PRICE(PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
                  P_INTCODE      => rec.CODART,
                  P_HOST_SHOP_ID => rec.FST_PDV,
                  PO_PRICEAMOUNT => vPrice
                  );
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'vPrice:'||vPrice, vPasso);
        IF PO_MESSAGGIO IS NOT NULL THEN RAISE ErroreGestito; END IF;
        --
        vPasso := 'GET_PROMO_PRICE';
        FRAME.FRM_PREZZI_PCK.GET_PROMO_PRICE(PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
                                             P_OBJ_CODE            => rec.CODART,
                                             P_ID_SHOP             => rec.FST_PDV,
                                             P_DATA_DEC            => SYSTIMESTAMP,
                                             PO_FIDE               => vFIDE, --[F: Fidelity- M: Mass Market]
                                             PO_TIPO_PROMO         => vTIPO_PROMO, --Tipologia Promo A400[TP, VI, PI, MN]    KLOY[PC- VC]
                                             PO_PREZZO             => vPREZZO,
                                             PO_PREZZO_PROMO       => vPREZZO_PROMO,
                                             PO_VALORE_SCNT        => vVALORE_SCNT,
                                             PO_DIPROMO            => vDIPROMO,
                                             PO_DFPROMO            => vDFPROMO
                                             );
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PO_MESSAGGIO:'||PO_MESSAGGIO, vPasso);
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PO_FIDE:'||vFIDE, vPasso);
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PO_PREZZO_PROMO:'||vPREZZO_PROMO, vPasso);
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PO_DIPROMO:'||vDIPROMO, vPasso);
        BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'PO_DFPROMO:'||vDFPROMO, vPasso);
        IF PO_MESSAGGIO IS NOT NULL THEN RAISE ErroreGestito; END IF;
        --
        vPasso := 'UPD';
        UPDATE FLW_EXTRACTOR FST
           SET FST.FST_KEY_1  = FLW_ESTRATTORE_PCK.GET_EAN_COD(rec.CODART),
               FST.FST_KEY_12 = REPLACE(TO_CHAR(vPrice/100),',','.'),
               FST.FST_KEY_13 = DECODE(vFIDE,'M', REPLACE(TO_CHAR(vPREZZO_PROMO),',','.')),
               --FST.FST_KEY_14 = DECODE(vFIDE,'F', REPLACE(TO_CHAR(vPREZZO_PROMO/100), '.', ','))
               FST_KEY_17     = DECODE(vFIDE,'M', TO_CHAR(TO_DATE(vDIPROMO,'YYYYMMDD'),'YYYY-MM-DD'),NULL),
               FST_KEY_18     = DECODE(vFIDE,'M', TO_CHAR(TO_DATE(vDFPROMO,'YYYYMMDD'),'YYYY-MM-DD'),NULL),
               FST_KEY_23     = FLW_ESTRATTORE_PCK.GET_INFO_FOTO(rec.CODART,'TIPOGRAFIA')  --url_image
               --FST_KEY_21     = DECODE(vFIDE,'F', TO_CHAR(TO_DATE(vDIPROMO,'YYYYMMDD'),'YYYY-MM-DD'),NULL)
               --FST_KEY_22     = DECODE(vFIDE,'F', TO_CHAR(TO_DATE(vDFPROMO,'YYYYMMDD'),'YYYY-MM-DD'),NULL)
        WHERE FST.FST_EXTRACTOR_ID = rec.FST_EXTRACTOR_ID;
      --
        vNumRowsElab := vNumRowsElab + 1;
        --
      EXCEPTION
        WHEN ErroreGestito THEN
        --
          BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'ERR:'||PO_MESSAGGIO, vPasso);
          BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
          --
          PO_MESSAGGIO:=NULL; PO_ERR_CODICE:=NULL; PO_ERR_SEVERITA:=NULL;
        WHEN OTHERS THEN
          BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'ERR:'||PO_MESSAGGIO, vPasso);
          BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
          --
          PO_MESSAGGIO:=NULL; PO_ERR_CODICE:=NULL; PO_ERR_SEVERITA:=NULL;
      END;
    END LOOP;
    --
    vPasso := 'ADD_EXT_INFO_REST';
    FOR rec IN (SELECT FST_EXTRACTOR_ID, 
                       FST_PDV, 
                       TO_NUMBER(FST_KEY_2) CODART
                  FROM FLW_EXTRACTOR
                 WHERE FLW_FLOW_ID =PO_SYNCR.FLW_FLOW_ID 
                   AND FSY_JOB_ID = PO_SYNCR.FSY_JOB_ID
                   AND FST_KEY_8 != 'PZ' --solo articoli a peso variabile
                )
    LOOP
    --
      UPDATE FLW_EXTRACTOR FST
           SET FST_KEY_8      = NVL(REPLACE(REPLACE(FLW_ESTRATTORE_PCK.GET_INFO_AGG(rec.CODART, 'UM_ORDINE'),'PZKG','GR'),'GR_PZ','GR'), FST_KEY_8),   --type
               FST_KEY_10     = DECODE(FLW_ESTRATTORE_PCK.GET_INFO_AGG(rec.CODART, 'UM_ORDINE'),'GR_PZ','PEZZO','PZKG','CONFEZIONE',NULL) ,  --selling_type_display
               FST_KEY_11     = NVL(REPLACE(FLW_ESTRATTORE_PCK.GET_INFO_AGG(rec.CODART, 'PESI_MEDI'),',','.'), FST_KEY_11),  --selling_value
               FST_KEY_24     = FLW_ESTRATTORE_PCK.GET_INFO_AGG(rec.CODART, 'DETPR0000600') || kAccapo|| 
                                FLW_ESTRATTORE_PCK.GET_INFO_AGG(rec.CODART, 'DETPR0000200') || kAccapo||   
                                FLW_ESTRATTORE_PCK.GET_INFO_AGG(rec.CODART, 'DETPR0000300') ,  --url_ingredients
               FST_KEY_28     = FLW_ESTRATTORE_PCK.GET_INFO_AGG(rec.CODART, 'TRACCIABILITA', rec.FST_PDV)    --url_traceability_values
        WHERE FST.FST_EXTRACTOR_ID = rec.FST_EXTRACTOR_ID;
    END LOOP;
    --
    PO_SYNCR.FSY_ROWS_ELAB := vNumRowsElab;
    PO_SYNCR.FSY_ROWS_TOT  := vNumRows;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'NumRows:'||PO_SYNCR.FSY_ROWS_TOT, vPasso);
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
  END RUNS_FLOW_24;
  --
  /**
  * Description: Procedura di elaborazione informazioni prodotti
  *
  * Author: R.Doti
  * Created: 08/04/2020
  * @param P_FLOW_NAME Nome del flusso
  * @param P_PDV_COD Codice del PDV Fisico
  * @param P_RETENTION Giorni di retention
  * @param PO_JOB_ID Id del job creato
  */
  PROCEDURE ELABORAZIONE(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_FLOW_NAME        IN            VARCHAR2,
      P_PDV_COD          IN            NUMBER,
      P_RETENTION        IN            NUMBER DEFAULT 6,
      PO_JOB_ID             OUT        NUMBER
   )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'FLW_ESTRATTORE_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    timeStart           PLS_INTEGER;
    timeEnd             PLS_INTEGER;
    vInput              VARCHAR2(100);
    vShopId             MG.GEAD020F.E2CPDV%TYPE;
    exDatiObbl          EXCEPTION;
    PRAGMA              EXCEPTION_INIT( exDatiObbl, -20001);
    --
    vFLWId              FLW_FLOWS.FLW_FLOW_ID%TYPE;
    vJobId              NUMBER; 
    vRowSYNCR           FLW_SYNCR%ROWTYPE;
    errGestito          EXCEPTION;
    vIdLog              FRAME.BASE_LOG.LOG_ID_SESSIONE%TYPE;
  BEGIN 
    --
    vProcName := 'ELABORAZIONE';
    --
    timeStart := dbms_utility.get_time;
    --
    vPasso := 'START';
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, vPasso, BASE_LOG_PCK.Info);
    --
    vPasso := 'CHECK_INPUT';
    --
    IF P_FLOW_NAME IS NULL THEN vInput     := 'P_FLOW_NAME'; RAISE exDatiObbl; END IF;
    --
    IF P_PDV_COD IS NOT NULL THEN
    --
      vPasso := 'CHECK_PDV';
      SELECT E2CPDV INTO vShopId FROM MG.GEAD020F WHERE E2CPDV = P_PDV_COD;
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'vShopId:'||vShopId, vPasso);
    --
    END IF;
    --
    vPasso := 'GET_FLOW_ID';
    vFLWId := GET_FLOW_ID(P_NAME => P_FLOW_NAME);
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'vFLWId:'|| vFLWId, vPasso);
    --
    vPasso := 'GET_JOB_ID';
    vJobId := FLW_FLOW_JOB_ID_SEQ.NEXTVAL;
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'vJobId:'|| vJobId, vPasso);
    --
    vPasso := 'GET_SESS_ID';
    vIdLog := FRAME.BASE_LOG_PCK.GETSESSIONID;
    --
    vPasso := 'Syncr' ;
    vRowSYNCR.FLW_FLOW_ID  := vFLWId;
    vRowSYNCR.FSY_JOB_ID   := vJobId;
    vRowSYNCR.FSY_STATE    := kStatoElab_NEW;
    vRowSYNCR.FSY_ROWS_TOT := 0;
    vRowSYNCR.FSY_ID_LOG   := vIdLog;
    --
    SET_SYNCR (PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
               PO_SYNCR => vRowSYNCR);
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, 'vFLWId:'|| vFLWId, vPasso);
    --
    IF PO_MESSAGGIO IS NOT NULL THEN RAISE errGestito; END IF;
    --
    vPasso := 'EXEC_RETENTION';
    EXEC_RETENTION(PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
                   P_FLOW_ID    => vFLWId,
                   P_RETENTION  => P_RETENTION);
    --
    IF PO_MESSAGGIO IS NOT NULL THEN RAISE errGestito; END IF;
    --
    IF vFLWId IN (KFlowIdS24, KFlowIdUNKMKT)  THEN
    --
      vRowSYNCR.FSY_STATE := kStatoElab_RUN;
      SET_SYNCR (PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
               PO_SYNCR => vRowSYNCR);
      vPasso := 'RUNS_FLOW_24';
      RUNS_FLOW_24(PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO ,
                   PO_SYNCR    => vRowSYNCR,
                   P_PDV_COD   => P_PDV_COD,
                   P_RETENTION => P_RETENTION);
      --
      IF PO_MESSAGGIO IS NOT NULL THEN RAISE errGestito; END IF;
      --
    END IF;
    --
    vRowSYNCR.FSY_STATE := kStatoElab_END;
    vRowSYNCR.FSY_JOB_END := SYSTIMESTAMP;
    SET_SYNCR (PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
               PO_SYNCR => vRowSYNCR);
    --
    PO_JOB_ID := vJobId;
    vPasso := 'END';
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, vPasso, BASE_LOG_PCK.Info);
    --
  EXCEPTION
    WHEN errGestito THEN
      --
      vRowSYNCR.FSY_LOG := PO_MESSAGGIO;
      vRowSYNCR.FSY_STATE := kStatoElab_ERR;
      SET_SYNCR (PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
               PO_SYNCR => vRowSYNCR);
      --
      BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, PO_MESSAGGIO , vPasso);
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
    WHEN exDatiObbl THEN
       PO_ERR_SEVERITA := 'E';
       PO_ERR_CODICE   := SQLCODE;
       PO_MESSAGGIO    :=  'ERRORE: IL PARAMETRO '|| vInput ||' E'' OBBLIGATORIO.';
       BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, PO_MESSAGGIO , vPasso);
       --
       vRowSYNCR.FSY_LOG := PO_MESSAGGIO;
       vRowSYNCR.FSY_STATE := kStatoElab_ERR;
       SET_SYNCR (PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
                  PO_SYNCR => vRowSYNCR);
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
      --
      vRowSYNCR.FSY_LOG := PO_MESSAGGIO;
      vRowSYNCR.FSY_STATE := kStatoElab_ERR;
      SET_SYNCR (PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
                 PO_SYNCR => vRowSYNCR);
  END ELABORAZIONE;
  --
  /**
  * Description: Utility per l'estrazione dati in formato CSV
  * 
  * Author: R.Doti
  * Created: 08/04/2020
  * @param P_PDV_COD Codice del PDV
  * @param P_JOB_ID Ide del JOB
  */
  PROCEDURE ESTRAZIONE(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_PDV_COD          IN            NUMBER,
      P_JOB_ID           IN            NUMBER
   )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'FLW_ESTRATTORE_PCK';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(100);
    --
    vSeparatore         VARCHAR2(10) := ';';
    vDelimitatore       VARCHAR2(10) := '"';
    vRowSYNCR           FLW_SYNCR%ROWTYPE;
  BEGIN 
    --
    vProcName := 'ESTRAZIONE';
    --
    SELECT *
      INTO vRowSYNCR
      FROM FLW_SYNCR
      WHERE FSY_JOB_ID	= P_JOB_ID;
    --
    vRowSYNCR.FSY_STATE := kStatoElab_SPOL;
    SET_SYNCR (PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
               PO_SYNCR => vRowSYNCR);
    --
    FOR rec IN (SELECT vDelimitatore||'ean'                           ||vDelimitatore||vSeparatore||
                       vDelimitatore||'gdo_id'                        ||vDelimitatore||vSeparatore||
                       vDelimitatore||'variable_weight'               ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||'stock'                         ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||'brand'                         ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||'name'                          ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||'description'                   ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||'type'                          ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||'selling_type'                  ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||'selling_type_display'          ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||'selling_value'                 ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||'price'                         ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||'price_promo'                   ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||'price_promo_fidelity'          ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||'suggested_price'               ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||'suggested_price_promo'         ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||'start_promo_date'              ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||'end_promo_date'                ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||'suggested_price_promo_fidelity'||vDelimitatore||vSeparatore|| 
                       vDelimitatore||'fidelity_points'               ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||'start_fidelity_date'           ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||'end_fidelity_date'             ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||'url_image'                     ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||'url_ingredients'               ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||'url_nutritional_values'        ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||'url_allergens_values'          ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||'url_coadjuvants_values'        ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||'url_traceability_values'       ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||'area'                          ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||'area_desc'                     ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||'rep'                           ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||'rep_desc'                      ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||'gru'                           ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||'gru_desc'                      ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||'sett'                          ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||'sett_desc'                     ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||'fam'                           ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||'fam_desc'                      ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||'quantity_sold'                 ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||'updated_date'                  ||vDelimitatore||vSeparatore Riga
                  FROM DUAL
                UNION ALL
                SELECT vDelimitatore||FST_KEY_1 ||vDelimitatore||vSeparatore||
                       vDelimitatore||CASE 
                       WHEN LENGTH(FST_KEY_2) <= 2 THEN  LPAD(FST_KEY_2,3,'0')
                       ELSE FST_KEY_2 
                       END    ||vDelimitatore||vSeparatore||
                       vDelimitatore||FST_KEY_3 ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||FST_KEY_4 ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||FST_KEY_5 ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||FST_KEY_6 ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||FST_KEY_7 ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||FST_KEY_8 ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||FST_KEY_9 ||vDelimitatore||vSeparatore|| 
                       vDelimitatore||FST_KEY_10||vDelimitatore||vSeparatore|| 
                       vDelimitatore||FST_KEY_11||vDelimitatore||vSeparatore|| 
                       vDelimitatore||FST_KEY_12||vDelimitatore||vSeparatore|| 
                       vDelimitatore||FST_KEY_13||vDelimitatore||vSeparatore|| 
                       vDelimitatore||FST_KEY_14||vDelimitatore||vSeparatore|| 
                       vDelimitatore||FST_KEY_15||vDelimitatore||vSeparatore|| 
                       vDelimitatore||FST_KEY_16||vDelimitatore||vSeparatore|| 
                       vDelimitatore||FST_KEY_17||vDelimitatore||vSeparatore|| 
                       vDelimitatore||FST_KEY_18||vDelimitatore||vSeparatore|| 
                       vDelimitatore||FST_KEY_19||vDelimitatore||vSeparatore|| 
                       vDelimitatore||FST_KEY_20||vDelimitatore||vSeparatore|| 
                       vDelimitatore||FST_KEY_21||vDelimitatore||vSeparatore|| 
                       vDelimitatore||FST_KEY_22||vDelimitatore||vSeparatore|| 
                       vDelimitatore||FST_KEY_23||vDelimitatore||vSeparatore|| 
                       vDelimitatore||FST_KEY_24||vDelimitatore||vSeparatore|| 
                       vDelimitatore||FST_KEY_25||vDelimitatore||vSeparatore|| 
                       vDelimitatore||FST_KEY_26||vDelimitatore||vSeparatore|| 
                       vDelimitatore||FST_KEY_27||vDelimitatore||vSeparatore|| 
                       vDelimitatore||FST_KEY_28||vDelimitatore||vSeparatore|| 
                       vDelimitatore||FST_KEY_29||vDelimitatore||vSeparatore|| 
                       vDelimitatore||FST_KEY_30||vDelimitatore||vSeparatore|| 
                       vDelimitatore||FST_KEY_31||vDelimitatore||vSeparatore|| 
                       vDelimitatore||FST_KEY_32||vDelimitatore||vSeparatore|| 
                       vDelimitatore||FST_KEY_33||vDelimitatore||vSeparatore|| 
                       vDelimitatore||FST_KEY_34||vDelimitatore||vSeparatore|| 
                       vDelimitatore||FST_KEY_35||vDelimitatore||vSeparatore|| 
                       vDelimitatore||FST_KEY_36||vDelimitatore||vSeparatore|| 
                       vDelimitatore||FST_KEY_37||vDelimitatore||vSeparatore|| 
                       vDelimitatore||FST_KEY_38||vDelimitatore||vSeparatore|| 
                       vDelimitatore||FST_KEY_39||vDelimitatore||vSeparatore|| 
                       vDelimitatore||FST_KEY_40||vDelimitatore||vSeparatore 
                  FROM FLW_EXTRACTOR
                 WHERE FSY_JOB_ID = P_JOB_ID
                   AND FST_PDV =P_PDV_COD
                   )
    LOOP
    --
      DBMS_OUTPUT.PUT_LINE(rec.Riga);
    END LOOP;
    --
    vRowSYNCR.FSY_STATE := kStatoElab_ESTR;
    vRowSYNCR.FSY_JOB_END := SYSTIMESTAMP;
    SET_SYNCR (PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
               PO_SYNCR => vRowSYNCR);
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO,BASE_LOG_PCK.Errore);
      --
      vRowSYNCR.FSY_LOG := PO_MESSAGGIO;
      vRowSYNCR.FSY_STATE := kStatoElab_ERR;
      SET_SYNCR (PO_ERR_SEVERITA, PO_ERR_CODICE, PO_MESSAGGIO,
                 PO_SYNCR => vRowSYNCR);
  END ESTRAZIONE;
  --
END FLW_ESTRATTORE_PCK;
/
SHOW ERR