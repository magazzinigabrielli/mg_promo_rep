CREATE OR REPLACE PACKAGE MGFLOW."FLW_ESTRATTORE_PCK" AS
  --
  /******************************************************************************
   NAME:       FLW_ESTRATTORE_PCK
   PURPOSE:    Package che si occupa dell’estrazioni delle informazioni dei prodotti.

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        08/04/2020           r.doti  1. Created this package.
   ******************************************************************************/
  --
  ErroreGestito EXCEPTION;
  /**
  * Description: Procedura di elaborazione informazioni prodotti
  *
  * Author: R.Doti
  * Created: 08/04/2020
  * @param P_FLOW_ID Identificativo del flusso
  * @param P_PDV_COD Codice del PDV Fisico
  * @param P_RETENTION Giorni di retention
  * @param PO_JOB_ID Id del job creato
  */
  PROCEDURE ELABORAZIONE(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_FLOW_NAME        IN            VARCHAR2,
      P_PDV_COD          IN            NUMBER,
      P_RETENTION        IN            NUMBER DEFAULT 6,
      PO_JOB_ID             OUT        NUMBER
   );
  --
  /**
  * Description: Utility per l'estrazione dati in formato CSV
  * 
  * Author: R.Doti
  * Created: 08/04/2020
  * @param P_PDV_COD Codice del PDV
  * @param P_JOB_ID Ide del JOB
  */
  PROCEDURE ESTRAZIONE(
      PO_ERR_SEVERITA    IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE      IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO       IN OUT NOCOPY VARCHAR2,
      P_PDV_COD          IN            NUMBER,
      P_JOB_ID           IN            NUMBER
   );
  --
  /**
  * Description: Funzione per recuperare l'ean più recente
  * 
  * Author: R.Doti
  * Created: 08/04/2020
  * @param P_PDV_COD Codice del PDV
  * @param P_JOB_ID Ide del JOB
  */
  FUNCTION GET_EAN_COD(
    P_ART_COD    NUMBER
  ) RETURN NUMBER PARALLEL_ENABLE;
  --
  /**
  * Description: Funzione per recuperare la foto dell'articolo
  * 
  * Author: R.Doti
  * Created: 08/04/2020
  * @param P_ART_COD Codice dell'articolo
  * @param P_ORIGINE origine della foto
  */
  FUNCTION GET_INFO_FOTO(
    P_ART_COD    NUMBER,
    P_ORIGINE    VARCHAR2
  ) RETURN VARCHAR2 PARALLEL_ENABLE;
  --
  FUNCTION GET_INFO_AGG(
    P_ART_COD  IN   NUMBER,
    P_KYCI     IN   VARCHAR2,
    P_PDV_COD  IN   NUMBER DEFAULT NULL
  ) RETURN VARCHAR2 PARALLEL_ENABLE;
END FLW_ESTRATTORE_PCK;
/

CREATE OR REPLACE PUBLIC SYNONYM FLW_ESTRATTORE_PCK FOR MGFLOW.FLW_ESTRATTORE_PCK
/


