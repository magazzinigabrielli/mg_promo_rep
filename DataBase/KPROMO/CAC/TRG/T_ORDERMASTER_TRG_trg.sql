CREATE OR REPLACE TRIGGER T_ORDERMASTER_TRG 
BEFORE INSERT OR UPDATE ON T_ORDERMASTER FOR EACH ROW
DECLARE
  vTotArticoli NUMBER;
  vTotPezzi    NUMBER;
BEGIN
--
  IF inserting THEN
    --
    SELECT COUNT(0), SUM(QTY)
      INTO vTotArticoli, vTotPezzi
      FROM ORDER_HEADER HEAD,
           ORDER_DETAIL DET
     WHERE HEAD.ORDER_CODE = :NEW.ORDER_CODE
       AND HEAD.STORE_ID = :NEW.STORE_ID
       AND HEAD.ID = DET.ORDER_HEADER_ID
       AND DET.FLG_STATE  = 0;
  
    INSERT INTO ORDER_HEADER_STATS (ID, 
                                    ORDER_CODE, 
                                    STORE_ID, 
                                    USER_ID, 
                                    DELIVERY_DATE, 
                                    DELIVERY_STORE_ID, 
                                    ORDER_STATUS, 
                                    TOTAL, 
                                    TOTAL_ART, 
                                    TOTAL_PZ,
                                    TIME_PICK_START, 
                                    TIME_PICK_END)
                             VALUES(ORDER_HEADER_STATS_ID_SEQ.NEXTVAL, 
                                    :NEW.ORDER_CODE, 
                                    :NEW.STORE_ID, 
                                    :NEW.USER_ID, 
                                    :NEW.DUE_DATE, 
                                    :NEW.DELIVERY_STORE_ID, 
                                    :NEW.ORDER_STATUS, 
                                    :NEW.TOTAL, 
                                    vTotArticoli, 
                                    vTotPezzi,
                                    SYSTIMESTAMP,
                                    NULL);
    COMMIT;
  ELSE
    --
    SELECT COUNT(0), SUM(PRELEVATO)
      INTO vTotArticoli, vTotPezzi
      FROM T_ORDERDETAILS DET
     WHERE DET.ORDER_ID = :OLD.ID;
    --
    UPDATE ORDER_HEADER_STATS
       SET TIME_PICK_END = SYSTIMESTAMP,
           ORDER_STATUS  = :NEW.ORDER_STATUS,
           TOTAL_ART_PICK = vTotArticoli, 
           TOTAL_PZ_PICK = vTotPezzi
     WHERE ORDER_CODE =:NEW.ORDER_CODE;
    COMMIT;  
  END IF;
  --
EXCEPTION 
  WHEN OTHERS THEN 
    BASE_LOG_PCK.WriteLog('T_ORDERMASTER_TRG','ERRORE: '|| SQLERRM , BASE_LOG_PCK.Errore);
END;
/
