DROP TABLE ORDER_HEADER_STATS
/
CREATE TABLE ORDER_HEADER_STATS
(
  ID                  NUMBER(10)                 NOT NULL,
  ORDER_CODE          NUMBER(22)                 NOT NULL,
  STORE_ID            NUMBER(10)                 NOT NULL,
  USER_ID             NUMBER(10)                 NOT NULL,
  DELIVERY_DATE       DATE  NOT NULL,
  DELIVERY_STORE_ID   NUMBER(10),
  ORDER_STATUS        CHAR(1 BYTE)               DEFAULT '0'   NOT NULL,
  TOTAL               NUMBER(10,2),
  TOTAL_ART           NUMBER,
  TOTAL_PZ            NUMBER,
  TOTAL_ART_PICK      NUMBER,
  TOTAL_PZ_PICK       NUMBER,
  TIME_PICK_START     TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL,
  TIME_PICK_END       TIMESTAMP
)
TABLESPACE DATI_CAC
;
PROMPT Creazione Commenti FRM_DDT_PESCHERIA
COMMENT ON TABLE ORDER_HEADER_STATS IS 'Tabella che raccolgie statistiche sugli ordini.
- Tempi di picking' ;

COMMENT ON COLUMN ORDER_HEADER_STATS.ID                   IS 'chiave fisica della tabella ORDER_HEADER_STATS' ;
COMMENT ON COLUMN ORDER_HEADER_STATS.ORDER_CODE	          IS 'Codice dell''ordine web';
COMMENT ON COLUMN ORDER_HEADER_STATS.STORE_ID	            IS 'Identificativo del PDV';
COMMENT ON COLUMN ORDER_HEADER_STATS.USER_ID              IS 'Identificativo dell''operatore';
COMMENT ON COLUMN ORDER_HEADER_STATS.DELIVERY_DATE	      IS 'Data di consegna ordine';
COMMENT ON COLUMN ORDER_HEADER_STATS.DELIVERY_STORE_ID	  IS 'Fascia di consegna';
COMMENT ON COLUMN ORDER_HEADER_STATS.ORDER_STATUS	        IS 'Stato dell''ordine';
COMMENT ON COLUMN ORDER_HEADER_STATS.TOTAL                IS 'Totale calcolato sul web';
COMMENT ON COLUMN ORDER_HEADER_STATS.TOTAL_ART            IS 'Numero di articoli richiesti';
COMMENT ON COLUMN ORDER_HEADER_STATS.TOTAL_PZ             IS 'Numero di pezzi richiesti';
COMMENT ON COLUMN ORDER_HEADER_STATS.TOTAL_ART_PICK       IS 'Numero di articoli di cui si è effettuato il picking';
COMMENT ON COLUMN ORDER_HEADER_STATS.TOTAL_PZ_PICK	      IS 'Numero di pezzi di cui si è effettuato il picking';
COMMENT ON COLUMN ORDER_HEADER_STATS.TIME_PICK_START      IS 'Data inizio picking';
COMMENT ON COLUMN ORDER_HEADER_STATS.TIME_PICK_END	      IS 'Data fine picking';

CREATE UNIQUE INDEX ORDER_HEADER_STATS_IDX_PK ON ORDER_HEADER_STATS
(ID)
TABLESPACE DATI_CAC
;



ALTER TABLE ORDER_HEADER_STATS ADD (
  CONSTRAINT ORDER_HEADER_STATS_PK
  PRIMARY KEY
  ( ID)
  USING INDEX ORDER_HEADER_STATS_IDX_PK
  ENABLE VALIDATE);


CREATE SEQUENCE ORDER_HEADER_STATS_ID_SEQ
  INCREMENT BY 1
  START WITH 1
  MAXVALUE 9999999999999999
  MINVALUE 1
  CYCLE
  CACHE 2
/