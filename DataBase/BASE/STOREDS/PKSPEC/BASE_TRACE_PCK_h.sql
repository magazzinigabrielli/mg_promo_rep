CREATE OR REPLACE PACKAGE "BASE_TRACE_PCK" AS
  --
  /******************************************************************************
   NAME:       BASE_TRACE_PCK
   PURPOSE:    Package per abilitazione del trace

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        12/02/2019           r.doti       1. Created this package.
   ******************************************************************************/
  --
  ErroreGestito EXCEPTION;
  --
  PROCEDURE DISABLE_TRACE_ALL;
  PROCEDURE ENABLE_TRACE_ALL(pSiNo IN BOOLEAN  := TRUE);
  PROCEDURE ENABLE_TRACE(P_PACKAGE_NAME IN VARCHAR2, P_PROC_NAME VARCHAR2 := NULL, pSiNo IN BOOLEAN  := TRUE); 
  --  
  PROCEDURE TRACE(P_PACKAGE_NAME VARCHAR2, P_PROC_NAME VARCHAR2, P_MESSAGGIO VARCHAR2, P_PASSO VARCHAR2 := NULL);
  FUNCTION CK_TRACE(P_PACKAGE_NAME VARCHAR2, P_PROC_NAME VARCHAR2) RETURN BOOLEAN;
  --
END BASE_TRACE_PCK;
/
CREATE OR REPLACE PUBLIC SYNONYM BASE_TRACE_PCK FOR "BASE_TRACE_PCK"
/
--FRAME
--GRANT EXECUTE ON BASE_TRACE_PCK TO MG
--/

--CRSMG
--GRANT EXECUTE ON BASE_TRACE_PCK TO MGBO
--/