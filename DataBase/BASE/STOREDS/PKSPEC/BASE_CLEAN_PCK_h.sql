CREATE OR REPLACE PACKAGE FRAME."BASE_CLEAN_PCK" 
AS
  --
  /******************************************************************************
   NAME:       BASE_CLEAN_PCK
   PURPOSE:    Package di utilità per lo schema FRAME

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        01/12/2021          r.doti       1. Created this package.
   ******************************************************************************/
  --
  PROCEDURE CLEAN_TABLE(
      PO_ERR_SEVERITA       IN OUT NOCOPY  VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY  VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY  VARCHAR2,
      P_COMMIT              IN             BOOLEAN  := TRUE
    );
    
  PROCEDURE CLEAN_TABLE_JB;

END BASE_CLEAN_PCK;
--
/
