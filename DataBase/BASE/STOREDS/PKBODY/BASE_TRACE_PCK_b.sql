CREATE OR REPLACE PACKAGE BODY "BASE_TRACE_PCK" AS
  --
  /******************************************************************************
   NAME:       BASE_TRACE_PCK
   PURPOSE:    Package per abilitazione del trace

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        12/02/2019           r.doti       1. Created this package.
   ******************************************************************************/
  --
  /**
  * Package di appoggio per raggruppare tutte le procedure e funzioni sviluppate
  * per la gestione dei WKF
  * @headcom
  */
  -- COSTANTI A LIVELLO PACKAGE
    kNomePackage       CONSTANT VARCHAR2(30) := 'BASE_TRACE_PCK';
    --
    kAccapo            CONSTANT VARCHAR2(2)  := CHR(10);
    --
    kFlagAnn           CONSTANT VARCHAR2(1)  := 'A';
    --
    -- TIPI E VARIABILI GLOBALI
    --
    TYPE T_TABELLA IS TABLE OF BOOLEAN INDEX BY VARCHAR2(61);
    G_LISTA_PCK_ABILITATI   T_TABELLA;
    G_LISTA_PRC_ABILITATE   T_TABELLA;
    G_ABILITA_ALL           BOOLEAN := FALSE;
    --
    --
    PROCEDURE SPLIT_PCK_NAME(
      P_FULL_NAME     IN VARCHAR2, 
      PO_PACKAGE_NAME IN OUT NOCOPY VARCHAR2, 
      PO_PROC_NAME    IN OUT NOCOPY VARCHAR2
      ) IS
      vNum NUMBER := NVL(INSTR(P_FULL_NAME, '.') - 1 , 30) ;
    BEGIN
      --
      IF vNum =  - 1 OR vNum > 30 THEN
        vNum := 30;
        PO_PACKAGE_NAME := SUBSTR(P_FULL_NAME, 1, 30);
        PO_PROC_NAME := '';
      ELSE
        PO_PACKAGE_NAME := SUBSTR(P_FULL_NAME, 1, vNum);
        PO_PROC_NAME    := SUBSTR(P_FULL_NAME, vNum + 2, 30);
      END IF;
      --
      --
      --
      --BASE_UTILITA.STAMPA_PL(PO_PACKAGE_NAME||'->'||PO_PROC_NAME||' '||P_FULL_NAME);
    EXCEPTION
      WHEN OTHERS THEN  
        PO_PACKAGE_NAME := NULL;
        PO_PROC_NAME := NULL;
    END SPLIT_PCK_NAME;
    --
    PROCEDURE ENABLE_TRACE_ALL(pSiNo IN BOOLEAN := TRUE) IS
    BEGIN
      G_ABILITA_ALL := NVL(pSiNo, FALSE) ;   
    END ENABLE_TRACE_ALL;
    --
    PROCEDURE ENABLE_TRACE(P_PACKAGE_NAME IN VARCHAR2, P_PROC_NAME VARCHAR2 := NULL, pSiNo IN BOOLEAN := TRUE) IS
      vNomeCompleto VARCHAR2(61);
      --
    BEGIN
      --
      IF P_PACKAGE_NAME IS NULL THEN
        RETURN;
      END IF;
      --
      IF P_PROC_NAME IS NULL THEN
        vNomeCompleto := SUBSTR(trim(P_PACKAGE_NAME), 1, 30);
        IF pSiNo THEN
          G_LISTA_PCK_ABILITATI(vNomeCompleto) := TRUE;
        ELSIF G_LISTA_PCK_ABILITATI.EXISTS(vNomeCompleto) THEN
            G_LISTA_PCK_ABILITATI.DELETE(vNomeCompleto);
        END IF;
      ELSE
        vNomeCompleto := SUBSTR(trim(P_PACKAGE_NAME), 1, 30) ||'.'||SUBSTR(trim(P_PROC_NAME), 1, 30);
        IF pSiNo THEN
          G_LISTA_PRC_ABILITATE(vNomeCompleto) := TRUE;
        ELSIF G_LISTA_PRC_ABILITATE.EXISTS(vNomeCompleto) THEN
            G_LISTA_PRC_ABILITATE.DELETE(vNomeCompleto);
        END IF;
      END IF;
      --
    EXCEPTION
      WHEN OTHERS THEN NULL;
    END ENABLE_TRACE;
    --
    PROCEDURE DISABLE_TRACE_ALL IS 
    BEGIN
      G_ABILITA_ALL := FALSE;
      G_LISTA_PRC_ABILITATE.DELETE;
      G_LISTA_PCK_ABILITATI.DELETE;
    EXCEPTION
      WHEN OTHERS THEN NULL;
    END DISABLE_TRACE_ALL;

    --
    FUNCTION CK_TRACE(P_PACKAGE_NAME VARCHAR2, P_PROC_NAME VARCHAR2) RETURN BOOLEAN IS
      vNomeCompleto VARCHAR2(61);
    BEGIN
      --vPckName := SPLIT_PCK_NAME(P_PACKAGE_NAME);
      --
      IF G_ABILITA_ALL = TRUE THEN 
        RETURN TRUE;
      END IF;
      --   
      IF P_PACKAGE_NAME IS NULL THEN 
        RETURN FALSE;
      END IF;
      --
      vNomeCompleto := SUBSTR(trim(P_PACKAGE_NAME), 1, 30);
      IF G_LISTA_PCK_ABILITATI.EXISTS(vNomeCompleto) AND G_LISTA_PCK_ABILITATI(vNomeCompleto) THEN 
        RETURN TRUE;
      END IF;
      --
      IF P_PROC_NAME IS NULL THEN 
        RETURN FALSE;
      END IF;
      --
      vNomeCompleto := vNomeCompleto ||'.'||SUBSTR(trim(P_PROC_NAME), 1, 30);
      IF G_LISTA_PRC_ABILITATE.EXISTS(vNomeCompleto) AND G_LISTA_PRC_ABILITATE(vNomeCompleto) THEN
        RETURN TRUE;
      END IF;
      --
      RETURN FALSE;
    EXCEPTION
      WHEN OTHERS THEN  
        RETURN FALSE;
    END;    
    --
    PROCEDURE TRACE(
      P_PACKAGE_NAME  VARCHAR2, 
      P_PROC_NAME     VARCHAR2, 
      P_MESSAGGIO     VARCHAR2, 
      P_PASSO         VARCHAR2 := NULL
      ) IS
      --
    BEGIN
      IF CK_TRACE(P_PACKAGE_NAME, P_PROC_NAME) THEN
          BASE_UTIL_PCK.STAMPA_PL(
          P_PACKAGE_NAME ||'.'|| P_PROC_NAME||
            CASE 
              WHEN P_PASSO IS NOT NULL THEN '('||P_PASSO||') -> '
              ELSE '-> '
            END ||
          P_MESSAGGIO
          , 255);
      END IF;
    END TRACE;
--
END BASE_TRACE_PCK;
/
