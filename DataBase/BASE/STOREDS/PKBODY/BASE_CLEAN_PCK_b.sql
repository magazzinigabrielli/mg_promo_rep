CREATE OR REPLACE PACKAGE BODY FRAME."BASE_CLEAN_PCK" AS
  --
  /******************************************************************************
   NAME:       BASE_CLEAN_PCK
   PURPOSE:    Package di utilità per lo schema FRAME

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        12/02/2019           r.doti       1. Created this package.
   ******************************************************************************/
  --
  kNomePackage   CONSTANT VARCHAR2 (100) := 'BASE_CLEAN_PCK';
  kFlagAnn                VARCHAR2 (5)   := 'A';
  --
  PROCEDURE CLEAN_TABLE(
      PO_ERR_SEVERITA       IN OUT NOCOPY  VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY  VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY  VARCHAR2,
      P_COMMIT              IN             BOOLEAN  := TRUE
    ) AS
    vProcName VARCHAR2(100) := 'CLEAN_TABLE';
    vPasso    VARCHAR2(100);
  BEGIN
    --
    --Example (Parte da replicare)
    vPasso := 'TABLE1';
    --
    --DELETE TABLE1 WHERE COL1 <= ;
    
    BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'Row deleted:'||SQL%ROWCOUNT, vPasso);
    --
    IF(P_COMMIT) THEN
      BASE_UTIL_PCK.DO_COMMIT(vProcName);
    ELSE
      BASE_UTIL_PCK.DO_ROLLBACK(vProcName);
    END IF;
    --End Example 
   --
  EXCEPTION 
    WHEN OTHERS THEN
      BASE_TRACE_PCK.TRACE(kNomePackage, vProcName, 'ERRORE:'||SQLCODE ||' '||SQLERRM, vPasso);
      PO_ERR_SEVERITA := 'B';
      PO_ERR_CODICE     := SQLCODE;
      PO_MESSAGGIO      := kNomePackage || '.' || vProcName||'['||vPasso||']' || ' - Error: ' || SQLERRM;
  END CLEAN_TABLE;
  --
  PROCEDURE CLEAN_TABLE_JB
  AS
    vProcName        VARCHAR2(100) := 'CLEAN_TABLE';
    vPasso           VARCHAR2(100);
    PO_ERR_SEVERITA  VARCHAR2(2000);
    PO_ERR_CODICE    VARCHAR2(100);
    PO_MESSAGGIO     VARCHAR2(100);
    P_COMMIT         BOOLEAN       := TRUE;
  BEGIN
    --
    CLEAN_TABLE(
      PO_ERR_SEVERITA,
      PO_ERR_CODICE,
      PO_MESSAGGIO,
      P_COMMIT
    );
  END CLEAN_TABLE_JB;
--
END BASE_CLEAN_PCK;
/
