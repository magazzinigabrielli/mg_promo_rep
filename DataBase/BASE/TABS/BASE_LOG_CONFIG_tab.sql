CREATE TABLE BASE_LOG_CONFIG
(
   CNF_PKG            VARCHAR2 (255 BYTE) CONSTRAINT BASE_CNF_NN1 NOT NULL,
   CNF_NOTE           VARCHAR2 (100 BYTE),
   CNF_TRACE_LVL      NUMBER (1),
   CNF_STAT           VARCHAR2 (5 BYTE),
   CNF_DATAMODIFICA   TIMESTAMP (6) DEFAULT SYSTIMESTAMP
);

COMMENT ON COLUMN BASE_LOG_CONFIG.CNF_PKG IS 'Nome del package';
COMMENT ON COLUMN BASE_LOG_CONFIG.CNF_NOTE IS 'descrizione';
COMMENT ON COLUMN BASE_LOG_CONFIG.CNF_TRACE_LVL IS
   'Livello di trace per la funzione di LOG. Errore=1,Warning=2,Info=3,Debug=4, 0=non abilitato, -1=Log funzioni del pck';

CREATE UNIQUE INDEX BASE_CNF_PK_IDX
   ON BASE_LOG_CONFIG (CNF_PKG);


ALTER TABLE BASE_LOG_CONFIG ADD (
  CONSTRAINT BASE_CNF_DTM_NN
  CHECK ("CNF_DATAMODIFICA" IS NOT NULL)
  DISABLE NOVALIDATE,
  CONSTRAINT BASE_CNF_STAT
  CHECK (CNF_STAT   IN ('A'))
  ENABLE VALIDATE,
  CONSTRAINT BASE_CNF_PK
  PRIMARY KEY
  (CNF_PKG)
  USING INDEX BASE_CNF_PK_IDX
  ENABLE VALIDATE);