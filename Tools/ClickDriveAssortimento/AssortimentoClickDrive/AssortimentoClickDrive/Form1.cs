﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BasicDAL;
using Oracle.DataAccess.Client;
using System.Data.SqlServerCe;

namespace AssortimentoClickDrive
{
    public partial class Form1 : Form
    {
        private DbConfig OracleConfig;
       

        public Form1()
        {
            InitializeComponent();
            this.Load += new EventHandler(Init);

        }

        private void Init(object sender, EventArgs e)
        {   
            AppConfig config = new AppConfig();
            config.LoadConfig();
            OracleConfig     = config.OraDBCOnfig;
            SetDataGridViewDoubleBuffered(this.DataGridView1, true);
            //ELIMINO TUTTI GLI ITEM CONTENUTI NEL BOX DEI PDV e service
            PDVBox.Items.Clear();
            ServiceBox.Items.Clear();
            //INSERISCO I PDV LETTI DAL FILE CONFIG
            foreach (string item in config.PuntiVendita)
            {
                PDVBox.Items.Add(item);
            }
            foreach (string item in config.Servizi)
            {     
                ServiceBox.Items.Add(item);
            }
            CreaAlberoReparti(ref TreeView1);
            //throw new NotImplementedException();
        }

        public void SetDataGridViewDoubleBuffered(DataGridView dgv, bool setting)
        {
            Type dgvType    = dgv.GetType();
            PropertyInfo pi = dgvType.GetProperty("DoubleBuffered", BindingFlags.Instance | BindingFlags.NonPublic);
            pi.SetValue(dgv, setting, null);
        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void dataGridView1_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Verifico gli elementi selezionati nelle box dall'utente
            String pdv = PDVBox.GetItemText(PDVBox.SelectedItem);
            String service = ServiceBox.GetItemText(ServiceBox.SelectedItem);
            if (service.Equals(""))
            {
                 //l'utente deve effettuare sia la scelta del servizio che del pdv
            }
            else
            {   
                //ricarico albero reparti
                CreaAlberoReparti(ref TreeView1);
                //splitto il codice pdv
                string[] codiNegozio          = pdv.Split('-');
                string[] codiServizio         = service.Split('-');
                MGpdvRelService PdvCheck      = new MGpdvRelService();
                PdvCheck.Init(this.OracleConfig);
                BasicDAL.DbFilters fPdvCheck  = new BasicDAL.DbFilters();
                fPdvCheck.Add(PdvCheck.PVSER_PDV_COD,  BasicDAL.ComparisionOperator.Equal, codiNegozio[0], BasicDAL.LogicOperator.AND);
                fPdvCheck.Add(PdvCheck.PVSER_SERVIZIO, BasicDAL.ComparisionOperator.Equal, codiServizio[1].Replace(" ", ""), BasicDAL.LogicOperator.AND);
                PdvCheck.FiltersGroup.Add(fPdvCheck);
                PdvCheck.LoadAll();
                if (PdvCheck.RowCount > 0)
                {
                    //Attivo le label e gli elementi che devono essere visibili dopo la pressione del tasto ricerca
                    LvlClsBox.Visible                = true;
                    TreeView1.Visible                = true;
                    //pulisco la tabella della class comm
                    this.DataGridView1.DataSource    = null;
                    this.DataGridView1.Rows.Clear();
                    DataGridView1.Visible            = true;
                    lvlClsBox2.Visible               = true;
                    artBox.Visible                   = true;
                    //pulisco tabella articoli
                    this.ArticoliGridView.DataSource = null;
                    this.ArticoliGridView.Rows.Clear();
                    ArticoliGridView.Visible         = true;
                }
                else
                {
                    //disattivo le label e gli elementi che devono essere visibili dopo la pressione del tasto ricerca
                    LvlClsBox.Visible                = false;
                    TreeView1.Visible                = false;
                    DataGridView1.Visible            = false;
                    lvlClsBox2.Visible               = false;
                    artBox.Visible                   = false;
                    ArticoliGridView.Visible         = false;
                    MessageBox.Show("ATTENZIONE: Il punto di vendita selezionato non è abilitato ad effettuare il servizio richiesto");

                }
            }
           
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Label2_Click(object sender, EventArgs e)
        {

        }

        private void Label5_Click(object sender, EventArgs e)
        {

        }

        private void RicercaButton_Click(object sender, EventArgs e)
        {

            /* BOTTONE CANCELLATO
            //Verifico gli elementi selezionati nelle box dall'utente
            String pdv     =  PDVBox.GetItemText(PDVBox.SelectedItem);
            String service = ServiceBox.GetItemText(ServiceBox.SelectedItem);
            //Console.WriteLine("Ho intercettato"+pdv + service);
            if (pdv.Equals("") || service.Equals(""))
            {
                //Stampo a video un messaggio di errore
                MessageBox.Show("ATTENZIONE: E' obbligatorio selezionare un pdv e un servizio ");
            }
            else
            {
                //splitto il codice pdv
                string[] codiNegozio = pdv.Split('-');
                string[] codiServizio = service.Split('-');
                MGpdvRelService PdvCheck = new MGpdvRelService();
                PdvCheck.Init(this.OracleConfig);
                BasicDAL.DbFilters fPdvCheck = new BasicDAL.DbFilters();
                fPdvCheck.Add(PdvCheck.PVSER_PDV_COD, BasicDAL.ComparisionOperator.Equal, codiNegozio[0], BasicDAL.LogicOperator.AND);
                fPdvCheck.Add(PdvCheck.PVSER_SERVIZIO, BasicDAL.ComparisionOperator.Equal, codiServizio[1].Replace(" ",""), BasicDAL.LogicOperator.AND);
                PdvCheck.FiltersGroup.Add(fPdvCheck);
                PdvCheck.LoadAll();
                if (PdvCheck.RowCount > 0)
                {
                    //Attivo le label e gli elementi che devono essere visibili dopo la pressione del tasto ricerca
                    LvlClsBox.Visible        = true;
                    TreeView1.Visible        = true;
                    DataGridView1.Visible    = true;
                    lvlClsBox2.Visible       = true;
                    artBox.Visible           = true;
                    ArticoliGridView.Visible = true;
                    //disabilito i pulsanti per non permettere il cambio di service e pdv
                    ServiceBox.Enabled       = false;
                    PDVBox.Enabled           = false;
                }else
                {
                    //disattivo le label e gli elementi che devono essere visibili dopo la pressione del tasto ricerca
                    LvlClsBox.Visible        = false;
                    TreeView1.Visible        = false;
                    DataGridView1.Visible    = false;
                    lvlClsBox2.Visible       = false;
                    artBox.Visible           = false;
                    ArticoliGridView.Visible = false;
                    MessageBox.Show("ATTENZIONE: Il punto di vendita selezionato non è abilitato ad effettuare il servizio richiesto");
                    
                }
                
                //richiamo la funzione per la ricerca dei dati
                
                MG_GEAD010F DB_ANAG_ARTICOLI = new MG_GEAD010F();
                //
                MgClassificazioneCommerciale MG_CLS_COM = new MgClassificazioneCommerciale();
                MG_CLS_COM.Init(this.OracleConfig);
                
                DB_ANAG_ARTICOLI.Init(this.OracleConfig);
                DbFilters fDB_ANAG_ARTICOLI = new BasicDAL.DbFilters();
                fDB_ANAG_ARTICOLI.Clear();
                fDB_ANAG_ARTICOLI.Add(DB_ANAG_ARTICOLI.E1CART, BasicDAL.ComparisionOperator.Equal, 10009, BasicDAL.LogicOperator.AND);
                DB_ANAG_ARTICOLI.FiltersGroup.Clear();
                DB_ANAG_ARTICOLI.FiltersGroup.Add(fDB_ANAG_ARTICOLI);
                DB_ANAG_ARTICOLI.LoadAll();
                DB_ANAG_ARTICOLI.MoveFirst();
                MG_CLS_COM.LoadAll();
                MG_CLS_COM.MoveFirst();

                //
                for (int i = 0; i < MG_CLS_COM.RowCount; i++)
                {
                    
                }
                for (int i = 0; i < DB_ANAG_ARTICOLI.RowCount; i++)
                {
         
                    DB_ANAG_ARTICOLI.MoveNext();
                }TEST
                 
            }*/

        }

        private void BackgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

        }

        private void GroupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void DataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //utilizzato per richiamare il metodo che si occupa di creare la vista per gli articoli
            //string che immagazzina il valore del codice negezio
            string[] codNegozio;
            string[] codServizio;
            string codiNegozio = "0";
            string servizio    = "";
            //se l'elem selezionato è null o vuoto ritorno 
            if (string.IsNullOrEmpty(PDVBox.GetItemText(PDVBox.SelectedItem).ToString()))
            {
                return;
            }
            if (string.IsNullOrEmpty(ServiceBox.GetItemText(ServiceBox.SelectedItem).ToString()))
            {
                return;
            }
            //splitto il codice negozio
            codServizio  = ServiceBox.GetItemText(ServiceBox.SelectedItem).ToString().Split('-');
            codNegozio   = PDVBox.GetItemText(PDVBox.SelectedItem).ToString().Split('-');
            //assegno i valori alle variabili
            if (!string.IsNullOrEmpty(codNegozio[0]) && !string.IsNullOrEmpty(codServizio[0]))
            {
                codiNegozio = codNegozio[0];
                servizio    = codServizio[1];
            }
            if (int.Parse(codiNegozio) == 0)
            {
                return;
            }
            if (string.IsNullOrEmpty(servizio))
            {
                return;
            }
            int columnNameIndex            = this.DataGridView1.CurrentCell.ColumnIndex;
            string columnName              = DataGridView1.Columns[columnNameIndex].Name;
            
            if (this.DataGridView1.CurrentCell.Value==null)
            {
                return;
            }
            string cellValue                = this.DataGridView1.CurrentCell.Value.ToString();
            //se ho cliccato sulla cella di un reparto
            if (columnName.Equals("Reparto"))
            {   
                //indice riga selezionata
                int selectedrowindex        = DataGridView1.SelectedCells[0].RowIndex;
                //riga selezionata
                DataGridViewRow selectedRow = DataGridView1.Rows[selectedrowindex];
                //reparto della riga selezionata
                string Reparto              = Convert.ToString(selectedRow.Cells["Reparto"].Value);
                /* DECOMMENTARE SE SI VUOLE RENDERE EFFETTIVA L'ESPLOSIONE PER REPARTO
                 * CreaVistaArticoli(codiNegozio,servizio,this.ArticoliGridView,Reparto,"","","");*/
            }
            //se ho cliccato sulla cella di un ug
            else if (columnName.Equals("UG"))
            {
                //indice riga selezionata
                int selectedrowindex         = DataGridView1.SelectedCells[0].RowIndex;
                //riga selezionata
                DataGridViewRow selectedRow  = DataGridView1.Rows[selectedrowindex];
                //reparto della riga selezionata
                string Reparto               = Convert.ToString(selectedRow.Cells["Reparto"].Value);
                //Ug della riga selezionata
                string UG                    = Convert.ToString(selectedRow.Cells["UG"].Value);
                /* DECOMMENTARE SE SI VUOLE RENDERE EFFETTIVA L'ESPLOSIONE PER UG 
                 * CreaVistaArticoli(codiNegozio, servizio, this.ArticoliGridView, Reparto, UG, "", "");*/
            }
            //se ho cliccato sulla cella di un raggruppamento
            else if (columnName.Equals("Raggruppamento"))
            {
                //indice riga selezionata
                int selectedrowindex        = DataGridView1.SelectedCells[0].RowIndex;
                //riga selezionata
                DataGridViewRow selectedRow = DataGridView1.Rows[selectedrowindex];
                //reparto della riga selezionata
                string Reparto              = Convert.ToString(selectedRow.Cells["Reparto"].Value);
                //Ug della riga selezionata
                string UG                   = Convert.ToString(selectedRow.Cells["UG"].Value);
                //Raggruppamento della riga selezionata
                string Raggruppamento       = Convert.ToString(selectedRow.Cells["Raggruppamento"].Value);
                /* DECOMMENTARE SE SI VUOLE RENDERE EFFETTIVA L'ESPLOSIONE ARTICOLI PER RAG
                 * CreaVistaArticoli(codiNegozio, servizio, this.ArticoliGridView, Reparto, UG, Raggruppamento, "");*/
            }
            //se ho cliccato sulla cella di una famiglia
            else if (columnName.Equals("cCodiceFamigliaEx"))
            {
                //indice riga selezionata
                int selectedrowindex        = DataGridView1.SelectedCells[0].RowIndex;
                //riga selezionata
                DataGridViewRow selectedRow = DataGridView1.Rows[selectedrowindex];
                //reparto della riga selezionata
                string[] Reparto            = Convert.ToString(selectedRow.Cells["Reparto"].Value).Split('-');
                //Ug della riga selezionata
                string[] UG                 = Convert.ToString(selectedRow.Cells["UG"].Value).Split('-');
                //Raggruppamento della riga selezionata
                string[] Raggruppamento     = Convert.ToString(selectedRow.Cells["Raggruppamento"].Value).Split('-');
                //Famiglia della riga selezionata
                string[] Famiglia           = Convert.ToString(selectedRow.Cells["cCodiceFamigliaEx"].Value).Split('-');
                CreaVistaArticoli(codiNegozio, servizio, this.ArticoliGridView, Reparto[0], UG[0], Raggruppamento[0],Famiglia[0]);
            }


        }
        private void CreaVistaArticoli(string CodiceNegozio, string CodiceServizio, DataGridView dataGridView1, string rep, string uG, string rAG, string fAM)
        {
            MG_GEAD010F ART              = new MG_GEAD010F();
            ART.Init(this.OracleConfig);
            BasicDAL.DbFilters fART      = new BasicDAL.DbFilters();
            fART.Add(ART.E1CREP, BasicDAL.ComparisionOperator.Equal, rep, BasicDAL.LogicOperator.AND);
            fART.Add(ART.E1CUGE, BasicDAL.ComparisionOperator.Equal, uG, BasicDAL.LogicOperator.AND);
            fART.Add(ART.E1CRAG, BasicDAL.ComparisionOperator.Equal, rAG, BasicDAL.LogicOperator.AND);
            fART.Add(ART.E1CFAM, BasicDAL.ComparisionOperator.Equal, fAM, BasicDAL.LogicOperator.AND);
            /*if (uG!="")
            {
                fART.Add(ART.E1CUGE, BasicDAL.ComparisionOperator.Equal, uG, BasicDAL.LogicOperator.AND);
            }
            if (rAG != "")
            {
                fART.Add(ART.E1CRAG, BasicDAL.ComparisionOperator.Equal, rAG, BasicDAL.LogicOperator.AND);
            }
            if (fAM!= "")
            {
                fART.Add(ART.E1CFAM, BasicDAL.ComparisionOperator.Equal, fAM, BasicDAL.LogicOperator.AND);
            }  DECOMMENTARE SE SI VUOLE RENDERE EFFETTIVA LA RICERCA DI ARTICOLI ANCHE A LIVELLI DI CLASS DIVERSI DALLA FAMIGLIA*/
            ART.FiltersGroup.Add(fART);
            ART.LoadAll();
            //vado al primo art
            ART.MoveFirst();
            //modifico le proprietà di default della grid view
            this.ArticoliGridView.AutoGenerateColumns         = false;
            this.ArticoliGridView.DefaultCellStyle.Font       = new Font("Tahoma", 8);
            this.ArticoliGridView.DefaultCellStyle.ForeColor  = Color.Black;
            //assegno alla grid view la data table della class commerciale
            this.ArticoliGridView.DataSource                  = ART.DataTable;
            //ciclo sul data source della grid
            foreach (DataGridViewRow Row in ArticoliGridView.Rows)
            {
                MG_GEAD030F ART_DETT                          = new MG_GEAD030F();
                ART_DETT.Init(this.OracleConfig);
                BasicDAL.DbFilters fART_DETT                  = new BasicDAL.DbFilters();
                fART_DETT.Add(ART_DETT.E3CPDV,                BasicDAL.ComparisionOperator.Equal, CodiceNegozio, BasicDAL.LogicOperator.AND);
                fART_DETT.Add(ART_DETT.E3CART,                BasicDAL.ComparisionOperator.Equal, ART.E1CART.Value.ToString(), BasicDAL.LogicOperator.AND);
                ART_DETT.FiltersGroup.Add(fART_DETT);
                ART_DETT.LoadAll();
                //se il negozio ha quell'articolo in assortimento e non è flaggato come A ossia annullato
                if (ART_DETT.RowCount > 0 && !ART_DETT.E3STAT.Value.Equals('A'))
                {
                    //creo oggetto mg_ass_serv per verificare se l'articolo è checkato o meno 
                    MGAssortimentoServizi check               = new MGAssortimentoServizi();
                    check.Init(this.OracleConfig);
                    BasicDAL.DbFilters fCHECK = new           BasicDAL.DbFilters();
                    fCHECK.Add(check.ASRSV_PDV_COD,           BasicDAL.ComparisionOperator.Equal, CodiceNegozio.Replace(" ", ""), BasicDAL.LogicOperator.AND);
                    fCHECK.Add(check.ASRSV_SERVIZIO,          BasicDAL.ComparisionOperator.Equal, CodiceServizio.Replace(" ", ""), BasicDAL.LogicOperator.AND);
                    fCHECK.Add(check.ASRSV_ART_COD,           BasicDAL.ComparisionOperator.Equal, ART_DETT.E3CART.Value, BasicDAL.LogicOperator.AND);
                    check.FiltersGroup.Add(fCHECK);
                    check.LoadAll();
                    //aggiungo articolo cod_desc e flg in assortimento 
                    Row.Cells["CodiceArticolo"].Value         = ART_DETT.E3CART.Value;
                    //prendo da gead 10f la desc art
                    MG_GEAD010F ART_DESC = new MG_GEAD010F();
                    ART_DESC.Init(this.OracleConfig);
                    BasicDAL.DbFilters fART_DESC              = new BasicDAL.DbFilters();
                    fART_DESC.Add(ART.E1CART,                 BasicDAL.ComparisionOperator.Equal, ART_DETT.E3CART.Value, BasicDAL.LogicOperator.AND);
                    ART_DESC.FiltersGroup.Add(fART_DESC);
                    ART_DESC.LoadAll();
                    Row.Cells["DescrizioneArticolo"].Value    = ART_DESC.E1XARC.Value;
                    //flg in base al valore del campo restituito dal db.
                    if (check.RowCount!=0)
                    {    
                        //se il flg in ass è s allora check articolo
                        if (check.ASRSV_IN_ASSORTIMENTO.Value.Equals("S"))
                        {
                            Row.Cells["InAssortimento"].Value  = true;
                        }
                        else //no altrimenti
                        {
                            Row.Cells["InAssortimento"].Value  = false;
                        }
                        
                    }
                    else //articolo non nella tabella quindi checkbox su false
                    {
                            Row.Cells["InAssortimento"].Value   = false;
                    }
                   
                }
                ART.MoveNext();//scorro al prossimo articolo
            }
            //elimino le righe vuote 
            for (int i = ArticoliGridView.Rows.Count - 1; i > -1; i--)
            {
                DataGridViewRow row = ArticoliGridView.Rows[i];
                if (!row.IsNewRow && row.Cells[0].Value == null)
                {
                    ArticoliGridView.Rows.RemoveAt(i);
                }
            }
        }
        private void CreaAlberoReparti(ref TreeView tv)
        {
            MgClassificazioneCommerciale MG_CLS_COM = new MgClassificazioneCommerciale();
            MG_CLS_COM.Init(this.OracleConfig);
            MG_CLS_COM.LoadAll();
            int _CodiceReparto         = 0;
            int _CodiceUnitàGestione   = 0;
            int _CodiceRraggruppamento = 0;
            int _CodiceFamiglia        = 0;
            int nIndex                 = 0;
            TreeNode cN                = new TreeNode();
            TreeNode nUG               = new TreeNode();
            TreeNode nR                = new TreeNode();
            TreeNode nF                = new TreeNode();
            tv.Nodes.Clear();
            for (int i = 0; i < MG_CLS_COM.RowCount; i++)
            {
                if (int.Parse(MG_CLS_COM.E1CREP.Value.ToString())!=_CodiceReparto)
                {
                    //inserisco codice reparto nell'albero
                    cN                 = tv.Nodes.Add(MG_CLS_COM.E1CREP.Value.ToString(), MG_CLS_COM.E1CREP.Value.ToString()+" - " + MG_CLS_COM.E1CG2D.Value.ToString());
                    cN.Tag             = MG_CLS_COM.E1CREP.Value.ToString();
                }
                if(int.Parse(MG_CLS_COM.E1CUGE.Value.ToString())!=_CodiceUnitàGestione)
                {   
                    //inserisco codice UG nell'albero
                    nUG                = cN.Nodes.Add(MG_CLS_COM.E1CREP.Value.ToString() + '.' +MG_CLS_COM.E1CUGE.Value.ToString(), MG_CLS_COM.E1CUGE.Value.ToString() + '-'+MG_CLS_COM.E1CG3D.Value.ToString());
                    nUG.Tag            = MG_CLS_COM.E1CUGE.Value.ToString();
                }
                if(int.Parse(MG_CLS_COM.E1CRAG.Value.ToString())!=_CodiceRraggruppamento)
                {   
                    //inserisco cod raggruppamento nell'albero
                    nR                 = nUG.Nodes.Add(MG_CLS_COM.E1CREP.Value.ToString()+'.'+MG_CLS_COM.E1CUGE.Value.ToString()+'.'+MG_CLS_COM.E1CRAG.Value.ToString(), MG_CLS_COM.E1CRAG.Value.ToString()+'-'+MG_CLS_COM.E1CG4D.Value.ToString());
                    nR.Tag             = MG_CLS_COM.E1CRAG.Value.ToString();
                }
                if(int.Parse(MG_CLS_COM.E1CFAM.Value.ToString()) != _CodiceFamiglia)
                {   
                    //inserisco codice famiglia nell'albero
                    nF                 = nR.Nodes.Add(MG_CLS_COM.E1CREP.Value.ToString() + '.' + MG_CLS_COM.E1CUGE.Value.ToString() + '.' +MG_CLS_COM.E1CRAG.Value.ToString()+'.'+MG_CLS_COM.E1CFAM.Value.ToString(),MG_CLS_COM.E1CFAM.Value.ToString()+'-'+MG_CLS_COM.E1CG5D.Value.ToString());
                    nF.Tag             = MG_CLS_COM.E1CFAM.Value.ToString();
                }
                _CodiceReparto         = int.Parse(MG_CLS_COM.E1CREP.Value.ToString());
                _CodiceUnitàGestione   = int.Parse(MG_CLS_COM.E1CUGE.Value.ToString());
                _CodiceRraggruppamento = int.Parse(MG_CLS_COM.E1CRAG.Value.ToString());
                _CodiceFamiglia        = int.Parse(MG_CLS_COM.E1CFAM.Value.ToString());
                MG_CLS_COM.MoveNext();
            }
            //dispose connection
            MG_CLS_COM.Dispose();
        }

            private void CaricaAssortimentoUI()
        {
            //string che immagazzina il valore del codice negezio
            string[] codNegozio;
            string[] codServizio;
            string codiNegozio   ="0";
            string codiServizio  ="";
            //se l'elem selezionato è null o vuoto ritorno 
            if (string.IsNullOrEmpty(PDVBox.GetItemText(PDVBox.SelectedItem).ToString()))
            {
                return;
            }
            if (string.IsNullOrEmpty(ServiceBox.GetItemText(ServiceBox.SelectedItem).ToString()))
            {
                return;
            }
            //splitto il codice negozio derivante dalle select box
            codServizio      = ServiceBox.GetItemText(ServiceBox.SelectedItem).ToString().Split('-');
            codNegozio       = PDVBox.GetItemText(PDVBox.SelectedItem).ToString().Split('-');
            //creo stringa avente valore negozio
            if (!string.IsNullOrEmpty(codNegozio[0])&& !string.IsNullOrEmpty(codServizio[0]))
            {
                codiNegozio  = codNegozio[0];
                codiServizio = codServizio[1];
            }
            if(int.Parse(codiNegozio)==0)
            {
                return;
            }
            if (string.IsNullOrEmpty(codiServizio))
            {
                return;
            }
            this.DataGridView1.Select();
            string Rep      = null;
            string UG       = null;
            string RAG      = null;
            string FAM      = null;

            string[] x = this.TreeView1.SelectedNode.Name.Split('.');
            //switch lunghezza di x
            if (x.Length == 1)
            {
                Rep = x[0];
            }else if (x.Length == 2)
            {
                Rep = x[0];
                UG = x[1];
            }else if (x.Length == 3)
            {
                Rep = x[0];
                UG = x[1];
                RAG = x[2];
            }else if (x.Length ==4)
            {
                Rep = x[0];
                UG = x[1];
                RAG = x[2];
                FAM = x[3];
            }
            this.CaricaAssortimento(codiNegozio,codiServizio ,this.DataGridView1, Rep, UG, RAG, FAM);
            this.DataGridView1.Select();

        }

        private void CaricaAssortimento(string CodiceNegozio, string CodiceServizio,DataGridView dataGridView1, string rep, string uG, string rAG, string fAM)
        {   
            //assortimento servizi
            MGAssortimentoServizi DB                                = new MGAssortimentoServizi();
            BasicDAL.DbFilters fDB                                  = new BasicDAL.DbFilters();
            DB.Init(this.OracleConfig);
            //var classificazione commerciale
            MgClassificazioneCommerciale MG_CLS_COM                 = new MgClassificazioneCommerciale();
            MG_CLS_COM.Init(this.OracleConfig);
            MG_CLS_COM.LoadAll();
            //MgClassificazioneCommerciale DB_WEB_REUGRAGFAM                               = new MgClassificazioneCommerciale();
            BasicDAL.DbFilters fMG_CLS_COM                          = new BasicDAL.DbFilters();
            //MgClassificazioneCommerciale DB_WEB_ASSORTIMENTO_CLICKCOLLECT = new MgClassificazioneCommerciale();
            //DB_WEB_ASSORTIMENTO_CLICKCOLLECT.Init(this.OracleConfig);
            //DB_WEB_ASSORTIMENTO_CLICKCOLLECT.Init().OrderBy(DB_WEB_ASSORTIMENTO_CLICKCOLLECT.CodiceReparto.DBColumnName+','+ DB_WEB_ASSORTIMENTO_CLICKCOLLECT.CodiceUnitàGestione.DBColumnName+','+ DB_WEB_ASSORTIMENTO_CLICKCOLLECT.CodiceRaggruppamento.DBColumnName+','+ DB_WEB_ASSORTIMENTO_CLICKCOLLECT.CodiceFamiglia.DBColumnName);
            if (!string.IsNullOrEmpty(rep))
            {
                fMG_CLS_COM.Add(MG_CLS_COM.E1CREP, BasicDAL.ComparisionOperator.Equal, int.Parse(rep), BasicDAL.LogicOperator.AND);
            }
            if (!string.IsNullOrEmpty(uG))
            {
                fMG_CLS_COM.Add(MG_CLS_COM.E1CUGE, BasicDAL.ComparisionOperator.Equal, int.Parse(uG), BasicDAL.LogicOperator.AND);
            }
            if (!string.IsNullOrEmpty(rAG))
            {
                fMG_CLS_COM.Add(MG_CLS_COM.E1CRAG, BasicDAL.ComparisionOperator.Equal, int.Parse(rAG), BasicDAL.LogicOperator.AND);
            }
            if (!string.IsNullOrEmpty(fAM))
            {
                fMG_CLS_COM.Add(MG_CLS_COM.E1CFAM, BasicDAL.ComparisionOperator.Equal, int.Parse(fAM), BasicDAL.LogicOperator.AND);
            }
            MG_CLS_COM.FiltersGroup.Add(fMG_CLS_COM);
            MG_CLS_COM.LoadAll();
            try
            {
                //dataGridView1.Rows.Clear();
                dataGridView1.Refresh();
                System.Windows.Forms.Application.DoEvents();
            }catch (Exception ex)
            {

            }
            //modifico le proprietà di default della grid view
            this.DataGridView1.AutoGenerateColumns        = false;
            this.DataGridView1.DefaultCellStyle.Font      = new Font("Tahoma", 8);
            this.DataGridView1.DefaultCellStyle.ForeColor = Color.Black;
            //assegno alla grid view la data table della class commerciale
            this.DataGridView1.DataSource                 = MG_CLS_COM.DataTable;
            //vado al primo elemento da caricare
            MG_CLS_COM.MoveFirst();
            foreach (DataGridViewRow Row in DataGridView1.Rows)
           {
                 fDB.Clear();
                 DB.FiltersGroup.Clear();
                 //creo i filtri
                 fDB.Add(DB.ASRSV_PDV_COD, BasicDAL.ComparisionOperator.Equal, CodiceNegozio, BasicDAL.LogicOperator.AND);
                 fDB.Add(DB.ASRSV_SERVIZIO,BasicDAL.ComparisionOperator.Equal, CodiceServizio.Replace(" ",""), BasicDAL.LogicOperator.AND);
                 fDB.Add(DB.ASRSV_REPARTO, BasicDAL.ComparisionOperator.Equal, MG_CLS_COM.E1CREP.Value, BasicDAL.LogicOperator.AND);
                 fDB.Add(DB.ASRSV_UG,      BasicDAL.ComparisionOperator.Equal, MG_CLS_COM.E1CUGE.Value, BasicDAL.LogicOperator.AND);
                 fDB.Add(DB.ASRSV_RAGG,    BasicDAL.ComparisionOperator.Equal, MG_CLS_COM.E1CRAG.Value, BasicDAL.LogicOperator.AND);
                 fDB.Add(DB.ASRSV_FAM,     BasicDAL.ComparisionOperator.Equal, MG_CLS_COM.E1CFAM.Value);
                 //aggiungo i filtri
                 DB.FiltersGroup.Add(fDB);
                 //carico tutto 
                 DB.LoadAll();
                 //assegno i valori alle celle della grid view
                 if (int.Parse(DB.RowCount.ToString()) == 1)
                 {
                    if (DB.ASRSV_IN_ASSORTIMENTO.Value.Equals("S"))//esiste una riga con questi param ed è settata su S allora la checko
                    {
                        Row.Cells["Reparto"].Value           = MG_CLS_COM.E1CREP.Value + " -" + MG_CLS_COM.E1CG2D.Value;
                        Row.Cells["UG"].Value                = MG_CLS_COM.E1CUGE.Value + " -" + MG_CLS_COM.E1CG3D.Value;
                        Row.Cells["cCodiceFamigliaEx"].Value = MG_CLS_COM.E1CFAM.Value + " -" + MG_CLS_COM.E1CG4D.Value;
                        Row.Cells["Raggruppamento"].Value    = MG_CLS_COM.E1CRAG.Value + " -" + MG_CLS_COM.E1CG5D.Value;
                        Row.Cells["cInAssortimento"].Value   = true;
                    }
                    else//c'è la riga ,ma non è settata su S
                    {
                        Row.Cells["Reparto"].Value           = MG_CLS_COM.E1CREP.Value + " -" + MG_CLS_COM.E1CG2D.Value;
                        Row.Cells["UG"].Value                = MG_CLS_COM.E1CUGE.Value + " -" + MG_CLS_COM.E1CG3D.Value;
                        Row.Cells["cCodiceFamigliaEx"].Value = MG_CLS_COM.E1CFAM.Value + " -" + MG_CLS_COM.E1CG4D.Value;
                        Row.Cells["Raggruppamento"].Value    = MG_CLS_COM.E1CRAG.Value + " -" + MG_CLS_COM.E1CG5D.Value;
                        Row.Cells["cInAssortimento"].Value   = false;
                    }
                    
                 }
                 else //il db non contiene righe per la cls_comm corrente quindi la setto su false
                 {

                    Row.Cells["Reparto"].Value                 = MG_CLS_COM.E1CREP.Value+" -"+MG_CLS_COM.E1CG2D.Value;
                    Row.Cells["UG"].Value                      = MG_CLS_COM.E1CUGE.Value+" -" + MG_CLS_COM.E1CG3D.Value;
                    Row.Cells["cCodiceFamigliaEx"].Value       = MG_CLS_COM.E1CFAM.Value+" -" + MG_CLS_COM.E1CG4D.Value;
                    Row.Cells["Raggruppamento"].Value          = MG_CLS_COM.E1CRAG.Value+ " -" + MG_CLS_COM.E1CG5D.Value;
                    Row.Cells["cInAssortimento"].Value         = false;
                 }
                MG_CLS_COM.MoveNext();
             }
            DB.Dispose();
        }

        private void TreeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            //Carico assortimento
            CaricaAssortimentoUI();

        }

        private void RicercaButton_AutoSizeChanged(object sender, EventArgs e)
        {

        }

        private void ArticoliGridView_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            //string che immagazzina il valore del codice negezio
            string[] codNegozio;
            string[] codServizio;
            //se l'elem selezionato è null o vuoto ritorno 
            if (string.IsNullOrEmpty(PDVBox.GetItemText(PDVBox.SelectedItem).ToString()))
            {
                return;
            }
            if (string.IsNullOrEmpty(ServiceBox.GetItemText(ServiceBox.SelectedItem).ToString()))
            {
                return;
            }
            //splitto il codice negozio
            codServizio         = ServiceBox.GetItemText(ServiceBox.SelectedItem).ToString().Split('-');
            codNegozio          = PDVBox.GetItemText(PDVBox.SelectedItem).ToString().Split('-');
            //click di un cell content della ArticoliGridView
            int columnNameIndex = this.ArticoliGridView.CurrentCell.ColumnIndex;
            string columnName   = ArticoliGridView.Columns[columnNameIndex].Name;
            if (columnName.Equals("InAssortimento"))
            {
                //se sto ckeckando un qualcosa di null return
                if (this.ArticoliGridView.CurrentCell.Value == null)
                {
                    return;
                }
                //se current era false allora sto checkando
                if (this.ArticoliGridView.CurrentCell.Value.Equals(false))
                {    
                    //prendo i dati della riga che ho checkato
                    int rowIndex                          = this.ArticoliGridView.CurrentCell.RowIndex;
                    DataGridViewRow selectedRow           = ArticoliGridView.Rows[rowIndex];
                    string cArt                           = Convert.ToString(selectedRow.Cells["CodiceArticolo"].Value);
                    string DescArt = Convert.ToString(selectedRow.Cells["DescrizioneArticolo"].Value);
                    MGAssortimentoServizi ASS_SERV        = new MGAssortimentoServizi();
                    ASS_SERV.Init(this.OracleConfig);
                    BasicDAL.DbFilters fASS_SERV          =  new BasicDAL.DbFilters();
                    fASS_SERV.Add(ASS_SERV.ASRSV_PDV_COD, BasicDAL.ComparisionOperator.Equal, int.Parse(codNegozio[0]), BasicDAL.LogicOperator.AND);
                    fASS_SERV.Add(ASS_SERV.ASRSV_SERVIZIO,BasicDAL.ComparisionOperator.Equal, codServizio[1].Replace(" ", ""), BasicDAL.LogicOperator.AND);
                    fASS_SERV.Add(ASS_SERV.ASRSV_ART_COD, BasicDAL.ComparisionOperator.Equal, int.Parse(cArt), BasicDAL.LogicOperator.AND);
                    ASS_SERV.FiltersGroup.Add(fASS_SERV);
                    ASS_SERV.LoadAll();
                    //se row count non è 0 modifico la riga
                    if (ASS_SERV.RowCount > 0)
                    {
                        this.OracleConfig.BeginTransaction();
                        ASS_SERV.ASRSV_IN_ASSORTIMENTO.Value = "S";
                        ASS_SERV.Update();
                        this.OracleConfig.CommitTransaction();
                        Console.WriteLine("Modifico la riga e metto s");
                    }
                    else
                    {
                        // creo timestamp
                        String timeStamp                                 = DateTime.Now.ToString();
                        //assegno i valori
                        MGAssortimentoServizi ASS_SERVICE                = new MGAssortimentoServizi();
                        ASS_SERVICE.Init(this.OracleConfig);
                        //inizio transazione dati
                        this.OracleConfig.BeginTransaction();
                        ASS_SERVICE.AddNew(false);
                        ASS_SERVICE.ASRSV_ART_COD.Value                  = int.Parse(cArt);
                        //oggetto per creare asss_Service_id
                        ASS_SERV_ID GUID                                 = new ASS_SERV_ID();
                        GUID.Init(this.OracleConfig);
                        GUID.LoadAll();
                        ASS_SERVICE.ASRSV_PDV_COD.Value                  = codNegozio[0];
                        ASS_SERVICE.ASRSV_SERVIZIO.Value                 = codServizio[1].Replace(" ", "");
                        ASS_SERVICE.ASRSV_ASSORTIMENTO_SERVIZIO_ID.Value = GUID.GUID.Value;
                        ASS_SERVICE.ASRSV_REPARTO.Value                  = null;
                        ASS_SERVICE.ASRSV_UG.Value                       = null;
                        ASS_SERVICE.ASRSV_RAGG.Value                     = null;
                        ASS_SERVICE.ASRSV_FAM.Value                      = null;
                        ASS_SERVICE.ASRSV_IN_ASSORTIMENTO.Value          = "S";
                        ASS_SERVICE.ASRSV_DATA_MOD.Value                 = timeStamp;
                        ASS_SERVICE.ASRSV_STAT.Value                     = null;
                        ASS_SERVICE.Update();
                        //commit transazione
                        this.OracleConfig.CommitTransaction();
                        Console.WriteLine("Aggiungo riga articolo con in ass s");
                    }
                    
                }
                //se current era true allora sto decheckando
                if (this.ArticoliGridView.CurrentCell.Value.Equals(true))
                {
                    //prendo i dati della riga che ho checkato
                    int rowIndex                          = this.ArticoliGridView.CurrentCell.RowIndex;
                    DataGridViewRow selectedRow           = ArticoliGridView.Rows[rowIndex];
                    string cArt                           = Convert.ToString(selectedRow.Cells["CodiceArticolo"].Value);
                    string DescArt                        = Convert.ToString(selectedRow.Cells["DescrizioneArticolo"].Value);
                    MGAssortimentoServizi ASS_SERV        = new MGAssortimentoServizi();
                    ASS_SERV.Init(this.OracleConfig);
                    BasicDAL.DbFilters fASS_SERV          = new  BasicDAL.DbFilters();
                    fASS_SERV.Add(ASS_SERV.ASRSV_PDV_COD, BasicDAL.ComparisionOperator.Equal, int.Parse(codNegozio[0]), BasicDAL.LogicOperator.AND);
                    fASS_SERV.Add(ASS_SERV.ASRSV_SERVIZIO,BasicDAL.ComparisionOperator.Equal, codServizio[1].Replace(" ", ""), BasicDAL.LogicOperator.AND);
                    fASS_SERV.Add(ASS_SERV.ASRSV_ART_COD, BasicDAL.ComparisionOperator.Equal, int.Parse(cArt), BasicDAL.LogicOperator.AND);
                    ASS_SERV.FiltersGroup.Add(fASS_SERV);
                    ASS_SERV.LoadAll();
                    //se row count non è 0 modifico la riga
                    if (ASS_SERV.RowCount > 0)
                    {
                        this.OracleConfig.BeginTransaction();
                        ASS_SERV.ASRSV_IN_ASSORTIMENTO.Value = "N";
                        ASS_SERV.Update();
                        this.OracleConfig.CommitTransaction();
                        Console.WriteLine("Modifico la riga e metto n");
                    }
                    else
                    {
                        // creo timestamp
                        String timeStamp                                 = DateTime.Now.ToString();
                        //assegno i valori
                        MGAssortimentoServizi ASS_SERVICE                = new MGAssortimentoServizi();
                        ASS_SERVICE.Init(this.OracleConfig);
                        //inizio transazione dati
                        this.OracleConfig.BeginTransaction();
                        ASS_SERVICE.AddNew(false);
                        ASS_SERVICE.ASRSV_ART_COD.Value                  = int.Parse(cArt);
                        //oggetto per creare asss_Service_id
                        ASS_SERV_ID GUID                                 = new ASS_SERV_ID();
                        GUID.Init(this.OracleConfig);
                        GUID.LoadAll();
                        ASS_SERVICE.ASRSV_PDV_COD.Value                  = codNegozio[0];
                        ASS_SERVICE.ASRSV_SERVIZIO.Value                 = codServizio[1].Replace(" ", "");
                        ASS_SERVICE.ASRSV_ASSORTIMENTO_SERVIZIO_ID.Value = GUID.GUID.Value;
                        ASS_SERVICE.ASRSV_REPARTO.Value                  = null;
                        ASS_SERVICE.ASRSV_UG.Value                       = null;
                        ASS_SERVICE.ASRSV_RAGG.Value                     = null;
                        ASS_SERVICE.ASRSV_FAM.Value                      = null;
                        ASS_SERVICE.ASRSV_IN_ASSORTIMENTO.Value          = "N";
                        ASS_SERVICE.ASRSV_DATA_MOD.Value                 = timeStamp;
                        ASS_SERVICE.ASRSV_STAT.Value                     = null;
                        ASS_SERVICE.Update();
                        //commit transazione
                        this.OracleConfig.CommitTransaction();
                        Console.WriteLine("Aggiungo riga articolo con in ass n");
                    }
                }
                this.ArticoliGridView.EndEdit();
            }
        }

        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //string che immagazzina il valore del codice negezio
            string[] codNegozio;
            string[] codServizio;
            //se l'elem selezionato è null o vuoto ritorno 
            if (string.IsNullOrEmpty(PDVBox.GetItemText(PDVBox.SelectedItem).ToString()))
            {
                return;
            }
            if (string.IsNullOrEmpty(ServiceBox.GetItemText(ServiceBox.SelectedItem).ToString()))
            {
                return;
            }
            //splitto il codice negozio
            codServizio                 = ServiceBox.GetItemText(ServiceBox.SelectedItem).ToString().Split('-');
            codNegozio                  = PDVBox.GetItemText(PDVBox.SelectedItem).ToString().Split('-');
            //click di un cell content della datagridview1
            int columnNameIndex         = this.DataGridView1.CurrentCell.ColumnIndex;
            string columnName           = DataGridView1.Columns[columnNameIndex].Name;
            if (columnName.Equals("cInAssortimento"))
            {   
                //se sto ckeckando un qualcosa di null return
                if (this.DataGridView1.CurrentCell.Value == null)
                {
                    return;
                }
                //se current era false allora sto checkando
                if (this.DataGridView1.CurrentCell.Value.Equals(false)){
                    //recupero i dati della riga corrente
                    int rowIndex                          = this.DataGridView1.CurrentCell.RowIndex;
                    DataGridViewRow selectedRow           = DataGridView1.Rows[rowIndex];
                    string[] rep                          = Convert.ToString(selectedRow.Cells["Reparto"].Value).Split('-');
                    string[] ug                           = Convert.ToString(selectedRow.Cells["UG"].Value).Split('-');
                    string[] rag                          = Convert.ToString(selectedRow.Cells["Raggruppamento"].Value).Split('-');
                    string[] fam                          = Convert.ToString(selectedRow.Cells["cCodiceFamigliaEx"].Value).Split('-');
                    MGAssortimentoServizi ASS_SERV        = new MGAssortimentoServizi();
                    ASS_SERV.Init(this.OracleConfig);
                    BasicDAL.DbFilters fASS_SERV          = new BasicDAL.DbFilters();
                    fASS_SERV.Add(ASS_SERV.ASRSV_PDV_COD, BasicDAL.ComparisionOperator.Equal, int.Parse(codNegozio[0]), BasicDAL.LogicOperator.AND);
                    fASS_SERV.Add(ASS_SERV.ASRSV_SERVIZIO,BasicDAL.ComparisionOperator.Equal, codServizio[1].Replace(" ",""), BasicDAL.LogicOperator.AND);
                    fASS_SERV.Add(ASS_SERV.ASRSV_REPARTO, BasicDAL.ComparisionOperator.Equal, int.Parse(rep[0]), BasicDAL.LogicOperator.AND);
                    fASS_SERV.Add(ASS_SERV.ASRSV_UG,      BasicDAL.ComparisionOperator.Equal, int.Parse(ug[0]), BasicDAL.LogicOperator.AND);
                    fASS_SERV.Add(ASS_SERV.ASRSV_RAGG,    BasicDAL.ComparisionOperator.Equal, int.Parse(rag[0]), BasicDAL.LogicOperator.AND);
                    fASS_SERV.Add(ASS_SERV.ASRSV_FAM,     BasicDAL.ComparisionOperator.Equal, int.Parse(fam[0]), BasicDAL.LogicOperator.AND);
                    ASS_SERV.FiltersGroup.Add(fASS_SERV);
                    ASS_SERV.LoadAll();
                    //se esiste già una riga con le caratteristiche di quella che sto checkando
                    if (ASS_SERV.RowCount > 0)
                    {   
                        //modifica riga già esistente
                        this.OracleConfig.BeginTransaction();
                        ASS_SERV.ASRSV_IN_ASSORTIMENTO.Value = "S";
                        ASS_SERV.Update();
                        this.OracleConfig.CommitTransaction();
                        Console.WriteLine("Modifico riga con in ass s");

                    }
                    else//la riga non esiste e la aggiungo
                    {
                        
                        // creo timestamp
                        String timeStamp                                 = DateTime.Now.ToString();
                        //assegno i valori
                        MGAssortimentoServizi ASS_SERVICE                = new MGAssortimentoServizi();
                        ASS_SERVICE.Init(this.OracleConfig);
                        //inizio transazione dati
                        this.OracleConfig.BeginTransaction();
                        ASS_SERVICE.AddNew(false);
                        ASS_SERVICE.ASRSV_ART_COD.Value                  = null;
                        //oggetto per creare asss_Service_id
                        ASS_SERV_ID GUID                                 = new ASS_SERV_ID();
                        GUID.Init(this.OracleConfig);
                        GUID.LoadAll();
                        ASS_SERVICE.ASRSV_PDV_COD.Value                  = codNegozio[0];
                        ASS_SERVICE.ASRSV_SERVIZIO.Value                 = codServizio[1].Replace(" ", "");
                        ASS_SERVICE.ASRSV_ART_COD.Value                  = null;
                        ASS_SERVICE.ASRSV_ASSORTIMENTO_SERVIZIO_ID.Value = GUID.GUID.Value;
                        ASS_SERVICE.ASRSV_REPARTO.Value                  = rep[0].Replace(" ", "");
                        ASS_SERVICE.ASRSV_UG.Value                       = ug[0].Replace(" ", "");
                        ASS_SERVICE.ASRSV_RAGG.Value                     = rag[0].Replace(" ", "");
                        ASS_SERVICE.ASRSV_FAM.Value                      = fam[0].Replace(" ", "");
                        ASS_SERVICE.ASRSV_IN_ASSORTIMENTO.Value          = "S";
                        ASS_SERVICE.ASRSV_DATA_MOD.Value                 = timeStamp;
                        ASS_SERVICE.ASRSV_STAT.Value                     = null;
                        ASS_SERVICE.Update();
                        //commit transazione
                        this.OracleConfig.CommitTransaction();
                        
                    }
                    
                   
                }
                //se current era true allora sto decheckando
                if (this.DataGridView1.CurrentCell.Value.Equals(true))
                {   
                    //prendo i dati della riga che ho checkato
                    int rowIndex                                  = this.DataGridView1.CurrentCell.RowIndex;
                    DataGridViewRow selectedRow                   = DataGridView1.Rows[rowIndex];
                    string[] rep                                  = Convert.ToString(selectedRow.Cells["Reparto"].Value).Split('-');
                    string[] ug                                   = Convert.ToString(selectedRow.Cells["UG"].Value).Split('-');
                    string[] rag                                  = Convert.ToString(selectedRow.Cells["Raggruppamento"].Value).Split('-');
                    string[] fam                                  = Convert.ToString(selectedRow.Cells["cCodiceFamigliaEx"].Value).Split('-');
                    //creo un ogg ass_serv per verificare se esiste già una riga
                    MGAssortimentoServizi ASS_SERV_UPD = new MGAssortimentoServizi();
                    ASS_SERV_UPD.Init(this.OracleConfig);
                    //setto i filtri
                    BasicDAL.DbFilters fASS_SERV_UPD              = new BasicDAL.DbFilters();
                    fASS_SERV_UPD.Add(ASS_SERV_UPD.ASRSV_PDV_COD, BasicDAL.ComparisionOperator.Equal, int.Parse(codNegozio[0]), BasicDAL.LogicOperator.AND);
                    fASS_SERV_UPD.Add(ASS_SERV_UPD.ASRSV_SERVIZIO,BasicDAL.ComparisionOperator.Equal, codServizio[1].Replace(" ", ""), BasicDAL.LogicOperator.AND);
                    fASS_SERV_UPD.Add(ASS_SERV_UPD.ASRSV_REPARTO, BasicDAL.ComparisionOperator.Equal, int.Parse(rep[0]), BasicDAL.LogicOperator.AND);
                    fASS_SERV_UPD.Add(ASS_SERV_UPD.ASRSV_UG,      BasicDAL.ComparisionOperator.Equal, int.Parse(ug[0]), BasicDAL.LogicOperator.AND);
                    fASS_SERV_UPD.Add(ASS_SERV_UPD.ASRSV_RAGG,    BasicDAL.ComparisionOperator.Equal, int.Parse(rag[0]), BasicDAL.LogicOperator.AND);
                    fASS_SERV_UPD.Add(ASS_SERV_UPD.ASRSV_FAM,     BasicDAL.ComparisionOperator.Equal, int.Parse(fam[0]), BasicDAL.LogicOperator.AND);
                    //aggiungo i filtri
                    ASS_SERV_UPD.FiltersGroup.Add(fASS_SERV_UPD);
                    ASS_SERV_UPD.LoadAll();
                    //se esiste già una riga allora devo modificarla
                    if (ASS_SERV_UPD.RowCount > 0)
                    {   
                        //modifica del record
                        this.OracleConfig.BeginTransaction();
                        ASS_SERV_UPD.ASRSV_IN_ASSORTIMENTO.Value = "N";
                        ASS_SERV_UPD.Update();
                        this.OracleConfig.CommitTransaction();
                        Console.WriteLine("Modifico riga con flag in ass N");
                    }
                    else//la riga non esiste e la aggiungo
                    {
                        
                        //se il negozio o il serv selezionato è null o vuoto ritorno 
                        if (string.IsNullOrEmpty(PDVBox.GetItemText(PDVBox.SelectedItem).ToString()))
                        {
                            return;
                        }
                        if (string.IsNullOrEmpty(ServiceBox.GetItemText(ServiceBox.SelectedItem).ToString()))
                        {
                            return;
                        }
                        //splitto il codice negozio e servizio
                        codServizio                                      = ServiceBox.GetItemText(ServiceBox.SelectedItem).ToString().Split('-');
                        codNegozio                                       = PDVBox.GetItemText(PDVBox.SelectedItem).ToString().Split('-');
                        //creo il timestamp di elaborazione
                        String timeStamp                                 = DateTime.Now.ToString();
                        //instanzio un oggetto ass_serv e assegno i valori
                        MGAssortimentoServizi ASS_SERVICE                = new MGAssortimentoServizi();
                        ASS_SERVICE.Init(this.OracleConfig);
                        ASS_SERV_ID GUID = new ASS_SERV_ID();
                        GUID.Init(this.OracleConfig);
                        GUID.LoadAll();
                        //inizio transazione dati
                        this.OracleConfig.BeginTransaction();
                        ASS_SERVICE.AddNew(false);
                        ASS_SERVICE.ASRSV_ART_COD.Value                  = null;
                        ASS_SERVICE.ASRSV_PDV_COD.Value                  = codNegozio[0];
                        ASS_SERVICE.ASRSV_SERVIZIO.Value                 = codServizio[1].Replace(" ", "");
                        ASS_SERVICE.ASRSV_ASSORTIMENTO_SERVIZIO_ID.Value = GUID.GUID.Value;
                        ASS_SERVICE.ASRSV_REPARTO.Value                  = rep[0].Replace(" ", "");
                        ASS_SERVICE.ASRSV_UG.Value                       = ug[0].Replace(" ", "");
                        ASS_SERVICE.ASRSV_RAGG.Value                     = rag[0].Replace(" ", "");
                        ASS_SERVICE.ASRSV_FAM.Value                      = fam[0].Replace(" ", "");
                        ASS_SERVICE.ASRSV_IN_ASSORTIMENTO.Value          = "N";
                        ASS_SERVICE.ASRSV_DATA_MOD.Value                 = timeStamp;
                        ASS_SERVICE.ASRSV_STAT.Value                     = null;
                        ASS_SERVICE.Update();
                        //commit dell'insert
                        this.OracleConfig.CommitTransaction();
                        Console.WriteLine("Aggiungo riga con flag in ass n");
                    }
                }
                //serve per consentire due click consecutivi sul checkbox
                this.DataGridView1.EndEdit();
            }
                
        }

        private void ServiceBox_SelectedIndexChanged(object sender, EventArgs e)
        {   

            
            //Verifico gli elementi selezionati nelle box dall'utente
            String pdv = PDVBox.GetItemText(PDVBox.SelectedItem);
            String service = ServiceBox.GetItemText(ServiceBox.SelectedItem);
            if (pdv.Equals(""))
            {
                //l'utente deve effettuare sia la scelta del servizio che del pdv
            }
            else
            {
                //splitto il codice pdv
                string[] codiNegozio = pdv.Split('-');
                string[] codiServizio = service.Split('-');
                MGpdvRelService PdvCheck = new MGpdvRelService();
                PdvCheck.Init(this.OracleConfig);
                BasicDAL.DbFilters fPdvCheck = new BasicDAL.DbFilters();
                fPdvCheck.Add(PdvCheck.PVSER_PDV_COD, BasicDAL.ComparisionOperator.Equal, codiNegozio[0], BasicDAL.LogicOperator.AND);
                fPdvCheck.Add(PdvCheck.PVSER_SERVIZIO, BasicDAL.ComparisionOperator.Equal, codiServizio[1].Replace(" ", ""), BasicDAL.LogicOperator.AND);
                PdvCheck.FiltersGroup.Add(fPdvCheck);
                PdvCheck.LoadAll();
                if (PdvCheck.RowCount > 0)
                {
                    //Attivo le label e gli elementi che devono essere visibili dopo la pressione del tasto ricerca
                    LvlClsBox.Visible = true;
                    TreeView1.Visible = true;
                    //pulisco la tabella della class comm
                    this.DataGridView1.DataSource = null;
                    this.DataGridView1.Rows.Clear();
                    DataGridView1.Visible = true;
                    lvlClsBox2.Visible = true;
                    artBox.Visible = true;
                    //pulisco tabella articoli
                    this.ArticoliGridView.DataSource = null;
                    this.ArticoliGridView.Rows.Clear();
                    ArticoliGridView.Visible = true;
                }
                else
                {
                    //disattivo le label e gli elementi che devono essere visibili dopo la pressione del tasto ricerca
                    LvlClsBox.Visible = false;
                    TreeView1.Visible = false;
                    DataGridView1.Visible = false;
                    lvlClsBox2.Visible = false;
                    artBox.Visible = false;
                    ArticoliGridView.Visible = false;
                    MessageBox.Show("ATTENZIONE: Il punto di vendita selezionato non è abilitato ad effettuare il servizio richiesto");

                }
            }
            
        }
    }
}
