﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssortimentoClickDrive
{
    using System.Data;
    using System;

    // ------------------------------------------------------------------------------
    // Class Definition for Table/View: MG.GEAD010F
    // Date   :01/03/2019
    // Author :
    // ------------------------------------------------------------------------------
    public class MG_GEAD010F : BasicDAL.DbObject
    {
        public BasicDAL.DbColumn E1STAT = new BasicDAL.DbColumn("E1STAT", DbType.String, false, "");
        public BasicDAL.DbColumn E1DTEX = new BasicDAL.DbColumn("E1DTEX", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E1OREX = new BasicDAL.DbColumn("E1OREX", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E1CART = new BasicDAL.DbColumn("E1CART", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E1XART = new BasicDAL.DbColumn("E1XART", DbType.String, false, "");
        public BasicDAL.DbColumn E1XARS = new BasicDAL.DbColumn("E1XARS", DbType.String, false, "");
        public BasicDAL.DbColumn E1XARE = new BasicDAL.DbColumn("E1XARE", DbType.String, false, "");
        public BasicDAL.DbColumn E1CPLU = new BasicDAL.DbColumn("E1CPLU", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E1BCDI = new BasicDAL.DbColumn("E1BCDI", DbType.Int64, false, 0);
        public BasicDAL.DbColumn E1BCDM = new BasicDAL.DbColumn("E1BCDM", DbType.Int64, false, 0);
        public BasicDAL.DbColumn E1CFOR = new BasicDAL.DbColumn("E1CFOR", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E1BRAND = new BasicDAL.DbColumn("E1BRAND", DbType.String, false, "");
        public BasicDAL.DbColumn E1CUAQ = new BasicDAL.DbColumn("E1CUAQ", DbType.String, false, "");
        public BasicDAL.DbColumn E1CS1C = new BasicDAL.DbColumn("E1CS1C", DbType.String, false, "");
        public BasicDAL.DbColumn E1CS2C = new BasicDAL.DbColumn("E1CS2C", DbType.String, false, "");
        public BasicDAL.DbColumn E1CS3C = new BasicDAL.DbColumn("E1CS3C", DbType.String, false, "");
        public BasicDAL.DbColumn E1CS1D = new BasicDAL.DbColumn("E1CS1D", DbType.String, false, "");
        public BasicDAL.DbColumn E1CS2D = new BasicDAL.DbColumn("E1CS2D", DbType.String, false, "");
        public BasicDAL.DbColumn E1CS3D = new BasicDAL.DbColumn("E1CS3D", DbType.String, false, "");
        public BasicDAL.DbColumn E1CG1C = new BasicDAL.DbColumn("E1CG1C", DbType.String, false, "");
        public BasicDAL.DbColumn E1CG2C = new BasicDAL.DbColumn("E1CG2C", DbType.String, false, "");
        public BasicDAL.DbColumn E1CG3C = new BasicDAL.DbColumn("E1CG3C", DbType.String, false, "");
        public BasicDAL.DbColumn E1CG4C = new BasicDAL.DbColumn("E1CG4C", DbType.String, false, "");
        public BasicDAL.DbColumn E1CG5C = new BasicDAL.DbColumn("E1CG5C", DbType.String, false, "");
        public BasicDAL.DbColumn E1CG1D = new BasicDAL.DbColumn("E1CG1D", DbType.String, false, "");
        public BasicDAL.DbColumn E1CG2D = new BasicDAL.DbColumn("E1CG2D", DbType.String, false, "");
        public BasicDAL.DbColumn E1CG3D = new BasicDAL.DbColumn("E1CG3D", DbType.String, false, "");
        public BasicDAL.DbColumn E1CG4D = new BasicDAL.DbColumn("E1CG4D", DbType.String, false, "");
        public BasicDAL.DbColumn E1CG5D = new BasicDAL.DbColumn("E1CG5D", DbType.String, false, "");
        public BasicDAL.DbColumn E1CLPG = new BasicDAL.DbColumn("E1CLPG", DbType.Int16, false, 0);
        public BasicDAL.DbColumn E1CREP = new BasicDAL.DbColumn("E1CREP", DbType.Int16, false, 0);
        public BasicDAL.DbColumn E1CUGE = new BasicDAL.DbColumn("E1CUGE", DbType.Int16, false, 0);
        public BasicDAL.DbColumn E1CRAG = new BasicDAL.DbColumn("E1CRAG", DbType.Int16, false, 0);
        public BasicDAL.DbColumn E1CFAM = new BasicDAL.DbColumn("E1CFAM", DbType.Int16, false, 0);
        public BasicDAL.DbColumn E1CIVA = new BasicDAL.DbColumn("E1CIVA", DbType.String, false, "");
        public BasicDAL.DbColumn E1PIVA = new BasicDAL.DbColumn("E1PIVA", DbType.Int16, false, 0);
        public BasicDAL.DbColumn E1XIVA = new BasicDAL.DbColumn("E1XIVA", DbType.String, false, "");
        public BasicDAL.DbColumn E1SRIF = new BasicDAL.DbColumn("E1SRIF", DbType.String, false, "");
        public BasicDAL.DbColumn E1SRIM = new BasicDAL.DbColumn("E1SRIM", DbType.String, false, "");
        public BasicDAL.DbColumn E1CIVI = new BasicDAL.DbColumn("E1CIVI", DbType.String, false, "");
        public BasicDAL.DbColumn E1DINS = new BasicDAL.DbColumn("E1DINS", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E1DESP = new BasicDAL.DbColumn("E1DESP", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E1UMPR = new BasicDAL.DbColumn("E1UMPR", DbType.String, false, "");
        public BasicDAL.DbColumn E1TCDT = new BasicDAL.DbColumn("E1TCDT", DbType.String, false, "");
        public BasicDAL.DbColumn E1NPEZ = new BasicDAL.DbColumn("E1NPEZ", DbType.Int16, false, 0);
        public BasicDAL.DbColumn E1TCNF = new BasicDAL.DbColumn("E1TCNF", DbType.String, false, "");
        public BasicDAL.DbColumn E1UCNF = new BasicDAL.DbColumn("E1UCNF", DbType.String, false, "");
        public BasicDAL.DbColumn E1VCNF = new BasicDAL.DbColumn("E1VCNF", DbType.Double, false, 0);
        public BasicDAL.DbColumn E1QTBA = new BasicDAL.DbColumn("E1QTBA", DbType.Double, false, 0);
        public BasicDAL.DbColumn E1UMBA = new BasicDAL.DbColumn("E1UMBA", DbType.String, false, "");
        public BasicDAL.DbColumn E1MIVE = new BasicDAL.DbColumn("E1MIVE", DbType.Int16, false, 0);
        public BasicDAL.DbColumn E1MIAQ = new BasicDAL.DbColumn("E1MIAQ", DbType.Int16, false, 0);
        public BasicDAL.DbColumn E1PIMB = new BasicDAL.DbColumn("E1PIMB", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E1MARK = new BasicDAL.DbColumn("E1MARK", DbType.String, false, "");
        public BasicDAL.DbColumn E1MARX = new BasicDAL.DbColumn("E1MARX", DbType.String, false, "");
        public BasicDAL.DbColumn E1PLBL = new BasicDAL.DbColumn("E1PLBL", DbType.String, false, "");
        public BasicDAL.DbColumn E1NAT = new BasicDAL.DbColumn("E1NAT", DbType.String, false, "");
        public BasicDAL.DbColumn E1ECO = new BasicDAL.DbColumn("E1ECO", DbType.String, false, "");
        public BasicDAL.DbColumn E1BIO = new BasicDAL.DbColumn("E1BIO", DbType.String, false, "");
        public BasicDAL.DbColumn E1VEG = new BasicDAL.DbColumn("E1VEG", DbType.String, false, "");
        public BasicDAL.DbColumn E1GLU = new BasicDAL.DbColumn("E1GLU", DbType.String, false, "");
        public BasicDAL.DbColumn E1LAT = new BasicDAL.DbColumn("E1LAT", DbType.String, false, "");
        public BasicDAL.DbColumn E1SAL = new BasicDAL.DbColumn("E1SAL", DbType.String, false, "");
        public BasicDAL.DbColumn E1LOC = new BasicDAL.DbColumn("E1LOC", DbType.String, false, "");
        public BasicDAL.DbColumn E1QUA = new BasicDAL.DbColumn("E1QUA", DbType.String, false, "");
        public BasicDAL.DbColumn E1BUO = new BasicDAL.DbColumn("E1BUO", DbType.String, false, "");
        public BasicDAL.DbColumn E1CNS = new BasicDAL.DbColumn("E1CNS", DbType.String, false, "");
        public BasicDAL.DbColumn E1FESP = new BasicDAL.DbColumn("E1FESP", DbType.String, false, "");
        public BasicDAL.DbColumn E1FMPK = new BasicDAL.DbColumn("E1FMPK", DbType.String, false, "");
        public BasicDAL.DbColumn E1FMPQ = new BasicDAL.DbColumn("E1FMPQ", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E1FCAT = new BasicDAL.DbColumn("E1FCAT", DbType.String, false, "");
        public BasicDAL.DbColumn E1FVNO = new BasicDAL.DbColumn("E1FVNO", DbType.String, false, "");
        public BasicDAL.DbColumn E1FPRE = new BasicDAL.DbColumn("E1FPRE", DbType.String, false, "");
        public BasicDAL.DbColumn E1SPOT = new BasicDAL.DbColumn("E1SPOT", DbType.String, false, "");
        public BasicDAL.DbColumn E1INFM = new BasicDAL.DbColumn("E1INFM", DbType.String, false, "");
        public BasicDAL.DbColumn E1FNVE = new BasicDAL.DbColumn("E1FNVE", DbType.String, false, "");
        public BasicDAL.DbColumn E1RAEE = new BasicDAL.DbColumn("E1RAEE", DbType.String, false, "");
        public BasicDAL.DbColumn E1CAOX = new BasicDAL.DbColumn("E1CAOX", DbType.String, false, "");
        public BasicDAL.DbColumn E1PTIP = new BasicDAL.DbColumn("E1PTIP", DbType.String, false, "");
        public BasicDAL.DbColumn E1SITO = new BasicDAL.DbColumn("E1SITO", DbType.String, false, "");
        public BasicDAL.DbColumn E1DARE = new BasicDAL.DbColumn("E1DARE", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E1OARE = new BasicDAL.DbColumn("E1OARE", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E1XARC = new BasicDAL.DbColumn("E1XARC", DbType.String, false, "");
        public MG_GEAD010F()
        {
            this.DbTableName = "MG.GEAD010F";
            this.DbObjectType = BasicDAL.DbObjectTypeEnum.Table;
        }
    }
    // ------------------------------------------------------------------------------

    // ------------------------------------------------------------------------------

    // ------------------------------------------------------------------------------
    // Class Definition for Table/View: GEAD030F - Assortimento - Relazione Articolo - PV
    // Date   :19/02/2019
    // Author :
    // ------------------------------------------------------------------------------
    public class MG_GEAD030F : BasicDAL.DbObject
    {
        public BasicDAL.DbColumn E3STAT = new BasicDAL.DbColumn("E3STAT", DbType.String, false, "");
        public BasicDAL.DbColumn E3DTEX = new BasicDAL.DbColumn("E3DTEX", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E3CPDV = new BasicDAL.DbColumn("E3CPDV", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E3CART = new BasicDAL.DbColumn("E3CART", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E3FSIT = new BasicDAL.DbColumn("E3FSIT", DbType.String, false, "");
        public MG_GEAD030F()
        {
            this.DbTableName = "MG.GEAD030F";
            this.DbObjectType = BasicDAL.DbObjectTypeEnum.Table;
        }
    }
    // -------------------------------------------------------------------------------
    /// <summary>

    /// Classe che mappa la tabella GEAD040F contenente le promo attive.

    /// </summary>

    /// <remarks></remarks>
    public class MG_GEAD040F : BasicDAL.DbObject
    {
        public BasicDAL.DbColumn E4DTEX = new BasicDAL.DbColumn("E4DTEX", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E4OREX = new BasicDAL.DbColumn("E4OREX", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E4APRO = new BasicDAL.DbColumn("E4APRO", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E4NPRO = new BasicDAL.DbColumn("E4NPRO", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E4DESC = new BasicDAL.DbColumn("E4DESC", DbType.String, false, "");
        public BasicDAL.DbColumn E4IPRO = new BasicDAL.DbColumn("E4IPRO", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E4FPRO = new BasicDAL.DbColumn("E4FPRO", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E4ICOM = new BasicDAL.DbColumn("E4ICOM", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E4FCOM = new BasicDAL.DbColumn("E4FCOM", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E4CPDV = new BasicDAL.DbColumn("E4CPDV", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E4CART = new BasicDAL.DbColumn("E4CART", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E4FIDE = new BasicDAL.DbColumn("E4FIDE", DbType.String, false, "");
        public BasicDAL.DbColumn E4TSCO = new BasicDAL.DbColumn("E4TSCO", DbType.String, false, "");
        public BasicDAL.DbColumn E4PRZP = new BasicDAL.DbColumn("E4PRZP", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E4PSCO = new BasicDAL.DbColumn("E4PSCO", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E4VOLA = new BasicDAL.DbColumn("E4VOLA", DbType.String, false, "");
        public BasicDAL.DbColumn E4TVOL = new BasicDAL.DbColumn("E4TVOL", DbType.String, false, "");
        public BasicDAL.DbColumn E4PRZN = new BasicDAL.DbColumn("E4PRZN", DbType.Int32, false, 0);
        public MG_GEAD040F()
        {
            this.DbTableName = "MG.GEAD040F";
        }
    }
    /// <summary>

    /// Classe che mappa la tabella GEAD050F contenente le promo attivedi tipo MxN

    /// </summary>

    /// <remarks></remarks>
    public class MG_GEAD050F : BasicDAL.DbObject
    {
        public BasicDAL.DbColumn E5DTEX = new BasicDAL.DbColumn("E5DTEX", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E5OREX = new BasicDAL.DbColumn("E5OREX", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E5AMXN = new BasicDAL.DbColumn("E5AMXN", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E5NMXN = new BasicDAL.DbColumn("E5NMXN", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E5DESC = new BasicDAL.DbColumn("E5DESC", DbType.String, false, "");
        public BasicDAL.DbColumn E5IMXN = new BasicDAL.DbColumn("E5IMXN", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E5FMXN = new BasicDAL.DbColumn("E5FMXN", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E5TMOF = new BasicDAL.DbColumn("E5TMOF", DbType.String, false, "");
        public BasicDAL.DbColumn E5CPDV = new BasicDAL.DbColumn("E5CPDV", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E5CAGG = new BasicDAL.DbColumn("E5CAGG", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E5CART = new BasicDAL.DbColumn("E5CART", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E5MXNT = new BasicDAL.DbColumn("E5MXNT", DbType.String, false, "");
        public BasicDAL.DbColumn E5MXND = new BasicDAL.DbColumn("E5MXND", DbType.String, false, "");
        public BasicDAL.DbColumn E5MXNM = new BasicDAL.DbColumn("E5MXNM", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E5MXNN = new BasicDAL.DbColumn("E5MXNN", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E5IFIS = new BasicDAL.DbColumn("E5IFIS", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E5PRZP = new BasicDAL.DbColumn("E5PRZP", DbType.Int32, false, 0);
        public MG_GEAD050F()
        {
            this.DbTableName = "MG.GEAD050F";
        }
    }

    /// <summary>

    /// Classe che mappa la tabella GEAD060F contenente gli EAN degli articoli.

    /// </summary>

    /// <remarks></remarks>
    public class MG_GEAD060F : BasicDAL.DbObject
    {
        public BasicDAL.DbColumn E6STAT = new BasicDAL.DbColumn("E6STAT", DbType.String, false, "");
        public BasicDAL.DbColumn E6DTEX = new BasicDAL.DbColumn("E6DTEX", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E6OREX = new BasicDAL.DbColumn("E6OREX", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E6CART = new BasicDAL.DbColumn("E6CART", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E6BCOD = new BasicDAL.DbColumn("E6BCOD", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E6DINS = new BasicDAL.DbColumn("E6DINS", DbType.Int32, false, 0);
        public BasicDAL.DbColumn E6DMOV = new BasicDAL.DbColumn("E6DMOV", DbType.Int32, false, 0);
        public MG_GEAD060F()
        {
            this.DbTableName = "MG.GEAD060F";
        }
    }
    // ------------------------------------------------------------------------------
    /*------------------------------------------------------------------------------
    ' Class Definition for Table/View: MgClassificazioneCommerciale
    ' Date   :12/04/2019
    ' Author :
    '------------------------------------------------------------------------------*/
    public class MgClassificazioneCommerciale : BasicDAL.DbObject
    {
        //public BasicDAL.DbColumn E1CLPG = new BasicDAL.DbColumn("E1CLPG", DbType.Int16, false,0);
        //public BasicDAL.DbColumn E1CG1D = new BasicDAL.DbColumn("E1CG1D", DbType.String, false,"");
        public BasicDAL.DbColumn E1CREP = new BasicDAL.DbColumn("E1CREP", DbType.Int16, false, 0);
        public BasicDAL.DbColumn E1CG2D = new BasicDAL.DbColumn("E1CG2D", DbType.String, false, "");
        public BasicDAL.DbColumn E1CUGE = new BasicDAL.DbColumn("E1CUGE", DbType.Int16, false, 0);
        public BasicDAL.DbColumn E1CG3D = new BasicDAL.DbColumn("E1CG3D", DbType.String, false, "");
        public BasicDAL.DbColumn E1CRAG = new BasicDAL.DbColumn("E1CRAG", DbType.Int16, false, 0);
        public BasicDAL.DbColumn E1CG4D = new BasicDAL.DbColumn("E1CG4D", DbType.String, false, "");
        public BasicDAL.DbColumn E1CFAM = new BasicDAL.DbColumn("E1CFAM", DbType.Int16, false, 0);
        public BasicDAL.DbColumn E1CG5D = new BasicDAL.DbColumn("E1CG5D", DbType.String, false, "");
        public MgClassificazioneCommerciale()
        {
            this.DbTableName = "MG.MG_W_CLASS_COMM_STD";
            //this.DbObjectType = BasicDAL.DbObjectTypeEnum.View;
        }
    }
    public class WEB_REUGRAGFAM : BasicDAL.DbObject
    {
        public BasicDAL.DbColumn CodiceReparto = new BasicDAL.DbColumn("CodiceReparto", DbType.Decimal, false, 0);
        public BasicDAL.DbColumn DescrizioneReparto = new BasicDAL.DbColumn("DescrizioneReparto", DbType.String, false, "");
        public BasicDAL.DbColumn CodiceUnitàGestione = new BasicDAL.DbColumn("CodiceUnitàGestione", DbType.Decimal, false, 0);
        public BasicDAL.DbColumn CodiceRaggruppamento = new BasicDAL.DbColumn("CodiceRaggruppamento", DbType.Decimal, false, 0);
        public BasicDAL.DbColumn CodiceFamiglia = new BasicDAL.DbColumn("CodiceFamiglia", DbType.Decimal, false, 0);
        public BasicDAL.DbColumn DescrizioneUnitàGestione = new BasicDAL.DbColumn("DescrizioneUnitàGestione", DbType.String, false, "");
        public BasicDAL.DbColumn DescrizioneRaggruppamento = new BasicDAL.DbColumn("DescrizioneRaggruppamento", DbType.String, false, "");
        public BasicDAL.DbColumn DescrizioneFamiglia = new BasicDAL.DbColumn("DescrizioneFamiglia", DbType.String, false, "");
        public BasicDAL.DbColumn CodiceUGRAGFAM = new BasicDAL.DbColumn("CodiceUGRAGFAM", DbType.String, false, "");
        public BasicDAL.DbColumn DescrizioneUGRAGFAM = new BasicDAL.DbColumn("DescrizioneUGRAGFAM", DbType.String, false, "");
        public BasicDAL.DbColumn CodiceRepartoEx = new BasicDAL.DbColumn("CodiceRepartoEx", DbType.String, false, "");
        public BasicDAL.DbColumn CodiceUnitàGestioneEx = new BasicDAL.DbColumn("CodiceUnitàGestioneEx", DbType.String, false, "");
        public BasicDAL.DbColumn CodiceRaggruppamentoEx = new BasicDAL.DbColumn("CodiceRaggruppamentoEx", DbType.String, false, "");
        public BasicDAL.DbColumn CodiceFamigliaEx = new BasicDAL.DbColumn("CodiceFamigliaEx", DbType.String, false, "");
        public BasicDAL.DbColumn CodiceUGRAGFAMEx = new BasicDAL.DbColumn("CodiceUGRAGFAMEx", DbType.String, false, "");
        public WEB_REUGRAGFAM()
        {
            this.DbTableName = "WEB_REUGRAGFAM";
            //this.DbObjectType = BasicDAL.DbObjectTypeEnum.View;
        }
    }
    public class WEB_ASSORTIMENTO_CLICKCOLLECT : BasicDAL.DbObject
    {
        public BasicDAL.DbColumn CodiceNegozio = new BasicDAL.DbColumn("CodiceNegozio", DbType.Int32, true, 0);
        public BasicDAL.DbColumn CodiceReparto = new BasicDAL.DbColumn("CodiceReparto", DbType.Int32, true, 0);
        public BasicDAL.DbColumn CodiceUnitàGestione = new BasicDAL.DbColumn("CodiceUnitàGestione", DbType.Int32, true, 0);
        public BasicDAL.DbColumn CodiceRaggruppamento = new BasicDAL.DbColumn("CodiceRaggruppamento", DbType.Int32, true, 0);
        public BasicDAL.DbColumn CodiceFamiglia = new BasicDAL.DbColumn("CodiceFamiglia", DbType.Int32, true, 0);
        public BasicDAL.DbColumn InAssortimento = new BasicDAL.DbColumn("InAssortimento", DbType.Boolean, false, false);
        public WEB_ASSORTIMENTO_CLICKCOLLECT()
        {
            this.DbTableName = "WEB_ASSORTIMENTO_CLICKCOLLECT";
        }
    }
    /*------------------------------------------------------------------------------
   ' Class Definition for Table/View: MGAssortimentoServizio
   ' Date   :15/04/2019
   ' Author :
   '------------------------------------------------------------------------------*/
    public class MGAssortimentoServizi:BasicDAL.DbObject
        {
        public BasicDAL.DbColumn ASRSV_ASSORTIMENTO_SERVIZIO_ID = new BasicDAL.DbColumn("ASRSV_ASSORTIMENTO_SERVIZIO_ID", DbType.String, true,"");
        public BasicDAL.DbColumn ASRSV_PDV_COD  = new BasicDAL.DbColumn("ASRSV_PDV_COD", DbType.Int32, false, 0);
        public BasicDAL.DbColumn ASRSV_SERVIZIO = new BasicDAL.DbColumn("ASRSV_SERVIZIO", DbType.String, false, "");
        public BasicDAL.DbColumn ASRSV_REPARTO  = new BasicDAL.DbColumn("ASRSV_REPARTO", DbType.String, false, "");
        public BasicDAL.DbColumn ASRSV_UG       = new BasicDAL.DbColumn("ASRSV_UG", DbType.String, false, "");
        public BasicDAL.DbColumn ASRSV_RAGG     = new BasicDAL.DbColumn("ASRSV_RAGG", DbType.String, false, "");
        public BasicDAL.DbColumn ASRSV_FAM = new BasicDAL.DbColumn("ASRSV_FAM", DbType.String, false, "");
        public BasicDAL.DbColumn ASRSV_ART_COD = new BasicDAL.DbColumn("ASRSV_ART_COD", DbType.String, false, "");
        public BasicDAL.DbColumn ASRSV_IN_ASSORTIMENTO = new BasicDAL.DbColumn("ASRSV_IN_ASSORTIMENTO", DbType.String, false, "");
        public BasicDAL.DbColumn ASRSV_DATA_MOD = new BasicDAL.DbColumn("ASRSV_DATA_MOD", DbType.DateTime, false, DateTime.Today);
        public BasicDAL.DbColumn ASRSV_STAT = new BasicDAL.DbColumn("ASRSV_STAT", DbType.String, false, "");
        public MGAssortimentoServizi()
        {
            this.DbTableName = "MG.MG_ASSORTIMENTO_SERVIZIO";
            //this.DbObjectType = BasicDAL.DbObjectTypeEnum.View;
        }


    }
    /*------------------------------------------------------------------------------
   ' Class Definition for Table/View: MGAssortimentoServizio
   ' Date   :15/04/2019
   ' Author :
   '------------------------------------------------------------------------------*/
    public class MGpdvRelService : BasicDAL.DbObject
    {
        public BasicDAL.DbColumn PVSER_PDV_REL_PDV_ID = new BasicDAL.DbColumn("PVSER_PDV_REL_PDV_ID", DbType.Int32, false, 0);
        public BasicDAL.DbColumn PVSER_PDV_COD = new BasicDAL.DbColumn("PVSER_PDV_COD", DbType.Int32, false, 0);
        public BasicDAL.DbColumn PVSER_SERVIZIO = new BasicDAL.DbColumn("PVSER_SERVIZIO", DbType.String, false, "");
        public BasicDAL.DbColumn PVSER_DATA_INI = new BasicDAL.DbColumn("PVSER_DATA_INI", DbType.String, false, "");
        public BasicDAL.DbColumn PVSER_DATA_FIN = new BasicDAL.DbColumn("PVSER_DATA_FIN", DbType.String, false, "");
        public BasicDAL.DbColumn PVSER_STAT = new BasicDAL.DbColumn("PVSER_STAT", DbType.String, false, "");
        public MGpdvRelService()
        {
            this.DbTableName = "MG.MG_PDV_REL_SERVICE";
            //this.DbObjectType = BasicDAL.DbObjectTypeEnum.View;
        }


    }

    /*------------------------------------------------------------------------------
 ' Class Definition for Table/View: DBObjectName
 ' Date   :06/09/2017
 ' Author :
 '------------------------------------------------------------------------------*/
    public class MgClassificazioneCommercialePdv : BasicDAL.DbObject
    {
        public BasicDAL.DbColumn E1CREP = new BasicDAL.DbColumn("E1CREP", DbType.Int16, false, 0);
        public BasicDAL.DbColumn E1CG2D = new BasicDAL.DbColumn("E1CG2D", DbType.String, false, "");
        public BasicDAL.DbColumn E1CUGE = new BasicDAL.DbColumn("E1CUGE", DbType.Int16, false, 0);
        public BasicDAL.DbColumn E1CG3D = new BasicDAL.DbColumn("E1CG3D", DbType.String, false, "");
        public BasicDAL.DbColumn E1CRAG = new BasicDAL.DbColumn("E1CRAG", DbType.Int16, false, 0);
        public BasicDAL.DbColumn E1CG4D = new BasicDAL.DbColumn("E1CG4D", DbType.String, false, "");
        public BasicDAL.DbColumn E1CFAM = new BasicDAL.DbColumn("E1CFAM", DbType.Int16, false, 0);
        public BasicDAL.DbColumn E1CG5D = new BasicDAL.DbColumn("E1CG5D", DbType.String, false, "");
        public MgClassificazioneCommercialePdv(string pdv)
        {
            this.DbTableName  = "MG.MG_W_CLASS_COMM_STD";
            this.DbObjectType = BasicDAL.DbObjectTypeEnum.SQLQuery;
            this.SQLQuery     = "SELECT E1CLPG, E1CG1D, E1CREP, E1CG2D, E1CUGE, E1CG3D, E1CRAG, E1CG4D, E1CFAM, E1CG5D FROM MG_W_CLASS_COMM_STD W WHERE EXISTS(SELECT 0 FROM GEAD030F E3,GEAD010F E1 WHERE E3.E3CPDV =" + pdv + " AND E3.E3CART = E1.E1CART AND E1.E1CLPG = W.E1CLPG AND E1.E1CREP = W.E1CREP AND E1.E1CUGE = W.E1CUGE AND E1.E1CRAG = W.E1CRAG  AND E1.E1CFAM = W.E1CFAM)";
        }
        
    
    }
    public class ASS_SERV_ID : BasicDAL.DbObject
    {
        public BasicDAL.DbColumn GUID = new BasicDAL.DbColumn("GUID", DbType.String, false, "");
        public ASS_SERV_ID()
        {
            //this.DbTableName = "MG.MG_W_CLASS_COMM_STD";
            this.DbObjectType = BasicDAL.DbObjectTypeEnum.SQLQuery;
            this.SQLQuery = "select FRAME.BASE_UTIL_PCK.MG_GUID as GUID from dual";
        }


    }


}


