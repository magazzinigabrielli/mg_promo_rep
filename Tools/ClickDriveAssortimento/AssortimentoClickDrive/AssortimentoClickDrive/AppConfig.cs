﻿using System.Drawing;
using System.Data;
using System.Diagnostics;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using System.Linq;
using System.Collections.Generic;
using System.Collections;
using System;
using System.Xml.Linq;
using Oracle.DataAccess.Client;

namespace AssortimentoClickDrive
{
    public class AppConfig
    {
  
        public BasicDAL.DbConfig OraDBCOnfig = new BasicDAL.DbConfig();
      
        public string IniFileName = "AssortimentoClickDrive.ini";

        #region Proprietà

        private string _ServerNameOracle;
        public string ServerNameOracle
        {
            get { return _ServerNameOracle; }
            set { _ServerNameOracle = value; }
        }

        private string _DataBaseNameOracle;
        public string DataBaseNameOracle
        {
            get { return _DataBaseNameOracle; }
            set { _DataBaseNameOracle = value; }
        }

        private string _UserNameOracle;
        public string UserNameOracle
        {
            get { return _UserNameOracle; }
            set { _UserNameOracle = value; }
        }

        private bool _UseSIDOracle;
        public bool UseSIDOracle
        {
            get { return _UseSIDOracle; }
            set { _UseSIDOracle = value; }
        }

        private string _ServiceNameOracle;
        public string ServiceNameOracle
        {
            get { return _ServiceNameOracle; }
            set { _ServiceNameOracle = value; }
        }
        
        private bool _PoolingOracle;
        public bool PoolingOracle
        {
            get { return _PoolingOracle; }
            set { _PoolingOracle = value; }
        }

        private string _ProviderOracle;
        public string ProviderOracle
        {
            get { return _ProviderOracle; }
            set { _ProviderOracle = value; }
        }

        private int _PortOracle;
        public int PortOracle
        {
            get { return _PortOracle; }
            set { _PortOracle = value; }
        }

        private int _AuthenticationMode;
        public int AuthenticationMode
        {
            get { return _AuthenticationMode; }
            set { _AuthenticationMode = value; }
        }

        private string _ConnectionStringOracle;
        public string ConnectionStringOracle
        {
            get { return _ConnectionStringOracle; }
            set { _ConnectionStringOracle = value; }
        }

        private string _PasswordOracle;
        public string PasswordOracle
        {
            get { return _PasswordOracle; }
            set { _PasswordOracle = value; }
        }

        private string _LogFilePath;
        public string LogFilePath
        {
            get { return _LogFilePath; }
            set { _LogFilePath = value; }
        }

        private int _LogLevel;
        public int LogLevel
        {
            get { return _LogLevel; }
            set { _LogLevel = value; }
        }

        private string _PuntoVenditaMaster;
        public string PuntoVenditaMaster
        {
            get { return _PuntoVenditaMaster; }
            set { _PuntoVenditaMaster = value; }
        }

        private List<string> _PuntiVendita;
        public List<string> PuntiVendita
        {
            get { return _PuntiVendita; }
            set { _PuntiVendita = value; }
        }

        private List<String> _Servizi;

        public List<string> Servizi
        {
            get { return _Servizi; }
            set { _Servizi = value; }
        }


        #endregion

        public bool SaveConfig()
        {
            IniFile IniFile = new IniFile();
            IniFile.IniSection.IniKey INI_CodiceOperatore = null;
            IniFile.IniSection.IniKey INI_CodicePuntoVendita = null;
            IniFile.IniSection.IniKey Ini_ORAServerName = null;
            IniFile.IniSection.IniKey Ini_ORADataBaseName = null;
            IniFile.IniSection.IniKey Ini_ORAUserName = null;
            IniFile.IniSection.IniKey Ini_ORAOracleUseSID = null;
            IniFile.IniSection.IniKey Ini_ORAOracleServiceNameSID = null;
            IniFile.IniSection.IniKey Ini_ORAPassword = null;
            IniFile.IniSection.IniKey Ini_ORAOraclePooling = null;
            IniFile.IniSection.IniKey Ini_ORAProvider = null;
            IniFile.IniSection.IniKey Ini_ORAOraclePort = null;
            IniFile.IniSection.IniKey Ini_ORAAuthenticationMode = null;
            IniFile.IniSection.IniKey Ini_ORAConnectionString = null;
            IniFile.IniSection.IniKey INI_LogFilePath = null;
            IniFile.IniSection.IniKey INI_LogLevel = null;
            //config sql 
           

            if (System.IO.File.Exists(this.IniFileName) == false)
                return false;

            IniFile.Load(this.IniFileName);

            // MAIN
            IniFile.IniSection Section;
            Section = IniFile.GetSection("MAIN");
           

            Ini_ORAServerName = GetKeyByName(Section, "ORAServerName");
            Ini_ORAServerName.SetValue(this.DataBaseNameOracle);

            Ini_ORAUserName = GetKeyByName(Section, "ORAUserName");
            Ini_ORAUserName.SetValue(this.UserNameOracle);

            Ini_ORAPassword = GetKeyByName(Section, "ORAPassword");
            Ini_ORAPassword.SetValue(this.UserNameOracle);

            Ini_ORAOracleUseSID = GetKeyByName(Section, "ORAOracleUseSID");
            Ini_ORAOracleUseSID.SetValue(this.UseSIDOracle.ToString());

            Ini_ORAOracleServiceNameSID = GetKeyByName(Section, "ORAOracleServiceNameSID");
            Ini_ORAOracleServiceNameSID.SetValue(this.ServiceNameOracle);

            Ini_ORAOraclePooling = GetKeyByName(Section, "ORAOraclePooling");
            Ini_ORAOraclePooling.SetValue(this.PoolingOracle.ToString());

            Ini_ORAProvider = GetKeyByName(Section, "ORAProvider");
            Ini_ORAProvider.SetValue(this.ProviderOracle);

            Ini_ORAOraclePort = GetKeyByName(Section, "ORAOraclePort");
            Ini_ORAOraclePort.SetValue(this.PortOracle.ToString());

            Ini_ORAAuthenticationMode = GetKeyByName(Section, "ORAAuthenticationMode");
            Ini_ORAAuthenticationMode.SetValue(this.AuthenticationMode.ToString());

            Ini_ORAConnectionString = GetKeyByName(Section, "ORAConnectionString");
            Ini_ORAConnectionString.SetValue(this.ConnectionStringOracle);

            INI_LogLevel = GetKeyByName(Section, "LOGLEVEL");
            INI_LogLevel.SetValue(this.LogLevel.ToString());

            INI_LogFilePath = GetKeyByName(Section, "LOGFILEPATH");
            INI_LogFilePath.SetValue(this.LogFilePath);
                       
            IniFile.Save(this.IniFileName);

            return false;
        }

        public bool GetConfig()
        {
            return this.LoadConfig();
        }
        public bool LoadConfig()
        {
            IniFile IniFile = new IniFile();

            if (System.IO.File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + this.IniFileName) == false)
                return false;

            IniFile.Load(this.IniFileName);

            IniFile.IniSection Section;

            // MAIN
            Section = IniFile.GetSection("MAIN");

            this.OraDBCOnfig.ServerName = GetKeyValueByName(Section, "ORAServerName").Trim();
            this.OraDBCOnfig.UserName = GetKeyValueByName(Section, "ORAUserName").Trim();
            this.OraDBCOnfig.Password = GetKeyValueByName(Section, "ORAPassword").Trim();
            this.OraDBCOnfig.OracleUseSID = GetKeyValueByName(Section, "ORAOracleUseSID").Trim() == "True" ? true : false;
            this.OraDBCOnfig.OracleServiceNameSID = GetKeyValueByName(Section, "ORAOracleServiceNameSID").Trim();
            this.OraDBCOnfig.OraclePort = int.Parse(GetKeyValueByName(Section, "ORAOraclePort").Trim());
            this.OraDBCOnfig.OraclePooling = GetKeyValueByName(Section, "ORAOraclePooling").Trim() == "True" ? true : false;
            this.OraDBCOnfig.Provider = GetBasicDalProvider(GetKeyValueByName(Section, "ORAProvider").Trim());
            
            //foreach (IniFile.IniSection.IniKey key in IniFile.GetSection("PUNTO VENDITA MASTER").Keys)
            //{
            //    if (key.Name.StartsWith("P"))
            //        this.PuntoVenditaMaster = key.Value;
            //}

            // carica i punti vendita
            this.PuntiVendita = new List<string>();
      
            foreach (IniFile.IniSection.IniKey key in IniFile.GetSection("PUNTI VENDITA").Keys)
            {
                if (key.Name.StartsWith("P"))
                    this.PuntiVendita.Add(key.Value);
            }
            //carico i servizi
            this.Servizi = new List<string>();
            foreach (IniFile.IniSection.IniKey key in IniFile.GetSection("SERVIZI").Keys)//DA SOSTITUIRE CON SERVIZI 
            {
                if (key.Name.StartsWith("S"))
                    this.Servizi.Add(key.Value);
            }
            return true;
        }
        //metodo per ottenere la connessione al db
        /*public  OracleConnection
                       GetDBConnection()
        {
           
            Console.WriteLine("Getting Connection ...");
            // 'Connection string' to connect directly to Oracle.
            string connString = "Data Source=(DESCRIPTION =(ADDRESS = (PROTOCOL = TCP)(HOST = "
                 + this.OraDBCOnfig.ServerName+ ")(PORT = " + this.OraDBCOnfig.OraclePort + "))(CONNECT_DATA = (SERVER = DEDICATED)(SID = "
                 + this.OraDBCOnfig.OracleServiceNameSID + ")));Password=" + this.OraDBCOnfig.Password+ ";User ID=" + this.OraDBCOnfig.UserName;


            OracleConnection conn = new OracleConnection();

            conn.ConnectionString = connString;
         
            return conn;
        }*/
             
        private string GetKeyValueByName(IniFile.IniSection Section, string Name)
        {
            if (Section.GetKey(Name) != null)
                return Section.GetKey(Name).Value.ToString();
            else
                return "";
        }

        private IniFile.IniSection.IniKey GetKeyByName(IniFile.IniSection Section, string KeyName)
        {
            IniFile.IniSection.IniKey Key;


            Key = Section.GetKey(KeyName);
            if (Key == null)
            {
                Key = Section.AddKey(KeyName);
                Key.SetValue("");
            }
            return Key;
        }

        private BasicDAL.Providers GetBasicDalProvider(string Provider)
        {
            switch (Provider.ToUpper().Trim())
            {
                case object _ when Provider.ToUpper().Trim() == "SQLSERVER":
                    {
                        return BasicDAL.Providers.SqlServer;
                    }

                case object _ when Provider.ToUpper().Trim() == "SQLSERVERCE":
                    {
                        return BasicDAL.Providers.SqlServerCe;
                    }

                case object _ when Provider.ToUpper().Trim() == "OLEDB":
                    {
                        return BasicDAL.Providers.OleDb;
                    }

                case object _ when Provider.ToUpper().Trim() == "OLEDB_DB2":
                    {
                        return BasicDAL.Providers.OleDb_DB2;
                    }

                case object _ when Provider.ToUpper().Trim() == "ODBC":
                    {
                        return BasicDAL.Providers.ODBC;
                    }

                case object _ when Provider.ToUpper().Trim() == "ODBC_DB2".ToUpper():
                    {
                        return BasicDAL.Providers.ODBC_DB2;
                    }

                case object _ when Provider.ToUpper().Trim() == "ORACLE":
                    {
                        return BasicDAL.Providers.Oracle;
                    }

                case object _ when Provider.ToUpper().Trim() == "ORACLEDATAACCESS":
                    {
                        return BasicDAL.Providers.OracleDataAccess;
                    }

                default:
                    {
                        return BasicDAL.Providers.SqlServer;
                    }
            }
        }
    }
}
