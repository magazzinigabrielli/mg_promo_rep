﻿namespace AssortimentoClickDrive
{
    partial class Form1
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.DataGridView1 = new System.Windows.Forms.DataGridView();
            this.PDVBox = new System.Windows.Forms.ComboBox();
            this.PDVLabel = new System.Windows.Forms.Label();
            this.ServiceBox = new System.Windows.Forms.ComboBox();
            this.ServiceLabel = new System.Windows.Forms.Label();
            this.TreeView1 = new System.Windows.Forms.TreeView();
            this.ArticoliGridView = new System.Windows.Forms.DataGridView();
            this.LvlClsBox = new System.Windows.Forms.GroupBox();
            this.pdvGroupBox = new System.Windows.Forms.GroupBox();
            this.lvlClsBox2 = new System.Windows.Forms.GroupBox();
            this.artBox = new System.Windows.Forms.GroupBox();
            this.CodiceNegozio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cCodiceReparto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cCodiceUG = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cCodiceRaggruppamento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cCodiceFamiglia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Reparto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UG = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Raggruppamento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cCodiceFamigliaEx = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cInAssortimento = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.CodiceArticolo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DescrizioneArticolo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InAssortimento = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ArticoliGridView)).BeginInit();
            this.LvlClsBox.SuspendLayout();
            this.pdvGroupBox.SuspendLayout();
            this.lvlClsBox2.SuspendLayout();
            this.artBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // DataGridView1
            // 
            this.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CodiceNegozio,
            this.cCodiceReparto,
            this.cCodiceUG,
            this.cCodiceRaggruppamento,
            this.cCodiceFamiglia,
            this.Reparto,
            this.UG,
            this.Raggruppamento,
            this.cCodiceFamigliaEx,
            this.cInAssortimento});
            this.DataGridView1.Location = new System.Drawing.Point(0, 24);
            this.DataGridView1.Margin = new System.Windows.Forms.Padding(2);
            this.DataGridView1.Name = "DataGridView1";
            this.DataGridView1.RowTemplate.Height = 24;
            this.DataGridView1.Size = new System.Drawing.Size(708, 552);
            this.DataGridView1.TabIndex = 0;
            this.DataGridView1.Visible = false;
            this.DataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1_CellClick);
            this.DataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1_CellContentClick);
            this.DataGridView1.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1_CellContentClick);
            this.DataGridView1.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dataGridView1_CellPainting);
            this.DataGridView1.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellValueChanged);
            this.DataGridView1.CurrentCellDirtyStateChanged += new System.EventHandler(this.dataGridView1_CurrentCellDirtyStateChanged);
            this.DataGridView1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView1_DataError);
            // 
            // PDVBox
            // 
            this.PDVBox.FormattingEnabled = true;
            this.PDVBox.Location = new System.Drawing.Point(246, 27);
            this.PDVBox.Margin = new System.Windows.Forms.Padding(2);
            this.PDVBox.Name = "PDVBox";
            this.PDVBox.Size = new System.Drawing.Size(196, 21);
            this.PDVBox.Sorted = true;
            this.PDVBox.TabIndex = 7;
            this.PDVBox.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // PDVLabel
            // 
            this.PDVLabel.AutoSize = true;
            this.PDVLabel.Location = new System.Drawing.Point(121, 30);
            this.PDVLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.PDVLabel.Name = "PDVLabel";
            this.PDVLabel.Size = new System.Drawing.Size(109, 13);
            this.PDVLabel.TabIndex = 2;
            this.PDVLabel.Text = "Punto Vendita Master";
            this.PDVLabel.Click += new System.EventHandler(this.label1_Click);
            // 
            // ServiceBox
            // 
            this.ServiceBox.FormattingEnabled = true;
            this.ServiceBox.Location = new System.Drawing.Point(606, 27);
            this.ServiceBox.Margin = new System.Windows.Forms.Padding(2);
            this.ServiceBox.Name = "ServiceBox";
            this.ServiceBox.Size = new System.Drawing.Size(193, 21);
            this.ServiceBox.Sorted = true;
            this.ServiceBox.TabIndex = 8;
            this.ServiceBox.SelectedIndexChanged += new System.EventHandler(this.ServiceBox_SelectedIndexChanged);
            // 
            // ServiceLabel
            // 
            this.ServiceLabel.AutoSize = true;
            this.ServiceLabel.Location = new System.Drawing.Point(538, 30);
            this.ServiceLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.ServiceLabel.Name = "ServiceLabel";
            this.ServiceLabel.Size = new System.Drawing.Size(44, 13);
            this.ServiceLabel.TabIndex = 9;
            this.ServiceLabel.Text = "Servizio";
            this.ServiceLabel.Click += new System.EventHandler(this.Label2_Click);
            // 
            // TreeView1
            // 
            this.TreeView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.TreeView1.Location = new System.Drawing.Point(6, 19);
            this.TreeView1.Name = "TreeView1";
            this.TreeView1.Size = new System.Drawing.Size(483, 562);
            this.TreeView1.TabIndex = 11;
            this.TreeView1.Visible = false;
            this.TreeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.TreeView1_AfterSelect);
            // 
            // ArticoliGridView
            // 
            this.ArticoliGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ArticoliGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CodiceArticolo,
            this.DescrizioneArticolo,
            this.InAssortimento});
            this.ArticoliGridView.Location = new System.Drawing.Point(8, 18);
            this.ArticoliGridView.Margin = new System.Windows.Forms.Padding(2);
            this.ArticoliGridView.Name = "ArticoliGridView";
            this.ArticoliGridView.RowTemplate.Height = 24;
            this.ArticoliGridView.Size = new System.Drawing.Size(1195, 136);
            this.ArticoliGridView.TabIndex = 15;
            this.ArticoliGridView.Visible = false;
            this.ArticoliGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ArticoliGridView_CellContentClick_1);
            // 
            // LvlClsBox
            // 
            this.LvlClsBox.Controls.Add(this.TreeView1);
            this.LvlClsBox.ForeColor = System.Drawing.SystemColors.ControlText;
            this.LvlClsBox.Location = new System.Drawing.Point(12, 94);
            this.LvlClsBox.Name = "LvlClsBox";
            this.LvlClsBox.Size = new System.Drawing.Size(495, 581);
            this.LvlClsBox.TabIndex = 16;
            this.LvlClsBox.TabStop = false;
            this.LvlClsBox.Text = "Livelli di classificazione";
            this.LvlClsBox.Visible = false;
            // 
            // pdvGroupBox
            // 
            this.pdvGroupBox.Controls.Add(this.PDVLabel);
            this.pdvGroupBox.Controls.Add(this.PDVBox);
            this.pdvGroupBox.Controls.Add(this.ServiceLabel);
            this.pdvGroupBox.Controls.Add(this.ServiceBox);
            this.pdvGroupBox.Location = new System.Drawing.Point(251, 12);
            this.pdvGroupBox.Name = "pdvGroupBox";
            this.pdvGroupBox.Size = new System.Drawing.Size(975, 76);
            this.pdvGroupBox.TabIndex = 17;
            this.pdvGroupBox.TabStop = false;
            this.pdvGroupBox.Text = "Seleziona Punto di Vendita";
            // 
            // lvlClsBox2
            // 
            this.lvlClsBox2.Controls.Add(this.DataGridView1);
            this.lvlClsBox2.Location = new System.Drawing.Point(513, 94);
            this.lvlClsBox2.Name = "lvlClsBox2";
            this.lvlClsBox2.Size = new System.Drawing.Size(713, 581);
            this.lvlClsBox2.TabIndex = 18;
            this.lvlClsBox2.TabStop = false;
            this.lvlClsBox2.Text = "Livelli di classificazione";
            this.lvlClsBox2.Visible = false;
            this.lvlClsBox2.Enter += new System.EventHandler(this.GroupBox1_Enter);
            // 
            // artBox
            // 
            this.artBox.Controls.Add(this.ArticoliGridView);
            this.artBox.Location = new System.Drawing.Point(18, 681);
            this.artBox.Name = "artBox";
            this.artBox.Size = new System.Drawing.Size(1208, 162);
            this.artBox.TabIndex = 19;
            this.artBox.TabStop = false;
            this.artBox.Text = "Elenco articoli";
            this.artBox.Visible = false;
            // 
            // CodiceNegozio
            // 
            this.CodiceNegozio.DataPropertyName = "CodiceNegozio";
            this.CodiceNegozio.HeaderText = "CodiceNegozio";
            this.CodiceNegozio.Name = "CodiceNegozio";
            this.CodiceNegozio.ReadOnly = true;
            this.CodiceNegozio.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CodiceNegozio.Visible = false;
            // 
            // cCodiceReparto
            // 
            this.cCodiceReparto.DataPropertyName = "CodiceReparto";
            this.cCodiceReparto.HeaderText = "CodiceReparto";
            this.cCodiceReparto.Name = "cCodiceReparto";
            this.cCodiceReparto.ReadOnly = true;
            this.cCodiceReparto.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.cCodiceReparto.Visible = false;
            // 
            // cCodiceUG
            // 
            this.cCodiceUG.DataPropertyName = "CodiceUnitàGestione";
            this.cCodiceUG.HeaderText = "CodiceUG";
            this.cCodiceUG.Name = "cCodiceUG";
            this.cCodiceUG.ReadOnly = true;
            this.cCodiceUG.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.cCodiceUG.Visible = false;
            // 
            // cCodiceRaggruppamento
            // 
            this.cCodiceRaggruppamento.DataPropertyName = "CodiceRaggruppamento";
            this.cCodiceRaggruppamento.HeaderText = "CodiceRaggruppamento";
            this.cCodiceRaggruppamento.Name = "cCodiceRaggruppamento";
            this.cCodiceRaggruppamento.ReadOnly = true;
            this.cCodiceRaggruppamento.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.cCodiceRaggruppamento.Visible = false;
            // 
            // cCodiceFamiglia
            // 
            this.cCodiceFamiglia.HeaderText = "CodiceFamiglia";
            this.cCodiceFamiglia.Name = "cCodiceFamiglia";
            this.cCodiceFamiglia.ReadOnly = true;
            this.cCodiceFamiglia.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.cCodiceFamiglia.Visible = false;
            // 
            // Reparto
            // 
            this.Reparto.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Reparto.DataPropertyName = "CodiceRepartoEx";
            this.Reparto.HeaderText = "Reparto";
            this.Reparto.Name = "Reparto";
            this.Reparto.ReadOnly = true;
            this.Reparto.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Reparto.Width = 141;
            // 
            // UG
            // 
            this.UG.HeaderText = "UG";
            this.UG.Name = "UG";
            this.UG.ReadOnly = true;
            this.UG.Width = 141;
            // 
            // Raggruppamento
            // 
            this.Raggruppamento.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Raggruppamento.HeaderText = "Raggruppamento";
            this.Raggruppamento.Name = "Raggruppamento";
            this.Raggruppamento.ReadOnly = true;
            this.Raggruppamento.Width = 141;
            // 
            // cCodiceFamigliaEx
            // 
            this.cCodiceFamigliaEx.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.cCodiceFamigliaEx.DataPropertyName = "CodiceFamigliaEx";
            this.cCodiceFamigliaEx.HeaderText = "Famiglia";
            this.cCodiceFamigliaEx.Name = "cCodiceFamigliaEx";
            this.cCodiceFamigliaEx.ReadOnly = true;
            this.cCodiceFamigliaEx.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.cCodiceFamigliaEx.Width = 141;
            // 
            // cInAssortimento
            // 
            this.cInAssortimento.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.cInAssortimento.HeaderText = "In Assortimento";
            this.cInAssortimento.Name = "cInAssortimento";
            this.cInAssortimento.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // CodiceArticolo
            // 
            this.CodiceArticolo.DataPropertyName = "CodiceArticolo";
            this.CodiceArticolo.HeaderText = "CodiceArticolo";
            this.CodiceArticolo.Name = "CodiceArticolo";
            this.CodiceArticolo.ReadOnly = true;
            this.CodiceArticolo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CodiceArticolo.Width = 525;
            // 
            // DescrizioneArticolo
            // 
            this.DescrizioneArticolo.DataPropertyName = "DescrizioneArticolo";
            this.DescrizioneArticolo.HeaderText = "DescrizioneArticolo";
            this.DescrizioneArticolo.Name = "DescrizioneArticolo";
            this.DescrizioneArticolo.ReadOnly = true;
            this.DescrizioneArticolo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.DescrizioneArticolo.Width = 525;
            // 
            // InAssortimento
            // 
            this.InAssortimento.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.InAssortimento.HeaderText = "In Assortimento";
            this.InAssortimento.Name = "InAssortimento";
            this.InAssortimento.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1512, 855);
            this.Controls.Add(this.artBox);
            this.Controls.Add(this.lvlClsBox2);
            this.Controls.Add(this.pdvGroupBox);
            this.Controls.Add(this.LvlClsBox);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "ClickCollect";
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ArticoliGridView)).EndInit();
            this.LvlClsBox.ResumeLayout(false);
            this.pdvGroupBox.ResumeLayout(false);
            this.pdvGroupBox.PerformLayout();
            this.lvlClsBox2.ResumeLayout(false);
            this.artBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView DataGridView1;
        private System.Windows.Forms.ComboBox PDVBox;
        private System.Windows.Forms.Label PDVLabel;
        private System.Windows.Forms.ComboBox ServiceBox;
        private System.Windows.Forms.Label ServiceLabel;
        private System.Windows.Forms.TreeView TreeView1;
        private System.Windows.Forms.DataGridView ArticoliGridView;
        private System.Windows.Forms.GroupBox LvlClsBox;
        private System.Windows.Forms.GroupBox pdvGroupBox;
        private System.Windows.Forms.GroupBox lvlClsBox2;
        private System.Windows.Forms.GroupBox artBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn CodiceNegozio;
        private System.Windows.Forms.DataGridViewTextBoxColumn cCodiceReparto;
        private System.Windows.Forms.DataGridViewTextBoxColumn cCodiceUG;
        private System.Windows.Forms.DataGridViewTextBoxColumn cCodiceRaggruppamento;
        private System.Windows.Forms.DataGridViewTextBoxColumn cCodiceFamiglia;
        private System.Windows.Forms.DataGridViewTextBoxColumn Reparto;
        private System.Windows.Forms.DataGridViewTextBoxColumn UG;
        private System.Windows.Forms.DataGridViewTextBoxColumn Raggruppamento;
        private System.Windows.Forms.DataGridViewTextBoxColumn cCodiceFamigliaEx;
        private System.Windows.Forms.DataGridViewCheckBoxColumn cInAssortimento;
        private System.Windows.Forms.DataGridViewTextBoxColumn CodiceArticolo;
        private System.Windows.Forms.DataGridViewTextBoxColumn DescrizioneArticolo;
        private System.Windows.Forms.DataGridViewCheckBoxColumn InAssortimento;
    }
}

