import cx_Oracle

try:
    #Creazione Connessione
    conn = cx_Oracle.connect('MGDWH/MGdwh2019@mgdwh.intra.gabriellispa.it:1521/mgdwh')
    print(conn.version)
except Exception as err:
    print("Errore in fase di creazione connnessione DB", err)
else:
    try:
        #creazione cursore
        cur = conn.cursor()
        sql_store = """SELECT PDV_PUNTO_VENDITA_COD, PDV_SIGLA, PDV_RAGIONE_SOC
                       FROM ANA_PUNTI_VENDITA
                       WHERE PDV_STAT IS NULL
                       AND PDV_DATA_CHIUSURA IS NULL"""
        cur.execute(sql_store)
        row = cur.fetchall()
        
        #ciclo il cursore
        for index, record in enumerate(row):
            #print("Index is", index, " : ", record)
            print(record[0])
            f = open("GABRIELLI_" +str(record[0])+".txt" , "a")
            f.write(str(record))
            f.close()
    except Exception as err:
        print("Errore in fase di fetch", err)
    else:
        print("Completed")
    finally:
        cur.close()
finally:
    conn.close()