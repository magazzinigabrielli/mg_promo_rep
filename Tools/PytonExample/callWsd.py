import logging.config
import zeep

logging.config.dictConfig({
"version": 1,
"formatters": {
"verbose": {
"format": "%(name)s: %(message)s"
}
},
"handlers": {
"console": {
"level": "DEBUG",
"class": "logging.StreamHandler",
"formatter": "verbose",
},
},
"loggers": {
"zeep.transports": {
"level": "DEBUG",
"propagate": True,
"handlers": ["console"],
},
}
})

wsdl = 'http://campaignms.gabriellispa.it/nl/jsp/schemawsdl.jsp?schema=xtk:session'
client = zeep.Client(wsdl=wsdl)
print(client.operation.Logon())