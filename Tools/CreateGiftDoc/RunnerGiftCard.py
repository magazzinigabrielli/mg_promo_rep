import csv
import barcode
from barcode.writer import ImageWriter
from collections import OrderedDict
import sys
import os
import docx
from docx import Document
from docx.shared import Inches
from docx.enum.dml import MSO_THEME_COLOR_INDEX
from docx.enum.text import WD_ALIGN_PARAGRAPH
import logging
from datetime import datetime
import docx2pdf
from docx2pdf import convert


pathInputCSV="c:/MAIN/MG_PROMO_REP/Tools/CreateGiftDoc/InputCSV/"
pathOut="c:/MAIN/MG_PROMO_REP/Tools/CreateGiftDoc/OutputGift/"

def startLog():
    logging.basicConfig(filename=pathOut +'logger_' + datetime.now().strftime('%Y_%m_%d_%H%M%S')+ '.log', level=logging.INFO , format='%(asctime)s | %(name)s | %(levelname)s | %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p')
    logging.info('Started')

def closeLog():
     logging.info('Finished')

def eliminaFile(path, estensione):
    dir_name = path
    test = os.listdir(dir_name)

    for item in test:
        if item.endswith(estensione):
            os.remove(os.path.join(dir_name, item))
            logging.info('Eliminazione file: ' + os.path.join(dir_name, item))

def generaBcode(path, txtBode):
    code = barcode.get_barcode_class('code128')   	   # Create barcode format object in code128 format

    #Get barcode object
    bar = code(txtBode,writer=ImageWriter()) 		 # Create a barcode object with the contentA0000012345678A
 
    #Save the barcode file
    bar.save(path+txtBode)
    logging.info('Generazione barcode: ' + path+txtBode)        

def readCSV(pathInput, fileName, listGift):
    
    with open(pathInput + fileName, newline="", encoding="ISO-8859-1") as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=';')
        line_count = 0
        
        for row in csv_reader:
            if line_count == 0:
                print(f'Nome colonne {", ".join(row)}')
                line_count += 1
            else:
                print(f'\t Numero carta: {row[0]} scade il: {row[1]} importo: {row[2]}.')
                logging.info(f'\t Numero carta: {row[0]} scade il: {row[1]} importo: {row[2]}.')               
                generaBcode(pathOut,row[0])
                line_count += 1
                listGift[line_count] = (row[0], row[1],row[2] )
                
            print(f'Processed {line_count} lines.')
            logging.info(f'Processed {line_count} lines.')

def add_hyperlink(paragraph, text, url):
    # This gets access to the document.xml.rels file and gets a new relation id value
    part = paragraph.part
    r_id = part.relate_to(url, docx.opc.constants.RELATIONSHIP_TYPE.HYPERLINK, is_external=True)

    # Create the w:hyperlink tag and add needed values
    hyperlink = docx.oxml.shared.OxmlElement('w:hyperlink')
    hyperlink.set(docx.oxml.shared.qn('r:id'), r_id, )

    # Create a w:r element and a new w:rPr element
    new_run = docx.oxml.shared.OxmlElement('w:r')
    rPr = docx.oxml.shared.OxmlElement('w:rPr')

    # Join all the xml elements together add add the required text to the w:r element
    new_run.append(rPr)
    new_run.text = text
    hyperlink.append(new_run)

    # Create a new Run object and add the hyperlink into it
    r = paragraph.add_run ()
    r._r.append (hyperlink)

    # A workaround for the lack of a hyperlink style (doesn't go purple after using the link)
    # Delete this if using a template that has the hyperlink style in it
    r.font.color.theme_color = MSO_THEME_COLOR_INDEX.HYPERLINK
    r.font.underline = True

    return hyperlink

def createDocx(path, ean, importo, data):
    document = Document()
    imgGift=ean+".png"
    #Aggiunta immagine
    document.add_picture(pathOut +'Header.jpg', width=Inches(5.9))
    document.add_heading('Ecco la tua gift card')

    paragraph = document.add_paragraph('Lorem ipsum dolor sit amet.')
    paragraph.add_run('dolor sit amet.')
    paragraph.add_run('dolor').bold = True
    add_hyperlink(paragraph, 'OasiTigre', "https://www.oasitigre.it/it.html")

    document.add_heading('Valore Gift Card: € ' + str(importo) , level=2)
    document.add_heading('Data scadenza: ' + str(data), level=2)

    document.add_picture(path +imgGift, width=Inches(2.0))
    last_paragraph = document.paragraphs[-1]
    last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER

    document.save(path +ean +".docx")
    logging.info(f'Documento creato-->' +path +ean +".docx")

def createPdf(path):
   for file in os.listdir(path):
       if file.endswith(".docx"):
           convert(os.path.join(pathOut, file))


startLog()

#Creazione collection Gift
listGift = OrderedDict()

eliminaFile(pathOut, ".png")
eliminaFile(pathOut, ".docx")   
eliminaFile(pathOut, ".pdf")   

#Leggo il CSV con le gift
readCSV(pathInputCSV, "exportGift.csv",listGift)

for key, value in listGift.items():
    createDocx(pathOut, value[0], value[2], value[1])
    createPdf(pathOut)

eliminaFile(pathOut, ".png")
#eliminaFile(pathOut, ".docx")  

closeLog()