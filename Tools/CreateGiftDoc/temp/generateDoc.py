import docx
from docx import Document
from docx.shared import Inches
from docx.enum.dml import MSO_THEME_COLOR_INDEX
from docx.enum.text import WD_ALIGN_PARAGRAPH

def add_hyperlink(paragraph, text, url):
    # This gets access to the document.xml.rels file and gets a new relation id value
    part = paragraph.part
    r_id = part.relate_to(url, docx.opc.constants.RELATIONSHIP_TYPE.HYPERLINK, is_external=True)

    # Create the w:hyperlink tag and add needed values
    hyperlink = docx.oxml.shared.OxmlElement('w:hyperlink')
    hyperlink.set(docx.oxml.shared.qn('r:id'), r_id, )

    # Create a w:r element and a new w:rPr element
    new_run = docx.oxml.shared.OxmlElement('w:r')
    rPr = docx.oxml.shared.OxmlElement('w:rPr')

    # Join all the xml elements together add add the required text to the w:r element
    new_run.append(rPr)
    new_run.text = text
    hyperlink.append(new_run)

    # Create a new Run object and add the hyperlink into it
    r = paragraph.add_run ()
    r._r.append (hyperlink)

    # A workaround for the lack of a hyperlink style (doesn't go purple after using the link)
    # Delete this if using a template that has the hyperlink style in it
    r.font.color.theme_color = MSO_THEME_COLOR_INDEX.HYPERLINK
    r.font.underline = True

    return hyperlink

document = Document()

pathOut="c:/MAIN/MG_PROMO_REP/Tools/CreateGiftDoc/OutputGift/"
importoGift="50"
Data="12/12/2021"
Ean="005123456787"
imgGift=Ean+".png"
#Aggiunta immagine
document.add_picture(pathOut +'Header.jpg', width=Inches(5.9))

document.add_heading('Ecco la tua gift card')


paragraph = document.add_paragraph('Lorem ipsum dolor sit amet.')
paragraph.add_run('dolor sit amet.')
paragraph.add_run('dolor').bold = True
add_hyperlink(paragraph, 'Link to my site', "http://supersitedelamortquitue.fr")

document.add_heading('Valore Gift Card: € ' + str(importoGift) , level=2)
document.add_heading('Data validità: ' + str(Data), level=2)

document.add_picture(pathOut +imgGift, width=Inches(2.0))
last_paragraph = document.paragraphs[-1]
last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER

document.save(pathOut +Ean +".docx")
