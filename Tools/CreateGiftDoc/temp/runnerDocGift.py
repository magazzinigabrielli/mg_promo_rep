from docx import Document
from docx.shared import Inches, Pt
import code128



code128.image("005123456789012").save("005123456789012.png")  # with PIL present

document = Document()

document.add_picture('Banner.jpg', width=Inches(6.2))

records = (
     ( 'vai su oasitigre.it', '50€'),
     ( 'terms & condition','005189128765'),
     ( ' ', 'data validità: 27/10/2022'),
)

table = document.add_table(rows=1, cols=2)
hdr_cells = table.rows[0].cells
hdr_cells[0].add_paragraph('Per utilizzare il buono....',style='Heading 1')
hdr_cells[1].add_paragraph('Valore Gift', style='Heading 1')

for col1, col2 in records:
    row_cells = table.add_row().cells
    p = row_cells[0].add_paragraph(col1, style='Normal').add_run
    p2 = row_cells[1].add_paragraph(col2, style='Normal')


document.add_heading('Document Title', 0)

p = document.add_paragraph('A plain paragraph having some ')
p.add_run('bold').bold = True
p.add_run(' and some ')
p.add_run('italic.').italic = True

document.add_heading('Heading, level 1', level=1)
document.add_paragraph('Intense quote', style='Intense Quote')

document.add_paragraph(
    'first item in unordered list', style='List Bullet'
)
document.add_paragraph(
    'first item in ordered list', style='List Number'
)


records = (
    (3, '101', 'Spam'),
    (7, '422', 'Eggs'),
    (4, '631', 'Spam, spam, eggs, and spam')
)

table = document.add_table(rows=1, cols=3)
hdr_cells = table.rows[0].cells
hdr_cells[0].text = 'Qty'
hdr_cells[1].text = 'Id'
hdr_cells[2].text = 'Desc'
for qty, id, desc in records:
    row_cells = table.add_row().cells
    row_cells[0].text = str(qty)
    row_cells[1].text = id
    row_cells[2].text = desc

document.add_page_break()

document.save('demo.docx')