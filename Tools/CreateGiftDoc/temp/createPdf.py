import os
from docx2pdf import convert

pathOut="c:/MAIN/MG_PROMO_REP/Tools/CreateGiftDoc/OutputGift/"

for file in os.listdir(pathOut):
    if file.endswith(".docx"):
        print(os.path.join(pathOut, file))
        convert(os.path.join(pathOut, file))