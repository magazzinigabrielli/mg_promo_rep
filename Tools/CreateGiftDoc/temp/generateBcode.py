#Import module
import barcode
from barcode.writer import ImageWriter

def generaBcode(pathInput, txtBode):
    code = barcode.get_barcode_class('code128')   	   # Create barcode format object in code128 format

    #Get barcode object
    bar = code(txtBode,writer=ImageWriter()) 		 # Create a barcode object with the contentA0000012345678A
    """
    The Code constructor has 3 parameters: Code(code, writer=None, add_checksum=True)
    The code parameter is the encoded data, such as'A0000012345678A'Wait 
    The default value of the writer parameter is None, and the barcodr is used by default..writer.SVGWriter(), The generated file isSVGformat. If you want to getPNG、JPEGorBMPImage format, you need to set this parameter to barcode.writer.ImageWriter(), For example: Code('123456',barcode.writer.ImageWriter(),False)
    The default value of the add_checksum parameter is True, the checksum is automatically added to the generated barcode, if it is False, the checksum is not added
    """

    #Save the barcode file
    bar.save("c:/MAIN/MG_PROMO_REP/Tools/CreateGiftDoc/OutputGift/"+txtBode)                #No need to input file suffix here,The system will automatically fill according to the writer
    """
    The save function has three parameters: save(filename,options=None,text=None)
    The filename parameter is the name of the saved file, no extension is required. The extension will be added automatically according to the writer settings, and the full file name will be returned by the function. When the previous constructor uses the default writer, save asSVGFile with extension.svg。
    The default value of the options parameter is None, in which case the default parameters are used. If you need to modify the settings, use the dictionary to pass in the parameters, for example: save("d:\\barcode",{'format':'JPEG'},text='12345679'). Available parameters are as follows:
        'module_width':Defaults0.2, Width of each barcode, unit is mm
        'module_height':Defaults15.0, Bar code height in mm
        'quiet_zone':Defaults6.5, Blank width at both ends, in millimeters
        'font_size':Defaults10, Text font size, in points
        'text_distance':Defaults5.0, The distance between text and barcode, in millimeters
        'background':Defaults'white', Background color
        'foreground':Defaults'black', Foreground
        'text':Defaults'', Display text, default display encoding, you can also set
        'write_text': The default value is True, whether to display text, if True is automatically generated text value, if it is False, it will not be generated (if the text value is manually set at this time, the text will still be displayed).
        'center_text': The default value is True, whether to display the text in the center
        'format':Defaults'PNG', Save file format, the default isPNG, Or can be set toJPEG、BMPEtc., only valid when using ImageWriter.
        'dpi':Defaults300, Image resolution, is only valid when using ImageWriter.
    """
