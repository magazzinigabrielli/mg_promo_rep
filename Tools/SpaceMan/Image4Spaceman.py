# myscript.py

import cx_Oracle
import os
import logging
from datetime import datetime
global USER_CONNECTION
global PASSWORD_CONNECTION
global DSN_CONNECTION
USER_CONNECTION = 'MG'
PASSWORD_CONNECTION = 'MG20180905'
DSN_CONNECTION = 'kpromo.intra.gabriellispa.it:1521/kpromo'

global VALUE_RESIZE
VALUE_RESIZE = '15%'
global PATH_OUT
PATH_OUT = '\\\\Vmgabftp\\Spaceman'

global FRONT
global BACK
global LEFT
global RIGHT
global TOP
global DOWN
global NOT_FOUND

FRONT = '.1'
BACK = '.7'
LEFT = '.2'
RIGHT = '.8'
TOP = '.3'
DOWN = '.9'
NOT_FOUND = 'NOT_FOUND'

global TYPE_FRONT
global TYPE_BACK
global TYPE_LEFT
global TYPE_RIGHT
global TYPE_TOP
global TYPE_DOWN

TYPE_FRONT = ('02-IDshotimage','02-Parteanterioreplanogrammaimage','03-Parteanterioreplanogrammaimage','04-Parteanterioreplanogrammaimage','05-Parteanterioreplanogrammaimage')

TYPE_BACK = ('04-Parteposterioreplanogrammaimage','05-Parteposterioreplanogrammaimage','06-Parteposterioreplanogrammaimage','07-Parteposterioreplanogrammaimage')

TYPE_LEFT = ('03-Partesinistraplanogrammaimage','04-Partesinistraplanogrammaimage','05-Partesinistraplanogrammaimage','06-Partesinistraplanogrammaimage')

TYPE_RIGHT = ('05-Partedestraplanogrammaimage','06-Partedestraplanogrammaimage','07-Partedestraplanogrammaimage','08-Partedestraplanogrammaimage')

TYPE_TOP = ('06-Partesuperioreplanogrammaimage','07-Partesuperioreplanogrammaimage','08-Partesuperioreplanogrammaimage','09-Partesuperioreplanogrammaimage')

TYPE_DOWN = ('07-Parteinferioreplanogrammaimage','08-Parteinferioreplanogrammaimage','09-Parteinferioreplanogrammaimage','10-Parteinferioreplanogrammaimage')

def getTypeFromTypeImage(typeImage):  
    if(typeImage in TYPE_FRONT):
        type = FRONT    
    elif(typeImage in TYPE_BACK):
        type = BACK
    elif(typeImage in TYPE_LEFT):
        type = LEFT
    elif(typeImage in TYPE_RIGHT):
        type = RIGHT
    elif(typeImage in TYPE_TOP):
        type = TOP
    elif(typeImage in TYPE_DOWN):
        type = DOWN
    else:
        type = NOT_FOUND
        logging.info('Tipo di immagine non trovata')
    
    return type

def getBarcode(artCode,cursor):
    try:
        cursor.execute("""
                    SELECT E6BCOD
                    FROM GEAD060F
                    WHERE TRIM(E6STAT) IS NULL
                      AND E6CART = """ + artCode +"""
                      ORDER BY E6DTEX DESC
                      """)
        barcode = cursor.fetchall()
        if barcode == None:
            logging.warning('BarCode non presente')            
            barcode = NOT_FOUND    
        else:
            #barcode = barcode[0]
            barcode = barcode
    except:
        logging.error('Errore nella query per il recupero del barcode')
        barcode = NOT_FOUND
    return  barcode

def getTreeFolder(codReparto, descrizioneReparto, codUg, descrizioneUg): 
    descrizioneReparto = descrizioneReparto.replace('/','_')
    descrizioneUg = descrizioneUg.replace('/','_')

    try:
        reparto = os.path.join(PATH_OUT,codReparto + '_' + descrizioneReparto)
        ug = os.path.join(reparto,codUg + '_' + descrizioneUg)

        if(not os.path.isdir(reparto)):
            os.makedirs(ug)            
        elif(not os.path.isdir(ug)):
            os.makedirs(ug)
    except:
        ug = NOT_FOUND
        logging.error('Errore nella controllo e nella creazione delle cartelle')      

    return ug

def resizeAndRename(barCode, type, pathIn, pathOut): 
    newName = barCode + type    
    try:
        command = 'magick "' + pathIn + '" -resize ' + VALUE_RESIZE + ' "' + os.path.join(pathOut,newName) +'"'
        logging.info('  -->resize esecuzione comando: ' + command)    
        os.system('cmd /c "' + command + '"')
        command = 'magick convert "' + os.path.join(pathOut,newName) + '" -bordercolor white -border 1x1 -trim +repage "' + os.path.join(pathOut,newName) + '"' 
        logging.info('  -->crop esecuzione comando: ' + command)        
        os.system('cmd /c "' + command + '"')
    except Exception as e:
        logging.error('Errore nel ridimensionamento')
        logging.error(e)
        return False
    return True

def startLog():
    logging.basicConfig(filename='Image4Spaceman' + datetime.now().strftime('%Y_%m_%d_%H%M%S')+ '.log', level=logging.INFO , format='%(asctime)s | %(name)s | %(levelname)s | %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
    logging.info('Started')

def closeLog():
     logging.info('Finished')

def resizeRepartoAndUg(reparto, ug):
    with cx_Oracle.connect(user = USER_CONNECTION, password = PASSWORD_CONNECTION, dsn = DSN_CONNECTION) as connection:
        logging.debug('Connessione al database eseguita')
        cursor = connection.cursor()
        stmnt = '''
            SELECT ART.E1CART, 
                ART.E1CREP, ART.E1CG2D,
                ART.E1CUGE, ART.E1CG3D,
                IMG.PATH,
                NVL(TYPE, '02-IDshotimage') TYPE
            FROM MG.ARTICLES_IMAGES IMG,
                MG.GEAD010F ART
            WHERE IMG.ARTCODE = ART.E1CART    
            AND ART.E1CREP = :1
            AND ART.E1CUGE = :2
            -- AND ART.E1CART IN(608146)
            --AND IMG.TYPE NOT IN ('FRONT','PANORAMIC','03-Angolato-Parteanteriore,superioreimage','03-Angolato-Parteanteriore,destra,superioreimage','04-Angolato-Parteanteriore,destra,superioreimage')            
            AND NVL(IMG.TYPE, '02-IDshotimage') IN ('02-IDshotimage','02-Parteanterioreplanogrammaimage','03-Parteanterioreplanogrammaimage','04-Parteanterioreplanogrammaimage','05-Parteanterioreplanogrammaimage')
            ORDER BY ART.E1CREP,
                ART.E1CUGE,
                ART.E1CRAG,
                ART.E1CFAM'''
        try:        
            cursor.execute(stmnt, (reparto, ug))
            #cursor.execute(stmnt)
            res = cursor.fetchall()
            #res = cursor.fetchmany(numRows = 10)  
        except:
            logging.error('Errore nell\'esecuzione della query')
        for artCode, codReparto, descrizioneReparto, codUg, descrizioneUg, pathIn , descriptionType in res:
            artCode = str(artCode)
            codReparto = str(codReparto)
            codUg = str(codUg)
            logging.info('Elaborazione articolo: ' + artCode + ' tipo immagine: ' + descriptionType)

            type = getTypeFromTypeImage(descriptionType)
            if type == NOT_FOUND:
                continue
            barCode = getBarcode(artCode, cursor)
            if barCode == NOT_FOUND:
                continue

            pathOut = getTreeFolder(codReparto, descrizioneReparto, codUg, descrizioneUg)
            if pathOut == NOT_FOUND:
                continue
            for bCode in barCode:
                if(resizeAndRename(str(bCode[0]), type, pathIn, pathOut)):
                    logging.info('  -->Bcode completato')
                else:
                    logging.error('Bcode Errore elaborazione') 

            #if(resizeAndRename(barCode, type, pathIn, pathOut)):
            #    logging.info('Elaborazione completata')
            #else:
             #   logging.error('Errore elaborazione')    

#main
startLog()
#resizeRepartoAndUg(21,71)
#resizeRepartoAndUg(21,714)
#resizeRepartoAndUg(1,10)
#resizeRepartoAndUg(1,11)
#resizeRepartoAndUg(1,12)
#resizeRepartoAndUg(1,13)
#resizeRepartoAndUg(1,14)
#resizeRepartoAndUg(1,15)
#resizeRepartoAndUg(1,16)
#resizeRepartoAndUg(1,17)
#resizeRepartoAndUg(1,18)
#resizeRepartoAndUg(1,19)
#resizeRepartoAndUg(1,1016)
#resizeRepartoAndUg(1,1017)
#resizeRepartoAndUg(1,1018)
#resizeRepartoAndUg(1,1019)
#resizeRepartoAndUg(1,1025)
#resizeRepartoAndUg(1,1026)
#resizeRepartoAndUg(1,2010)
#resizeRepartoAndUg(1,2011)
#resizeRepartoAndUg(1,2012)
#resizeRepartoAndUg(1,2013)
#resizeRepartoAndUg(1,2014)
#resizeRepartoAndUg(1,2015)
#resizeRepartoAndUg(1,2016)
#resizeRepartoAndUg(1,2017)
#resizeRepartoAndUg(2,20)
#resizeRepartoAndUg(2,21)
#resizeRepartoAndUg(4,40)
#resizeRepartoAndUg(4,41)
#resizeRepartoAndUg(4,42)
#resizeRepartoAndUg(4,43)
#resizeRepartoAndUg(4,44)
#resizeRepartoAndUg(4,45)
#resizeRepartoAndUg(4,46)
#resizeRepartoAndUg(4,47)
#resizeRepartoAndUg(4,499)
#resizeRepartoAndUg(9,90)
#resizeRepartoAndUg(9,91)
#resizeRepartoAndUg(9,92)
#resizeRepartoAndUg(9,93)
#resizeRepartoAndUg(9,94)
#resizeRepartoAndUg(9,95)
#resizeRepartoAndUg(9,96)
#resizeRepartoAndUg(9,97)
#resizeRepartoAndUg(9,98)
#resizeRepartoAndUg(10,101)
#resizeRepartoAndUg(10,102)
#resizeRepartoAndUg(10,103)
#resizeRepartoAndUg(10,104)
#resizeRepartoAndUg(10,105)
#resizeRepartoAndUg(10,107)
#resizeRepartoAndUg(10,108)
#resizeRepartoAndUg(10,109)
#resizeRepartoAndUg(11,110)
#resizeRepartoAndUg(11,111)
#resizeRepartoAndUg(11,112)
#resizeRepartoAndUg(11,113)
#resizeRepartoAndUg(11,114)
#resizeRepartoAndUg(11,115)
#resizeRepartoAndUg(11,116)
#resizeRepartoAndUg(11,117)
#resizeRepartoAndUg(11,118)
#resizeRepartoAndUg(11,119)
#resizeRepartoAndUg(12,120)
#resizeRepartoAndUg(12,121)
#resizeRepartoAndUg(12,122)
#resizeRepartoAndUg(12,123)
#resizeRepartoAndUg(12,124)
#resizeRepartoAndUg(12,125)
#resizeRepartoAndUg(12,127)
#resizeRepartoAndUg(12,128)
#resizeRepartoAndUg(12,129)
#resizeRepartoAndUg(12,1140)
#resizeRepartoAndUg(12,1141)
#resizeRepartoAndUg(12,1142)
#resizeRepartoAndUg(13,130)
#resizeRepartoAndUg(13,132)
#resizeRepartoAndUg(13,133)
#resizeRepartoAndUg(13,136)
#resizeRepartoAndUg(15,150)
#resizeRepartoAndUg(15,151)
#resizeRepartoAndUg(16,1601)
#resizeRepartoAndUg(16,1602)
#resizeRepartoAndUg(16,1603)
#resizeRepartoAndUg(16,1604)
#resizeRepartoAndUg(16,1606)
#resizeRepartoAndUg(16,1607)
#resizeRepartoAndUg(16,1608)
#resizeRepartoAndUg(16,1609)
#resizeRepartoAndUg(16,1611)
#resizeRepartoAndUg(17,171)
#resizeRepartoAndUg(17,173)
#resizeRepartoAndUg(17,174)
#resizeRepartoAndUg(17,175)
#resizeRepartoAndUg(17,177)
#resizeRepartoAndUg(17,178)

#resizeRepartoAndUg(18,180)
#resizeRepartoAndUg(19,196)
#resizeRepartoAndUg(19,197)

#resizeRepartoAndUg(21,70)

#resizeRepartoAndUg(21,79)
#resizeRepartoAndUg(21,701)
#resizeRepartoAndUg(21,703)
#resizeRepartoAndUg(21,712)
#resizeRepartoAndUg(21,713)
#resizeRepartoAndUg(21,715)
#resizeRepartoAndUg(21,716)
#resizeRepartoAndUg(22,75)
#resizeRepartoAndUg(22,76)
#resizeRepartoAndUg(22,77)
#resizeRepartoAndUg(22,83)
#resizeRepartoAndUg(22,86)
#resizeRepartoAndUg(22,87)
#resizeRepartoAndUg(22,220)
#resizeRepartoAndUg(22,221)
#resizeRepartoAndUg(22,707)
#resizeRepartoAndUg(22,708)
#resizeRepartoAndUg(22,709)
#resizeRepartoAndUg(22,711)
#resizeRepartoAndUg(23,72)
#resizeRepartoAndUg(23,73)
#resizeRepartoAndUg(23,74)
#resizeRepartoAndUg(23,78)
#resizeRepartoAndUg(23,80)
#resizeRepartoAndUg(23,704)
#resizeRepartoAndUg(23,705)
#resizeRepartoAndUg(23,706)
#resizeRepartoAndUg(23,2399)
#resizeRepartoAndUg(24,84)
#resizeRepartoAndUg(24,85)
#resizeRepartoAndUg(30,3001)
#resizeRepartoAndUg(30,3012)
#resizeRepartoAndUg(30,3015)
#resizeRepartoAndUg(30,3016)
#resizeRepartoAndUg(32,3215)
#resizeRepartoAndUg(32,3216)
#resizeRepartoAndUg(32,3217)
#resizeRepartoAndUg(33,3303)
#resizeRepartoAndUg(33,3304)
#resizeRepartoAndUg(33,3310)
#resizeRepartoAndUg(34,3430)
#resizeRepartoAndUg(37,3703)
#resizeRepartoAndUg(37,3707)
#resizeRepartoAndUg(50,5001)
#resizeRepartoAndUg(50,5016)
#resizeRepartoAndUg(50,5017)
#resizeRepartoAndUg(50,5018)
#resizeRepartoAndUg(50,5019)
#resizeRepartoAndUg(51,5112)
#resizeRepartoAndUg(51,5113)
#resizeRepartoAndUg(51,5114)
##resizeRepartoAndUg(51,5115)
##resizeRepartoAndUg(51,5116)
#resizeRepartoAndUg(51,5117)
#resizeRepartoAndUg(51,5119)
#resizeRepartoAndUg(51,5120)
#resizeRepartoAndUg(51,5121)
#resizeRepartoAndUg(51,5122)
#resizeRepartoAndUg(51,5123)
#resizeRepartoAndUg(52,5201)
#resizeRepartoAndUg(52,5202)
#resizeRepartoAndUg(52,5203)
#resizeRepartoAndUg(52,5204)
#resizeRepartoAndUg(53,5301)
#resizeRepartoAndUg(53,5302)
#resizeRepartoAndUg(53,5303)
#resizeRepartoAndUg(53,5304)
#resizeRepartoAndUg(53,5305)
#resizeRepartoAndUg(53,5306)
#resizeRepartoAndUg(53,5307)
#resizeRepartoAndUg(53,5308)
#resizeRepartoAndUg(53,5309)
#resizeRepartoAndUg(53,5310)
#resizeRepartoAndUg(53,5311)
#resizeRepartoAndUg(53,5313)
#resizeRepartoAndUg(54,5402)
#resizeRepartoAndUg(56,5604)
#resizeRepartoAndUg(57,5701)
#resizeRepartoAndUg(59,5906)
#resizeRepartoAndUg(60,6003)
#resizeRepartoAndUg(60,6004)
#resizeRepartoAndUg(60,6006)
#resizeRepartoAndUg(61,6102)
#resizeRepartoAndUg(61,6109)
#resizeRepartoAndUg(61,6110)
#resizeRepartoAndUg(65,6501)
#resizeRepartoAndUg(66,6601)
#resizeRepartoAndUg(66,6602)
#resizeRepartoAndUg(70,7003)
#resizeRepartoAndUg(71,7104)
#resizeRepartoAndUg(71,7105)
#resizeRepartoAndUg(71,7106)
#resizeRepartoAndUg(72,7201)
#resizeRepartoAndUg(72,7202)
#resizeRepartoAndUg(72,7203)
#resizeRepartoAndUg(72,7204)
resizeRepartoAndUg(73,7301)
resizeRepartoAndUg(73,7302)
resizeRepartoAndUg(91,912)
closeLog()